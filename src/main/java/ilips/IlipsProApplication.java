package ilips;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Ilips ProApplication.
 *
 * @author CanhLP
 * created on 7/13/2018
 */
@SpringBootApplication
public class IlipsProApplication {

    /**
     * Input of program.
     *
     * @param args args.
     */
    public static void main(String[] args) {
        SpringApplication.run(IlipsProApplication.class, args);
    }
}
