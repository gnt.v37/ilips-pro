package ilips.domain.repository.maintenancetype;

import ilips.domain.model.maintenancetype.MainteTypeData;
import ilips.domain.model.maintenancetype.MainteTypeDataGridRow;
import ilips.domain.model.maintenancetype.MainteTypeDataGridRowMapper;
import ilips.domain.model.maintenancetype.MainteTypeDataGridRowCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * class Mainte Type Data Repository.
 * @author PhuongVV
 * created on 7/18/2018
 */
@Transactional
@Repository
public class MainteTypeDataRepository {

    //connect jdbc mysql
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public MainteTypeDataRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //SQL update
    private static final String SQL_UPDATE_MAINTE_TYPE_DATA =
            "UPDATE d_mainte_type " +
                    "SET " +
                    "mainte_type = ?, " +
                    "mainte_type_name = ? " +
                    "WHERE " +
                    "maker_org_id = ? and model_id = ? and mainte_type = ?";

    //SQL insert
    private static final String SQL_INSERT_MAINTE_TYPE_DATA =
            "INSERT INTO d_mainte_type (maker_org_id, model_id, mainte_type, mainte_type_name) " +
                    "VALUES (?, ?, ?, ?)";

    //SQL delete
    private static final String SQL_DELETE_MAINTE_TYPE_DATA =
            "DELETE FROM d_mainte_type WHERE maker_org_id = ? and model_id = ? and mainte_type = ?";

    //SQL check dupplicate (insert)
    private static final String SQL_IS_DUPPLICATE_KEY_MAINTE_TYPE =
            "SELECT count(*) FROM d_mainte_type WHERE maker_org_id = ? and model_id = ? and mainte_type = ?";

    /**
     * Get data from DB.
     *
     * @param makerOrgId
     * @param modelId
     * @return List object Maintenace Type
     */
    public List<MainteTypeData> getMainteTypeList(Integer makerOrgId, Integer modelId) {
        final String sql = "SELECT * FROM d_mainte_type WHERE maker_org_id = ? and model_id = ?";
        RowMapper<MainteTypeData> rowMapper = new MainteTypeDataGridRowMapper();

        return jdbcTemplate.query(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, makerOrgId);
            ps.setInt(2, modelId);
            return ps;
        }, rowMapper);
    }

    /**
     * save data to DB.
     *
     * @param mainteTypeDataGridRowWrapper
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateMainteTypeData(MainteTypeDataGridRowCollection mainteTypeDataGridRowWrapper) {

        List<MainteTypeDataGridRow> insertDataList = mainteTypeDataGridRowWrapper.getArrInsertedData();
        List<MainteTypeDataGridRow> updateDataList = mainteTypeDataGridRowWrapper.getArrUpdatedData();
        List<MainteTypeDataGridRow> deleteDataList = mainteTypeDataGridRowWrapper.getArrDeletedData();

        try {
            //delete process
            jdbcTemplate.batchUpdate(SQL_DELETE_MAINTE_TYPE_DATA, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    int paramIndex = 1;

                    //Get object Maintenance Type Data Grid Row from List data delete
                    MainteTypeDataGridRow mainteTypeDataGridRow = deleteDataList.get(i);
                    MainteTypeData mainteTypeData = mainteTypeDataGridRow.getRowData();

                    ps.setInt(paramIndex++, mainteTypeData.getMakerOrgId());
                    ps.setInt(paramIndex++, mainteTypeData.getModelId());
                    ps.setInt(paramIndex++, mainteTypeData.getMainteType());
                }

                @Override
                public int getBatchSize() {
                    return deleteDataList.size();
                }
            });

            //update process
            jdbcTemplate.batchUpdate(SQL_UPDATE_MAINTE_TYPE_DATA, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    int paramIndex = 1;

                    //Get object Maintenance Type Data Grid Row from List data update
                    MainteTypeDataGridRow mainteTypeDataGridRow = updateDataList.get(i);
                    MainteTypeData mainteTypeData = mainteTypeDataGridRow.getRowData();

                    ps.setInt(paramIndex++, mainteTypeData.getMainteType());
                    ps.setNString(paramIndex++, mainteTypeData.getMainteTypeName());

                    ps.setInt(paramIndex++, mainteTypeData.getMakerOrgId());
                    ps.setInt(paramIndex++, mainteTypeData.getModelId());
                    ps.setInt(paramIndex++, mainteTypeData.getOldMainteType());
                }

                @Override
                public int getBatchSize() {
                    return updateDataList.size();
                }
            });

            //insert process
            jdbcTemplate.batchUpdate(SQL_INSERT_MAINTE_TYPE_DATA, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    int paramIndex = 1;

                    //Get object Maintenance Type Data Grid Row from List data insert
                    MainteTypeDataGridRow mainteTypeDataGridRow = insertDataList.get(i);
                    MainteTypeData mainteTypeData = mainteTypeDataGridRow.getRowData();

                    ps.setInt(paramIndex++, mainteTypeData.getMakerOrgId());
                    ps.setInt(paramIndex++, mainteTypeData.getModelId());
                    ps.setInt(paramIndex++, mainteTypeData.getMainteType());
                    ps.setNString(paramIndex++, mainteTypeData.getMainteTypeName());
                }

                @Override
                public int getBatchSize() {
                    return insertDataList.size();
                }
            });
        } catch (InvalidResultSetAccessException e) {
            throw new RuntimeException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Check record for insert.
     *
     * @param makerOrgId
     * @param modelId
     * @param mainteType
     * @return true if exist key else false
     */
    public boolean isDupplicateKey(Integer makerOrgId, Integer modelId, Integer mainteType) {
        boolean result = false;
        int count = jdbcTemplate.queryForObject(SQL_IS_DUPPLICATE_KEY_MAINTE_TYPE,
                new Object[]{makerOrgId, modelId, mainteType}, Integer.class);

        if (count > 0) {
            result = true;
        }
        return result;
    }
}
