package ilips.domain.repository.persondata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Function Repository.
 *
 * @author GiangNPT
 * created on 7/18/2018
 */
@Repository
public class FunctionRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public FunctionRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Get Enable Function.
     *
     * @param orgId    the org id.
     * @param personId the person id.
     * @return the list of function id.
     */
    public List<Integer> getEnableFunction(final int orgId, final int personId) {
        final String sql = " SELECT mf.function_number " +
                " FROM d_person AS dp " +
                "   LEFT OUTER JOIN d_system_permission AS dsp " +
                "   ON dp.permission = dsp.permission " +
                "   LEFT OUTER JOIN m_function AS mf " +
                "   ON dsp.function_number = mf.function_number " +
                " WHERE dp.org_id = ? AND dp.person_id = ?;";
        List<Integer> function;

        try {
            function = jdbcTemplate.queryForList(
                    sql, new Object[]{orgId, personId}, Integer.class);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

        return function;
    }
}
