package ilips.domain.repository.persondata;

import ilips.constant.AppConst;
import ilips.domain.model.orgdata.OrgDataRowMapper;
import ilips.domain.model.persondata.PersonData;
import ilips.domain.model.persondata.PersonDataGridRow;
import ilips.domain.model.persondata.PersonDataGridRowCollection;
import ilips.domain.model.persondata.PersonDataRowMapper;
import ilips.util.common.DateUtilEx;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Person Repository.
 */
@Transactional
@Repository
public class PersonDataRepository {

    private final JdbcTemplate jdbcTemplate;

    private Map<Integer, List<String>> errList;

    @Autowired
    public PersonDataRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_UPDATE_PERSON_DATA =
            "UPDATE d_person "
                    + "SET "
                    + "person_id = ?, "
                    + "org_id = ?, "
                    + "person_name = ?, "
                    + "addr = ?, "
                    + "tel = ?, "
                    + "fax = ?, "
                    + "email = ?, "
                    + "login_id = ?, "
                    + "password = ?, "
                    + "permission = ?, "
                    + "password_expiration_utc = ?, "
                    + "account_expiration_utc = ? "
                    + "WHERE person_id = ? AND org_id = ?";

    private static final String SQL_INSERT_PERSON_DATA =
            "INSERT INTO d_person "
                    + "(person_id, "
                    + "org_id, "
                    + "person_name, "
                    + "addr, "
                    + "tel, "
                    + "fax, "
                    + "email, "
                    + "login_id, "
                    + "password, "
                    + "permission, "
                    + "password_expiration_utc, "
                    + "account_expiration_utc) "
                    + "  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String SQL_DELETE_PERSON_DATA =
            "DELETE FROM d_person "
                    + "WHERE person_id = ? AND org_id = ?";

    private static final String SQL_IS_DUPPLICATE_KEY_PERSON_DATA =
            "SELECT count(*) FROM d_person WHERE person_id = ? AND org_id = ?";

    public List<PersonData> getPersonDataList(Integer selectOrgId, Integer userPermission) {
        final String sql = "SELECT * FROM d_person WHERE permission <= ? AND org_id = ? ";
        RowMapper<PersonData> rowMapper = new PersonDataRowMapper();
        return jdbcTemplate.query(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userPermission);
            ps.setInt(2, selectOrgId);
            return ps;
        }, rowMapper);
    }

    /**
     * Get person by login id.
     *
     * @param loginId the login id.
     * @return the person has input login id.
     */
    public PersonData getPerson(final String loginId) {
        final String sql = "SELECT * FROM d_person WHERE d_person.login_id = ?;";
        PersonData person;

        try {
            person = jdbcTemplate.queryForObject(
                    sql, new Object[]{loginId}, new PersonDataRowMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

        return person;
    }

    /**
     * Get person.
     *
     * @param orgId    the ord id.
     * @param personId the person id.
     * @return the person.
     */
    public PersonData getPerson(final int orgId, final int personId) {
        final String sql = "SELECT * FROM d_person WHERE d_person.person_id = ? AND d_person.org_id = ?;";
        PersonData person;

        try {
            person = jdbcTemplate.queryForObject(
                    sql, new Object[]{personId, orgId}, new PersonDataRowMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

        return person;
    }

    @Transactional(rollbackFor = Exception.class)
    public void updatePersonData(PersonDataGridRowCollection personDataGridRowCollection) {
        List<PersonDataGridRow> insertDataList = personDataGridRowCollection.getArrInsertedData();
        List<PersonDataGridRow> updateDataList = personDataGridRowCollection.getArrUpdatedData();
        List<PersonDataGridRow> deleteDataList = personDataGridRowCollection.getArrDeletedData();

        //delete process
        jdbcTemplate.batchUpdate(SQL_DELETE_PERSON_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement deleteStatement, int i) throws SQLException {
                int paramIndex = 0;
                PersonDataGridRow personDataGridRow = deleteDataList.get(i);
                PersonData personData = personDataGridRow.getRowData();
                deleteStatement.setInt(++paramIndex, personData.getOldPersonId());
                deleteStatement.setInt(++paramIndex, personData.getOldOrgId());
            }

            @Override
            public int getBatchSize() {
                return deleteDataList.size();
            }
        });

        //Update process
        jdbcTemplate.batchUpdate(SQL_UPDATE_PERSON_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement updateStatement, int i) throws SQLException {
                int paramIndex = 0;
                PersonDataGridRow personDataGridRow = updateDataList.get(i);
                PersonData personData = personDataGridRow.getRowData();
                updateStatement.setInt(++paramIndex, personData.getPersonId());
                updateStatement.setInt(++paramIndex, personData.getOrgId());
                updateStatement.setString(++paramIndex, personData.getPersonName());
                updateStatement.setString(++paramIndex, personData.getAddr());
                updateStatement.setString(++paramIndex, personData.getTel());
                updateStatement.setString(++paramIndex, personData.getFax());
                updateStatement.setString(++paramIndex, personData.getEmail());
                updateStatement.setString(++paramIndex, personData.getLoginId());
                updateStatement.setString(++paramIndex, personData.getPassword());
                updateStatement.setInt(++paramIndex, personData.getPermission());

                try {
                    if (!StringUtils.isEmpty(personData.getPasswordExpirationUtc())) {
                        Timestamp ts = DateUtilEx.toTimestamp(personData.getPasswordExpirationUtc(),
                                AppConst.DATE_FORMAT_DATE_TIME);
                        updateStatement.setTimestamp(++paramIndex, ts);
                    } else {
                        updateStatement.setNull(++paramIndex, Types.VARCHAR);
                    }

                    if (!StringUtils.isEmpty(personData.getAccountExpirationUtc())) {
                        Timestamp ts = DateUtilEx.toTimestamp(personData.getAccountExpirationUtc(),
                                AppConst.DATE_FORMAT_DATE_TIME);
                        updateStatement.setTimestamp(++paramIndex, ts);
                    } else {
                        updateStatement.setNull(++paramIndex, Types.VARCHAR);
                    }
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }

                updateStatement.setInt(++paramIndex, personData.getOldPersonId());
                updateStatement.setInt(++paramIndex, personData.getOldOrgId());
            }

            @Override
            public int getBatchSize() {
                return updateDataList.size();
            }
        });

        //Insert process
        jdbcTemplate.batchUpdate(SQL_INSERT_PERSON_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement insertStatement, int i) throws SQLException {
                int paramIndex = 0;
                PersonDataGridRow personDataGridRow = insertDataList.get(i);
                PersonData personData = personDataGridRow.getRowData();
                insertStatement.setInt(++paramIndex, personData.getPersonId());
                insertStatement.setInt(++paramIndex, personData.getOrgId());
                insertStatement.setString(++paramIndex, personData.getPersonName());
                insertStatement.setString(++paramIndex, personData.getAddr());
                insertStatement.setString(++paramIndex, personData.getTel());
                insertStatement.setString(++paramIndex, personData.getFax());
                insertStatement.setString(++paramIndex, personData.getEmail());
                insertStatement.setString(++paramIndex, personData.getLoginId());
                insertStatement.setString(++paramIndex, personData.getPassword());
                insertStatement.setInt(++paramIndex, personData.getPermission());

                try {
                    if (!StringUtils.isEmpty(personData.getPasswordExpirationUtc())) {
                        Timestamp ts = DateUtilEx.toTimestamp(personData.getPasswordExpirationUtc(),
                                AppConst.DATE_FORMAT_DATE_TIME);
                        insertStatement.setTimestamp(++paramIndex, ts);
                    } else {
                        insertStatement.setNull(++paramIndex, Types.VARCHAR);
                    }

                    if (!StringUtils.isEmpty(personData.getAccountExpirationUtc())) {
                        Timestamp ts = DateUtilEx.toTimestamp(personData.getAccountExpirationUtc(),
                                AppConst.DATE_FORMAT_DATE_TIME);
                        insertStatement.setTimestamp(++paramIndex, ts);
                    } else {
                        insertStatement.setNull(++paramIndex, Types.VARCHAR);
                    }
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public int getBatchSize() {
                return insertDataList.size();
            }
        });
    }

    public boolean isExistKey(Integer personId, Integer orgId) {
        boolean result = false;
        int count = jdbcTemplate.queryForObject(SQL_IS_DUPPLICATE_KEY_PERSON_DATA,
                new Object[]{personId, orgId}, Integer.class);
        if (count > 0) {
            result = true;
        }
        return result;
    }
}
