package ilips.domain.repository.modeldata;

import ilips.domain.model.modeldata.ModelData;
import ilips.domain.model.modeldata.ModelDataRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Model Data Repository.
 *
 * @author GiangNPT
 * created on 7/18/2018
 */
@Repository
public class ModelRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ModelRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Get model data by maker org id and model data id.
     *
     * @param makerOrgId the maker org id.
     * @param modelId the model data id.
     * @return the model data entity.
     */
    public ModelData getModel(final int makerOrgId, final int modelId) {
        final String sql = "SELECT * FROM m_model WHERE m_model.maker_org_id = ? AND m_model.model_id = ?;";
        ModelData modelData;

        try {
            modelData = jdbcTemplate.queryForObject(
                    sql, new Object[]{makerOrgId, modelId}, new ModelDataRowMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

        return modelData;
    }

}
