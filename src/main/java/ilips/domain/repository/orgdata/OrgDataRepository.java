package ilips.domain.repository.orgdata;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ilips.constant.AppConst;
import ilips.domain.model.orgdata.OrgData;
import ilips.domain.model.orgdata.OrgDataGridRow;
import ilips.domain.model.orgdata.OrgDataGridRowCollection;
import ilips.domain.model.orgdata.OrgDataRowMapper;
/**
 * Org Data Repository.
 *
 * @author GiangNPT 
 * created on 7/18/2018
 */
@Repository
public class OrgDataRepository {

	private final JdbcTemplate jdbcTemplate;

	private static final String SQL_GET_ORG_DATA_NONE_DEVEOPPER = "SELECT " + "org.org_id, " + "org.org_name, "
			+ "org.parent_id, " + "org.org_code, " + "org.org_type, " + "org.short_name, " + "org.address, "
			+ "org.addition, " + "org.attr_maker, " + "org.attr_customer, " + "org.attr_owner, " + "org.attr_mainte "
			+ "FROM " + " m_org org " + "INNER JOIN "
			+ " (SELECT DISTINCT referable_org_id FROM d_referable_org WHERE org_id = ?) def "
			+ "ON org.org_id = def.referable_org_id ";
	private static final String SQL_GET_ORG_DATA__DEVEOPPER = "SELECT " + "org.org_id, " + "org.org_name, "
			+ "org.parent_id, " + "org.org_code, " + "org.org_type, " + "org.short_name, " + "org.address, "
			+ "org.addition, " + "org.attr_maker, " + "org.attr_customer, " + "org.attr_owner, " + "org.attr_mainte "
			+ "FROM " + " m_org org ";
	private static final String SQL_HAVE_PARENT_ID = " WHERE org.parent_id = ?";
	private static final String SQL_INSERT_ORG_DATA = " INSERT INTO m_org(" + "org_id, " + "org_name, " + "parent_id, "
			+ "org_code, " + "org_type, " + "short_name, " + "address, " + "addition, " + "attr_maker, "
			+ "attr_customer, " + "attr_owner, " + "attr_mainte) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String SQL_UPDATE_ORG_DATA = " UPDATE m_org SET " + "org_id = ?, " + "org_name = ?, "
			+ "parent_id = ?, " + "org_code = ?, " + "org_type = ?, " + "short_name = ?, " + "address = ?, "
			+ "addition = ?, " + "attr_maker = ?, " + "attr_customer = ?, " + "attr_owner = ?, " + "attr_mainte = ? "
			+ " WHERE org_id = ? ";
	private static final String SQL_DELETE_ORG_DATA = "DELETE FROM m_org WHERE org_id = ? ";
	private static final String SQL_INSERT_REFERABLE_ORG_DATA = " INSERT INTO d_referable_org(org_id, referable_org_id) VALUES(?,?)";
	private static final String SQL_UPDATE_REFERABLE_ORG_DATA = " UPDATE d_referable_org SET org_id = ?, referable_org_id = ? WHERE org_id = ? AND referable_org_id = ?";
	private static final String SQL_EXIST_DATA = " SELECT COUNT(*) FROM m_org WHERE org_id = ? ";
	private static final String SQL_EXIST_ORG_NAME = " SELECT count(*) FROM m_org WHERE org_name = ?";

	@Autowired
	public OrgDataRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * Get org by org id.
	 *
	 * @param orgId the org id.
	 * @return the org has input org id.
	 */
	public OrgData getOrg(final int orgId) {
		final String sql = "SELECT * FROM m_org WHERE m_org.org_id = ?;";
		OrgData org;

		try {
			org = jdbcTemplate.queryForObject(sql, new Object[] { orgId }, new OrgDataRowMapper());
		} catch (EmptyResultDataAccessException e) {
			return null;
		}

		return org;
	}

	/**
	 * Get org by org id and parent id.
	 *
	 * @param orgId    the org id.
	 * @param parentId the parent id.
	 * @return the org has input org id and parent id.
	 */
	public OrgData getOrg(final int orgId, final Integer parentId) {
		String sql;
		if (parentId != null) {
			sql = "SELECT * FROM m_org WHERE m_org.org_id = ? AND m_org.parent_id = ?;";
		} else {
			sql = "SELECT * FROM m_org WHERE m_org.org_id = ? AND m_org.parent_id IS ?;";
		}
		OrgData org;

		try {
			org = jdbcTemplate.queryForObject(sql, new Object[] { orgId, parentId }, new OrgDataRowMapper());
		} catch (EmptyResultDataAccessException e) {
			return null;
		}

		return org;
	}

	/**
	 * Get Org List
	 * 
	 * @param orgId      orgId
	 * @param parentId   parent id
	 * @param permission permission
	 * @return List
	 */
	public List<OrgData> getOrgDataList(final Integer orgId, final Integer parentId, final Integer permission) {

		RowMapper<OrgData> rowMapper = new OrgDataRowMapper();

		return jdbcTemplate.query(connection -> {

			// sql string with none-developer or deveploer
			String sql = permission >= 800 ? SQL_GET_ORG_DATA__DEVEOPPER : SQL_GET_ORG_DATA_NONE_DEVEOPPER;

			// having parent id
			if (parentId != null) {
				sql = sql + SQL_HAVE_PARENT_ID;
			}

			PreparedStatement ps = connection.prepareStatement(sql);

			// none developer
			// first param is orgId
			if (permission < 800) {
				ps.setInt(1, orgId);

				if (parentId != null) {
					ps.setInt(2, parentId);
				}
			} else if (permission >= 800 && parentId != null) {

				// user is developer and parent id is passed
				ps.setInt(1, parentId);
			}

			return ps;
		}, rowMapper);
	}

	/**
	 * Execute : insert, update, delete
	 * 
	 * @param orgDataGridRowCollection collection
	 * @param parentId                 parent id
	 * @param userOrgId                userlogin.orgid
	 */
	@Transactional(rollbackFor = Exception.class)
	public void updateData(OrgDataGridRowCollection orgDataGridRowCollection, Integer parentId,
			final Integer userOrgId) {

		try {

			// delete
			final List<OrgDataGridRow> deleteOrgDataList = orgDataGridRowCollection.getArrDeletedOrgData();

			jdbcTemplate.batchUpdate(SQL_DELETE_ORG_DATA, new BatchPreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					OrgDataGridRow row = deleteOrgDataList.get(i);
					OrgData data = row.getRowData();

					ps.setInt(1, data.getOrgId());
				}

				@Override
				public int getBatchSize() {

					return deleteOrgDataList.size();
				}
			});

			// update
			final List<OrgDataGridRow> updateOrgDataList = orgDataGridRowCollection.getArrUpdatedOrgData();

			jdbcTemplate.batchUpdate(SQL_UPDATE_ORG_DATA, new BatchPreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					OrgDataGridRow row = updateOrgDataList.get(i);
					OrgData data = row.getRowData();

					ps.setObject(1, data.getOrgId());
					ps.setObject(2, data.getOrgName());
					ps.setObject(3, parentId);
					ps.setObject(4, data.getOrgCode());
					ps.setObject(5, data.getOrgType());
					ps.setObject(6, data.getShortName());
					ps.setObject(7, data.getAddress());
					ps.setObject(8, data.getAddition());
					ps.setInt(9, data.getAttrMaker());
					ps.setInt(10, data.getAttrCustomer());
					ps.setInt(11, data.getAttrOwner());
					ps.setInt(12, data.getAttrMainte());
					ps.setInt(13, data.getOldOrgId());
				}

				@Override
				public int getBatchSize() {

					return updateOrgDataList.size();
				}
			});

			// insert
			final List<OrgDataGridRow> insertOrgDataList = orgDataGridRowCollection.getArrInsertedOrgData();

			jdbcTemplate.batchUpdate(SQL_INSERT_ORG_DATA, new BatchPreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					OrgDataGridRow row = insertOrgDataList.get(i);
					OrgData data = row.getRowData();

					ps.setInt(1, data.getOrgId());
					ps.setObject(2, data.getOrgName());
					ps.setObject(3, parentId);
					ps.setObject(4, data.getOrgCode());
					ps.setObject(5, data.getOrgType());
					ps.setObject(6, data.getShortName());
					ps.setObject(7, data.getAddress());
					ps.setObject(8, data.getAddition());
					ps.setInt(9, data.getAttrMaker());
					ps.setInt(10, data.getAttrCustomer());
					ps.setInt(11, data.getAttrOwner());
					ps.setInt(12, data.getAttrMainte());
				}

				@Override
				public int getBatchSize() {

					return insertOrgDataList.size();
				}
			});

			// insert in referable_org after inserting m_org
			if (insertOrgDataList != null && insertOrgDataList.size() > 0) {
				jdbcTemplate.batchUpdate(SQL_INSERT_REFERABLE_ORG_DATA, new BatchPreparedStatementSetter() {

					@Override
					public void setValues(PreparedStatement ps, int i) throws SQLException {

						OrgDataGridRow row = insertOrgDataList.get(i);
						OrgData data = row.getRowData();

						ps.setInt(1, userOrgId);
						ps.setInt(2, data.getOrgId());
					}

					@Override
					public int getBatchSize() {

						return insertOrgDataList.size();
					}
				});
			} else if (CollectionUtils.isNotEmpty(updateOrgDataList)) {

				List<OrgData> updateNewKey = new ArrayList<OrgData>();
				OrgData orgUpdateNewKeyData = null;

				// adding collections with data is not same with org_id
				for (OrgDataGridRow row : updateOrgDataList) {

					orgUpdateNewKeyData = row.getRowData();

					if (orgUpdateNewKeyData.getOrgId().intValue() != orgUpdateNewKeyData.getOldOrgId().intValue()) {

						updateNewKey.add(orgUpdateNewKeyData);
					} else {

						continue;
					}
				}

				// update with new key
				jdbcTemplate.batchUpdate(SQL_UPDATE_REFERABLE_ORG_DATA, new BatchPreparedStatementSetter() {

					@Override
					public void setValues(PreparedStatement ps, int i) throws SQLException {

						OrgData data = updateNewKey.get(i);

						// update new key
						ps.setInt(1, userOrgId);
						ps.setInt(2, data.getOrgId());
						ps.setInt(3, userOrgId);
						ps.setInt(4, data.getOldOrgId());

					}

					@Override
					public int getBatchSize() {

						return updateNewKey.size();
					}
				});
			}
		} catch (Exception e) {

			throw new RuntimeException(e);
		}
	}

	/**
	 * Check exist data by orgId <br />
	 * - Condition <br />
	 * + deleting : check only org id <br />
	 * + inserting and updating: check org id with org name <br />
	 * 
	 * @param orgId orgId
	 * @return true or false
	 */
	public boolean isDuplicateOrgIdOrOrgName(final Integer orgId, String orgName, boolean isDelete) {

		Integer countRecordByOrgId = jdbcTemplate.queryForObject(SQL_EXIST_DATA, new Object[] { orgId }, Integer.class);
		
		// deleting only, check only exist by org id
		if (isDelete) {
			
			return countRecordByOrgId > 0;
		}
		
		Integer countRecordByOrgName = jdbcTemplate.queryForObject(SQL_EXIST_ORG_NAME, new Object[] { orgName }, Integer.class);

		// with inserting or updating, check both org id and org name
		return countRecordByOrgId > 0 || countRecordByOrgName > 0;
	}

	/**
	 * 参照可能子組織取得SQL作成
	 * @param in_OrgId 所属組織ID
	 * @param in_Permission 権限
	 * @param in_ParentOrgId 親組織ID
	 * @return
	 */
	public List<OrgData> GetSqlReferableChildOrg(final int in_OrgId, final int in_Permission, final int in_ParentOrgId) {
		RowMapper<OrgData> rowMapper;
		try {
			String sql = " SELECT * FROM m_org  AS mo ";
			// 開発者より権限が低い場合（開発者は全データ参照可）
			if (in_Permission < AppConst.PERMISSION_DEVELOPER) {
				// 参照可能な組織のみ取得
				sql = sql + " INNER JOIN (SELECT DISTINCT referable_org_id AS referable_org_id FROM d_referable_org " +
						" WHERE org_id =? ) AS reforg ON mo.org_id = reforg.referable_org_id ;";
			}
			sql = sql + " WHERE mo.parent_id = ?;";

			final String sqlExe = sql;
			rowMapper = new OrgDataRowMapper();
			return jdbcTemplate.query(connection -> {
				PreparedStatement ps = connection.prepareStatement(sqlExe);
				ps.setInt(1, in_ParentOrgId);
				ps.setInt(2, in_OrgId);
				return ps;
			}, rowMapper);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	/**
	 * 参照可能最上位組織取得SQL作成
	 * @param in_OrgId 所属組織ID
	 * @param in_Permission 権限
	 * @remarks  最上位の組織（parent_id IS NULL）を取得
	 * @return
	 */
	public OrgData GetSqlReferableTopOrg(final int in_OrgId, final Integer in_Permission) {
		String sql ;
		sql= " SELECT  *  FROM  m_org AS mo";
		// 開発者より権限が低い場合（開発者は全データ参照可）
		if (in_Permission < AppConst.PERMISSION_DEVELOPER) {
			// 参照可能な組織のみ取得
			sql = sql + " INNER JOIN (SELECT DISTINCT referable_org_id AS referable_org_id FROM d_referable_org " +
					" WHERE org_id =? ) AS reforg ON mo.org_id = reforg.referable_org_id ;";
		}
		sql = sql + " WHERE mo.parent_id IS NULL;";
		OrgData org;
		try {
			org = jdbcTemplate.queryForObject(
					sql, new Object[]{in_OrgId, in_Permission}, new OrgDataRowMapper());
		} catch (EmptyResultDataAccessException e) {
			return null;
		}

		return org;
	}
}
