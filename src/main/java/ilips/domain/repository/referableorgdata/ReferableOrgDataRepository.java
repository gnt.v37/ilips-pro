package ilips.domain.repository.referableorgdata;

import ilips.domain.model.orgdata.OrgData;
import ilips.domain.model.orgdata.OrgDataRowMapper;
import ilips.domain.model.referableorgdata.ReferableOrgData;
import ilips.domain.model.referableorgdata.ReferableOrgDataGridRow;
import ilips.domain.model.referableorgdata.ReferableOrgDataGridRowCollection;
import ilips.domain.model.referableorgdata.ReferableOrgDataRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * ReferalbeOrg Repository.
 *
 * @author ThanhNP4
 * created on 9/19/2018.
 */
@Transactional
@Repository
public class ReferableOrgDataRepository {

    private static JdbcTemplate jdbcTemplate;

    private Map<Integer, List<String>> errList;

    @Autowired
    public ReferableOrgDataRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_UPDATE_REFERABLE_ORG_DATA =
            "UPDATE d_referable_org"
                    + " SET referable_org_id = ?"
                    + " WHERE org_id = ? and referable_org_id = ?;";

    private static final String SQL_INSERT_PERSON_DATA =
            "INSERT INTO d_referable_org (org_id, referable_org_id)"
                    + " VALUES (?, ?);";

    private static final String SQL_DELETE_PERSON_DATA =
            "DELETE FROM d_referable_org "
                    + "WHERE org_id = ? AND referable_org_id = ?";

//    private static final String SQL_IS_DUPPLICATE_KEY_PERSON_DATA =
//            "SELECT count(*) FROM d_referable_org WHERE org_id = ? AND referable_org_id = ?";

    public static List<ReferableOrgData> getReferableOrgData(Integer selectReferableOrgId) {
        final String sql = "SELECT d_referable_org.org_id, d_referable_org.referable_org_id, m_org.org_name" +
                " FROM d_referable_org" +
                " INNER JOIN m_org" +
                " ON d_referable_org.referable_org_id = m_org.org_id" +
                " WHERE d_referable_org.org_id = ?;";

        List<ReferableOrgData> referableOrgDataList;

        try {
            referableOrgDataList = jdbcTemplate.query(sql, new Object[] {selectReferableOrgId}, new ReferableOrgDataRowMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

        return referableOrgDataList;
    }

    /**
     * Execute : insert, update, delete
     *
     * @param referableOrgDataGridRowCollection collection
     */
    @Transactional(rollbackFor = Exception.class)
    public static void updateReferableOrgData(ReferableOrgDataGridRowCollection referableOrgDataGridRowCollection) {

        List<ReferableOrgDataGridRow> insertDataList = referableOrgDataGridRowCollection.getArrInsertedData();
        List<ReferableOrgDataGridRow> updateDataList = referableOrgDataGridRowCollection.getArrUpdatedData();
        List<ReferableOrgDataGridRow> deleteDataList = referableOrgDataGridRowCollection.getArrDeletedData();

        try {
            //delete process
            jdbcTemplate.batchUpdate(SQL_DELETE_PERSON_DATA, new BatchPreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement deleteStatement, int i) throws SQLException {

                    int paramIndex = 0;
                    ReferableOrgDataGridRow referableOrgDataGridRow = deleteDataList.get(i);
                    ReferableOrgData referableOrgData = referableOrgDataGridRow.getRowData();
                    deleteStatement.setInt(++paramIndex, referableOrgData.getOrgId());
                    deleteStatement.setInt(++paramIndex, referableOrgData.getReferableOrgId());
                }

                @Override
                public int getBatchSize() {
                    return deleteDataList.size();
                }
            });

            //Update process
            jdbcTemplate.batchUpdate(SQL_UPDATE_REFERABLE_ORG_DATA, new BatchPreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement updateStatement, int i) throws SQLException {

                    int paramIndex = 0;
                    ReferableOrgDataGridRow referableOrgDataGridRow = updateDataList.get(i);
                    ReferableOrgData referableOrgData = referableOrgDataGridRow.getRowData();

                    updateStatement.setInt(++paramIndex, referableOrgData.getOrgId());
                    updateStatement.setInt(++paramIndex, referableOrgData.getReferableOrgId());

                }

                @Override
                public int getBatchSize() {
                    return updateDataList.size();
                }
            });

            //Insert process
            jdbcTemplate.batchUpdate(SQL_INSERT_PERSON_DATA, new BatchPreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement insertStatement, int i) throws SQLException {

                    int paramIndex = 0;
                    ReferableOrgDataGridRow referableOrgDataGridRow = insertDataList.get(i);
                    ReferableOrgData referableOrgData = referableOrgDataGridRow.getRowData();
                    insertStatement.setInt(++paramIndex, referableOrgData.getOrgId());
                    insertStatement.setInt(++paramIndex, referableOrgData.getReferableOrgId());

                }

                @Override
                public int getBatchSize() {

                    return insertDataList.size();
                }
            });

        } catch (Exception e) {

            throw new RuntimeException(e);
        }
    }
}
