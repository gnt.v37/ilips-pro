package ilips.domain.repository.selectwork;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import ilips.domain.model.selectwork.SelectWorkData;
import ilips.domain.model.selectwork.SelectWorkDataRowMapper;

/**
 * 
 * Class name:   Select work repository
 * 
 * @author ThangNT2
 * Created Date: Jul 23, 2018
 */
@Repository
public class SelectWorkRepository {

	private final static String SQL_SELECT_WORK = "SELECT work_id, work_no, work_name FROM d_work";
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public SelectWorkRepository(JdbcTemplate jdbcTemplate) {
		
		this.jdbcTemplate = jdbcTemplate;
	}
	
	/**
	 * Get all work data
	 * 
	 * @return list work data
	 */
	public List<SelectWorkData> getAll() {
		
		RowMapper<SelectWorkData> rowMapper = new SelectWorkDataRowMapper();
		
		return jdbcTemplate.query(SQL_SELECT_WORK, rowMapper);
	}
}
