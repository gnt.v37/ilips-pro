package ilips.domain.repository.alarm;

import ilips.domain.model.AlarmLevel.AlarmLevelData;
import ilips.domain.model.AlarmLevel.AlarmLevelDataGridRow;
import ilips.domain.model.AlarmLevel.AlarmLevelDataGridRowCollection;
import ilips.domain.model.AlarmLevel.AlarmLevelDataRowMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Alarm Level Repository.
 *
 * @author TuyenND6
 * created on 7/20/2018
 */
@Repository
public class AlarmLevelRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AlarmLevelRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_UPDATE_ALARM_LEVEL_DATA =
            "UPDATE m_alarm_level "
                    + "SET "
                    + "alarm_level = ?, "
                    + "alarm_level_name = ?, "
                    + "description = ?, "
                    + "event_type = ?, "
                    + "is_disp = ?, "
                    + "WHERE alarm_level = ?";

    private static final String SQL_INSERT_ALARM_LEVEL_DATA =
            "INSERT INTO m_alarm_level "
                    + "(alarm_level, "
                    + "alarm_level_name, "
                    + "description, "
                    + "event_type, "
                    + "is_disp) "
                    + "VALUES (?, ?, ?, ?, ?)";

    private static final String SQL_DELETE_ALARM_LEVEL_DATA =
            "DELETE FROM m_alarm_level WHERE alarm_level = ?";

    private static final String SQL_IS_DUPLICATE_KEY_ALARM_LEVEL_DATA =
            "SELECT count(alarm_level) FROM m_alarm_level WHERE alarm_level = ?";

    private static final String SQL_IS_DUPLICATE_KEY_ALARM_LEVEL_NAME =
            "SELECT count(alarm_level_name) FROM m_alarm_level WHERE alarm_level_name = ?";

    public List<AlarmLevelData> getAlarmLevelDataList() {
        final String sql = "SELECT * FROM m_alarm_level";
        List<AlarmLevelData> alarmLevelDataList;
        try {
            alarmLevelDataList =  jdbcTemplate.query(sql, new AlarmLevelDataRowMapper());
        } catch (EmptyResultDataAccessException e){
            alarmLevelDataList =  null;
        }
        return alarmLevelDataList;
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateAlarmLevelData(AlarmLevelDataGridRowCollection alarmLevelDataGridRowCollection) {
        List<AlarmLevelDataGridRow> insertDataList = alarmLevelDataGridRowCollection.getArrInsertedData();
        List<AlarmLevelDataGridRow> updateDataList = alarmLevelDataGridRowCollection.getArrUpdatedData();
        List<AlarmLevelDataGridRow> deleteDataList = alarmLevelDataGridRowCollection.getArrDeletedData();

        //delete process
        jdbcTemplate.batchUpdate(SQL_DELETE_ALARM_LEVEL_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement deleteStatement, int i) throws SQLException {
                int paramIndex = 0;
                AlarmLevelDataGridRow alarmLevelDataGridRow = deleteDataList.get(i);
                AlarmLevelData alarmLevelData = alarmLevelDataGridRow.getRowData();
                deleteStatement.setInt(++paramIndex, alarmLevelData.getAlarmLevel());
            }

            @Override
            public int getBatchSize() {
                return deleteDataList.size();
            }
        });

        //Update process
        jdbcTemplate.batchUpdate(SQL_UPDATE_ALARM_LEVEL_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement updateStatement, int i) throws SQLException {
                int paramIndex = 0;
                AlarmLevelDataGridRow alarmLevelDataGridRow = updateDataList.get(i);
                AlarmLevelData alarmLevelData = alarmLevelDataGridRow.getRowData();
                updateStatement.setInt(++paramIndex, alarmLevelData.getAlarmLevel());
                updateStatement.setString(++paramIndex, alarmLevelData.getAlarmLevelName());
                updateStatement.setString(++paramIndex, alarmLevelData.getDescription());
                updateStatement.setInt(++paramIndex, alarmLevelData.getEventType());
                updateStatement.setInt(++paramIndex, alarmLevelData.getIsDisp());
            }
            @Override
            public int getBatchSize() {
                return updateDataList.size();
            }
        });

        //Insert process
        jdbcTemplate.batchUpdate(SQL_INSERT_ALARM_LEVEL_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement insertStatement, int i) throws SQLException {
                int paramIndex = 0;
                AlarmLevelDataGridRow alarmLevelDataGridRow = insertDataList.get(i);
                AlarmLevelData alarmLevelData = alarmLevelDataGridRow.getRowData();
                insertStatement.setInt(++paramIndex, alarmLevelData.getAlarmLevel());
                insertStatement.setString(++paramIndex, alarmLevelData.getAlarmLevelName());
                insertStatement.setString(++paramIndex, alarmLevelData.getDescription());
                insertStatement.setInt(++paramIndex, alarmLevelData.getEventType());
                insertStatement.setInt(++paramIndex, alarmLevelData.getIsDisp());
            }

            @Override
            public int getBatchSize() {
                return insertDataList.size();
            }
        });
    }

    public boolean isExistKey(Integer alarmLevel) {
        boolean result = false;
        int count = jdbcTemplate.queryForObject(SQL_IS_DUPLICATE_KEY_ALARM_LEVEL_DATA,
                new Object[]{alarmLevel}, Integer.class);
        if (count > 0) {
            result = true;
        }
        return result;
    }

    public boolean isExistAlarmLevelName(String alarmLevelName) {
        boolean result = false;
        int count = jdbcTemplate.queryForObject(SQL_IS_DUPLICATE_KEY_ALARM_LEVEL_NAME,
                new Object[]{alarmLevelName}, Integer.class);
        if (count > 0) {
            result = true;
        }
        return result;
    }

    /**
     * Get alarm level name.
     *
     * @param alarmId alarm Id.
     * @return alarm level name.
     */
    public String getAlarmName(final int alarmId) {
        final String sql = "SELECT m_alarm_level.alarm_level_name FROM m_alarm_level" +
                " WHERE m_alarm_level.alarm_level = ?;";
        try {
            return jdbcTemplate.queryForObject(
                    sql, new Object[]{alarmId}, String.class);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }


}
