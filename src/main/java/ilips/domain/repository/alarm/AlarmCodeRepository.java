package ilips.domain.repository.alarm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Alarm Code Repository.
 *
 * @author GiangNPT
 * created on 7/18/2018
 */
@Repository
public class AlarmCodeRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AlarmCodeRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Get alarm code name.
     *
     * @param orgId     the org id.
     * @param modelId   the model id.
     * @param alarmCode the alarm code.
     * @return alarm code name.
     */
    public String getAlarmCodeName(final int orgId, final int modelId, final int alarmCode) {
        final String sql = "SELECT m_alarm_code.alarm_code_name FROM m_alarm_code" +
                " WHERE m_alarm_code.maker_org_id = ?" +
                " AND m_alarm_code.model_id = ?" +
                " AND m_alarm_code.alarm_code = ?;";
        try {
            return jdbcTemplate.queryForObject(
                    sql, new Object[]{orgId, modelId, alarmCode}, String.class);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    /**
     * Alarm code existence check.
     *
     * @param orgId     the org id.
     * @param modelId   the model id.
     * @param alarmCode the alarm code.
     * @return true if exist. false if otherwise.
     */
    public boolean isExistAlarmCode(final int orgId, final int modelId, final int alarmCode) {
        final String sql = "SELECT COUNT(m_alarm_code.maker_org_id) FROM m_alarm_code" +
                " WHERE m_alarm_code.maker_org_id = ?" +
                " AND m_alarm_code.model_id = ?" +
                " AND m_alarm_code.alarm_code = ?;";
        try {
            int count = jdbcTemplate.queryForObject(
                    sql, new Object[]{orgId, modelId, alarmCode}, Integer.class);
            return count > 0;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}
