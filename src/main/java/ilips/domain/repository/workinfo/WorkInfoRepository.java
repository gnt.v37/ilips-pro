package ilips.domain.repository.workinfo;


import ilips.domain.model.workinfo.WorkInfo;
import ilips.domain.model.workinfo.WorkInfoGridRow;
import ilips.domain.model.workinfo.WorkInfoGridRowCollection;
import ilips.domain.model.workinfo.WorkInfoRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Work Repository.
 *
 * @author QuynhPP.
 * created on 9/17/2018.
 */
@Transactional
@Repository
public class WorkInfoRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public WorkInfoRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_UPDATE_WORK_DATA =
            "UPDATE d_work "
                    + "SET "
                    + "work_id = ?, "
                    + "work_no = ?, "
                    + "work_name = ?, "
                    + "description = ? "
                    + "WHERE work_id = ?;";

    private static final String SQL_INSERT_WORK_DATA =
            "INSERT INTO d_work "
                    + "(work_id, "
                    + "work_no, "
                    + "work_name, "
                    + "description) "
                    + "VALUES (?, ?, ?, ?);";

    private static final String SQL_DELETE_WORK_DATA =
            "DELETE FROM d_work "
                    + "WHERE work_id = ?;";

    private static final String SQL_IS_DUPLICATE_KEY_WORK_DATA =
            "SELECT count(*) FROM d_work "
                    + "WHERE work_id = ?;";

    private static final String SQL_IS_FOREIGN_KEY_DATA_USED =
            "SELECT count(*) FROM d_product "
                    + "WHERE work_id = ?;";

    /**
     * Get Work data.
     *
     * @return list of work data.
     */
    public List<WorkInfo> getWorkDataList() {
        final String sql = "SELECT * FROM d_work;";
        List<WorkInfo> workInfoList;
        try {
            workInfoList = jdbcTemplate.query(sql, new WorkInfoRowMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

        return workInfoList;
    }

    /**
     * Update database.
     *
     * @param workInfoGridRowCollection collection of data row to update.
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateWorkData(WorkInfoGridRowCollection workInfoGridRowCollection) {
        List<WorkInfoGridRow> deleteDataList = workInfoGridRowCollection.getArrDeletedData();
        List<WorkInfoGridRow> updateDataList = workInfoGridRowCollection.getArrUpdatedData();
        List<WorkInfoGridRow> insertDataList = workInfoGridRowCollection.getArrInsertedData();


        //delete process
        jdbcTemplate.batchUpdate(SQL_DELETE_WORK_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement deleteStatement, int i) throws SQLException {
                int paramIndex = 0;
                WorkInfoGridRow workInfoGridRow = deleteDataList.get(i);
                WorkInfo workInfo = workInfoGridRow.getRowData();
                deleteStatement.setInt(++paramIndex, workInfo.getOldWorkId());
            }

            @Override
            public int getBatchSize() {
                return deleteDataList.size();
            }
        });

        //update process
        jdbcTemplate.batchUpdate(SQL_UPDATE_WORK_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement updateStatement, int i) throws SQLException {
                int paramIndex = 0;
                WorkInfoGridRow workInfoGridRow = updateDataList.get(i);
                WorkInfo workInfo = workInfoGridRow.getRowData();
                updateStatement.setInt(++paramIndex, workInfo.getWorkId());
                updateStatement.setString(++paramIndex, workInfo.getWorkNo());
                updateStatement.setString(++paramIndex, workInfo.getWorkName());
                updateStatement.setString(++paramIndex, workInfo.getDescription());
                updateStatement.setInt(++paramIndex, workInfo.getOldWorkId());
            }

            @Override
            public int getBatchSize() {
                return updateDataList.size();
            }
        });

        //insert process
        jdbcTemplate.batchUpdate(SQL_INSERT_WORK_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement insertStatement, int i) throws SQLException {
                int paramIndex = 0;
                WorkInfoGridRow workInfoGridRow = insertDataList.get(i);
                WorkInfo workInfo = workInfoGridRow.getRowData();
                insertStatement.setInt(++paramIndex, workInfo.getWorkId());
                insertStatement.setString(++paramIndex, workInfo.getWorkNo());
                insertStatement.setString(++paramIndex, workInfo.getWorkName());
                insertStatement.setString(++paramIndex, workInfo.getDescription());
            }

            @Override
            public int getBatchSize() {
                return insertDataList.size();
            }
        });

    }

    /**
     * Check duplicate of work id.
     *
     * @param workId work id to check.
     * @return true if duplicate. false if not duplicate.
     */
    public boolean isExistKey(Integer workId) {
        boolean result = false;
        int duplicateCount = jdbcTemplate.queryForObject(
                SQL_IS_DUPLICATE_KEY_WORK_DATA, new Object[]{workId}, Integer.class);
        if (duplicateCount > 0) {
            result = true;
        }
        return result;
    }

    /**
     * Check data is referenced from other table.
     *
     * @param workId work id to check.
     * @return true if data is referenced. false if data is not referenced.
     */
    public boolean isDataReferenced(Integer workId) {
        boolean result = false;
        int refereceneCount = jdbcTemplate.queryForObject(
                SQL_IS_FOREIGN_KEY_DATA_USED, new Object[]{workId}, Integer.class);
        if (refereceneCount > 0) {
            result = true;
        }
        return result;
    }
}
