package ilips.domain.repository.reporttemplatedata;

import ilips.domain.model.reporttemplatedata.ReportTemplateData;
import ilips.domain.model.reporttemplatedata.ReportTemplateDataGridRow;
import ilips.domain.model.reporttemplatedata.ReportTemplateDataGridRowCollection;
import ilips.domain.model.reporttemplatedata.ReportTemplateDataRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Report template repository.
 *
 * @author QuynhPP
 * created on 7/20/2018
 */
@Transactional
@Repository
public class ReportTemplateDataRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ReportTemplateDataRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_UPDATE_REPORT_TEMPLATE_DATA =
            "UPDATE d_report_template "
                    + "SET maker_org_id = ?, "
                    + "model_id = ?, "
                    + "product_id = ?, "
                    + "report_template_id = ?, "
                    + "report_template_name = ?, "
                    + "report_template_filename = ? "
                    + "WHERE maker_org_id = ? "
                    + "AND model_id = ? "
                    + "AND product_id = ? "
                    + "AND report_template_id = ?;";

    private static final String SQL_INSERT_REPORT_TEMPLATE_DATA =
            "INSERT INTO d_report_template "
                    + "(maker_org_id, "
                    + "model_id, "
                    + "product_id, "
                    + "report_template_id, "
                    + "report_template_name, "
                    + "report_template_filename) "
                    + "VALUES (?, ?, ?, ?, ?, ?);  ";

    private static final String SQL_DELETE_REPORT_TEMPLATE_DATA =
            "DELETE FROM d_report_template "
                    + "WHERE maker_org_id = ? "
                    + "AND model_id = ? "
                    + "AND product_id = ? "
                    + "AND report_template_id = ?;";

    /**
     * Get Report template data.
     *
     * @return list of report template data.
     */
    public List<ReportTemplateData> getReportTemplateDataList(Integer makerOrgId, Integer modelId, Integer productId) {
        final String sql = "SELECT * FROM d_report_template "
                + "WHERE maker_org_id = ? "
                + "AND model_id = ? "
                + "AND product_id = ? ;";
        List<ReportTemplateData> reportTemplateDataList;
        try {
            reportTemplateDataList = jdbcTemplate.query(sql, new Object[]{makerOrgId, modelId, productId}, new ReportTemplateDataRowMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        return reportTemplateDataList;
    }

    /**
     * Update database.
     *
     * @param reportTemplateDataGridRowCollection collection of data row to update.
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateReportTemplateData(ReportTemplateDataGridRowCollection reportTemplateDataGridRowCollection) {
        List<ReportTemplateDataGridRow> deleteDataList = reportTemplateDataGridRowCollection.getArrDeletedData();
        List<ReportTemplateDataGridRow> updateDataList = reportTemplateDataGridRowCollection.getArrUpdatedData();
        List<ReportTemplateDataGridRow> insertDataList = reportTemplateDataGridRowCollection.getArrInsertedData();

        //delete process
        jdbcTemplate.batchUpdate(SQL_DELETE_REPORT_TEMPLATE_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement deleteStatement, int i) throws SQLException {
                int paramIndex = 0;
                ReportTemplateDataGridRow reportTemplateDataGridRow = deleteDataList.get(i);
                ReportTemplateData reportTemplateData = reportTemplateDataGridRow.getRowData();
                deleteStatement.setInt(++paramIndex, reportTemplateData.getMakerOrgId());
                deleteStatement.setInt(++paramIndex, reportTemplateData.getModelId());
                deleteStatement.setInt(++paramIndex, reportTemplateData.getProductId());
                deleteStatement.setInt(++paramIndex, reportTemplateData.getOldReportTemplateId());
            }

            @Override
            public int getBatchSize() {
                return deleteDataList.size();
            }
        });

        //update process
        jdbcTemplate.batchUpdate(SQL_UPDATE_REPORT_TEMPLATE_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement updateStatement, int i) throws SQLException {
                int paramIndex = 0;
                ReportTemplateDataGridRow reportTemplateDataGridRow = updateDataList.get(i);
                ReportTemplateData reportTemplateData = reportTemplateDataGridRow.getRowData();
                updateStatement.setInt(++paramIndex, reportTemplateData.getMakerOrgId());
                updateStatement.setInt(++paramIndex, reportTemplateData.getModelId());
                updateStatement.setInt(++paramIndex, reportTemplateData.getProductId());
                updateStatement.setInt(++paramIndex, reportTemplateData.getReportTemplateId());
                updateStatement.setString(++paramIndex, reportTemplateData.getReportTemplateName());
                updateStatement.setString(++paramIndex, reportTemplateData.getReportTemplateFileName());
                updateStatement.setInt(++paramIndex, reportTemplateData.getMakerOrgId());
                updateStatement.setInt(++paramIndex, reportTemplateData.getModelId());
                updateStatement.setInt(++paramIndex, reportTemplateData.getProductId());
                updateStatement.setInt(++paramIndex, reportTemplateData.getOldReportTemplateId());
            }

            @Override
            public int getBatchSize() {
                return updateDataList.size();
            }
        });

        //insert process
        jdbcTemplate.batchUpdate(SQL_INSERT_REPORT_TEMPLATE_DATA, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement insertStatement, int i) throws SQLException {
                int paramIndex = 0;
                ReportTemplateDataGridRow reportTemplateDataGridRow = insertDataList.get(i);
                ReportTemplateData reportTemplateData = reportTemplateDataGridRow.getRowData();
                insertStatement.setInt(++paramIndex, reportTemplateData.getMakerOrgId());
                insertStatement.setInt(++paramIndex, reportTemplateData.getModelId());
                insertStatement.setInt(++paramIndex, reportTemplateData.getProductId());
                insertStatement.setInt(++paramIndex, reportTemplateData.getReportTemplateId());
                insertStatement.setString(++paramIndex, reportTemplateData.getReportTemplateName());
                insertStatement.setString(++paramIndex, reportTemplateData.getReportTemplateFileName());
            }

            @Override
            public int getBatchSize() {
                return insertDataList.size();
            }
        });
    }
}
