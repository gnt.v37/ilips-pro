package ilips.domain.repository.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Alarm Send To Repository.
 *
 * @author GiangNPT
 * created on 7/18/2018
 */
@Repository
public class AlarmSendToRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AlarmSendToRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Alarm mail transmission destination data existence check.
     *
     * @param orgId      org id.
     * @param modelId    model id.
     * @param productId  product id.
     * @param alarmLevel alarm level.
     * @param groupId    group id.
     * @return true if exist. false if otherwise.
     */
    public boolean isExistAlarmSendTo(final int orgId, final int modelId, final int productId,
                                      final int alarmLevel, final int groupId) {
        final String sql = "SELECT COUNT(d_alarm_sendto.maker_org_id) FROM d_alarm_sendto" +
                " WHERE d_alarm_sendto.maker_org_id = ?" +
                " AND d_alarm_sendto.model_id = ?" +
                " AND d_alarm_sendto.product_id = ?" +
                " AND d_alarm_sendto.alarm_level = ?" +
                " AND d_alarm_sendto.group_id = ?;";
        try {
            int count = jdbcTemplate.queryForObject(
                    sql, new Object[]{orgId, modelId, productId, alarmLevel, groupId}, Integer.class);
            return count > 0;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}
