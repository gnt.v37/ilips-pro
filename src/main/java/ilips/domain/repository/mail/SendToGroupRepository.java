package ilips.domain.repository.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Send To Group Repository.
 *
 * @author GiangNPT
 * created on 7/18/2018
 */
@Repository
public class SendToGroupRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public SendToGroupRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Get send mail to group name.
     *
     * @param orgId     the org id.
     * @param modelId   the model id.
     * @param productId the product id.
     * @param groupId   the group id.
     * @return Group Name.
     */
    public String getGroupName(final int orgId, final int modelId, final int productId, final int groupId) {
        final String sql = "SELECT d_sendto_group.group_name FROM d_sendto_group" +
                " WHERE d_sendto_group.maker_org_id = ?" +
                " AND d_sendto_group.model_id = ?" +
                " AND d_sendto_group.product_id = ?" +
                " AND d_sendto_group.group_id = ?;";
        try {
            return jdbcTemplate.queryForObject(
                    sql, new Object[]{orgId, modelId, productId, groupId}, String.class);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
