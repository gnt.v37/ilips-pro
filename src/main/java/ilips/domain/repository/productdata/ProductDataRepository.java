package ilips.domain.repository.productdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Product Repository.
 *
 * @author GiangNPT
 * created on 7/18/2018
 */
@Repository
public class ProductDataRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductDataRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Get product name.
     *
     * @param orgId     the org id.
     * @param modelId   the model id.
     * @param productId the product id.
     * @return the product name.
     */
    public String getProductName(final int orgId, final int modelId, final int productId) {
        final String sql = "SELECT d_product.product_name FROM d_product WHERE d_product.maker_org_id = ?" +
                " AND d_product.model_id = ? AND d_product.product_id = ?;";
        try {
            return jdbcTemplate.queryForObject(
                    sql, new Object[]{orgId, modelId, productId}, String.class);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    /**
     * Duplication check within d_product of data collection module number + device number
     *
     * @param rmcom    module number
     * @param deviceId device number
     * @return true if exist d_product, false if otherwise.
     */
    public boolean isDuplicatedRmcomDeviceId(final int rmcom, final int deviceId) {
        final String sql = "SELECT COUNT(d_product.product_id) FROM d_product WHERE d_product.rm_com_dev_id = ?" +
                " AND d_product.dev_id = ?;";
        try {
            int count = jdbcTemplate.queryForObject(
                    sql, new Object[]{rmcom, deviceId}, Integer.class);
            return count > 0;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    /**
     * Acquire data collection module number name.
     *
     * @param rmcomId module number id.
     * @return module number name
     */
    public String getRmcomName(final int rmcomId) {
        final String sql = "SELECT d_rmcom.rm_com_name FROM d_rmcom WHERE d_rmcom.rm_com_id = ?;";
        try {
            return jdbcTemplate.queryForObject(
                    sql, new Object[]{rmcomId}, String.class);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
