package ilips.domain.service.workinfo;

import ilips.domain.model.workinfo.WorkInfo;
import ilips.domain.model.workinfo.WorkInfoGridRowCollection;
import ilips.domain.repository.workinfo.WorkInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Work data service.
 *
 * @author QuynhPP .
 * created on 9/17/2018.
 */
@Service
public class WorkInfoService {

    private final WorkInfoRepository workInfoRepository;

    @Autowired
    public WorkInfoService(WorkInfoRepository workInfoRepository) {
        this.workInfoRepository = workInfoRepository;
    }

    /**
     * Get work data list.
     *
     * @return list of work data.
     */
    public List<WorkInfo> getWorkDataList() {
        return workInfoRepository.getWorkDataList();
    }

    /**
     * Update work data to database.
     *
     * @param workInfoGridRowCollection grid row data.
     */
    public void updateWorkDataList(WorkInfoGridRowCollection workInfoGridRowCollection) {
        workInfoRepository.updateWorkData(workInfoGridRowCollection);
    }

    /**
     * Check exist key.
     * @param workId work id key.
     * @return true if key is exist. false if key is not exist.
     */
    public boolean isExistKey(Integer workId) {
        return workInfoRepository.isExistKey(workId);
    }

    /**
     * Check referenced data.
     * @param workId work id key.
     * @return true if data is referenced. false if key is not referenced.
     */
    public boolean isDataReferenced(Integer workId) {
        return workInfoRepository.isDataReferenced(workId);
    }
}
