package ilips.domain.service.persondata;

import ilips.domain.model.persondata.PersonData;
import ilips.domain.model.persondata.PersonDataGridRowCollection;
import ilips.domain.repository.persondata.PersonDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonDataService {
    private final PersonDataRepository personDataRepository;

    @Autowired
    public PersonDataService(PersonDataRepository personDataRepository) {
        this.personDataRepository = personDataRepository;
    }

    public List<PersonData> getPersonDataList(Integer selectOrgId, Integer userPermission) {
        return personDataRepository.getPersonDataList(selectOrgId, userPermission);
    }

    public void updatePersonData(PersonDataGridRowCollection personDataGridRowCollection) {
        personDataRepository.updatePersonData(personDataGridRowCollection);
    }

    public boolean isExistKey(Integer personId, Integer orgId) {
        return personDataRepository.isExistKey(personId, orgId);
    }
}
