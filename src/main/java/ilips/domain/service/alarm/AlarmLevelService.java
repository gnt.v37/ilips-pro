package ilips.domain.service.alarm;

import ilips.domain.model.AlarmLevel.AlarmLevelData;
import ilips.domain.model.AlarmLevel.AlarmLevelDataGridRowCollection;
import ilips.domain.repository.alarm.AlarmLevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlarmLevelService {
    private final AlarmLevelRepository alarmLevelRepository;

    @Autowired
    public AlarmLevelService(AlarmLevelRepository alarmLevelRepository) {
        this.alarmLevelRepository = alarmLevelRepository;
    }

    public List<AlarmLevelData> getAlarmLevelDataList() {
        return alarmLevelRepository.getAlarmLevelDataList();
    }

    public void updateAlarmLevelData(AlarmLevelDataGridRowCollection alarmLevelDataGridRowCollection) {
        alarmLevelRepository.updateAlarmLevelData(alarmLevelDataGridRowCollection);
    }

    public boolean isExistKey(Integer alarmLevel) {
        return alarmLevelRepository.isExistKey(alarmLevel);
    }

    public boolean isExistAlarmLevelName(String alarmLevelName) {
        return alarmLevelRepository.isExistAlarmLevelName(alarmLevelName);
    }
}
