package ilips.domain.service.authentication;

import ilips.domain.model.user.UserDetail;
import ilips.domain.service.user.UserService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Authentication Provider.
 *
 * @author CanhLP
 * created on 7/13/2018
 */
@Component
public class IlipsAuthenticationProvider implements AuthenticationProvider {

    private final UserService userService;

    public IlipsAuthenticationProvider(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = "";
        if (authentication.getCredentials() != null) {
            password = authentication.getCredentials().toString();
        }

        if (StringUtils.hasText(name) && StringUtils.hasText(password)) {
            UserDetail userDetail = userService.login(name, password);
            return new UsernamePasswordAuthenticationToken(userDetail, null, userDetail.getAuthorities());
        }

        throw new BadCredentialsException("");
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}
