package ilips.domain.service.authentication;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

/**
 * Authority.
 *
 * @author CanhLP
 * created on 7/13/2018
 */
public class IlipsAuthority implements GrantedAuthority {
    @Getter
    private Integer permission;

    public IlipsAuthority(Integer p) {
        this.permission = p;
    }

    @Override
    public String getAuthority() {
        return permission.toString();
    }
}
