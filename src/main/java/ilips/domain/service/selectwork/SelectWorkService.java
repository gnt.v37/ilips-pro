package ilips.domain.service.selectwork;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ilips.domain.model.selectwork.SelectWorkData;
import ilips.domain.repository.selectwork.SelectWorkRepository;

/**
 * 
 * Class name:   Select Work Data Service
 * 
 * @author ThangNT2
 * Created Date: Jul 23, 2018
 */
@Service
public class SelectWorkService {

	private SelectWorkRepository selectWorkRepository;
	
	@Autowired
	public SelectWorkService(SelectWorkRepository selectWorkRepository) {
		
		this.selectWorkRepository = selectWorkRepository;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<SelectWorkData> getAll() {
		return this.selectWorkRepository.getAll();
	}
}
