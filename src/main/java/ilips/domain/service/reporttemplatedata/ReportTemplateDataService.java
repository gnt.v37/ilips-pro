package ilips.domain.service.reporttemplatedata;

import ilips.domain.model.reporttemplatedata.ReportTemplateData;
import ilips.domain.model.reporttemplatedata.ReportTemplateDataGridRowCollection;
import ilips.domain.repository.reporttemplatedata.ReportTemplateDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Report template data service.
 *
 * @author QuynhPP
 * created on 7/20/2018
 */
@Service
public class ReportTemplateDataService {

    private final ReportTemplateDataRepository reportTemplateDataRepository;

    @Autowired
    public ReportTemplateDataService(ReportTemplateDataRepository reportTemplateDataRepository) {
        this.reportTemplateDataRepository = reportTemplateDataRepository;
    }

    /**
     * Get report template data list.
     * @param makerOrgId maker organization id.
     * @param modelId model id.
     * @param productId product id.
     * @return list of report template data.
     */
    public List<ReportTemplateData> getReportTemplateDataList(Integer makerOrgId, Integer modelId, Integer productId) {
        return reportTemplateDataRepository.getReportTemplateDataList(makerOrgId, modelId, productId);
    }

    public void updateReportTemplateDataList(ReportTemplateDataGridRowCollection reportTemplateDataGridRowCollection) {
        reportTemplateDataRepository.updateReportTemplateData(reportTemplateDataGridRowCollection);
    }
}
