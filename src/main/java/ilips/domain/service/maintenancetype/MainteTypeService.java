package ilips.domain.service.maintenancetype;

import ilips.domain.model.maintenancetype.MainteTypeData;
import ilips.domain.model.maintenancetype.MainteTypeDataGridRowCollection;
import ilips.domain.repository.maintenancetype.MainteTypeDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MainteTypeService {

    //Maintenance Type Data Repository
    private final MainteTypeDataRepository mainteTypeDataRepository;

    @Autowired
    public MainteTypeService(MainteTypeDataRepository mainteTypeDataRepository) {
        this.mainteTypeDataRepository = mainteTypeDataRepository;
    }

    /**
     * Get list Maintenance Type Data.
     *
     * @param makerOrgId
     * @param modelId
     * @return List Maintenance Type Data.
     */
    public List<MainteTypeData> getMainteTypeList(int makerOrgId, int modelId){
        return mainteTypeDataRepository.getMainteTypeList(makerOrgId, modelId);
    }

    /**
     * Set list Maintenance Type Data.
     *
     * @param mainteTypeDataGridRowWrapper
     */
    public void updateMainteTypeData(MainteTypeDataGridRowCollection mainteTypeDataGridRowWrapper){
        mainteTypeDataRepository.updateMainteTypeData(mainteTypeDataGridRowWrapper);
    }

    public boolean isDupplicateKey(Integer makerOrgId, Integer modelId, Integer mainteType) {
        return mainteTypeDataRepository.isDupplicateKey(makerOrgId, modelId, mainteType);
    }
}
