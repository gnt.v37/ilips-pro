package ilips.domain.service.user;

import ilips.constant.FunctionConst;
import ilips.domain.model.persondata.PersonData;
import ilips.domain.model.user.UserDetail;
import ilips.domain.repository.persondata.FunctionRepository;
import ilips.domain.repository.persondata.PersonDataRepository;
import ilips.domain.service.authentication.IlipsAuthority;
import ilips.domain.service.common.CommonService;
import ilips.util.ILIPSUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * User Service.
 *
 * @author CanhLP
 * created on 7/13/2018
 */
@Service
public class UserService {
    private final PersonDataRepository personRepository;
    private final FunctionRepository permissionRepository;
    private final CommonService commonService;

    @Autowired
    public UserService(PersonDataRepository personRepository,
                       FunctionRepository permissionRepository,
                       CommonService commonService) {
        this.personRepository = personRepository;
        this.permissionRepository = permissionRepository;
        this.commonService = commonService;
    }

    /**
     * Login.
     *
     * @param username user name.
     * @param password password.
     * @return user information.
     */
    public UserDetail login(String username, String password) {
        //Get person information.
        PersonData person = personRepository.getPerson(username);

        String encryptedPassword = ILIPSUtil.passwordEncrypt(password);
        if (person == null || !encryptedPassword.equals(person.getPassword())) {
            //TODO correct message
            throw new BadCredentialsException("EM136");
        }

        //Get permission
        List<Integer> enableFunction = permissionRepository.getEnableFunction(person.getOrgId(), person.getPersonId());
        List<IlipsAuthority> permissions = new ArrayList<>();
        boolean isContainLoginPermission = false;
        if (enableFunction != null) {
            for (int function : enableFunction) {
                permissions.add(new IlipsAuthority(function));
                if (function == FunctionConst.LOGIN) {
                    isContainLoginPermission = true;
                }
            }
        }

        if (!isContainLoginPermission) {
            //TODO correct message
            throw new BadCredentialsException("EM136");
        }

        //Get org name
        String orgName = commonService.getOrgNameRecursive(person.getOrgId());

        int systemPermission = person.getPermission();

        return new UserDetail(person, permissions, orgName, systemPermission);
    }
}
