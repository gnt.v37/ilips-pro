package ilips.domain.service.common;

import ilips.domain.model.modeldata.ModelData;
import ilips.domain.model.orgdata.OrgData;
import ilips.domain.model.persondata.PersonData;
import ilips.domain.model.systempermission.SystemPermission;
import ilips.domain.repository.alarm.AlarmCodeRepository;
import ilips.domain.repository.alarm.AlarmLevelRepository;
import ilips.domain.repository.mail.AlarmSendToRepository;
import ilips.domain.repository.mail.SendToGroupRepository;
import ilips.domain.repository.modeldata.ModelRepository;
import ilips.domain.repository.orgdata.OrgDataRepository;
import ilips.domain.repository.persondata.PersonDataRepository;
import ilips.domain.repository.productdata.ProductDataRepository;
import ilips.domain.repository.systempermission.SystemPermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Base service class.
 *
 * @author GiangNPT
 * created on 7/18/2018
 */
@Service
public class CommonService {
    private OrgDataRepository orgRepository;

    private ModelRepository modelRepository;

    private ProductDataRepository productRepository;

    private PersonDataRepository personRepository;

    private SendToGroupRepository sendToGroupRepository;

    private AlarmLevelRepository alarmLevelRepository;

    private AlarmCodeRepository alarmCodeRepository;

    private AlarmSendToRepository alarmSendToRepository;

    private SystemPermissionRepository systemPermissionRepository;

    @Autowired
    public CommonService(OrgDataRepository orgRepository,
                         ModelRepository modelRepository,
                         ProductDataRepository productRepository,
                         PersonDataRepository personRepository,
                         SendToGroupRepository sendToGroupRepository,
                         AlarmLevelRepository alarmLevelRepository,
                         AlarmCodeRepository alarmCodeRepository,
                         AlarmSendToRepository alarmSendToRepository,
                         SystemPermissionRepository systemPermissionRepository) {
        this.orgRepository = orgRepository;
        this.modelRepository = modelRepository;
        this.productRepository = productRepository;
        this.personRepository = personRepository;
        this.sendToGroupRepository = sendToGroupRepository;
        this.alarmLevelRepository = alarmLevelRepository;
        this.alarmCodeRepository = alarmCodeRepository;
        this.systemPermissionRepository = systemPermissionRepository;
        this.alarmSendToRepository = alarmSendToRepository;
    }

    /**
     * Get all system permission.
     *
     * @return system permission.
     */
    public List<SystemPermission> getSystemPermissionList() {
        return systemPermissionRepository.getSystemPermissionList();
    }

    /**
     * Get Org Name Recursive from top to child
     *
     * @param orgId the child org Id
     * @return the Org Name Recursive
     */
    public String getOrgNameRecursive(int orgId) {
        //Get org information.
        OrgData org = orgRepository.getOrg(orgId);
        if (org == null) {
            return null;
        }
        StringBuilder orgName = new StringBuilder(org.getOrgName() == null ? "" : org.getOrgName());
        Integer parentOrgId = org.getParentId();

        //Create list prevent infinity loop.
        List<Integer> orgList = new ArrayList<>();
        orgList.add(parentOrgId);

        while (parentOrgId != null) {
            org = orgRepository.getOrg(parentOrgId);
            if (org == null) {
                break;
            }
            //insert parent org before
            orgName.insert(0, "　");
            orgName.insert(0, org.getOrgName());
            parentOrgId = org.getParentId();

            if (orgList.contains(parentOrgId)) {
                //infinity loop, need to end process.
                break;
            }
            orgList.add(parentOrgId);
        }

        return orgName.toString();
    }

    /**
     * Get Org Name
     *
     * @param orgId the org Id to get name.
     * @return the Org Name.
     */
    public String getOrgName(int orgId) {
        OrgData org = orgRepository.getOrg(orgId);

        if (org == null) {
            return null;
        }

        return org.getOrgName();
    }

    /**
     * Get Maker Name
     *
     * @param orgId the org Id to get name.
     * @return the Maker Name.
     */
    public String getMakerName(int orgId) {
        OrgData org = orgRepository.getOrg(orgId, null);

        if (org == null) {
            return null;
        }

        return org.getOrgName();
    }

    /**
     * Get ModelData Name
     *
     * @param orgId   the org Id to get name.
     * @param modelId the modeldata Id to get name.
     * @return the ModelData Name.
     */
    public String getModelName(int orgId, int modelId) {
        ModelData modelData = modelRepository.getModel(orgId, modelId);

        if (modelData == null) {
            return null;
        }

        return modelData.getModelName();
    }

    /**
     * Get product name.
     *
     * @param orgId     the org id.
     * @param modelId   the model id.
     * @param productId the product id.
     * @return the product name.
     */
    public String getProductName(int orgId, int modelId, int productId) {
        return productRepository.getProductName(orgId, modelId, productId);
    }

    /**
     * Get a person's name
     *
     * @param orgId    the org id.
     * @param personId the person id
     * @return the person name.
     */
    public String getPersonName(int orgId, int personId) {
        PersonData person = personRepository.getPerson(orgId, personId);

        if (person == null) {
            return null;
        }

        return person.getPersonName();
    }

    /**
     * Duplication check within d_product of data collection module number + device number
     *
     * @param rmcom    module number
     * @param deviceId device number
     * @return true if exist d_product, false if otherwise.
     */
    public boolean isDuplicatedRmcomDeviceId(int rmcom, int deviceId) {
        return productRepository.isDuplicatedRmcomDeviceId(rmcom, deviceId);
    }

    /**
     * Acquire data collection module number name.
     *
     * @param rmcomId module number id.
     * @return module number name
     */
    public String getRmcomName(int rmcomId) {
        return productRepository.getRmcomName(rmcomId);
    }

    /**
     * Get send mail to group name.
     *
     * @param orgId     the org id.
     * @param modelId   the model id.
     * @param productId the product id.
     * @param groupId   the group id.
     * @return Group Name.
     */
    public String getSendToGroupName(int orgId, int modelId, int productId, int groupId) {
        return sendToGroupRepository.getGroupName(orgId, modelId, productId, groupId);
    }

    /**
     * Get alarm level name.
     *
     * @param alarmId alarm Id.
     * @return alarm level name.
     */
    public String getAlarmName(int alarmId) {
        return alarmLevelRepository.getAlarmName(alarmId);
    }

    /**
     * Alarm code existence check.
     *
     * @param orgId     the org id.
     * @param modelId   the model id.
     * @param alarmCode the alarm code.
     * @return true if exist. false if otherwise.
     */
    public boolean isExistAlarmCode(int orgId, int modelId, int alarmCode) {
        return alarmCodeRepository.isExistAlarmCode(orgId, modelId, alarmCode);
    }

    /**
     * Alarm mail transmission destination data existence check.
     *
     * @param orgId      org id.
     * @param modelId    model id.
     * @param productId  product id.
     * @param alarmLevel alarm level.
     * @param groupId    group id.
     * @return true if exist. false if otherwise.
     */
    public boolean isExistAlarmSendTo(int orgId, int modelId, int productId,
                                      int alarmLevel, int groupId) {
        return alarmSendToRepository.isExistAlarmSendTo(orgId, modelId, productId, alarmLevel, groupId);
    }

    /**
     * Get alarm code name.
     *
     * @param orgId     the org id.
     * @param modelId   the model id.
     * @param alarmCode the alarm code.
     * @return alarm code name.
     */
    public String getAlarmCodeName(int orgId, int modelId, int alarmCode) {
        return alarmCodeRepository.getAlarmCodeName(orgId, modelId, alarmCode);
    }
}
