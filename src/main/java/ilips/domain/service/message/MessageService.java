package ilips.domain.service.message;

import ilips.util.WebContextHolder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class MessageService {
    private final MessageSource messageSource;

    @Autowired
    public MessageService(@Qualifier(value = "messageSource") MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String text(String key, String... params) {
        return key + ": "
                + messageSource.getMessage(getMessagePrefix(key) + key,
                params, WebContextHolder.getInstance().getLocale());
    }

    private String getMessagePrefix(String messageCode) {
        if (StringUtils.isEmpty(messageCode)) {
            return StringUtils.EMPTY;
        }

        //F（FATAL）,　E（ERROR）,　W（WARN）,　I（INFO）,　D（DEBUG）
        switch (messageCode.charAt(0)) {
            case 'F':
                return "fatal.";
            case 'E':
                return "error.";
            case 'W':
                return "warn.";
            case 'I':
                return "info.";
            case 'D':
                return "debug.";

            default:
                return StringUtils.EMPTY;
        }
    }
}
