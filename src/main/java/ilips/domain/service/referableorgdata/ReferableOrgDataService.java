package ilips.domain.service.referableorgdata;

import ilips.domain.model.referableorgdata.ReferableOrgData;
import ilips.domain.model.referableorgdata.ReferableOrgDataGridRowCollection;
import ilips.domain.repository.referableorgdata.ReferableOrgDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReferableOrgDataService {

    private final ReferableOrgDataRepository referableOrgDataRepository;

    @Autowired
    public ReferableOrgDataService(ReferableOrgDataRepository referableOrgDataRepository) {
        this.referableOrgDataRepository = referableOrgDataRepository;
    }

    public List<ReferableOrgData> getReferableOrgDataList(Integer selectReferableOrgId) {
        return ReferableOrgDataRepository.getReferableOrgData(selectReferableOrgId);
    }

    /**
     * Update referableorg data to database.
     *
     * @param referableOrgDataGridRowCollection grid row data.
     */
    public void updateWorkDataList(ReferableOrgDataGridRowCollection referableOrgDataGridRowCollection) {
        ReferableOrgDataRepository.updateReferableOrgData(referableOrgDataGridRowCollection);
    }
}
