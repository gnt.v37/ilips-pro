package ilips.domain.service.orgdata;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ilips.domain.model.orgdata.OrgData;
import ilips.domain.model.orgdata.OrgDataGridRowCollection;
import ilips.domain.repository.orgdata.OrgDataRepository;

/**
 * 
 * Class name:   Organization Service
 * 
 * @author ThangNT2
 * Created Date: Jul 20, 2018
 */
@Service
public class OrgDataService {

    private final OrgDataRepository orgDataRepository;

    @Autowired
    public OrgDataService(OrgDataRepository orgDataRepository) {
        this.orgDataRepository = orgDataRepository;
    }
    
    /**
     * load data
     * 
     * @param orgId organization id 
     * @param selectParentId select parent id
     * @param permission user_permission 
     * @return list data
     */
    public List<OrgData> getListOrgData(Integer orgId, Integer selectParentId, Integer permission) {
    	
    	return this.orgDataRepository.getOrgDataList(orgId, selectParentId, permission);
    }
    
    /**
     * Update data
     * 
     * @param orgDataGridRowCollection collection data for inserting, updating and deleting
     * @param selectParentId select parent id
     * @param userOrgId user login orgId
     */
    public void updateData(OrgDataGridRowCollection orgDataGridRowCollection, Integer selectParentId, Integer userOrgId) {
    	
    	this.orgDataRepository.updateData(orgDataGridRowCollection, selectParentId, userOrgId);
    }

    /**
     * Duplicate org id and org name
     * @param orgId org id
     * @param orgName org name
     * @param delete action is delete (check only by org id)
     * @param 
     * @return
     */
    public boolean isExistOrgId(Integer orgId, String orgName, boolean delete) {
    	
    	return this.orgDataRepository.isDuplicateOrgIdOrOrgName(orgId, orgName, delete);
    }
    /**
     * get child org
     * @param in_OrgId
     * @param in_Permission
     * @param in_ParentOrgId
     * @return
     */
    public List<OrgData> GetSqlReferableChildOrg(final int in_OrgId, final int in_Permission, final int in_ParentOrgId) {
        return  this.orgDataRepository.GetSqlReferableChildOrg(in_OrgId,in_Permission,in_ParentOrgId);
    }
    /**
     *
     * @param in_OrgId
     * @param in_Permission
     * @return
     */
    public OrgData GetSqlReferableTopOrg(final int in_OrgId, final Integer in_Permission) {
        return  this.orgDataRepository.GetSqlReferableTopOrg(in_OrgId,in_Permission);
    }
}
