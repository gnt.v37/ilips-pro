package ilips.domain.model.selectorgwindow;

import lombok.Data;

@Data
public class SelectOrgWindow {

    private String id;
    private String parent;
    private String text;

    public SelectOrgWindow(String id, String parent, String text) {
        this.id = id;
        this.parent = parent;
        this.text = text;
    }
}
