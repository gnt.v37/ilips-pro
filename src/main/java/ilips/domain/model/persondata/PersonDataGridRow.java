package ilips.domain.model.persondata;

public class PersonDataGridRow {

    private Integer rowNum;

    private PersonData rowData;

    public PersonData getRowData() {
        return rowData;
    }

    public void setRowData(PersonData rowData) {
        this.rowData = rowData;
    }

    public Integer getRowNum() {
        return rowNum;
    }

    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }
}
