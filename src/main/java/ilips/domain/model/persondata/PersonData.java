package ilips.domain.model.persondata;

import lombok.Data;

/**
 * Person Entity corresponding to m_person table.
 */
@Data
public class PersonData {

    private Integer personId;

    private String personName;

    private Integer orgId;

    private Integer permission;

    private String loginId;

    private String password;

    private String addr;

    private String tel;

    private String fax;

    private String email;

    private String passwordExpirationUtc;

    private String accountExpirationUtc;
    //old key
    private Integer oldPersonId;

    private Integer oldOrgId;
}
