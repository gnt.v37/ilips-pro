package ilips.domain.model.persondata;

import ilips.constant.AppConst;
import ilips.domain.model.base.BaseRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper of Person Entity {@link PersonData}.
 */
public class PersonDataRowMapper extends BaseRowMapper implements RowMapper<PersonData> {

    @Override
    public PersonData mapRow(ResultSet row, int rowNum) throws SQLException {

        PersonData personData = new PersonData();
        personData.setPersonId(row.getInt("person_id"));
        personData.setPersonName(row.getString("person_name"));
        personData.setOrgId(row.getInt("org_id"));
        personData.setPermission(row.getInt("permission"));
        personData.setLoginId(row.getString("login_id"));
        personData.setPassword(row.getString("password"));
        personData.setAddr(row.getString("addr"));
        personData.setTel(row.getString("tel"));
        personData.setFax(row.getString("fax"));
        personData.setEmail(row.getString("email"));
        personData.setPasswordExpirationUtc(super.getDateStringFromResultSet(row,
                "password_expiration_utc",
                AppConst.DATE_FORMAT_DATE_TIME_JDBC));
        personData.setAccountExpirationUtc(super.getDateStringFromResultSet(row,
                "account_expiration_utc",
                AppConst.DATE_FORMAT_DATE_TIME_JDBC));

        personData.setOldPersonId(personData.getPersonId());
        personData.setOldOrgId(personData.getOrgId());

        return personData;
    }
}
