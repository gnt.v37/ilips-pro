package ilips.domain.model.persondata;

import java.util.List;

public class PersonDataGridRowCollection {

    private List<PersonDataGridRow> arrInsertedData;

    private List<PersonDataGridRow> arrUpdatedData;

    private List<PersonDataGridRow> arrDeletedData;

    public List<PersonDataGridRow> getArrInsertedData() {
        return arrInsertedData;
    }

    public void setArrInsertedData(List<PersonDataGridRow> arrInsertedData) {
        this.arrInsertedData = arrInsertedData;
    }

    public List<PersonDataGridRow> getArrUpdatedData() {
        return arrUpdatedData;
    }

    public void setArrUpdatedData(List<PersonDataGridRow> arrUpdatedData) {
        this.arrUpdatedData = arrUpdatedData;
    }

    public List<PersonDataGridRow> getArrDeletedData() {
        return arrDeletedData;
    }

    public void setArrDeletedData(List<PersonDataGridRow> arrDeletedData) {
        this.arrDeletedData = arrDeletedData;
    }
}
