package ilips.domain.model.persondata;

import ilips.domain.model.systempermission.SystemPermission;

import java.util.List;

public class PersonDataDisp {

    private List<PersonData> personDataList;

    private List<SystemPermission> systemPermissionList;

    public List<PersonData> getPersonDataList() {
        return personDataList;
    }

    public void setPersonDataList(List<PersonData> personDataList) {
        this.personDataList = personDataList;
    }

    public List<SystemPermission> getSystemPermissionList() {
        return systemPermissionList;
    }

    public void setSystemPermissionList(List<SystemPermission> systemPermissionList) {
        this.systemPermissionList = systemPermissionList;
    }
}
