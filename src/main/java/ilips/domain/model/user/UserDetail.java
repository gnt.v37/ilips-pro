package ilips.domain.model.user;

import ilips.domain.model.persondata.PersonData;
import ilips.domain.service.authentication.IlipsAuthority;
import lombok.Data;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Data
public class UserDetail implements UserDetails {
    private PersonData person;
    private List<IlipsAuthority> permissions;
    private int systemPermission;
    private String companyName;

    public UserDetail(PersonData person, List<IlipsAuthority> permissions, String companyName, int systemPermission) {
        this.person = person;
        this.permissions = permissions;
        this.systemPermission = systemPermission;
        this.companyName = companyName;
    }

    @Override
    public Collection<IlipsAuthority> getAuthorities() {
        return permissions;
    }

    public boolean hasAuthorize(Integer permission) {
        return permissions.parallelStream()
                .map(IlipsAuthority::getPermission)
                .anyMatch(t -> t.equals(permission));
    }

    public boolean hasOneOfAuthorizes(Integer... _permissions) {
        final List _perms = Arrays.asList(_permissions);
        return permissions.parallelStream()
                .map(IlipsAuthority::getPermission)
                .anyMatch(_perms::contains);
    }

    @Override
    public String getPassword() {
        return person.getPassword();
    }

    @Override
    public String getUsername() {
        return person.getLoginId();
    }

    @Override
    public boolean isAccountNonExpired() {
//        if (person.getAccountExpirationUtc() == null) {
//            return true;
//        }
//
//        Date now = new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime().getTime());
//
//        return person.getAccountExpirationUtc().compareTo(now) < 0;
        //TODO FIX IT LATER
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
//        if (person.getPasswordExpirationUtc() == null) {
//            return true;
//        }
//
//        Date now = new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime().getTime());
//
//        return person.getPasswordExpirationUtc().compareTo(now) < 0;
//        //TODO FIX IT LATER
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public int getSystemPermission() {
        return systemPermission;
    }
}
