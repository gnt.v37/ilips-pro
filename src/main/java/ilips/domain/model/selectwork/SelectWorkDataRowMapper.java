package ilips.domain.model.selectwork;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * 
 * Class name:   Select work mapper
 * 
 * @author ThangNT2
 * Created date on Jul 23, 2018
 */
public class SelectWorkDataRowMapper implements RowMapper<SelectWorkData>{

	@Override
	public SelectWorkData mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		SelectWorkData selectWorkData = new SelectWorkData();
		
		selectWorkData.setWorkId(rs.getInt("work_id"));
		selectWorkData.setWorkNo(rs.getString("work_no"));
		selectWorkData.setWorkName(rs.getString("work_name"));
		
		return selectWorkData;
	}

}
