package ilips.domain.model.selectwork;

import lombok.Data;

@Data
public class SelectWorkData {

	private Integer workId;
	
	private String workNo;
	
	private String workName;
}
