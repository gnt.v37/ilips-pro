package ilips.domain.model.systempermission;

import ilips.domain.model.base.BaseRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SystemPermissionRowMapper extends BaseRowMapper implements RowMapper<SystemPermission> {
    @Override
    public SystemPermission mapRow(ResultSet row, int rowNum) throws SQLException {

        SystemPermission systemPermission = new SystemPermission();
        systemPermission.setPermission(row.getInt("permission"));
        systemPermission.setPermissionName(row.getString("permission_name"));
        return systemPermission;
    }
}
