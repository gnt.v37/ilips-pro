package ilips.domain.model.systempermission;

import lombok.Data;

@Data
public class SystemPermission {

    private int permission;

    private String permissionName;

}
