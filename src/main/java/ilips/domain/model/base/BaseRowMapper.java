package ilips.domain.model.base;

import ilips.util.common.DateUtilEx;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

/**
 * Base row mapper class
 */
public class BaseRowMapper {

    /**
     * Get Date type from ResultSet as character string
     *
     * @param rs
     * @param colName
     * @param format Date format
     * @return
     * @throws SQLException
     */
    protected String getDateStringFromResultSet(ResultSet rs, String colName, String format) throws SQLException {
        try {
            if(rs.getObject(colName) == null){
                return null;
            }else{
                Calendar cal = Calendar.getInstance();
                cal.setTime(rs.getTimestamp(colName));

                String rtnValue = DateUtilEx.toString(cal.getTime(), format);
                return rtnValue;
            }
        }
        catch(SQLException e){
            return "";
        }
    }
}
