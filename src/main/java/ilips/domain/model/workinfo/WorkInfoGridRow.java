package ilips.domain.model.workinfo;

import lombok.Data;

/**
 * Grid row data.
 *
 * @author QuynhPP.
 * created on 9/17/2018.
 */
@Data
public class WorkInfoGridRow { //DES: save data each row for back-end process

    private Integer rowIndex;

    private WorkInfo rowData;
}
