package ilips.domain.model.workinfo;

import java.util.List;

import lombok.Data;

/**
 * Grid row data collection.
 *
 * @author QuynhPP.
 * created on 9/17/2018.
 */
@Data
public class WorkInfoGridRowCollection {

    private List<WorkInfoGridRow> arrInsertedData;

    private List<WorkInfoGridRow> arrUpdatedData;

    private List<WorkInfoGridRow> arrDeletedData;
}
