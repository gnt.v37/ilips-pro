package ilips.domain.model.workinfo;

import lombok.Data;

/**
 * Work Entity corresponding to d_work table.
 *
 * @author QuynhPP.
 * created on 7/19/2018.
 */
@Data
public class WorkInfo {
    //ID
    private Integer workId;
    //工事番号
    private String workNo;
    //工事名称
    private String workName;
    //説明
    private String description;
    //old ID
    private Integer oldWorkId;
}
