package ilips.domain.model.workinfo;

import java.util.List;

import lombok.Data;

/**
 * Display data of FormWorkInfo.
 *
 * @author QuynhPP.
 * created on 9/17/2018.
 */
@Data
public class WorkInfoDisp {

    private List<WorkInfo> workInfoList; //DES: workData list for displaying on front-end

}
