package ilips.domain.model.workinfo;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper of WorkInfo Entity {@link WorkInfo}.
 *
 * @author QuynhPP.
 * created on 9/17/2018.
 */
public class WorkInfoRowMapper implements RowMapper<WorkInfo> {

    @Override
    public WorkInfo mapRow(ResultSet resultSet, int rowNum) throws SQLException { //DES: get data mapped to database

        WorkInfo workInfo = new WorkInfo();
        workInfo.setWorkId(resultSet.getInt("work_id"));
        workInfo.setWorkNo(resultSet.getString("work_no"));
        workInfo.setWorkName(resultSet.getString("work_name"));
        workInfo.setDescription(resultSet.getString("description"));
        workInfo.setOldWorkId(workInfo.getWorkId());

        return workInfo;
    }
}
