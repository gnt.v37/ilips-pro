package ilips.domain.model.referableorgdata;


import ilips.domain.model.orgdata.OrgData;
import ilips.domain.model.referableorgdata.ReferableOrgData;
import lombok.Data;

import java.util.List;

@Data
public class ReferableOrgDataDisp {

    private List<ReferableOrgData> referableOrgDataList;

}
