package ilips.domain.model.referableorgdata;

import lombok.Data;

import java.util.List;

@Data
public class ReferableOrgDataGridRowCollection {

    private List<ReferableOrgDataGridRow> arrInsertedData;

    private List<ReferableOrgDataGridRow> arrUpdatedData;

    private List<ReferableOrgDataGridRow> arrDeletedData;
}
