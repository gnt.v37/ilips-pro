package ilips.domain.model.referableorgdata;

import lombok.Data;

/**
 * Person Entity corresponding to m_person table.
 *
 * @author ThanhNP4
 * create on 7/19/2018
 */
@Data
public class ReferableOrgData {

    private Integer orgId;

    private Integer referableOrgId;

    private String orgName;

    // Old Key

    private Integer oldOrgId;

    private Integer oldReferableOrgId;


}
