package ilips.domain.model.referableorgdata;

public class ReferableOrgDataGridRow {

    private Integer rowIndex;

    private ReferableOrgData rowData;

    public ReferableOrgData getRowData() {
        return rowData;
    }

    public void setRowData(ReferableOrgData rowData) {
        this.rowData = rowData;
    }

    public Integer getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }
}
