package ilips.domain.model.referableorgdata;

import ilips.domain.model.base.BaseRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper of ReferableOrg Entity {@link ReferableOrgData}.
 */
public class ReferableOrgDataRowMapper extends BaseRowMapper implements RowMapper<ReferableOrgData> {

    @Override
    public ReferableOrgData mapRow(ResultSet row, int rowNum) throws SQLException {

        ReferableOrgData referableOrgData = new ReferableOrgData();
        referableOrgData.setOrgId(row.getInt("org_id"));
        referableOrgData.setReferableOrgId(row.getInt("referable_org_id"));
        referableOrgData.setOrgName(row.getString("org_name"));

        referableOrgData.setOldOrgId(referableOrgData.getOrgId());
        referableOrgData.setOldReferableOrgId(referableOrgData.getReferableOrgId());

        return referableOrgData;

    }
}
