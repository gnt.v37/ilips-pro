package ilips.domain.model.AlarmLevel;

import lombok.Data;

import java.util.List;

/**
 * Row collection of insert, update, delete data
 */
@Data
public class AlarmLevelDataGridRowCollection {
    // Insert list
    private List<AlarmLevelDataGridRow> arrInsertedData;
    // Update list
    private List<AlarmLevelDataGridRow> arrUpdatedData;
    // Delete list
    private List<AlarmLevelDataGridRow> arrDeletedData;
}
