package ilips.domain.model.AlarmLevel;

import lombok.Data;

/**
 * Alarm Level Entity corresponding to m_alarm_level table.
 *
 * @author TuyenND6.
 * created on 7/20/2018.
 */
@Data
public class AlarmLevelData {
    //ＩＤ
    private Integer alarmLevel;
    //アラームレベル名称
    private String alarmLevelName;
    //説明
    private String description;
    //イベント種別
    private Integer eventType;
    //アラーム表示
    private Integer isDisp;
}
