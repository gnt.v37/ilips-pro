package ilips.domain.model.AlarmLevel;

import lombok.Data;

import java.util.List;

/**
 * Display data of FormAlarmLevel.
 *
 * @author TuyenND6.
 * created on 9/20/2018.
 */

@Data
public class AlarmLevelDisp {
    // List alarmLevelData for gird display
    private List<AlarmLevelData> alarmLevelDataList;
}
