package ilips.domain.model.AlarmLevel;

import lombok.Data;

/**
 * Data in Row on AlarmLevel grid.
 *
 * @author TuyenND6.
 * created on 9/20/2018.
 */
@Data
public class AlarmLevelDataGridRow {
    // index of collumn on grid
    private Integer rowIndex;

    private AlarmLevelData rowData;
}
