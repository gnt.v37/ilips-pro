package ilips.domain.model.AlarmLevel;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper of Person Entity {@link AlarmLevelData}.
 *
 * @author TuyenND6.
 * created on 9/20/2018.
 */
public class AlarmLevelDataRowMapper  implements RowMapper<AlarmLevelData> {
        @Override
        public AlarmLevelData mapRow(ResultSet resultSet, int i) throws SQLException {
            AlarmLevelData alarmLevelData = new AlarmLevelData();
            alarmLevelData.setAlarmLevel(resultSet.getInt("alarm_level"));
            alarmLevelData.setAlarmLevelName(resultSet.getString("alarm_level_name"));
            alarmLevelData.setDescription(resultSet.getString("description"));
            alarmLevelData.setEventType(resultSet.getInt("event_type"));
            alarmLevelData.setIsDisp(resultSet.getInt("is_disp"));
            return alarmLevelData;
        }
}
