package ilips.domain.model.reporttemplatedata;

import lombok.Data;

/**
 * Grid row data of FormReportTemplate.
 *
 * @author QuynhPP
 * created on 7/20/2018
 */
@Data
public class ReportTemplateDataGridRow {

    private Integer rowIndex;

    private ReportTemplateData rowData;

}
