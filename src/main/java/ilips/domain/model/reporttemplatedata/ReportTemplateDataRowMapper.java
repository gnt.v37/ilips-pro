package ilips.domain.model.reporttemplatedata;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper of ReportTemplate Entity {@link ReportTemplateData}.
 *
 * @author QuynhPP
 * created on 7/20/2018
 */
public class ReportTemplateDataRowMapper implements RowMapper<ReportTemplateData> {

    @Override
    public ReportTemplateData mapRow(ResultSet resultSet, int rowNum) throws SQLException {

        ReportTemplateData reportTemplateData = new ReportTemplateData();
        reportTemplateData.setMakerOrgId(resultSet.getInt("maker_org_id"));
        reportTemplateData.setModelId(resultSet.getInt("model_id"));
        reportTemplateData.setProductId(resultSet.getInt("product_id"));
        reportTemplateData.setReportTemplateId(resultSet.getInt("report_template_id"));
        reportTemplateData.setReportTemplateName(resultSet.getString("report_template_name"));
        reportTemplateData.setReportTemplateFileName(resultSet.getString("report_template_filename"));
        reportTemplateData.setOldReportTemplateId(reportTemplateData.getReportTemplateId());

        return reportTemplateData;
    }
}
