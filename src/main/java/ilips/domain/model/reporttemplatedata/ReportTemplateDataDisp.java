package ilips.domain.model.reporttemplatedata;

import java.util.List;

import lombok.Data;

/**
 * Display data of FormReportTemplate.
 *
 * @author QuynhPP
 * created on 7/20/2018
 */
@Data
public class ReportTemplateDataDisp {

    private List<ReportTemplateData> reportTemplateDataList;

}
