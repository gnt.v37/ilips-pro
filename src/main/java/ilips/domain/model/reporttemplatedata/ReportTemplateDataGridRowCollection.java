package ilips.domain.model.reporttemplatedata;

import java.util.List;

import lombok.Data;

/**
 * Grid row data collection of FormReportTemplate.
 *
 * @author QuynhPP
 * created on 7/20/2018
 */
@Data
public class ReportTemplateDataGridRowCollection {

    private List<ReportTemplateDataGridRow> arrInsertedData;

    private List<ReportTemplateDataGridRow> arrUpdatedData;

    private List<ReportTemplateDataGridRow> arrDeletedData;
}
