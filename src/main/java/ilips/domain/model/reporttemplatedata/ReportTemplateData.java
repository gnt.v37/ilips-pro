package ilips.domain.model.reporttemplatedata;

import lombok.Data;

/**
 * ReportTemplate Entity corresponding to d_report_template table.
 *
 * @author QuynhPP
 * created on 7/20/2018
 */
@Data
public class ReportTemplateData {

    private Integer makerOrgId; //TODO: is not item

    private Integer modelId;    //TODO: is not item

    private Integer productId;  //TODO: is not item
    //ID
    private Integer reportTemplateId; //TODO: hide this item
    //表示名
    private String reportTemplateName;
    //ファイル名
    private String reportTemplateFileName;

    //Old key
    private Integer oldReportTemplateId;

}
