package ilips.domain.model.orgdata;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * 
 * Class name: Content parent of org
 * 
 * @author ThangNT2 
 * Created Date: Jul 19, 2018
 */
public class ParentDataRowMapper implements RowMapper<ParentOrgData> {

	@Override
	public ParentOrgData mapRow(ResultSet resultSet, int i) throws SQLException {
		ParentOrgData org = new ParentOrgData();
		org.setAttrMaker(resultSet.getInt("attr_maker"));
		org.setAttrCustomer(resultSet.getInt("attr_customer"));
		org.setAttrOwner(resultSet.getInt("attr_owner"));
		org.setAttrMainte(resultSet.getInt("attr_mainte"));
		return org;
	}
}