package ilips.domain.model.orgdata;

import ilips.domain.model.base.BaseRowMapper;
import ilips.util.ILIPSUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper of OrgData Entity {@link OrgData}.
 *
 * @author GiangNPT
 * created on 7/18/2018
 */
public class OrgDataRowMapper extends BaseRowMapper implements RowMapper<OrgData> {
    @Override
    public OrgData mapRow(ResultSet resultSet, int i) throws SQLException {
        OrgData org = new OrgData();
        org.setOrgId(resultSet.getInt("org_id"));
        org.setOrgName(resultSet.getString("org_name"));
        org.setParentId(ILIPSUtil.getNullableInt(resultSet, "parent_id"));
        org.setOrgCode(resultSet.getString("org_code"));
        org.setOrgType(resultSet.getString("org_type"));
        org.setShortName(resultSet.getString("short_name"));
        org.setAddress(resultSet.getString("address"));
        org.setAddition(resultSet.getString("addition"));
        org.setAttrMaker(resultSet.getInt("attr_maker"));
        org.setAttrCustomer(resultSet.getInt("attr_customer"));
        org.setAttrOwner(resultSet.getInt("attr_owner"));
        org.setAttrMainte(resultSet.getInt("attr_mainte"));
        org.setOldOrgId(org.getOrgId());
        org.setOldOrgName(org.getOrgName());
        return org;
    }
}
