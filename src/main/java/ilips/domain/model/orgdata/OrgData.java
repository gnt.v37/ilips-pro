package ilips.domain.model.orgdata;

import lombok.Data;

/**
 * Org Entity corresponding to m_org table.
 *
 * @author GiangNPT
 * created on 7/18/2018
 */
@Data
public class OrgData {
    //ID
    private Integer orgId;
    //組織名称
    private String orgName;
    //親組織
    private Integer parentId;
    //組織コード
    private String orgCode;
    //組織分類
    private String orgType;
    //組織略称
    private String shortName;
    //組織住所
    private String address;
    //組織詳細
    private String addition;
    //メーカ属性
    private Integer attrMaker;
    //お客さま属性
    private Integer attrCustomer;
    //所有組織属性
    private Integer attrOwner;
    //メンテ組織属性
    private Integer attrMainte;
    
    // Old key
    private Integer oldOrgId;
    
    // Old org name
    private String oldOrgName;
}
