package ilips.domain.model.orgdata;

import ilips.domain.model.persondata.PersonData;
import ilips.domain.model.systempermission.SystemPermission;
import lombok.Data;

import java.util.List;
@Data
public class OrgDataDisp {
    private List<OrgData> orgDataList;

    public List<OrgData> getOrgDataList() {
        return orgDataList;
    }
    public void setPersonDataList(List<OrgData> personDataList) {
        this.orgDataList = orgDataList;
    }
}
