package ilips.domain.model.orgdata;

import lombok.Data;

/**
 * 
 * Class name:   Parent data
 * 
 * @author ThangNT2
 * Created Date: Jul 20, 2018
 */
@Data
public class ParentOrgData {

	//メーカ属性
    private int attrMaker;
    //お客さま属性
    private int attrCustomer;
    //所有組織属性
    private int attrOwner;
    //メンテ組織属性
    private int attrMainte;
}
