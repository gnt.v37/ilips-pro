package ilips.domain.model.orgdata;

import java.util.List;

import lombok.Data;

/**
 * 
 * Class name:   Colection
 * 
 * @author ThangNT2
 * Created Date: Jul 20, 2018
 */
@Data
public class OrgDataGridRowCollection {

	/** select parent id */
	private Integer selectParentId;
	
	/** insert list */
    private List<OrgDataGridRow> arrInsertedOrgData;

    /** update list */
    private List<OrgDataGridRow> arrUpdatedOrgData;

    /** delete list */
    private List<OrgDataGridRow> arrDeletedOrgData;
}
