package ilips.domain.model.orgdata;

import lombok.Data;

/**
 * 
 * Class name:   Selecting row
 * 
 * @author ThangNT2
 * Created Date: Jul 20, 2018
 */
@Data
public class OrgDataGridRow {

	/** row index */
    private Integer rowIndex;

    /** data */
    private OrgData rowData;
}
