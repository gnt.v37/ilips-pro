package ilips.domain.model.modeldata;

import lombok.Data;

/**
 * Model Data Entity corresponding to m_model table.
 *
 * @author GiangNPT
 * created on 7/18/2018
 */
@Data
public class ModelData {
    //メーカID
    private Integer makerOrgId;
    //ID
    private Integer modelId;
    //型式名称
    private String modelName;
    //説明
    private String description;
}
