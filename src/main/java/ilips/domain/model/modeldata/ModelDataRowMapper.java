package ilips.domain.model.modeldata;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper of Model Data Entity {@link ModelData}.
 *
 * @author GiangNPT
 * created on 7/18/2018
 */
public class ModelDataRowMapper implements RowMapper<ModelData> {
    @Override
    public ModelData mapRow(ResultSet resultSet, int i) throws SQLException {
        ModelData modelData = new ModelData();
        modelData.setMakerOrgId(resultSet.getInt("maker_org_id"));
        modelData.setModelId(resultSet.getInt("model_id"));
        modelData.setModelName(resultSet.getString("model_name"));
        modelData.setDescription(resultSet.getString("description"));

        return modelData;
    }
}
