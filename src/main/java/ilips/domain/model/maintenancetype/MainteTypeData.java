package ilips.domain.model.maintenancetype;

import lombok.Data;

/**
 * class Mainte Type Data Repository.
 * @author PhuongVV
 * created on 7/19/2018
 */
@Data
public class MainteTypeData {
    //Maker Org ID
    private Integer makerOrgId;

    //Model ID
    private Integer modelId;

    //Maintenance Type
    private Integer mainteType;

    //used to store old variables Maintenance Type
    private Integer oldMainteType;

    //Maintenance Type Name
    private String mainteTypeName;
}
