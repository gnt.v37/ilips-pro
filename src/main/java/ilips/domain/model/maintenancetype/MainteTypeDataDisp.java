package ilips.domain.model.maintenancetype;

import java.util.List;

/**
 * class Mainte Type Data Repository
 * @author PhuongVV
 * created on 7/18/2018
 */
public class MainteTypeDataDisp {

    //List Maintenance Type Data
    private  List<MainteTypeData> mainteTypeDataList;

    /**
     * Get list Maintenance Type Data.
     *
     * @return List Maintenance Type Data.
     */
    public List<MainteTypeData> getMainteTypeDataList() {
        return mainteTypeDataList;
    }

    /**
     * Set list Maintenance Type Data.
     *
     * @param mainteTypeDataList
     */
    public void setMainteTypeDataList(List<MainteTypeData> mainteTypeDataList) {
        this.mainteTypeDataList = mainteTypeDataList;
    }
}
