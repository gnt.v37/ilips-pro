package ilips.domain.model.maintenancetype;

import java.util.List;
/**
 * class Mainte Type Data Repository
 * @author PhuongVV
 * created on 7/18/2018
 */
public class MainteTypeDataGridRowCollection {

    //List Insert Data
    private List<MainteTypeDataGridRow> arrInsertedData;

    //List Update Data
    private List<MainteTypeDataGridRow> arrUpdatedData;

    //List Delete Data
    private List<MainteTypeDataGridRow> arrDeletedData;

    /**
     * Get list Insert Data.
     *
     * @return List Insert Data.
     */
    public List<MainteTypeDataGridRow> getArrInsertedData() {
        return arrInsertedData;
    }

    /**
     * Set list Insert Data.
     *
     * @param arrInsertedData
     */
    public void setArrInsertedData(List<MainteTypeDataGridRow> arrInsertedData) {
        this.arrInsertedData = arrInsertedData;
    }

    /**
     * Get List Update Data.
     *
     * @return List Update Data.
     */
    public List<MainteTypeDataGridRow> getArrUpdatedData() {
        return arrUpdatedData;
    }

    /**
     * Set List Update Data.
     *
     * @param arrUpdatedData
     */
    public void setArrUpdatedData(List<MainteTypeDataGridRow> arrUpdatedData) {
        this.arrUpdatedData = arrUpdatedData;
    }

    /**
     * Get List Delete Data.
     *
     * @return List Delete Data.
     */
    public List<MainteTypeDataGridRow> getArrDeletedData() {
        return arrDeletedData;
    }

    /**
     * Set List Delete Data.
     *
     * @param arrDeletedData
     */
    public void setArrDeletedData(List<MainteTypeDataGridRow> arrDeletedData) {
        this.arrDeletedData = arrDeletedData;
    }
}
