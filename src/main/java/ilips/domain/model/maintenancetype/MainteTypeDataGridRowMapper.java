package ilips.domain.model.maintenancetype;

import ilips.domain.model.base.BaseRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * class Mainte Type Data Repository
 * @author PhuongVV
 * created on 7/19/2018
 */
public class MainteTypeDataGridRowMapper extends BaseRowMapper implements RowMapper<MainteTypeData> {

    /**
     * Get value from DB
     * Assign values to fields in the object
     *
     * @param rs
     * @param rowNum
     *
     * @return object
     *
     * @throws SQLException
     */
    @Override
    public MainteTypeData mapRow(ResultSet rs, int rowNum) throws SQLException {
        MainteTypeData mainteTypeData = new MainteTypeData();
        mainteTypeData.setMakerOrgId(rs.getInt("maker_org_id"));
        mainteTypeData.setModelId(rs.getInt("model_id"));
        mainteTypeData.setMainteType(rs.getInt("mainte_type"));
        mainteTypeData.setOldMainteType(rs.getInt("mainte_type"));
        mainteTypeData.setMainteTypeName(rs.getNString("mainte_type_name"));
        return mainteTypeData;
    }
}
