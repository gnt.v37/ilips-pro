package ilips.domain.model.maintenancetype;

/**
 * class Mainte Type Data Repository
 * @author PhuongVV
 * created on 7/18/2018
 */
public class MainteTypeDataGridRow {
    //Row Index
    private Integer rowIndex;

    //Row Data
    private MainteTypeData rowData;

    /**
     *  Get Row Index.
     *
     * @return Row Index.
     */
    public Integer getRowIndex() {
        return rowIndex;
    }

    /**
     * Set Row Index.
     *
     * @param rowIndex
     */
    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }

    /**
     * Get object Maintenance Type Data.
     *
     * @return object Maintenance Type Data.
     */
    public MainteTypeData getRowData() {
        return rowData;
    }

    /**
     * Set object Maintenance Type Data.
     *
     * @param rowData
     */
    public void setRowData(MainteTypeData rowData) {
        this.rowData = rowData;
    }
}
