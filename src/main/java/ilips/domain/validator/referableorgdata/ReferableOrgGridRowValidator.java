package ilips.domain.validator.referableorgdata;

import ilips.domain.model.referableorgdata.ReferableOrgData;
import ilips.domain.model.referableorgdata.ReferableOrgDataGridRow;
import ilips.domain.model.referableorgdata.ReferableOrgDataGridRowCollection;
import ilips.domain.repository.referableorgdata.ReferableOrgDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Referableorg data grid row vaidator
 *
 * @author ThanhNP4
 * created on 7/20/2018
 */
@Service
public class ReferableOrgGridRowValidator {

    private List<String> rowErrCodeList;

    private Map<Integer, List<String>> errCodeList;

    private final ReferableOrgDataRepository referableOrgDataRepository;

    /**
     * Class contain ReferableOrg data key
     *
     * @author ThanhNP4
     * created on 7/20/2018
     */
    public static class ReferableOrgDataKey {

        private int orgId;

        private int referableOrgId;

        public ReferableOrgDataKey(int orgId, int referableOrgId) {
            this.orgId = orgId;
            this.referableOrgId = referableOrgId;
        }

        @Override
        public int hashCode() {
            int hash = 1;
            hash = hash * 31 + orgId;
            hash = hash * 31 + referableOrgId;

            return hash;
        }

        public int getOrgId() {
            return orgId;
        }

        public int getReferableOrgId() {
            return referableOrgId;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (!(obj instanceof ReferableOrgDataKey)) {
                return false;
            } else if (obj == this) {
                return true;
            }

            ReferableOrgDataKey referableOrgDataKey = (ReferableOrgDataKey) obj;

            return this.getOrgId() == referableOrgDataKey.getOrgId() &&
                    this.getReferableOrgId() == referableOrgDataKey.getReferableOrgId();
        }
    }

    /**
     * Validate grid row data
     *
     * @param referableOrgDataRepository
     * @return error code list
     */
    @Autowired
    public ReferableOrgGridRowValidator(ReferableOrgDataRepository referableOrgDataRepository) {
        this.referableOrgDataRepository = referableOrgDataRepository;
    }

    public Map<Integer, List<String>> validateGridRowData(ReferableOrgDataGridRowCollection referableOrgDataGridRowCollection){

        errCodeList = new HashMap<>();

        List<ReferableOrgDataKey> personDataKeys = getValidKey(referableOrgDataGridRowCollection);

        validateInsertAndUpdateData(referableOrgDataGridRowCollection.getArrInsertedData(), personDataKeys);
        validateInsertAndUpdateData(referableOrgDataGridRowCollection.getArrUpdatedData(), personDataKeys);

        return errCodeList;
    }

    /**
     * Get valid key after update or delete.
     *
     * @param referableOrgDataGridRowCollection data update, insert, delete.
     * @return valid key after update or delete.
     */
    private List<ReferableOrgDataKey> getValidKey(ReferableOrgDataGridRowCollection referableOrgDataGridRowCollection) {
        List<ReferableOrgDataKey> referableOrgDataKeys = new ArrayList<>();

        for (ReferableOrgDataGridRow referableOrgDataGridRow : referableOrgDataGridRowCollection.getArrUpdatedData()) {
            ReferableOrgData referableOrgData = referableOrgDataGridRow.getRowData();
            if (referableOrgData.getOrgId().intValue() != referableOrgData.getOldOrgId()
                    || referableOrgData.getReferableOrgId().intValue() != referableOrgData.getOldReferableOrgId()) {
                //update key
                referableOrgDataKeys.add(new ReferableOrgDataKey(referableOrgData.getOldOrgId(), referableOrgData.getOldReferableOrgId()));
            }
        }

        return referableOrgDataKeys;
    }

    /**
     * Validate insert or update data
     *
     * @param insertDataList insert or update data list
     * @param validKey valid key
     */
    private void validateInsertAndUpdateData(List<ReferableOrgDataGridRow> insertDataList,
                                         List<ReferableOrgDataKey> validKey) {

        for (ReferableOrgDataGridRow referableOrgDataGridRow : insertDataList) {

            rowErrCodeList = new ArrayList<>();

            int rowIndex = referableOrgDataGridRow.getRowIndex();

            if (rowErrCodeList.size() > 0) {
                errCodeList.put(rowIndex, rowErrCodeList);
            }
        }
    }

}
