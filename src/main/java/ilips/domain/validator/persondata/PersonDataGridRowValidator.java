package ilips.domain.validator.persondata;

import ilips.constant.MsgConst;
import ilips.domain.model.persondata.PersonData;
import ilips.domain.model.persondata.PersonDataGridRow;
import ilips.domain.model.persondata.PersonDataGridRowCollection;
import ilips.domain.service.message.MessageService;
import ilips.domain.service.persondata.PersonDataService;
import ilips.util.common.CommonUtilEx;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Person Data Validator.
 *
 * @author GiangNPT
 * created on 7/20/2018
 */
public class PersonDataGridRowValidator {

    private static final int MAX_LENGTH_PERSON_NAME = 400;

    private static final int MIN_LENGTH_LOGIN_ID = 4;

    private static final int MAX_LENGTH_LOGIN_ID = 63;

    private static final int MAX_LENGTH_PASSWORD = 400;

    private static final int MAX_LENGTH_ADDR = 400;

    private static final int MAX_LENGTH_TEL = 400;

    private static final int MAX_LENGTH_FAX = 400;

    private static final int MAX_LENGTH_EMAIL = 400;

    private final PersonDataService personDataService;

    private final MessageService messageService;

    private List<String> rowErrCodeList;

    private Map<Integer, String> errList;

    private List<PersonDataKey> updatedKey;

    private List<PersonDataKey> deletedKey;

    private PersonDataGridRowCollection personDataGridRowCollection;

    /**
     * Class contain person data key.
     *
     * @author GiangNPT
     * created on 7/18/2018
     */
    static class PersonDataKey {

        private int personId;

        private int orgId;

        PersonDataKey(int personId, int orgId) {
            this.personId = personId;
            this.orgId = orgId;
        }

        @Override
        public int hashCode() {
            int hash = 1;
            hash = hash * 31 + personId;
            hash = hash * 31 + orgId;
            return hash;
        }

        int getPersonId() {
            return personId;
        }

        public int getOrgId() {
            return orgId;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (!(obj instanceof PersonDataKey)) {
                return false;
            } else if (obj == this) {
                return true;
            }

            PersonDataKey personDataKey = (PersonDataKey) obj;

            return this.getPersonId() == personDataKey.getPersonId() &&
                    this.getOrgId() == personDataKey.getOrgId();
        }
    }

    public PersonDataGridRowValidator(PersonDataGridRowCollection personDataGridRowCollection,
                                      PersonDataService personDataService,
                                      MessageService messageService) {
        this.personDataGridRowCollection = personDataGridRowCollection;
        this.personDataService = personDataService;
        this.messageService = messageService;
    }

    /**
     * Valid row data.
     *
     * @return list of error.
     */
    public Map<Integer, String> validateGridRowData() {

        errList = new HashMap<>();
        getValidKey();

        validateInsertAndUpdateData(personDataGridRowCollection.getArrInsertedData(), true);
        validateInsertAndUpdateData(personDataGridRowCollection.getArrUpdatedData(), false);
        validateDeleteData(personDataGridRowCollection.getArrDeletedData());

        return errList;
    }

    /**
     * Get valid key after update or delete.
     */
    private void getValidKey() {
        updatedKey = new ArrayList<>();
        deletedKey = new ArrayList<>();
        for (PersonDataGridRow personDataGridRow : personDataGridRowCollection.getArrDeletedData()) {
            //If delete a row then the delete row key will be valid to insert or update
            PersonData personData = personDataGridRow.getRowData();
            deletedKey.add(new PersonDataKey(personData.getOldPersonId(), personData.getOldOrgId()));
        }

        for (PersonDataGridRow personDataGridRow : personDataGridRowCollection.getArrUpdatedData()) {
            PersonData personData = personDataGridRow.getRowData();
            if (personData.getPersonId().intValue() != personData.getOldPersonId()
                    || personData.getOldOrgId().intValue() != personData.getOldOrgId()) {
                //If update key of a row then the old row key will be valid to insert or update
                updatedKey.add(new PersonDataKey(personData.getOldPersonId(), personData.getOldOrgId()));
            }
        }
    }

    private void validateInsertAndUpdateData(List<PersonDataGridRow> insertDataList, boolean isInsert) {

        for (PersonDataGridRow personDataGridRow : insertDataList) {

            rowErrCodeList = new ArrayList<>();

            int rowNum = personDataGridRow.getRowNum();
            PersonData rowData = personDataGridRow.getRowData();

            Integer personId = rowData.getPersonId();
            checkPersonId(personId);

            String personName = rowData.getPersonName();
            checkPersonName(personName);

            Integer permission = rowData.getPermission();
            checkPermission(permission);

            String loginId = rowData.getLoginId();
            checkLoginId(loginId);

            String password = rowData.getPassword();
            checkPassword(password);

            String addr = rowData.getAddr();
            checkAddr(addr);

            String tel = rowData.getTel();
            checkTel(tel);

            String fax = rowData.getFax();
            checkFax(fax);

            String email = rowData.getEmail();
            checkEmail(email);

            if (isInsert) {
                if (personDataService.isExistKey(rowData.getPersonId(), rowData.getOrgId())
                        && !deletedKey.contains(new PersonDataKey(rowData.getPersonId(), rowData.getOrgId()))
                        && !updatedKey.contains(new PersonDataKey(rowData.getPersonId(), rowData.getOrgId()))) {
                    //if insert
                    rowErrCodeList.add(messageService.text(MsgConst.ERR_EM151, StringUtils.EMPTY));
                }
            } else {
                if ((rowData.getOldPersonId().intValue() != rowData.getPersonId()
                        || rowData.getOldOrgId().intValue() != rowData.getOrgId())) {
                    //if update key the check new key is ok or not?
                    if (!personDataService.isExistKey(rowData.getOldPersonId(), rowData.getOldOrgId())) {
                        //if update a row is not in db
                        rowErrCodeList.add(messageService.text(MsgConst.ERR_EM155,
                                String.valueOf(rowData.getPersonId()), rowData.getPersonName()));
                    } else if (personDataService.isExistKey(rowData.getPersonId(), rowData.getOrgId())
                            && !deletedKey.contains(new PersonDataKey(rowData.getPersonId(), rowData.getOrgId()))) {
                        //if new key is not valid
                        rowErrCodeList.add(messageService.text(MsgConst.ERR_EM151, StringUtils.EMPTY));
                    }
                }
            }

            if (rowErrCodeList.size() > 0) {
                errList.put(rowNum, convertErrCodeList(rowErrCodeList));
            }
        }
    }

    private void validateDeleteData(List<PersonDataGridRow> deleteDataList) {

        for (PersonDataGridRow rowDto : deleteDataList) {

            rowErrCodeList = new ArrayList<>();

            Integer rowNum = rowDto.getRowNum();
            PersonData rowData = rowDto.getRowData();

            Integer personId = rowData.getPersonId();
            Integer orgId = rowData.getOrgId();

            if (!personDataService.isExistKey(personId, orgId)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM155,
                        String.valueOf(rowData.getPersonId()), rowData.getPersonName()));
            }

            if (rowErrCodeList.size() > 0) {
                errList.put(rowNum, convertErrCodeList(rowErrCodeList));
            }
        }
    }

    private void checkEmail(String email) {
        if (!CommonUtilEx.isEmpty(email)) {
            if (!CommonUtilEx.validateMaxLength(email, MAX_LENGTH_EMAIL)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM235, MsgConst.WORD_EMAIL, String.valueOf(MAX_LENGTH_EMAIL)));
            }

            int res = CommonUtilEx.validateEmail(email);
            if (res == CommonUtilEx.EMAIL_NO_ATMARK) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM192, StringUtils.EMPTY));

            } else if (res == CommonUtilEx.EMAIL_EXCESS_ATMARK) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM193, StringUtils.EMPTY));

            } else if (res == CommonUtilEx.EMAIL_SYNTAX_ERR_ATMARK) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM236, StringUtils.EMPTY));

            } else if (res == CommonUtilEx.EMAIL_NOT_MATCH_REGEX) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM189, StringUtils.EMPTY));

            } else if (res == CommonUtilEx.EMAIL_SEQUENT_PERIOD) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM191, StringUtils.EMPTY));

            } else if (res == CommonUtilEx.EMAIL_SYNTAX_ERR_PERIOD) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM190, StringUtils.EMPTY));
            }
        }
    }

    private void checkFax(String fax) {
        if (!CommonUtilEx.isEmpty(fax)) {
            if (!CommonUtilEx.validateMaxLength(fax, MAX_LENGTH_FAX)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM235, MsgConst.WORD_FAX, String.valueOf(MAX_LENGTH_FAX)));
            }
        }
    }

    private void checkTel(String tel) {
        if (!CommonUtilEx.isEmpty(tel)) {
            if (!CommonUtilEx.validateMaxLength(tel, MAX_LENGTH_TEL)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM235, MsgConst.WORD_TEL, String.valueOf(MAX_LENGTH_TEL)));
            }
        }
    }

    private void checkAddr(String addr) {
        if (!CommonUtilEx.isEmpty(addr)) {
            if (!CommonUtilEx.validateMaxLength(addr, MAX_LENGTH_ADDR)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM235, MsgConst.WORD_ADDR, String.valueOf(MAX_LENGTH_ADDR)));
            }
        }
    }

    private void checkPassword(String password) {
        if (!CommonUtilEx.isEmpty(password)) {
            if (!CommonUtilEx.validateMaxLength(password, MAX_LENGTH_PASSWORD)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM235, MsgConst.WORD_PASSWORD, String.valueOf(MAX_LENGTH_PASSWORD)));
            }
        }
    }

    private void checkPermission(Integer permission) {
        if (CommonUtilEx.isNull(permission)) {
            rowErrCodeList.add(messageService.text(MsgConst.ERR_EM150));
        }
    }

    private void checkPersonId(Integer personId) {
        if (CommonUtilEx.isNull(personId)) {
            rowErrCodeList.add(messageService.text(MsgConst.ERR_EM150));
        }
    }

    private void checkPersonName(String personName) {

        if (CommonUtilEx.isEmpty(personName)) {
            rowErrCodeList.add(messageService.text(MsgConst.ERR_EM150));
        } else {
            if (!CommonUtilEx.validateMaxLength(personName, MAX_LENGTH_PERSON_NAME)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM235, MsgConst.WORD_PERSON_NAME, String.valueOf(MAX_LENGTH_PERSON_NAME)));
            }
        }
    }

    private void checkLoginId(String loginId) {

        if (!CommonUtilEx.isEmpty(loginId)) {
            if (!CommonUtilEx.validateLength(loginId, MIN_LENGTH_LOGIN_ID, MAX_LENGTH_LOGIN_ID)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM168, StringUtils.EMPTY));
            }

            int res = CommonUtilEx.validateLoginIdUsable(loginId);
            if (res == CommonUtilEx.LOGIN_ID_SEQUENT_PERIOD) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM171, StringUtils.EMPTY));
            } else if (res == CommonUtilEx.LOGIN_ID_SYNTAX_ERR) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM170, StringUtils.EMPTY));
            } else if (res == CommonUtilEx.LOGIN_ID_NOT_MATCH_REGEX) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM169, StringUtils.EMPTY));
            }
        }
    }


    /**
     * Convert Err codes to Err messages
     *
     * @param errList error List.
     * @return Error messages
     */
    private String convertErrCodeList(List<String> errList) {
        StringBuilder errMsg = new StringBuilder();

        // reverse err code list
        for (Iterator<String> idx = errList.iterator(); idx.hasNext(); ) {
            String errMessage = idx.next();

            // convert to err message
            errMsg.append(errMessage);
            if (idx.hasNext()) {
                errMsg.append(", ");
            }
        }
        return errMsg.toString();
    }
}
