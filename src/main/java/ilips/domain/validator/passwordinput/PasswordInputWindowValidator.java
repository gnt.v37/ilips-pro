package ilips.domain.validator.passwordinput;
import ilips.domain.model.passwordinput.PasswordInputWindow;
import ilips.util.ILIPSUtil;
import org.springframework.stereotype.Service;

/**
 * Class name: Validate for  PasswordInputWindow
 *
 * @author LoanNT13
 * Created Date: Jul 20, 2018
 */
@Service
public class PasswordInputWindowValidator {

    private PasswordInputWindow passwordInputWindow;

    public String getSHA256Password() {
        return ILIPSUtil.passwordEncrypt(this.passwordInputWindow.getPassword());
    }

    public PasswordInputWindow getPasswordInputWindow() {
        return passwordInputWindow;
    }

    public void setPasswordInputWindow(PasswordInputWindow passwordInputWindow) {
        this.passwordInputWindow = passwordInputWindow;
    }
}
