package ilips.domain.validator.passwordinput;

public class PasswordInputWindowException extends RuntimeException {

    private String message;

    public PasswordInputWindowException(String message) {
        super(message);
    }
}

