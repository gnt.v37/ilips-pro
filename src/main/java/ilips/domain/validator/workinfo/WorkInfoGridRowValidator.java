package ilips.domain.validator.workinfo;

import ilips.constant.MsgConst;
import ilips.domain.model.workinfo.WorkInfo;
import ilips.domain.model.workinfo.WorkInfoGridRow;
import ilips.domain.model.workinfo.WorkInfoGridRowCollection;
import ilips.domain.service.message.MessageService;
import ilips.domain.service.workinfo.WorkInfoService;
import ilips.util.common.CommonUtilEx;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Work data grid row validator.
 *
 * @author QuynhPP .
 * created on 9/17/2018.
 */
public class WorkInfoGridRowValidator {

    private static final int MAX_LENGTH_WORK_NUMBER = 400;

    private static final int MAX_LENGTH_WORK_NAME = 400;

    private static final int MAX_LENGTH_DESCRIPTION = 400;

    private final WorkInfoService workInfoService;

    private final MessageService messageService;

    private List<String> rowErrCodeList;

    private Map<Integer, String> errList;

    private List<WorkDataKey> updatedKeys;

    private List<WorkDataKey> deletedKeys;

    private WorkInfoGridRowCollection workInfoGridRowCollection;

    /**
     * Class contains work data key
     *
     * @author QuynhPP
     * created on 7/19/2018
     */
    @Data
    public static class WorkDataKey {

        //ID
        private int workId;

        public WorkDataKey(int workId) {
            this.workId = workId;
        }

        @Override
        public int hashCode() {
            int hash = 1;
            hash = hash * 31 + workId;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (null == obj) {
                return false;
            }
            if (!(obj instanceof WorkDataKey)) {
                return false;
            } else if (obj == this) {
                return true;
            }

            WorkDataKey workDataKey = (WorkDataKey) obj;

            return (this.getWorkId() == workDataKey.getWorkId());
        }
    }

    public WorkInfoGridRowValidator(WorkInfoGridRowCollection workInfoGridRowCollection,
                                    WorkInfoService workInfoService,
                                    MessageService messageService) {
        this.workInfoGridRowCollection = workInfoGridRowCollection;
        this.workInfoService = workInfoService;
        this.messageService = messageService;
    }

    /**
     * Validate grid row data
     *
     * @param
     * @return error code list
     */
    public Map<Integer, String> validateGridRowData() {

        errList = new HashMap<>();
        getValidKey();

        validateInsertOrUpdateData(workInfoGridRowCollection.getArrInsertedData(), true);
        validateInsertOrUpdateData(workInfoGridRowCollection.getArrUpdatedData(), false);
        validateDeleteData(workInfoGridRowCollection.getArrDeletedData());

        return errList;
    }

    /**
     * Get valid key before update or delete.
     */
    private void getValidKey() {
        deletedKeys = new ArrayList<>();
        for (WorkInfoGridRow workInfoGridRow : workInfoGridRowCollection.getArrDeletedData()) {
            //If delete a row then the delete row key will be valid to insert or update
            WorkInfo workInfo = workInfoGridRow.getRowData();
            deletedKeys.add(new WorkDataKey(workInfo.getOldWorkId()));
        }
        updatedKeys = new ArrayList<>();
        for (WorkInfoGridRow workInfoGridRow : workInfoGridRowCollection.getArrUpdatedData()) {
            WorkInfo workInfo = workInfoGridRow.getRowData();
            if (workInfo.getWorkId().intValue() != workInfo.getOldWorkId()) {
                //If update key of a row then the old row key will be valid to insert or update
                updatedKeys.add(new WorkDataKey(workInfo.getOldWorkId()));
            }
        }
    }

    /**
     * Validate insert or update data.
     *
     * @param dataList insert or update data list.
     * @param isInsert check insert or update.
     */
    private void validateInsertOrUpdateData(List<WorkInfoGridRow> dataList, boolean isInsert) {

        for (WorkInfoGridRow workInfoGridRow : dataList) {

            rowErrCodeList = new ArrayList<>();

            int rowIndex = workInfoGridRow.getRowIndex();

            WorkInfo rowData = workInfoGridRow.getRowData();

            checkWorkId(rowData);

            checkWorkNo(rowData);

            checkWorkName(rowData);

            checkDescription(rowData);

            if (isInsert) {
                if (workInfoService.isExistKey(rowData.getWorkId())
                        && !deletedKeys.contains(new WorkDataKey(rowData.getWorkId()))
                        && !updatedKeys.contains(new WorkDataKey(rowData.getWorkId()))) {
                    //if insert
                    rowErrCodeList.add(messageService.text(MsgConst.ERR_EM151, StringUtils.EMPTY));
                }
            } else {
                if (rowData.getOldWorkId().intValue() != rowData.getWorkId()) {
                    //if update key the check new key is ok or not?
                    if (!workInfoService.isExistKey(rowData.getOldWorkId())) {
                        //if update a row is not in db
                        rowErrCodeList.add(messageService.text(MsgConst.ERR_EM155, StringUtils.EMPTY));
                    } else if (workInfoService.isExistKey(rowData.getWorkId())
                            && !deletedKeys.contains(new WorkDataKey(rowData.getWorkId()))) {
                        //if new key is not valid
                        rowErrCodeList.add(messageService.text(MsgConst.ERR_EM151, StringUtils.EMPTY));
                    }
                }
            }

            if (rowErrCodeList.size() > 0) {
                errList.put(rowIndex, convertErrCodeList(rowErrCodeList));
            }
        }
    }

    /**
     * Validate delete data.
     *
     * @param deleteDataList
     */
    private void validateDeleteData(List<WorkInfoGridRow> deleteDataList) {

        for (WorkInfoGridRow deleteRow : deleteDataList) {

            rowErrCodeList = new ArrayList<>();
            Integer rowIndex = deleteRow.getRowIndex();
            WorkInfo rowData = deleteRow.getRowData();
            Integer workId = rowData.getWorkId();

            if (!workInfoService.isExistKey(workId)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM155, StringUtils.EMPTY));
            }

            if (rowErrCodeList.size() > 0) {
                errList.put(rowIndex, convertErrCodeList(rowErrCodeList));
            }
        }
    }

    /**
     * Check referenced data.
     *
     * @return warning code list.
     */
    public Map<Integer, String> checkReferencedData() {

        errList = new HashMap<>();

        for (WorkInfoGridRow deleteRow : workInfoGridRowCollection.getArrDeletedData()) {

            rowErrCodeList = new ArrayList<>();
            Integer rowIndex = deleteRow.getRowIndex();
            WorkInfo rowData = deleteRow.getRowData();
            Integer workId = rowData.getWorkId();

            if (!workInfoService.isDataReferenced(workId)) {
                rowErrCodeList.add(messageService.text(MsgConst.WRN_WN004, StringUtils.EMPTY));
            }

            if (rowErrCodeList.size() > 0) {
                errList.put(rowIndex, convertErrCodeList(rowErrCodeList));
            }
        }
        return errList;  //FIXME: should use warnList?
    }

    /**
     * Check work id data.
     *
     * @param workInfo work data to check workId.
     */
    private void checkWorkId(WorkInfo workInfo) {
        Integer workId = workInfo.getWorkId();
        //Check null
        if (CommonUtilEx.isNull(workId)) {
            rowErrCodeList.add(messageService.text(MsgConst.ERR_EM150, StringUtils.EMPTY));
        }
    }

    /**
     * Check work number data.
     *
     * @param workInfo work data to check work number.
     */
    private void checkWorkNo(WorkInfo workInfo) {
        String workNo = workInfo.getWorkNo();
        if (CommonUtilEx.isEmpty(workNo)) {
            rowErrCodeList.add(messageService.text(MsgConst.ERR_EM150, StringUtils.EMPTY));
        } else {
            if (!CommonUtilEx.validateMaxLength(workNo, MAX_LENGTH_WORK_NUMBER)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM234
                        , String.valueOf(MAX_LENGTH_WORK_NUMBER))); //TODO: correct message. PIC: TRINHNTM1
            }
        }
    }

    /**
     * Check work name data.
     *
     * @param workInfo work data to check work name.
     */
    private void checkWorkName(WorkInfo workInfo) {
        String workName = workInfo.getWorkName();
        if (CommonUtilEx.isEmpty(workName)) {
            rowErrCodeList.add(messageService.text(MsgConst.ERR_EM150, StringUtils.EMPTY));
        } else {
            if (!CommonUtilEx.validateMaxLength(workName, MAX_LENGTH_WORK_NAME)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM234
                        , String.valueOf(MAX_LENGTH_WORK_NAME))); //TODO: correct message. PIC: TRINHNTM1
            }
        }
    }

    /**
     * Check description data.
     *
     * @param workInfo work data to check description.
     */
    private void checkDescription(WorkInfo workInfo) {
        String description = workInfo.getDescription();
        if (!CommonUtilEx.isEmpty(description)) {
            if (!CommonUtilEx.validateMaxLength(description, MAX_LENGTH_DESCRIPTION)) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM234
                        , String.valueOf(MAX_LENGTH_DESCRIPTION))); //TODO: correct message. PIC: TRINHNTM1
            }
        }
    }

    /**
     * Convert Err codes to Err messages
     *
     * @param errList error List.
     * @return Error messages
     */
    private String convertErrCodeList(List<String> errList) {
        StringBuilder errMsg = new StringBuilder();

        // reverse err code list
        for (Iterator<String> idx = errList.iterator(); idx.hasNext(); ) {
            String errMessage = idx.next();

            // convert to err message
            errMsg.append(errMessage);
            if (idx.hasNext()) {
                errMsg.append(", ");
            }
        }
        return errMsg.toString();
    }
}
