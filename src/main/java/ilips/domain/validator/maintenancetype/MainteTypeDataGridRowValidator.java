package ilips.domain.validator.maintenancetype;

import ilips.domain.model.maintenancetype.MainteTypeData;
import ilips.domain.model.maintenancetype.MainteTypeDataGridRow;
import ilips.domain.model.maintenancetype.MainteTypeDataGridRowCollection;
import ilips.domain.repository.maintenancetype.MainteTypeDataRepository;
import ilips.domain.service.maintenancetype.MainteTypeService;
import ilips.domain.service.message.MessageService;
import ilips.util.common.CommonUtilEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import ilips.constant.MsgConst;


/**
 * class Mainte Type Data Repository.
 *
 * @author PhuongVV
 * created on 7/18/2018
 */
public class MainteTypeDataGridRowValidator {

    // List Row error code
    private List<String> rowErrCodeList;

    // List error code
    private Map<Integer, String> errCodeList;

    //Max length maintenance type name
    private static final int MAX_LENGTH_MAINTE_TYPE_NAME = 101;

    //Min length maintenance type
    private static final int MIN_LENGTH_MAINTE_TYPE = 0;

    //Max length maintenance type
    private static final int MAX_LENGTH_MAINTE_TYPE = 999;

    private final MainteTypeService mainteTypeService;

    private List<MainteTypeDataKey> updateKeyList;

    private List<MainteTypeDataKey> deleteKeyList;

    private MainteTypeDataGridRowCollection mainteTypeDataGridRowCollection;

    private final MessageService messageService;

    public MainteTypeDataGridRowValidator(MainteTypeService mainteTypeService,
                                          MainteTypeDataGridRowCollection mainteTypeDataGridRowCollection,
                                          MessageService messageService) {
        this.mainteTypeService = mainteTypeService;
        this.mainteTypeDataGridRowCollection = mainteTypeDataGridRowCollection;
        this.messageService = messageService;
    }

    /**
     * Check Maintenance Type.
     *
     * @param mainteType
     */
    private void checkMainteType(Integer mainteType) {
        if (CommonUtilEx.isNull(mainteType)) {
            rowErrCodeList.add(MsgConst.ERR_EM150);
        }
    }

    /**
     * Check range Maintenance Type.
     *
     * @param mainteType
     */
    private void checkRangeMainteType(Integer mainteType) {
        if (MIN_LENGTH_MAINTE_TYPE > mainteType || mainteType > MAX_LENGTH_MAINTE_TYPE) {
            rowErrCodeList.add(MsgConst.ERR_EM166);
        }
    }

    /**
     * Check Maintenance Type Name.
     *
     * @param mainteTypeName
     */
    private void checkMainteTypeName(String mainteTypeName) {
        if (CommonUtilEx.isEmpty(mainteTypeName)) {
            rowErrCodeList.add(MsgConst.ERR_EM150);
        }
    }

    /**
     * Check range Maintenance Type Name.
     *
     * @param mainteTypeName
     */
    private void checkRangeMainteTypeName(String mainteTypeName) {
        if (!CommonUtilEx.validateMaxLength(mainteTypeName, MAX_LENGTH_MAINTE_TYPE_NAME)) {
            rowErrCodeList.add(MsgConst.ERR_EM150);
        }
    }

    /**
     * Convert Err codes to Err messages
     *
     * @param errList error List.
     * @return Error messages
     */
    private String convertErrCodeList(List<String> errList) {
        StringBuilder errMsg = new StringBuilder();

        // reverse err code list
        for (Iterator<String> idx = errList.iterator(); idx.hasNext(); ) {
            String errMessage = idx.next();

            // convert to err message
            errMsg.append(errMessage);
            if (idx.hasNext()) {
                errMsg.append(", ");
            }
        }
        return errMsg.toString();
    }

    /**
     * Validate Insert Data to DB.
     *
     * @param insertDataList
     */
    private void validateInsertAndUpdateData(List<MainteTypeDataGridRow> insertDataList,
                                             boolean isInsert) {
        insertDataList.forEach(mainteTypeDataGridRow -> {

            rowErrCodeList = new ArrayList<>();

            Integer rowIndex = mainteTypeDataGridRow.getRowIndex();
            MainteTypeData rowData = mainteTypeDataGridRow.getRowData();

            String mainteTypeName = rowData.getMainteTypeName();
            checkRangeMainteTypeName(mainteTypeName);
            checkMainteTypeName(mainteTypeName);

            Integer mainteType = rowData.getMainteType();
            checkMainteType(mainteType);
            checkRangeMainteType(mainteType);


            Integer makerOrgId = rowData.getMakerOrgId();
            Integer modelId = rowData.getModelId();
            Integer mainteTypeId = rowData.getMainteType();

            if (isInsert) {
                if (mainteTypeService.isDupplicateKey(makerOrgId, modelId, mainteTypeId) &&
                        !updateKeyList.contains(new MainteTypeDataKey(mainteTypeId)) &&
                        !deleteKeyList.contains(new MainteTypeDataKey(mainteTypeId))) {
                    rowErrCodeList.add(messageService.text(MsgConst.ERR_EM151, String.valueOf(mainteTypeId),
                            mainteTypeName));
                }
            } else {
                if (rowData.getMainteType().intValue() != rowData.getOldMainteType()) {
                    if (!mainteTypeService.isDupplicateKey(makerOrgId, modelId, rowData.getOldMainteType())) {

                        rowErrCodeList.add(messageService.text(MsgConst.ERR_EM155, String.valueOf(mainteTypeId),
                                mainteTypeName));

                    } else if (mainteTypeService.isDupplicateKey(makerOrgId, modelId, mainteTypeId) &&
                            !deleteKeyList.contains(new MainteTypeDataKey(mainteTypeId))) {

                        rowErrCodeList.add(messageService.text(MsgConst.ERR_EM151, String.valueOf(modelId),
                                mainteTypeName));

                    }
                }
            }

            if (rowErrCodeList.size() > 0) {
                errCodeList.put(rowIndex, convertErrCodeList(rowErrCodeList));
            }
        });
    }

    /**
     * Validate Delete Data to DB.
     *
     * @param deleteDataList
     */
    private void validateDeleteData(List<MainteTypeDataGridRow> deleteDataList) {
        deleteDataList.forEach(mainteTypeDataGridRow -> {

            rowErrCodeList = new ArrayList<>();

            Integer rowIndex = mainteTypeDataGridRow.getRowIndex();
            MainteTypeData rowData = mainteTypeDataGridRow.getRowData();

            if (!mainteTypeService.isDupplicateKey(rowData.getMakerOrgId(), rowData.getModelId(), rowData.getMainteType())) {
                rowErrCodeList.add(messageService.text(MsgConst.ERR_EM155,
                        String.valueOf(rowData.getMainteType()), rowData.getMainteTypeName()));
            }

            if (rowErrCodeList.size() > 0) {
                errCodeList.put(rowIndex, convertErrCodeList(rowErrCodeList));
            }
        });
    }

    static class MainteTypeDataKey {
        private Integer mainteType;

        public MainteTypeDataKey(Integer mainteType) {
            this.mainteType = mainteType;
        }

        public Integer getMainteType() {
            return mainteType;
        }

        @Override
        public int hashCode() {
            int hash = 1;
            hash = hash * 31 + mainteType;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (!(obj instanceof MainteTypeDataKey)) {
                return false;
            } else if (obj == this) {
                return true;
            }

            MainteTypeDataKey mainteTypeDataKey = (MainteTypeDataKey) obj;

            return this.getMainteType() == mainteTypeDataKey.getMainteType();
        }
    }

    /**
     * Get key update, delete
     */
    private void getValidKey() {
        deleteKeyList = new ArrayList<>();
        updateKeyList = new ArrayList<>();

        mainteTypeDataGridRowCollection.getArrDeletedData().forEach(mainteTypeDataGridRow -> {
            MainteTypeData mainteTypeData = mainteTypeDataGridRow.getRowData();
            deleteKeyList.add(new MainteTypeDataKey(mainteTypeData.getOldMainteType()));
        });

        mainteTypeDataGridRowCollection.getArrUpdatedData().forEach(mainteTypeDataGridRow -> {
            MainteTypeData mainteTypeData = mainteTypeDataGridRow.getRowData();
            if (mainteTypeData.getMainteType().intValue() != mainteTypeData.getOldMainteType()) {
                updateKeyList.add(new MainteTypeDataKey(mainteTypeData.getOldMainteType()));
            }
        });
    }

    /**
     * Validate Grid Row Data.
     *
     * @param mainteTypeDataGridRowCollection
     * @return Map String Error code.
     */
    public Map<Integer, String> validateGridRowData(MainteTypeDataGridRowCollection
                                                            mainteTypeDataGridRowCollection) {
        errCodeList = new HashMap<>();

        getValidKey();

        validateInsertAndUpdateData(mainteTypeDataGridRowCollection.getArrInsertedData(), true);
        validateInsertAndUpdateData(mainteTypeDataGridRowCollection.getArrUpdatedData(), false);
        validateDeleteData(mainteTypeDataGridRowCollection.getArrDeletedData());

        return errCodeList;
    }
}
