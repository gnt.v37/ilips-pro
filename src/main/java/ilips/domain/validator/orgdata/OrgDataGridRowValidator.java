package ilips.domain.validator.orgdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import ilips.constant.MsgConst;
import ilips.domain.model.orgdata.OrgData;
import ilips.domain.model.orgdata.OrgDataGridRow;
import ilips.domain.model.orgdata.OrgDataGridRowCollection;
import ilips.domain.service.message.MessageService;
import ilips.domain.service.orgdata.OrgDataService;
import ilips.util.common.CommonUtilEx;

/**
 * 
 * Class name: Validate for Organization Data
 * 
 * @author ThangNT2 Created Date: Jul 19, 2018
 */
public class OrgDataGridRowValidator {

	private static final Integer VAL_MAX_LENGTH = 400;

	private Map<Integer, String> errCodeList;

	private List<OrgDataKey> updatedKey;

	private List<OrgDataKey> deleteKey;

	private MessageService messageService;

	private final OrgDataService orgDataService;

	private OrgDataGridRowCollection collection;

	public OrgDataGridRowValidator(OrgDataService orgDataService, MessageService messageService,
			OrgDataGridRowCollection collection) {

		this.orgDataService = orgDataService;
		this.messageService = messageService;
		this.collection = collection;
	}

	/**
	 * Validation row
	 * 
	 * @param collection
	 * @return message's code list (if has error)
	 */
	public Map<Integer, String> validationRow() {
		getValidKeys(collection);
		errCodeList = new HashMap<>();

		validateForInsertAndUpdate(collection.getArrInsertedOrgData(), true);
		validateForInsertAndUpdate(collection.getArrUpdatedOrgData(), false);
		validateForDelete(collection.getArrDeletedOrgData());

		return errCodeList;
	}

	/**
	 * Get valid key
	 * 
	 * @param collection
	 * @return valid key list
	 */
	private void getValidKeys(OrgDataGridRowCollection collection) {

		updatedKey = new ArrayList<>();
		deleteKey = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(collection.getArrDeletedOrgData())) {
			for (OrgDataGridRow row : collection.getArrDeletedOrgData()) {
				OrgData orgData = row.getRowData();
				deleteKey.add(new OrgDataKey(orgData.getOldOrgId(), orgData.getOldOrgName()));
			}
		}

		if (CollectionUtils.isNotEmpty(collection.getArrUpdatedOrgData())) {
			for (OrgDataGridRow row : collection.getArrUpdatedOrgData()) {
				OrgData orgData = row.getRowData();
				if (orgData.getOrgId().intValue() != orgData.getOldOrgId().intValue()) {
					// update key
					updatedKey.add(new OrgDataKey(orgData.getOldOrgId(), orgData.getOldOrgName()));
				}
			}
		}
	}

	/**
	 * Validation for inserting or updating data
	 * 
	 * @param data -> organization list
	 */
	private void validateForInsertAndUpdate(List<OrgDataGridRow> data, boolean isInsert) {

		List<String> errorCodeList = null;
		OrgData orgData = null;

		// if collection is empty
		// don't do anything
		if (CollectionUtils.isEmpty(data)) {

			return;
		}

		// prepare validate
		for (OrgDataGridRow row : data) {
			orgData = row.getRowData();
			errorCodeList = validated(orgData, isInsert);

			// if has error
			if (!errorCodeList.isEmpty()) {
				errCodeList.put(row.getRowIndex(), convertErrCodeList(errorCodeList));
			}
		}
	}

	/**
	 * Validation for delete row
	 * 
	 * @param orgDataGridRowList
	 */
	private void validateForDelete(List<OrgDataGridRow> orgDataGridRowList) {

		List<String> errorCodeList = null;
		OrgData orgData = null;

		if (CollectionUtils.isEmpty(orgDataGridRowList)) {

			return;
		}

		for (OrgDataGridRow row : orgDataGridRowList) {
			orgData = row.getRowData();
			errorCodeList = new ArrayList<>();

			if (!orgDataService.isExistOrgId(orgData.getOrgId(), orgData.getOrgName(), true)) {
				errorCodeList.add(convertErrorCodeToMsg(MsgConst.ERR_EM155, StringUtils.EMPTY));
			}

			// if has error
			if (!errorCodeList.isEmpty()) {
				errCodeList.put(row.getRowIndex(), convertErrCodeList(errorCodeList));
			}
		}
	}

	/**
	 * Check Maxlength
	 * 
	 * @param value
	 * @return
	 */
	private String checkMaxLength(String value) {

		if (!CommonUtilEx.validateMaxLength(value, VAL_MAX_LENGTH)) {
			return convertErrorCodeToMsg(MsgConst.ERR_EM193E4);
		}

		return null;
	}

	/**
	 * Check Required for Integer type
	 * 
	 * @param value
	 * @return
	 */
	private String checkRequired(Integer value) {

		if (CommonUtilEx.isNull(value)) {
			return convertErrorCodeToMsg(MsgConst.ERR_EM150);
		}

		return null;
	}

	/**
	 * Check Required for String type
	 * 
	 * @param value
	 * @return
	 */
	private String checkRequired(String value) {

		if (CommonUtilEx.isEmpty(value)) {
			return convertErrorCodeToMsg(MsgConst.ERR_EM150);
		}

		return null;
	}

	/**
	 * Validate data
	 * 
	 * @param data
	 * @return
	 */
	private List<String> validated(OrgData data, boolean insert) {

		List<String> errorCodeList = new ArrayList<>();
		String errorCode = null;

		// check required
		errorCode = checkRequired(data.getOrgId());
		if (errorCode != null) {
			errorCodeList.add(errorCode);
		}

		errorCode = checkRequired(data.getOrgName());
		if (errorCode != null) {
			errorCodeList.add(errorCode);
		}

		errorCode = checkRequired(data.getAttrMaker());
		if (errorCode != null) {
			errorCodeList.add(errorCode);
		}

		errorCode = checkRequired(data.getAttrCustomer());
		if (errorCode != null) {
			errorCodeList.add(errorCode);
		}

		errorCode = checkRequired(data.getAttrOwner());
		if (errorCode != null) {
			errorCodeList.add(errorCode);
		}

		errorCode = checkRequired(data.getAttrMainte());
		if (errorCode != null) {
			errorCodeList.add(errorCode);
		}

		// check max-length
		if (!CommonUtilEx.isEmpty(data.getOrgName())) {
			errorCode = checkMaxLength(data.getOrgName());

			if (errorCode != null) {

				errorCodeList.add(errorCode);
			}
		}

		if (!CommonUtilEx.isEmpty(data.getOrgCode())) {
			errorCode = checkMaxLength(data.getOrgCode());

			if (errorCode != null) {

				errorCodeList.add(errorCode);
			}
		}

		if (!CommonUtilEx.isEmpty(data.getOrgType())) {
			errorCode = checkMaxLength(data.getOrgType());

			if (errorCode != null) {

				errorCodeList.add(errorCode);
			}
		}

		if (!CommonUtilEx.isEmpty(data.getShortName())) {
			errorCode = checkMaxLength(data.getShortName());

			if (errorCode != null) {

				errorCodeList.add(errorCode);
			}
		}

		if (!CommonUtilEx.isEmpty(data.getAddress())) {
			errorCode = checkMaxLength(data.getAddress());

			if (errorCode != null) {

				errorCodeList.add(errorCode);
			}
		}

		if (insert) {

			if (orgDataService.isExistOrgId(data.getOrgId(), data.getOrgName(), false) 
					&& !deleteKey.contains(new OrgDataKey(data.getOldOrgId(), data.getOldOrgName()))
					&& !updatedKey.contains(new OrgDataKey(data.getOldOrgId(), data.getOldOrgName()))) {
				errorCodeList.add(convertErrorCodeToMsg(MsgConst.ERR_EM151));
			}
		} else {

			if (Integer.compare(data.getOrgId().intValue(), data.getOldOrgId()) != 0) {

				if (!orgDataService.isExistOrgId(data.getOldOrgId(), data.getOrgName(), false)) {
					errorCodeList.add(convertErrorCodeToMsg(MsgConst.ERR_EM155, StringUtils.EMPTY));
				} else if (orgDataService.isExistOrgId(data.getOldOrgId(), data.getOldOrgName(), false)
						&& !deleteKey.contains(new OrgDataKey(data.getOldOrgId(), data.getOldOrgName()))) {
//					errorCodeList.add(convertErrorCodeToMsg(MsgConst.ERR_EM193E11, String.valueOf(data.getOrgId()), data.getOrgName()));
				}
			}
		}
		return errorCodeList;
	}

	/**
	 * Convert Err codes to Err messages
	 *
	 * @param errList error List.
	 * @return Error messages
	 */
	private String convertErrCodeList(List<String> errList) {
		StringBuilder errMsg = new StringBuilder();

		// reverse err code list
		for (Iterator<String> idx = errList.iterator(); idx.hasNext();) {
			String errMessage = idx.next();

			// convert to err message
			errMsg.append(errMessage);
			if (idx.hasNext()) {
				errMsg.append(", ");
			}
		}
		return errMsg.toString();
	}

	private String convertErrorCodeToMsg(String code, String... objects) {
		String msg = null;

		if (objects == null) {
			msg = messageService.text(msg);
		}

		msg = messageService.text(code, objects);

		return msg;
	}

	/**
	 * Class contain person data key.
	 *
	 * @author ThangNT2 created on 7/19/2018
	 */
	public static class OrgDataKey {

		private int orgId;

		private String orgName;

		public OrgDataKey(Integer orgId, String orgName) {
			this.orgId = orgId;
			this.orgName = orgName;
		}

		@Override
		public int hashCode() {
			int hash = 1;
			hash = hash * 31 + orgId;
			return hash;
		}

		public int getOrgId() {
			return orgId;
		}

		public String getOrgName() {
			return orgName;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null)
				return false;
			if (!(obj instanceof OrgDataKey)) {
				return false;
			} else if (obj == this) {
				return true;
			}

			OrgDataKey orgDataKey = (OrgDataKey) obj;

			return this.getOrgId() == orgDataKey.getOrgId() || this.getOrgName().equals(orgDataKey.getOrgName());
		}
	}
}
