package ilips.domain.validator.alarm;

import ilips.domain.model.AlarmLevel.AlarmLevelData;
import ilips.domain.model.AlarmLevel.AlarmLevelDataGridRow;
import ilips.domain.model.AlarmLevel.AlarmLevelDataGridRowCollection;
import ilips.domain.service.alarm.AlarmLevelService;
import ilips.domain.service.message.MessageService;
import ilips.util.common.CommonUtilEx;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 *
 * @author TuyenND6
 * created on 7/20/2018
 */

public class AlarmLevelGridRowValidator {

    private static final int MAX_LENGTH_ALARM_LEVEL_NAME = 400;

    private static final int MAX_LENGTH_DESCRIPTION = 400;

    private static final int MAX_LENGTH_ALARM_LEVEL = 10;

    private static final int MAX_VALUE_ALARM_LEVEL = 2147483647;

    private static final int MIN_VALUE_ALARM_LEVEL = 0;

    private final AlarmLevelService alarmLevelService;

    private final MessageService messageService;

    private List<String> rowErrCodeList;

    private Map<Integer, String> errList;

    private List<AlarmLevelDataKey> updatedKey;

    private List<AlarmLevelDataKey> deletedKey;

    private AlarmLevelDataGridRowCollection alarmLevelDataGridRowCollection;

    /**
     * Class contain person data key.
     *
     * @author TuyenND6
     * created on 7/18/2018
     */
    @Data
    static class AlarmLevelDataKey {

        private int alarmLevel;

        AlarmLevelDataKey(int alarmLevel) {
            this.alarmLevel = alarmLevel;
        }

        @Override
        public int hashCode() {
            int hash = 1;
            hash = hash * 31 + alarmLevel;
            return hash;
        }


        @Override
        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (!(obj instanceof AlarmLevelDataKey)) {
                return false;
            } else if (obj == this) {
                return true;
            }

            AlarmLevelDataKey alarmLevelDataKey = (AlarmLevelDataKey) obj;

            return this.getAlarmLevel() == alarmLevelDataKey.getAlarmLevel();
        }
    }

    public AlarmLevelGridRowValidator(AlarmLevelDataGridRowCollection alarmLevelDataGridRowCollection,
                                      AlarmLevelService alarmLevelService,
                                      MessageService messageService) {
        this.alarmLevelDataGridRowCollection = alarmLevelDataGridRowCollection;
        this.alarmLevelService = alarmLevelService;
        this.messageService = messageService;
    }

    /**
     * Valid row data.
     *
     * @return list of error.
     */
    public Map<Integer, String> validateGridRowData() {

        errList = new HashMap<>();
        getValidKey();

        validateInsertAndUpdateData(alarmLevelDataGridRowCollection.getArrInsertedData(), true);
        validateInsertAndUpdateData(alarmLevelDataGridRowCollection.getArrUpdatedData(), false);
        validateDeleteData(alarmLevelDataGridRowCollection.getArrDeletedData());

        return errList;
    }

    /**
     * Get valid key after update or delete.
     */
    private void getValidKey() {
        updatedKey = new ArrayList<>();
        deletedKey = new ArrayList<>();
        for (AlarmLevelDataGridRow alarmLevelDataGridRow : alarmLevelDataGridRowCollection.getArrDeletedData()) {
            //If delete a row then the delete row key will be valid to insert or update
            AlarmLevelData alarmLevelData = alarmLevelDataGridRow.getRowData();
            updatedKey.add(new AlarmLevelDataKey(alarmLevelData.getAlarmLevel()));
        }

        for (AlarmLevelDataGridRow alarmLevelDataGridRow : alarmLevelDataGridRowCollection.getArrUpdatedData()) {
            AlarmLevelData alarmLevelData = alarmLevelDataGridRow.getRowData();
            if (alarmLevelData.getAlarmLevel() != alarmLevelData.getAlarmLevel()) {
                //If update key of a row then the old row key will be valid to insert or update
                deletedKey.add(new AlarmLevelDataKey(alarmLevelData.getAlarmLevel()));
            }
        }
    }

    private void validateInsertAndUpdateData(List<AlarmLevelDataGridRow> insertDataList, boolean isInsert) {

        for (AlarmLevelDataGridRow alarmLevelDataGridRow : insertDataList) {

            rowErrCodeList = new ArrayList<>();
            int rowIndex = alarmLevelDataGridRow.getRowIndex();
            AlarmLevelData rowData = alarmLevelDataGridRow.getRowData();

            int alarmLevel = rowData.getAlarmLevel();
            checkAlarmLevel(alarmLevel);

            String alarmLevelName = rowData.getAlarmLevelName();
            checkAlarmLevelName(alarmLevelName);



            if (isInsert) {
                if (alarmLevelService.isExistKey(rowData.getAlarmLevel())
                        && !deletedKey.contains(new AlarmLevelDataKey(rowData.getAlarmLevel()))
                        && !updatedKey.contains(new AlarmLevelDataKey(rowData.getAlarmLevel()))) {
                    //if insert
                    rowErrCodeList.add(messageService.text("",
                            String.valueOf(rowData.getAlarmLevel())));
                }
            } else {
                if ((rowData.getAlarmLevel() != rowData.getAlarmLevel())) {
                    //if update key the check new key is ok or not?
                    if (!alarmLevelService.isExistKey(rowData.getAlarmLevel())) {
                        //if update a row is not in db
                        rowErrCodeList.add(messageService.text("", StringUtils.EMPTY));
                    } else if (alarmLevelService.isExistKey(rowData.getAlarmLevel())
                            && !updatedKey.contains(new AlarmLevelDataKey(rowData.getAlarmLevel()))) {
                        //if new key is not valid
                        rowErrCodeList.add(messageService.text("",
                                String.valueOf(rowData.getAlarmLevel())));
                    }
                }
            }

            if (rowErrCodeList.size() > 0) {
                errList.put(rowIndex, convertErrCodeList(rowErrCodeList));
            }
        }
    }

    private void validateDeleteData(List<AlarmLevelDataGridRow> deleteDataList) {

        for (AlarmLevelDataGridRow rowDto : deleteDataList) {

            rowErrCodeList = new ArrayList<>();

            Integer rowIndex = rowDto.getRowIndex();
            AlarmLevelData rowData = rowDto.getRowData();

            Integer alarmLevel = rowData.getAlarmLevel();

            if (!alarmLevelService.isExistKey(alarmLevel)) {
                rowErrCodeList.add(messageService.text("", StringUtils.EMPTY));
            }

            if (rowErrCodeList.size() > 0) {
                errList.put(rowIndex, convertErrCodeList(rowErrCodeList));
            }
        }
    }

    private void checkAlarmLevel(int alarmLevel) {
        if (CommonUtilEx.isNull(alarmLevel)) {
            rowErrCodeList.add(messageService.text(""));
        }
    }

    private void checkAlarmLevelName(String alarmLevelName) {
        if (CommonUtilEx.isEmpty(alarmLevelName)) {
            rowErrCodeList.add(messageService.text(""));
        } else {
            if (!CommonUtilEx.validateMaxLength(alarmLevelName, MAX_LENGTH_ALARM_LEVEL_NAME)) {
                rowErrCodeList.add(messageService.text(""));
            }
        }
    }

    private void checkAlarmLevelDescription(String alarmDescription) {
        if (!CommonUtilEx.isEmpty(alarmDescription)) {
            if (!CommonUtilEx.validateMaxLength(alarmDescription, MAX_LENGTH_DESCRIPTION)) {
                rowErrCodeList.add(messageService.text(""));
            }
        }
    }
    /**
     * Convert Err codes to Err messages
     *
     * @param errList error List.
     * @return Error messages
     */
    private String convertErrCodeList(List<String> errList) {
        StringBuilder errMsg = new StringBuilder();

        // reverse err code list
        for (Iterator<String> idx = errList.iterator(); idx.hasNext(); ) {
            String errMessage = idx.next();

            // convert to err message
            errMsg.append(errMessage);
            if (idx.hasNext()) {
                errMsg.append(", ");
            }
        }
        return errMsg.toString();
    }
}