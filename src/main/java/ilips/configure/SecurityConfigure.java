package ilips.configure;

import ilips.constant.FunctionConst;
import ilips.domain.service.authentication.IlipsAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Application Security Configure.
 *
 * @author CanhLP
 * created on 7/13/2018
 */
@Configuration
@EnableWebSecurity
public class SecurityConfigure extends WebSecurityConfigurerAdapter {

    private final IlipsAuthenticationProvider authProvider;

    @Autowired
    public SecurityConfigure(IlipsAuthenticationProvider authProvider) {
        this.authProvider = authProvider;
    }

    @Override
    protected void configure(
            AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //組織データ
                .antMatchers("/product/productWizard/**")
                .hasAuthority(String.valueOf(FunctionConst.PRODUCT_WIZARD))
                //組織データ
                .antMatchers("/account/orgData/**")
                .hasAuthority(String.valueOf(FunctionConst.ORG_REFER))
                //参照可能組織
                .antMatchers("/product/referableOrg/**")
                .hasAuthority(String.valueOf(FunctionConst.REF_ORG_REFER))
                //システム権限
                .antMatchers("/account/systemPermission/**")
                .hasAuthority(String.valueOf(FunctionConst.SYS_PERM_REFER))
                //人データ
                .antMatchers("/account/personData/**")
                .hasAuthority(String.valueOf(FunctionConst.PERSON_REFER))
                //アラームレベル
                .antMatchers("/alarm/alarmLevel/**")
                .hasAuthority(String.valueOf(FunctionConst.A_LV_REFER))
                //工事情報
                .antMatchers("/product/workInfo/**")
                .hasAuthority(String.valueOf(FunctionConst.WORK_REFER))
                //型式情報
                .antMatchers("/product/modelInfo/**")
                .hasAuthority(String.valueOf(FunctionConst.MODEL_REFER))
                //アラームコード
                .antMatchers("/alarm/alarmCode/**")
                .hasAuthority(String.valueOf(FunctionConst.A_CODE_REFER))
                //データ収集モジュール
                .antMatchers("/product/dataCollector/**")
                .hasAuthority(String.valueOf(FunctionConst.DC_REFER))
                //製品情報
                .antMatchers("/product/productInfoTbl/**")
                .hasAuthority(String.valueOf(FunctionConst.PRODUCT_REFER))
                //権限設定
                .antMatchers("/account/permission/**")
                .hasAuthority(String.valueOf(FunctionConst.PERMISSION_REFER))
                //タグ定義
                .antMatchers("/product/tagDefine/**")
                .hasAuthority(String.valueOf(FunctionConst.TAG_REFER))
                //メール送信設定
                .antMatchers("/account/mailConfig/**")
                .hasAuthority(String.valueOf(FunctionConst.MAIL_REFER))
                //メンテナンスタイプ
                .antMatchers("/product/mainteType/**")
                .hasAuthority(String.valueOf(FunctionConst.MAINTE_REFER))
                //共有グラフ設定
                .antMatchers("/product/sharedChart/**")
                .hasAnyAuthority(String.valueOf(FunctionConst.SHARED_CHART_GRAPH_REFER),
                        String.valueOf(FunctionConst.SHARED_CHART_USER_REFER),
                        String.valueOf(FunctionConst.SHARED_CHART_PRODUCT_REFER))
                //帳票テンプレート登録
                .antMatchers("/product/reportTemplate/**")
                .hasAuthority(String.valueOf(FunctionConst.REPORT_REGISTER_REFER))
        ;

        http
                .authorizeRequests()
                .antMatchers("/assets/**", "/login", "/user/**", "/messages.js").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll();
    }
}

