package ilips.util;

import javax.validation.constraints.NotNull;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * ILIPS Util.
 */
public class ILIPSUtil {

    /**
     * Get nullable int value from result set
     *
     * @param resultSet   the result set is be get value.
     * @param columnLabel the label for the column specified with the SQL AS clause.
     * @return the column value;
     * @throws SQLException if the columnLabel is not valid;
     *                      if a database access error occurs or this method is
     *                      called on a closed result set
     */
    public static Integer getNullableInt(@NotNull ResultSet resultSet,
                                         @NotNull String columnLabel) throws SQLException {
        int value = resultSet.getInt(columnLabel);
        if (resultSet.wasNull()) {
            return null;
        } else {
            return value;
        }
    }

    /**
     * Encrypt data
     *
     * @param password String before encryption.
     * @return Encrypted character string.
     */
    public static String passwordEncrypt(@NotNull String password) {
        try {
            byte[] byteArr = password.getBytes("UTF-8");

            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] resultArr = digest.digest(byteArr);

            StringBuilder resStr = new StringBuilder();
            for (byte b : resultArr) {
                resStr.append(String.format("%02x", b));
            }

            return resStr.toString();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }
}
