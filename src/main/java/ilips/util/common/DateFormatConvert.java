package ilips.util.common;


import ilips.constant.AppConst;
import org.apache.log4j.Logger;

import java.util.Hashtable;
import java.util.Locale;

/**
 * このクラスは日付フォーマットの変換を管理するクラスです。
 * Singletonパターンで実装されています。
 *
 * @author nitta
 * @version 1.0
 */
public class DateFormatConvert {

    /**
     * インスタンス
     */
    private static DateFormatConvert instance = new DateFormatConvert();

    /**
     * ロガー
     */
    protected Logger log = Logger.getLogger(getClass());

    /**
     * フォーマット定義ハッシュ
     */
    private Hashtable<String, Hashtable<Locale, String>> formatConfigHash = new Hashtable<String, Hashtable<Locale, String>>();


	/**
	 * コンストラクタ
	 */
	private DateFormatConvert() {

		Hashtable<Locale, String> formatDateHash = new Hashtable<Locale, String>();
		formatDateHash.put(Locale.JAPANESE, AppConst.DATE_FORMAT_DATE);
		formatDateHash.put(Locale.ENGLISH, "MM/dd/yyyy");
		this.formatConfigHash.put(AppConst.DATE_FORMAT_DATE, formatDateHash);

		Hashtable<Locale, String> formatYearMonthHash = new Hashtable<Locale, String>();
		formatYearMonthHash.put(Locale.JAPANESE, AppConst.DATE_FORMAT_YEAR_MONTH);
		formatYearMonthHash.put(Locale.ENGLISH, "MM/yyyy");
		this.formatConfigHash.put(AppConst.DATE_FORMAT_YEAR_MONTH, formatYearMonthHash);

		Hashtable<Locale, String> formatMonthDateTimeHash = new Hashtable<Locale, String>();
		formatMonthDateTimeHash.put(Locale.JAPANESE, AppConst.DATE_FORMAT_MONTH_DATE_TIME);
		formatMonthDateTimeHash.put(Locale.ENGLISH, "MM/dd HH:mm");
		this.formatConfigHash.put(AppConst.DATE_FORMAT_MONTH_DATE_TIME, formatMonthDateTimeHash);

		Hashtable<Locale, String> formatDateTimeHash = new Hashtable<Locale, String>();
		formatDateTimeHash.put(Locale.JAPANESE, AppConst.DATE_FORMAT_DATE_TIME);
		formatDateTimeHash.put(Locale.ENGLISH, "MM/dd/yyyy HH:mm:ss");
		this.formatConfigHash.put(AppConst.DATE_FORMAT_DATE_TIME, formatDateTimeHash);

		Hashtable<Locale, String> formatDateTimeNoSecHash = new Hashtable<Locale, String>();
		formatDateTimeNoSecHash.put(Locale.JAPANESE, AppConst.DATE_FORMAT_DATE_TIME_NO_SEC);
		formatDateTimeNoSecHash.put(Locale.ENGLISH, "MM/dd/yyyy HH:mm");
		this.formatConfigHash.put(AppConst.DATE_FORMAT_DATE_TIME_NO_SEC, formatDateTimeNoSecHash);

		Hashtable<Locale, String> formatTimeHash = new Hashtable<Locale, String>();
		formatTimeHash.put(Locale.JAPANESE, AppConst.DATE_FORMAT_TIME);
		formatTimeHash.put(Locale.ENGLISH, "HH:mm:ss");
		this.formatConfigHash.put(AppConst.DATE_FORMAT_TIME, formatTimeHash);



		Hashtable<Locale, String> formatDateNotSeparateHash = new Hashtable<Locale, String>();
		formatDateNotSeparateHash.put(Locale.JAPANESE, AppConst.DATE_FORMAT_DATE_NOT_SEPARATE);
		formatDateNotSeparateHash.put(Locale.ENGLISH, "MMddyyyy");
		this.formatConfigHash.put(AppConst.DATE_FORMAT_DATE_NOT_SEPARATE, formatDateNotSeparateHash);

		Hashtable<Locale, String> formatDateTimeNotSeparateHash = new Hashtable<Locale, String>();
		formatDateTimeNotSeparateHash.put(Locale.JAPANESE, AppConst.DATE_FORMAT_DATE_TIME_NOT_SEPARATE);
		formatDateTimeNotSeparateHash.put(Locale.ENGLISH, "MMddyyyyHHmmss");
		this.formatConfigHash.put(AppConst.DATE_FORMAT_DATE_TIME_NOT_SEPARATE, formatDateTimeNotSeparateHash);

		Hashtable<Locale, String> formatYearMonthNotSeparateHash = new Hashtable<Locale, String>();
		formatYearMonthNotSeparateHash.put(Locale.JAPANESE, AppConst.DATE_FORMAT_YYYY_MM_NOT_SEPARATE);
		formatYearMonthNotSeparateHash.put(Locale.ENGLISH, "MMyyyy");
		this.formatConfigHash.put(AppConst.DATE_FORMAT_YYYY_MM_NOT_SEPARATE, formatYearMonthNotSeparateHash);


	}

	/**
	 * インスタンス取得メソッド
	 */
    public static DateFormatConvert getInstance() {
	    return instance;
    }

    /**
     * 日本語フォーマットを指定したロケールに該当するフォーマットに変換する
     *
	 * @param japaneseFormat 日本語フォーマット
     * @return
     */
    public String getFormat(String japaneseFormat, Locale locale){
    	if(!this.formatConfigHash.containsKey(japaneseFormat)){
    		return japaneseFormat;
    	}else{
    		Hashtable<Locale, String> localeFormatHash = this.formatConfigHash.get(japaneseFormat);
        	if(!localeFormatHash.containsKey(locale)){
        		return japaneseFormat;
        	}else{
        		return localeFormatHash.get(locale);
        	}
    	}


    }

}
