package ilips.util.common;

import org.apache.commons.lang3.StringUtils;

/**
 * 20180106 FPT - Update methods for Numeric Util class (base on ver1.0)
 *
 * このクラスは数値に関するUtilクラスです。
 */
public class NumberUtilEx {

    /**
     * ユーティリティクラスのため、コンストラクタは使用禁止。
     */
    private NumberUtilEx() {
    }

    /**
     * IntegerをString型へ変換します。
     *
     * @param value 変換するInteger型の値。
     * @return Stringオブジェクト
     */
    public static String toString(Integer value) {
        String rtnValue = StringUtils.EMPTY;
        try {
            if (value != null) {
                rtnValue = value.toString();
            }

            return rtnValue;

        } catch (Exception e) {
            return rtnValue;
        }
    }

    /**
     * DoubleをString型へ変換します。
     *
     * @param value 変換するDouble型の値。
     * @return Stringオブジェクト
     */
    public static String toString(Double value) {
        String rtnValue = StringUtils.EMPTY;
        try {
            if (value != null) {
                rtnValue = value.toString();
            }

            return rtnValue;
        } catch (Exception e) {
            return rtnValue;
        }
    }

    /**
     * 文字列をInteger型へ変換します。
     *
     * @param str   変換する文字列。
     * @return Integerオブジェクト
     */
    public static Integer createInteger(String str) {
        Integer rtnValue = null;
        try {
            if (StringUtils.isNotEmpty(str)) {
                rtnValue = new Integer(str);
            }
        } catch (Exception e) {
        }
        return rtnValue;
    }

    /**
     * Integer型の値が同じかどうか判定する
     *
     * @param value1    値1。
     * @param value2    値2。
     * @return boolean true:同じ,false:同じでない
     */
    public static boolean equals(Integer value1, Integer value2) {
        try {
            if (value1 == null || value2 == null) {
                return false;
            } else {
                if (value1.intValue() == value2.intValue()) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Integer型の値が同じかどうか判定する
     *
     * @param value1    値1。
     * @param value2    値2。
     * @return boolean true:同じ,false:同じでない
     */
    public static boolean equalsExceptNull(Integer value1, Integer value2) {
        try {
            if (value1 == null && value2 == null) {
                return true;
            } else if (value1 == null || value2 == null) {
                return false;
            } else {
                if (value1.intValue() == value2.intValue()) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
    }

    // 20180106 FPT - Add methods from ver 1.0 - ADD - Start
    /**
     * 文字列をInteger型へ変換します。
     *
     * @param str
     * @return Integerオブジェクト
     */
    public static Double createDouble(String str) {
        Double rtnValue = null;
        try {
            if (StringUtils.isNotEmpty(str)) {
                rtnValue = new Double(str);
            }
        } catch (Exception e) {
        }
        return rtnValue;
    }

    /**
     * 文字列が数値かどうかチェックする。
     *
     * @param str
     * @return true:数値/false:数値でない
     */
    public static boolean isNumeric(String str) {

        try {
            Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
    // 20180106 FPT - Add methods from ver 1.0 - ADD - End
}
