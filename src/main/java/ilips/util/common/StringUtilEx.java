package ilips.util.common;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

/**
 * このクラスは文字列に関するUtilクラスです。
 *
 * @author nitta
 * @version 1.0
 */
public class StringUtilEx {

	/** 半角カタカナ文字 */
    private static final String[] hanKanaMoji = {
        "ｶﾞ", "ｷﾞ", "ｸﾞ", "ｹﾞ", "ｺﾞ", "ｻﾞ", "ｼﾞ", "ｽﾞ", "ｾﾞ", "ｿﾞ",
        "ﾀﾞ", "ﾁﾞ", "ﾂﾞ", "ﾃﾞ", "ﾄﾞ", "ﾊﾞ", "ﾋﾞ", "ﾌﾞ", "ﾍﾞ", "ﾎﾞ",
        "ﾊﾟ", "ﾋﾟ", "ﾌﾟ", "ﾍﾟ", "ﾎﾟ", "ｳﾞ",

		"ｦ", "ｧ", "ｨ", "ｩ", "ｪ", "ｫ", "ｬ", "ｭ", "ｮ", "ｯ", "ｰ",
        "ｱ", "ｲ", "ｳ", "ｴ", "ｵ", "ｶ", "ｷ", "ｸ", "ｹ", "ｺ",
        "ｻ", "ｼ", "ｽ", "ｾ", "ｿ", "ﾀ", "ﾁ", "ﾂ", "ﾃ", "ﾄ",
        "ﾅ", "ﾆ", "ﾇ", "ﾈ", "ﾉ", "ﾊ", "ﾋ", "ﾌ", "ﾍ", "ﾎ",
        "ﾏ", "ﾐ", "ﾑ", "ﾒ", "ﾓ", "ﾔ", "ﾕ", "ﾖ",
        "ﾗ", "ﾘ", "ﾙ", "ﾚ", "ﾛ", "ﾜ", "ﾝ",

        "｡", "｢", "｣", "､", "ﾞ", "ﾟ",
    };

	/** 半角カタカナ記号 */
    private static final String[] hanKigou = {
        "･", "\"","-",
    };

	/** 全角数字 */
    private static final String ZENKAKU_NUMERIC = "１２３４５６７８９０";

    /**
     * 全角カナ
     */
	private static final String ZENKAKU_KANA =
			"アイウエオ" +	"カキクケコ" +
			"サシスセソ" +	"タチツテト" +
			"ナニヌネノ" +	"ハヒフヘホ" +
			"マミムメモ" +	"ヤユヨ" +
			"ラリルレロ" +	"ワヲン" +
			"ガギグゲゴ" +	"ザジズゼゾ" +
			"ダヂヅデド" +	"バビブベボ" +
			"ヴ" +
			"パピプペポ" +
			"ァィゥェォ" +	"ャュョ" +	"ッヮヵヶ" +
			"　" +			//全角スペース
			" " + 			//半角スペース
			"ー";

	/** 半角カタカナ */
    private static final String[] hanKana = getHankana();

    /** 半角カタカナ文字、半角カタカナ記号から、半角カタカナを作る */
    private static String[] getHankana () {

    	// 半角カタカナ文字
    	List<String>  hanKanaMojiList = java.util.Arrays.asList(hanKanaMoji);
    	// 半角カタカナ記号
		List<String> hanKigouList = java.util.Arrays.asList(hanKigou);

		List<String> hanKanaList = new ArrayList<String>();

		// 連結
		hanKanaList.addAll(hanKanaMojiList);
		hanKanaList.addAll(hanKigouList);

		return (String[])hanKanaList.toArray(new String[0]);

    }

	/**
	 * カナのチェックを行う
	 * @param text
     * @param request   Current request object.
	 * @return true:カナである,false:カナでない
	 */
	public static boolean isKana(final String text, HttpServletRequest request) {

    	boolean isKana = false;
    	boolean isCompare = false;

    	if (text == null || text.length() <= 0) {
    		return isKana;
    	}
        String str = new String(text);

        for (int i = 0; i < str.length(); i++) {
        	String compare = str.substring(i , (i + 1));
        	isCompare = false;
        	for (int j = 0 ; j < hanKana.length ; j++) {
        		if ( compare.equals(hanKana[j]) ) {
        			isCompare = true;
        			continue;
        		}
        	}

        	if (!(ZENKAKU_KANA.indexOf( text.substring( i, i + 1) ) < 0)) {
    			isCompare = true;
    			continue;
			}

        	if (!(ZENKAKU_NUMERIC.indexOf( text.substring( i, i + 1) ) < 0)) {
    			isCompare = true;
    			continue;
			}

        	//Encordingの取得
      	    String Encoding = request.getCharacterEncoding();

      	    try{
      	    	int cnt = compare.getBytes(Encoding).length;

      	 	    if(cnt <= 1){
      	 	    	isCompare = true;
      	 	    	continue;
      	 	    }
      	    }catch(UnsupportedEncodingException e){
      	 	    return false;
      	    }

      	   if (!isCompare) {
        		break;
        	}
        }
        isKana = isCompare;

        return isKana;
	}

	/**
	 * 最大文字数のチェックを行う
     * @param request　 HTTPリクエスト
	 * @param value     検証値
	 * @param maxLength 最大文字数
	 * @return true:正常,false:異常
	 */
	public static boolean validateMaxLength(HttpServletRequest request, String value, int maxLength) {

		if(StringUtils.isBlank(value)){
			return true;
		}

 	   //Encordingの取得
 	   String Encoding = request.getCharacterEncoding();

 	   int cnt = 0;

 	   try{
 	 	   cnt = value.getBytes(Encoding).length;
 	   }catch(UnsupportedEncodingException e){
 		   return true;
 	   }

	   if(maxLength < cnt){

		   return false;
	   }else{
		   return true;
	   }
	}

	/**
	 * 半角文字のチェックを行うを行う
	 * @param value     検証値
	 * @return true:正常,false:異常
	 */
	public static boolean validateHankaku(String value) {

		byte[] bytes = value.getBytes();

		int beams = value.length();

		StringBuffer sb = new StringBuffer(value);

		for (int i = 0; i < value.length(); i++) {
			//改行コードは対象外とする
			if ('\n' == sb.charAt(i)) {
				beams = beams - 2;
			}
		}

		//lengthとバイト数が異なる場合は全角が含まれている
		if (beams != bytes.length) {

            return false;
		}

		return true;
	}

	/**
	 * 半角英数文字のチェックを行うを行う
	 * @param value     検証値
	 * @return true:正常,false:異常
	 */
	public static boolean validateHankakuEisu(String value) {

		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			if ((c < '0' || c > '9') && //数字でない
				(c < 'a' || c > 'z') && //小文字アルファベットでない
				(c < 'A' || c > 'Z')) { //大文字アルファベットでない
				return false;
			}
		}

		return true;
	}

	/**
	 * ログインID使用可能文字チェック
	 *
	 * @param value
	 * @return true : 使用可能文字だけで構成されている false：使用不可能な文字が含まれる
	 */
	public static boolean validateLoginIdUsable(String value) {

		//2個以上連続のピリオドはNG
		if (StringUtils.contains(value, "..")){
			return false;
		}
		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			// 先頭か末尾の@とピリオドはNG
			if( i == 0 || i == value.length()-1){
				if(c == '@' || c == '.'){
					return false;
				}
			}

			if ((c < '0' || c > '9') && //数字でない
				(c < 'a' || c > 'z') && //小文字アルファベットでない
				(c < 'A' || c > 'Z') && //大文字アルファベットでない
				(c != '_' && c != '-' && c != '@' && c != '.')		// アンダーバー、ハイフン、アットマーク、ピリオドでない（特定条件以外）
				) {
				return false;
			}
		}

		return true;
	}

	/**
	 * 数値を表示用通貨文字列に変換する<br/>
	 *
     * @param value 値
     * @return 通貨
     */
    public static String toDispCurreny(final Number value) {
        return value == null ? "" : new DecimalFormat("#,###").format(value.longValue());
    }

    /**
	 * 数値を表示用通貨文字列に変換する<br/>
	 *
     * @param value 値
     * @return 通貨
     */
    public static String toDispCurreny(final String value) {
    	if(StringUtils.isBlank(value) || !StringUtils.isNumeric(value)){
    		return StringUtils.EMPTY;
    	}

        return value == null ? "" : new DecimalFormat("#,###").format(Long.valueOf(value));
    }

    /**
	 * 数値を表示用通貨文字列に変換する<br/>
	 *
     * @param value 値
     * @return 通貨
     */
    public static String toDispCurreny(final long value) {
        return new DecimalFormat("#,###").format(value);
    }

    /**
	 * valueがfromValueと完全一致の場合にtoValueに変換する<br/>
	 *
     * @param value 値
     * @param fromValue 変換元の値
     * @param toValue 　変換後の値
     * @return 変換後の値
     */
    public static String replace(final String value, final String fromValue, final String toValue) {
        if(value == null){
        	return null;
        }

        if(value.equals(fromValue)){
        	return toValue;
        }else{
        	return value;
        }
    }

    /**
     * 文字列がnullの場合、空白文字列に変換する<br>
     *
     * @param str 文字列
     * @return 文字列
     */
    public static String toEmpty(String str) {

    	if(StringUtils.isEmpty(str)) {
    		return StringUtils.EMPTY;
    	}

    	return str;

    }

    /**
     * 文字列が空白の場合、nullに変換する<br>
     *
     * @param str 文字列
     * @return 文字列
     */
    public static String toNull(String str) {

    	if(StringUtils.isEmpty(str)) {
    		return null;
    	}

    	return str;

    }

    /**
	 * Map型から取得したString型をMap型に変換します
	 *
	 * @param stringMap 文字列
	 * @return Map型に変換した値
	 */
	public static Map<String, String> getStringMap(String str) {

		str = StringUtils.replace(str, "{", "");
		str = StringUtils.replace(str, "}", "");

		String[] splitStrings = StringUtils.split(str, ",");

		//Map型に変換
		Map<String, String> map = new HashMap<String, String>();
		for(String splitString : splitStrings) {
			String[] mapString = StringUtils.split(splitString, "=");
			if(mapString != null && mapString.length == 2) {
				String key   = StringUtils.trimToEmpty(mapString[0]);
				String value = StringUtils.trimToEmpty(mapString[1]);
				map.put(key, value);
			}
		}
		return map;
	}

	/**
	  * 半角（半角英数記号、半角ｶﾅ）か判断する
	  * @param c 対象char
	  * @return 半角（半角英数記号、半角ｶﾅ）の場合 true
	  */
	 public static boolean isHankaku(char c){
	     boolean res = false;
	    if( ('\u0020' <= c && c <= '\u007e') ||    //半角英数記号
	        ('\uff61' <= c && c <= '\uff9f')){    //半角カタカナ
	        res = true;
	    }
	    return res;
	 }

    /**
     * 半角・全角を判断して、指定文字数内で
     * 文字のを切り取る
     * @param source 対象文字列
     * @param len 指定バイト数
     * @return 指定文字数内で切り取った文字列
     */
    public static String split(String source,int len){
        String res = source;
        if(StringUtils.isNotEmpty(source)){
            int num = source.length();
            StringBuilder buf = new StringBuilder();
            int length = 0;
            for(int i = 0 ; i < num ; i++){
                char c = source.charAt(i);
                if (isHankaku(c)) {
                    //半角
                    length++;
                }else{
                    //全角
                    length += 2;
                }
                if(length <= len){
                    buf.append(c);
                }else{
                    break;
                }
            }
            res = buf.toString();
        }
        return res;
    }

    /**
     * IntをStringに変換する
     *
	 * @param  value 数値
     * @return String
	 * @throws Exception 例外が発生した場合にスローされる
     */
	public static String getString(Integer value) throws Exception {
		if(value == null){
			return StringUtils.EMPTY;
		}

		String retVal = StringUtils.EMPTY;
		try{
			retVal = String.valueOf(value);
		}catch(Exception e){
			return StringUtils.EMPTY;
		}

		return retVal;
	}

    /**
     * DoubleをStringに変換する
     *
	 * @param  value 数値
     * @return String
	 * @throws Exception 例外が発生した場合にスローされる
     */
	public static String getString(Double value) throws Exception {
		if(value == null){
			return StringUtils.EMPTY;
		}

		String retVal = StringUtils.EMPTY;
		try{
			retVal = String.valueOf(value);
		}catch(Exception e){
			return StringUtils.EMPTY;
		}
		return retVal;
	}

}
