package ilips.util.common;

import ilips.constant.AppConst;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.DateValidator;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 20180106 FPT - Util class for dates (base on ver1.0)
 *
 * このクラスは日付に関するUtilクラスです。
 */
public class DateUtilEx {
    /**
     * ユーティリティクラスのため、コンストラクタは使用禁止。
     */
    private DateUtilEx() {
    }

    /**
     * 日付文字列をjava.util.Calendar型へ変換します。
     *
     * @param str       変換対象の文字列
     * @param format    日付フォーマット。
     * @param timeZone  タイムゾーン。
     *
     * @return 変換後のjava.util.Calendarオブジェクト
     *
     * @throws ParseException   文字列の解析に失敗すると発生する
     */
    public static Calendar toCalendar(String str, String format, TimeZone timeZone)
            throws ParseException {
        DateFormat df = new SimpleDateFormat(format);
        df.setTimeZone(timeZone);

        Calendar date = Calendar.getInstance(timeZone);
        date.setTime(df.parse(str));
        return date;
    }

    /**
     * 日付文字列をjava.util.Date型へ変換します。
     *
     * @param str       変換対象の文字列
     * @param format    日付フォーマット。
     *
     * @return 変換後のjava.util.Dateオブジェクト
     *
     * @throws ParseException   文字列の解析に失敗すると発生する
     */
    public static Date toDate(String str, String format) throws ParseException {
        DateFormat df = new SimpleDateFormat(format);
        Date date = new Date(0);

        try {
            date = df.parse(str);
        } catch (Exception e) {
        }
        return date;
    }

    /**
     * 日付文字列を指定された日の最終日付(23:59:59)のjava.util.Date型へ変換します。
     *
     * @param str       変換対象の文字列
     * @param format    日付フォーマット。
     *
     * @return 変換後のjava.util.Dateオブジェクト
     *
     * @throws ParseException   文字列の解析に失敗すると発生する
     */
    public static Date toDateEnd(String str, String format) throws ParseException {
        DateFormat df = new SimpleDateFormat(format);

        Date date = df.parse(str);
        date = DateUtilEx.truncHHmmss(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.SECOND, -1);

        return cal.getTime();
    }

    /**
     * 月末を取得する
     *
     * @param value 変換対象の日付。
     *
     * @return 変換後のjava.util.Dateオブジェクト
     *
     * @throws ParseException   文字列の解析に失敗すると発生する
     */
    public static Date getMonthEnd(Date value) throws ParseException {
        Date date = DateUtilEx.truncHHmmss(value);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // 翌月
        cal.add(Calendar.MONTH, 1);
        // 翌月の１日
        cal.set(Calendar.DAY_OF_MONTH, 1);
        // 前日
        cal.add(Calendar.SECOND, -1);

        return cal.getTime();
    }

    /**
     * 日付文字列をjava.sql.Time型へ変換します。
     *
     * @param str 変換対象の文字列
     * @param format    日付フォーマット。
     *
     * @return 変換後のjava.util.Dateオブジェクト
     *
     * @throws ParseException   文字列の解析に失敗すると発生する
     */
    public static Timestamp toTimestamp(String str, String format) throws ParseException {
        DateFormat df = new SimpleDateFormat(format);

        Date date = df.parse(str);

        return new Timestamp(date.getTime());
    }

    /**
     * Date型の時分秒を切り捨てます
     *
     * @param value  変換対象の日付
     *
     * @return 変換後のjava.util.Dateオブジェクト
     *
     * @throws ParseException   文字列の解析に失敗すると発生する
     */
    public static Date truncHHmmss(Date value) throws ParseException {
        String str = DateUtilEx.toString(value, AppConst.DATE_FORMAT_DATE);

        return DateUtilEx.toDate(str, AppConst.DATE_FORMAT_DATE);
    }

    /**
     * java.util.Date型をjava.sql.Timestamp型へ変換します。
     *
     * @param value  変換対象の日付
     *
     * @return 変換後のjava.util.Timestampオブジェクト
     *
     * @throws ParseException   文字列の解析に失敗すると発生する
     */
    public static Timestamp toTimestamp(Date value) throws ParseException {
        return new Timestamp(value.getTime());
    }

    /**
     * java.util.Date型を日付文字列へ変換します。
     *
     * @param value  変換対象の日付
     * @param format 日付フォーマット
     *
     * @return 文字列
     */
    public static String toString(Date value, String format) {
        String rtnValue = StringUtils.EMPTY;
        try {
            DateFormat df = new SimpleDateFormat(format);

            rtnValue = df.format(value);
            return rtnValue;
        } catch (Exception e) {
            return rtnValue;
        }
    }

    /**
     * java.util.Calendar型を日付文字列へ変換します。
     *
     * @param value  変換対象の日付
     * @param format 日付フォーマット
     *
     * @return 文字列
     */
    public static String toString(Calendar value, String format) {
        String rtnValue = StringUtils.EMPTY;
        try {
            DateFormat df = new SimpleDateFormat(format);
            df.setTimeZone(value.getTimeZone());

            rtnValue = df.format(value.getTime());
            return rtnValue;
        } catch (Exception e) {
            return rtnValue;
        }
    }

    /**
     * java.util.Date型を指定ロケールの日付文字列へ変換します。
     *
     * @param value  変換対象の日付
     * @param format 日付フォーマット
     * @param locale ロケール。
     *
     * @return 文字列
     */
    public static String toString(Date value, String format, Locale locale) {
        String rtnValue = StringUtils.EMPTY;
        String outputFormatDate = DateFormatConvert.getInstance().getFormat(format, locale);

        try {
            DateFormat df = new SimpleDateFormat(outputFormatDate);

            rtnValue = df.format(value);
            return rtnValue;
        } catch (Exception e) {
            return rtnValue;
        }
    }

    /**
     * 日付のチェックを行う
     *
     * @param value  検証値
     * @param format 日付フォーマット
     *
     * @return true:正常,false:異常
     */
    public static boolean validateDate(String value, String format) {
        if (StringUtils.isBlank(value)) {
            return true;
        }

        return DateValidator.getInstance().isValid(value, format, true);
    }

    /**
     * 検証値1が検証値2より新しいかどうかをチェックする
     *
     * @param value1  検証値1
     * @param value2  検証値2
     * @param format 日付フォーマット
     *
     * @return true:正常,false:異常
     *
     * @throws ParseException   文字列の解析に失敗すると発生する
     */
    public static boolean after(String value1, String value2, String format) throws ParseException {
        if (StringUtils.isBlank(value1) && StringUtils.isBlank(value2)) {
            return false;
        }

        if (StringUtils.isBlank(value1)) {
            return false;
        }

        if (StringUtils.isBlank(value2)) {
            return true;
        }

        Calendar calVal1 = Calendar.getInstance();
        calVal1.setTime(DateUtilEx.toDate(value1, format));

        Calendar calVal2 = Calendar.getInstance();
        calVal2.setTime(DateUtilEx.toDate(value2, format));

        return calVal1.after(calVal2);
    }

    /**
     * 検証値1が検証値2より古いかどうかをチェックする
     *
     * @param value1  検証値1
     * @param value2  検証値2
     * @param format 日付フォーマット
     *
     * @return true:正常,false:異常
     *
     * @throws ParseException   文字列の解析に失敗すると発生する
     */
    public static boolean before(String value1, String value2, String format) throws ParseException {
        if (StringUtils.isBlank(value1) && StringUtils.isBlank(value2)) {
            return false;
        }

        if (StringUtils.isBlank(value1)) {
            return false;
        }

        if (StringUtils.isBlank(value2)) {
            return true;
        }

        Calendar calVal1 = Calendar.getInstance();
        calVal1.setTime(DateUtilEx.toDate(value1, format));

        Calendar calVal2 = Calendar.getInstance();
        calVal2.setTime(DateUtilEx.toDate(value2, format));

        return calVal1.before(calVal2);
    }

    /**
     * 検証値1と検証値2が等しいかどうかをチェックする
     *
     * @param value1  検証値1
     * @param value2  検証値2
     * @param format 日付フォーマット
     *
     * @return true:正常,false:異常
     *
     * @throws ParseException   文字列の解析に失敗すると発生する
     */
    public static boolean equals(String value1, String value2, String format) throws ParseException {
        if (StringUtils.isBlank(value1)) {
            return false;
        }

        if (StringUtils.isBlank(value2)) {
            return false;
        }

        Calendar calVal1 = Calendar.getInstance();
        calVal1.setTime(DateUtilEx.toDate(value1, format));

        Calendar calVal2 = Calendar.getInstance();
        calVal2.setTime(DateUtilEx.toDate(value2, format));

        return calVal1.equals(calVal2);
    }

    /**
     * UTCを取得する
     *
     * @param dateTime  日時文字列
     * @param timeZone  タイムゾーン
     * @param isDst     サマータイム適用有無
     *
     * @return UTC日時を取得
     *
     * @throws Exception 例外が発生した場合にスローされる
     */
    public static String getUtcTime(String dateTime, String timeZone, int isDst) throws Exception {
        if (StringUtils.isEmpty(dateTime)) {
            return StringUtils.EMPTY;
        }

        TimeZone tz = TimeZone.getDefault();
        if (!timeZone.isEmpty()) {
            tz = TimeZone.getTimeZone(timeZone);
        }

        SimpleDateFormat sdf = new SimpleDateFormat(AppConst.DATE_FORMAT_DATE_TIME);
        sdf.setTimeZone(tz);

        Date date = sdf.parse(dateTime);

        if (isDst == 0 && tz.inDaylightTime(date)) {
            int timeDiff = tz.getDSTSavings();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.MILLISECOND, timeDiff);
            date = cal.getTime();
        }
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String utc = sdf.format(date);

        return utc;
    }

    /**
     * ローカル時刻をUTCに変換したものを取得します。
     * 夏時間の切り替え時のローカル時の重複時は早い方を選択します。
     *
     * @param dateTime	日時文字列。
     * @param timeZone	タイムゾーン。
     *
     * @return UTCで表された日時。
     */
    public static String getUtcTimeEarlier(String dateTime, String timeZone) {
        if (StringUtils.isEmpty(dateTime)) {
            return StringUtils.EMPTY;
        }

        // タイムゾーンを取得する。
        ZoneId localZoneId;
        if (timeZone.isEmpty()) {
            localZoneId = ZoneId.systemDefault();
        } else {
            localZoneId = ZoneId.of(timeZone);
        }

        // 日時オブジェクト(タイムゾーン情報無し)を作成する。
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConst.DATE_FORMAT_DATE_TIME);
        LocalDateTime ldt = LocalDateTime.parse(dateTime, formatter);

        // 日時オブジェクト(タイムゾーン情報付き)を作成する。
        ZonedDateTime zdt = ldt.atZone(localZoneId);

        // 秋の夏時間の切換え時など、ローカル時系列の重複時は早い方を選択する。
        zdt = zdt.withEarlierOffsetAtOverlap();

        // UTCに変換する。
        String utc = zdt.withZoneSameInstant(ZoneId.of("UTC")).format(formatter);
        return utc;
    }

    /**
     * ローカル時刻をUTCに変換したものを取得します。
     * 夏時間の切り替え時のローカル時の重複時は遅い方を選択します。
     *
     * @param dateTime	日時文字列。
     * @param timeZone	タイムゾーン。
     *
     * @return UTCで表された日時。
     */
    public static String getUtcTimeLater(String dateTime, String timeZone) {
        if (StringUtils.isEmpty(dateTime)) {
            return StringUtils.EMPTY;
        }

        // タイムゾーンを取得する。
        ZoneId localZoneId;
        if (timeZone.isEmpty()) {
            localZoneId = ZoneId.systemDefault();
        } else {
            localZoneId = ZoneId.of(timeZone);
        }

        // 日時オブジェクト(タイムゾーン情報無し)を作成する。
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConst.DATE_FORMAT_DATE_TIME);
        LocalDateTime ldt = LocalDateTime.parse(dateTime, formatter);

        // 日時オブジェクト(タイムゾーン情報付き)を作成する。
        ZonedDateTime zdt = ldt.atZone(localZoneId);

        // 秋の夏時間の切換え時など、ローカル時系列の重複時は遅い方を選択する。
        zdt = zdt.withLaterOffsetAtOverlap();

        // UTCに変換する。
        String utc = zdt.withZoneSameInstant(ZoneId.of("UTC")).format(formatter);
        return utc;
    }

    /**
     * ローカル時刻を取得する
     *
     * @param  dateTimeUtc   日時文字列(UTC)
     * @param  timeZone      タイムゾーン
     * @param  isDst         サマータイム適用有無
     *
     * @return UTC日時を取得
     *
     * @throws Exception 例外が発生した場合にスローされる
     */
    public static String getLocalTime(String dateTimeUtc, String timeZone, int isDst)
            throws Exception {
        if (StringUtils.isEmpty(dateTimeUtc)) {
            return StringUtils.EMPTY;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(AppConst.DATE_FORMAT_DATE_TIME);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = sdf.parse(dateTimeUtc);

        TimeZone tz = TimeZone.getDefault();
        if (!timeZone.isEmpty()) {
            tz = TimeZone.getTimeZone(timeZone);
        }

        if (isDst == 0 && tz.inDaylightTime(date)) {
            int timeDiff = tz.getDSTSavings();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.MILLISECOND, timeDiff * -1);
            date = cal.getTime();
        }
        sdf.setTimeZone(tz);
        String localTime = sdf.format(date);

        return localTime;
    }

    /**
     * 和暦文字列へ変換します。
     *
     * @param value  変換対象の日付
     * @return 文字列
     */
    public static String toJapaneseString(Date value) {
        // 和暦で表示
        Locale locale = new Locale("ja", "JP", "JP");
        DateFormat japaneseFormat = DateFormat.getDateInstance(DateFormat.FULL, locale);
        return japaneseFormat.format(value);
    }

    /**
     * 和暦文字列へ変換します。
     *
     * @param value  変換対象の日付
     * @param format 日付フォーマット
     *
     * @return 文字列
     */
    public static String toJapaneseString(Date value, String format) {
        // 和暦で表示
        Locale locale = new Locale("ja", "JP", "JP");

        DateFormat df = new SimpleDateFormat(format, locale);

        return df.format(value);
    }

    /**
     * 日付文字列のフォーマットを変更する
     *
     * @param value 値
     * @param beforeFormat  変更前の日付フォーマット。
     * @param afterFormat   変更後の日付フォーマット。
     *
     * @return 変換した日付文字列。
     */
    public static String strDateChangeFormat(String value, String beforeFormat, String afterFormat) {
        try {
            if (StringUtils.isEmpty(value)) {
                return value;
            }

            Date date = toDate(value, beforeFormat);
            String dateStr = DateUtilEx.toString(date, afterFormat);

            return dateStr;
        } catch (Exception e) {
            return StringUtils.EMPTY;
        }
    }

    /**
     * 日付文字列のロケールを変更する
     *
     * @param value     値
     * @param format    日付フォーマット。
     * @param locale    ロケール。
     *
     * @return ロケールを変更した日付文字列。
     */
    public static String strDateChangeLocale(String value, String format, Locale locale) {
        String rtnValue = StringUtils.EMPTY;
        String outputFormatDate = DateFormatConvert.getInstance().getFormat(format, locale);

        try {
            Date date = toDate(value, format);
            DateFormat df = new SimpleDateFormat(outputFormatDate);

            rtnValue = df.format(date);
            return rtnValue;
        } catch (Exception e) {
            return rtnValue;
        }
    }

    /**
     * 日付のフォーマットの取得を行う
     *
     * @param value  検証値
     * @param format 日付フォーマット
     * @return true:正常,false:異常
     */
    public static String getDateFormat(String value) {

        if (StringUtils.isBlank(value)) {
            return StringUtils.EMPTY;
        }

        List<String> formatList = new ArrayList<String>();
        formatList.add(AppConst.DATE_FORMAT_DATE_TIME);
        formatList.add(AppConst.DATE_FORMAT_DATE);
        formatList.add(AppConst.DATE_FORMAT_DATE_NOT_SEPARATE);
        formatList.add(AppConst.DATE_FORMAT_DATE_TIME_NOT_SEPARATE);
        formatList.add(AppConst.DATE_FORMAT_YYYY_MM_NOT_SEPARATE);
        formatList.add(AppConst.DATE_FORMAT_YYYY_MM);

        for (String format : formatList) {
            if (DateValidator.getInstance().isValid(value, format, true)) {
                return format;
            }
        }

        return StringUtils.EMPTY;
    }
}
