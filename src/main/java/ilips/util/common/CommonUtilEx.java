package ilips.util.common;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class CommonUtilEx {

    public final static int LOGIN_ID_VALID = 0;
    public final static int LOGIN_ID_SEQUENT_PERIOD = 1;
    public final static int LOGIN_ID_SYNTAX_ERR = 2;
    public final static int LOGIN_ID_NOT_MATCH_REGEX = 3;

    /* Email check error codes */
    public final static int EMAIL_VALID = 0;
    public final static int EMAIL_NO_ATMARK = 1;
    public final static int EMAIL_EXCESS_ATMARK = 2;
    public final static int EMAIL_SYNTAX_ERR_ATMARK = 3;
    public final static int EMAIL_NOT_MATCH_REGEX = 4;
    public final static int EMAIL_SEQUENT_PERIOD = 5;
    public final static int EMAIL_SYNTAX_ERR_PERIOD = 6;

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[!#$%&'*+/=?^_`{|}~.@a-zA-Z0-9-]+$", Pattern.CASE_INSENSITIVE);

    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isNull(Integer in) {
        return in == null;
    }

    public static boolean validateMaxLength(String str, int maxLength) {
        if (StringUtils.isBlank(str)) {
            return true;
        }

        int cnt = str.length();

        return cnt < maxLength;
    }

    public static boolean validateLength(String str, int minLength, int maxLength) {
        int cnt = str.length();

        return cnt >= minLength && cnt <= maxLength;
    }

    public static int validateLoginIdUsable(String value) {

        //2個以上連続のピリオドはNG
        if (StringUtils.contains(value, "..")) {
            return LOGIN_ID_SEQUENT_PERIOD;
        }
        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            // 先頭か末尾の@とピリオドはNG
            if (i == 0 || i == value.length() - 1) {
                if (c == '@' || c == '.') {
                    return LOGIN_ID_SYNTAX_ERR;
                }
            }

            if ((c < '0' || c > '9') && //数字でない
                    (c < 'a' || c > 'z') && //小文字アルファベットでない
                    (c < 'A' || c > 'Z') && //大文字アルファベットでない
                    (c != '_' && c != '-' && c != '@' && c != '.') // アンダーバー、ハイフン、アットマーク、ピリオドでない（特定条件以外）
            ) {
                return LOGIN_ID_NOT_MATCH_REGEX;
            }
        }

        return LOGIN_ID_VALID;
    }

    public static int validateEmail(String s) {

        String user = "";
        String domain = "";

        if (!s.contains("@")) {
            return EMAIL_NO_ATMARK; //EM192
        }

        if (s.indexOf("@", s.indexOf("@") + 1) > -1) {
            return EMAIL_EXCESS_ATMARK; //EM193
        }

        if ((s.endsWith("@")) || (s.startsWith("@"))) {
            return EMAIL_SYNTAX_ERR_ATMARK; //EM236
        }

        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(s);
        if (!matcher.find())
            return EMAIL_NOT_MATCH_REGEX; //EM189

        if (s.contains("..")) {
            return EMAIL_SEQUENT_PERIOD; //EM191
        }

        StringTokenizer email = new StringTokenizer(s, "@");
        if (email.countTokens() == 2) {
            user = email.nextToken();
            domain = email.nextToken();
        }

        if (user.startsWith(".") || domain.endsWith(".")) {
            return EMAIL_SYNTAX_ERR_PERIOD; //EM190
        } else if (user.endsWith(".") || domain.startsWith(".")) {
            return EMAIL_SYNTAX_ERR_PERIOD; //EM190
        }

        return EMAIL_VALID;
    }
}
