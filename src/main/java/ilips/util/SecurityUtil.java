package ilips.util;

import ilips.domain.model.user.UserDetail;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {
    /**
     * Get Current User
     *
     * @return null if user not authenticate
     */
    public static UserDetail currentUser() {
        UserDetail rs = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof UserDetail) {
                rs = (UserDetail) principal;
            }
        }

        return rs;
    }

    public static boolean hasAuthorize(Integer permission){
        boolean rs = false;
        UserDetail currentUser = currentUser();
        if(currentUser != null){
            rs = currentUser.hasAuthorize(permission);
        }
        return rs;
    }

    public static boolean hasOneOfAuthorizes(Integer... permission){
        boolean rs = false;
        UserDetail currentUser = currentUser();
        if(currentUser != null){
            rs = currentUser.hasOneOfAuthorizes(permission);
        }
        return rs;
    }
}
