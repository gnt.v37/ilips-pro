package ilips.util;

import ilips.domain.model.user.UserDetail;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

public class WebContextHolder {

    private static final WebContextHolder instance = new WebContextHolder();

    private WebContextHolder() {
    }

    public static WebContextHolder getInstance() {
        return instance;
    }

    public HttpServletRequest getRequest() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attributes.getRequest();
    }

    public HttpSession getSession() {
        return getSession(true);
    }

    public HttpSession getSession(boolean create) {
        return getRequest().getSession(create);
    }

    public String getSessionId() {
        return getSession().getId();
    }

    public Locale getLocale() {
        return RequestContextUtils.getLocale(getRequest());
    }
}
