package ilips.constant;

/**
 * メッセージ定義クラスです。
 */
public class MsgConst {

    public static final String ERR_EM155 = "EM155";

    public static final String ERR_EM168 = "EM168";

    public static final String ERR_EM169 = "EM169";

    public static final String ERR_EM170 = "EM170";

    public static final String ERR_EM171 = "EM171";

    public static final String ERR_EM192 = "EM192";

    public static final String ERR_EM193 = "EM193";

    public static final String ERR_EM189 = "EM189";

    public static final String ERR_EM191 = "EM191";

    public static final String ERR_EM190 = "EM190";

    public static final String ERR_EM235 = "EM235";

    public static final String ERR_EM236 = "EM236";

    public static final String ERR_EM193E4 = "EM193E4";

    public static final String ERR_EM150 = "EM150";

    public static final String ERR_EM151 = "EM151";

    public static final String ERR_EM166 = "EM166";

    public static final String ERR_EM234 = "EM234";

    public static final String WRN_WN004 = "WM004";

    public static final String WORD_EMAIL = "personDataMail";

    public static final String WORD_FAX = "personDataFax";

    public static final String WORD_TEL = "personDataTel";

    public static final String WORD_ADDR = "personDataAdr";

    public static final String WORD_PASSWORD = "personDataPwd";

    public static final String WORD_PERSON_NAME = "personDataName";

    /**
     * ユーティリティクラスのため、コンストラクタは使用禁止。
     */

    private MsgConst() {
    }
}
