package ilips.constant;

/**
 * 機能定義クラス
 */
public class FunctionConst {
    public static final int LOGIN = 9001;
    //    public static final int IMPORT = 9002;
//    public static final int EXPORT = 9003;
//    public static final int MODEL_WIZARD = 9004;
    public static final int PRODUCT_WIZARD = 9005;
    public static final int ORG_REFER = 9006;
    public static final int ORG_INSERT = 9007;
    public static final int ORG_UPDATE = 9008;
    public static final int ORG_DELETE = 9009;
    public static final int REF_ORG_REFER = 9010;
    public static final int REF_ORG_INSERT = 9011;
    public static final int REF_ORG_UPDATE = 9012;
    public static final int REF_ORG_DELETE = 9013;
    public static final int SYS_PERM_REFER = 9014;
    public static final int SYS_PERM_INSERT = 9015;
    public static final int SYS_PERM_UPDATE = 9016;
    public static final int SYS_PERM_DELETE = 9017;
    public static final int PERSON_REFER = 9018;
    public static final int PERSON_INSERT = 9019;
    public static final int PERSON_UPDATE = 9020;
    public static final int PERSON_DELETE = 9021;
    public static final int A_LV_REFER = 9022;
    public static final int A_LV_INSERT = 9023;
    public static final int A_LV_UPDATE = 9024;
    public static final int A_LV_DELETE = 9025;
    public static final int WORK_REFER = 9026;
    public static final int WORK_INSERT = 9027;
    public static final int WORK_UPDATE = 9028;
    public static final int WORK_DELETE = 9029;
    //    public static final int P_TYPE_REFER = 9030;
//    public static final int P_TYPE_INSERT = 9031;
//    public static final int P_TYPE_UPDATE = 9032;
//    public static final int P_TYPE_DELETE = 9033;
    public static final int MODEL_REFER = 9034;
    public static final int MODEL_INSERT = 9035;
    public static final int MODEL_UPDATE = 9036;
    public static final int MODEL_DELETE = 9037;
    public static final int A_CODE_REFER = 9038;
    public static final int A_CODE_INSERT = 9039;
    public static final int A_CODE_UPDATE = 9040;
    public static final int A_CODE_DELETE = 9041;
    //    public static final int DEV_NO = 9042;
    public static final int DC_REFER = 9043;
    public static final int DC_INSERT = 9044;
    public static final int DC_UPDATE = 9045;
    public static final int DC_DELETE = 9046;
    public static final int PRODUCT_REFER = 9047;
    public static final int PRODUCT_INSERT = 9048;
    public static final int PRODUCT_UPDATE = 9049;
    public static final int PERMISSION_REFER = 9050;
    public static final int PERMISSION_INSERT = 9051;
    public static final int PERMISSION_UPDATE = 9052;
    public static final int PERMISSION_DELETE = 9053;
    public static final int TAG_REFER = 9054;
    public static final int TAG_INSERT = 9055;
    public static final int TAG_UPDATE = 9056;
    public static final int TAG_DELETE = 9057;
    //    public static final int P_INFO_REFER = 9058;
//    public static final int P_INFO_INSERT = 9059;
//    public static final int P_INFO_UPDATE = 9060;
//    public static final int P_INFO_DELETE = 9061;
    public static final int MAIL_REFER = 9062;
    public static final int MAIL_INSERT = 9063;
    public static final int MAIL_UPDATE = 9064;
    public static final int MAIL_DELETE = 9065;
    public static final int MONTHLY_REPORT = 201;
    public static final int MAINTE_REFER = 9066;
    public static final int MAINTE_INSERT = 9067;
    public static final int MAINTE_UPDATE = 9068;
    public static final int MAINTE_DELETE = 9069;
    public static final int SHARED_CHART_GRAPH_REFER = 9070;
    public static final int SHARED_CHART_GRAPH_INSERT = 9071;
    public static final int SHARED_CHART_GRAPH_UPDATE = 9072;
    public static final int SHARED_CHART_GRAPH_DELETE = 9073;
    public static final int SHARED_CHART_USER_REFER = 9074;
    public static final int SHARED_CHART_USER_INSERT = 9075;
    public static final int SHARED_CHART_USER_UPDATE = 9076;
    public static final int SHARED_CHART_USER_DELETE = 9077;
    public static final int SHARED_CHART_PRODUCT_REFER = 9078;
    public static final int SHARED_CHART_PRODUCT_INSERT = 9079;
    public static final int SHARED_CHART_PRODUCT_UPDATE = 9080;
    public static final int SHARED_CHART_PRODUCT_DELETE = 9081;
    public static final int REPORT_REGISTER_REFER = 9082;
    public static final int REPORT_REGISTER_INSERT = 9083;
    public static final int REPORT_REGISTER_UPDATE = 9084;
    public static final int REPORT_REGISTER_DELETE = 9085;
//    public static final int P_INFO_IMPORT = 9086;

    /**
     * ユーティリティクラスのため、コンストラクタは使用禁止。
     */
    private FunctionConst() {
    }
}
