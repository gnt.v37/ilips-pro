package ilips.constant;

/**
 * 20180106 FPT - Definition XML file constant for graph display (base on ver1.0)
 *
 * グラフ表示用定義XMLファイル定数
 */
public class DefXmlConst {

    /**
     * 解析なし
     */
    public static String ANALYZE_CHART_DEF_SENSED_TROUBLE_NO_ANALYZE = "analyzeChartDefSensedTroubleNoAnalyze.xml";

    public static String ANALYZE_CHART_DEF_SENSED_ALARM_NO_ANALYZE = "analyzeChartDefSensedAlarmNoAnalyze.xml";

    public static String ANALYZE_CHART_DEF_SENSED_CAUTION_NO_ANALYZE = "analyzeChartDefSensedCautionNoAnalyze.xml";

    /**
     * 移動平均
     */
    public static String ANALYZE_CHART_DEF_SENSED_TROUBLE_MOVE_AVERAGE = "analyzeChartDefSensedTroubleMoveAverage.xml";

    public static String ANALYZE_CHART_DEF_SENSED_ALARM_MOVE_AVERAGE = "analyzeChartDefSensedAlarmMoveAverage.xml";

    public static String ANALYZE_CHART_DEF_SENSED_CAUTION_MOVE_AVERAGE = "analyzeChartDefSensedCautionMoveAverage.xml";

    /**
     * MT法異常診断
     */
    public static String ANALYZE_CHART_DEF_MD_VALUE = "analyzeChartDefMdValue.xml";

    public static String ANALYZE_CHART_DEF_MD_VALUE_INCREASE = "analyzeChartDefMdValueIncrease.xml";

    /**
     * ４：閾値
     */
    public static String ANALYZE_CHART_DEF_SENSED_TROUBLE_LIMIT_VALUE = "analyzeChartDefSensedTroubleLimitValue.xml";
    public static String ANALYZE_CHART_DEF_SENSED_ALARM_LIMIT_VALUE = "analyzeChartDefSensedAlarmLimitValue.xml";
    public static String ANALYZE_CHART_DEF_SENSED_CAUTION_LIMIT_VALUE = "analyzeChartDefSensedCautionLimitValue.xml";

    /**
     * ５：移動平均
     */
    public static String ANALYZE_CHART_DEF_SENSED_TROUBLE_TAKE_OFF_MOVE_AVERAGE = "analyzeChartDefSensedTroubleTakeoffMoveAverage.xml";
    public static String ANALYZE_CHART_DEF_SENSED_ALARM_TAKE_OFF_MOVE_AVERAGE = "analyzeChartDefSensedAlarmTakeoffMoveAverage.xml";
    public static String ANALYZE_CHART_DEF_SENSED_CAUTION_TAKE_OFF_MOVE_AVERAGE = "analyzeChartDefSensedCautionTakeoffMoveAverage.xml";

    /**
     * ６：移動分散
     */
    public static String ANALYZE_CHART_DEF_SENSED_TROUBLE_DISPERSION = "analyzeChartDefSensedTroubleDispersion.xml";
    public static String ANALYZE_CHART_DEF_SENSED_ALARM_DISPERSION = "analyzeChartDefSensedAlarmDispersion.xml";
    public static String ANALYZE_CHART_DEF_SENSED_CAUTION_DISPERSION = "analyzeChartDefSensedCautionDispersion.xml";

    /**
     * ７：ベースカーブ
     */
    public static String ANALYZE_CHART_DEF_SENSED_TROUBLE_BASE_CURVE = "analyzeChartDefSensedTroubleBaseCurve.xml";
    public static String ANALYZE_CHART_DEF_SENSED_ALARM_BASE_CURVE = "analyzeChartDefSensedAlarmBaseCurve.xml";
    public static String ANALYZE_CHART_DEF_SENSED_CAUTION_BASE_CURVE = "analyzeChartDefSensedCautionBaseCurve.xml";
    public static String ANALYZE_CHART_DEF_BASE_CURVE = "analyzeChartDefBaseCurve.xml";

    /**
     * ３：集約値
     */
    public static String ANALYZE_CHART_DEF_SENSED_TROUBLE_SUMMARY = "analyzeChartDefSensedTroubleSummary.xml";
    public static String ANALYZE_CHART_DEF_SENSED_ALARM_SUMMARY = "analyzeChartDefSensedAlarmSummary.xml";
    public static String ANALYZE_CHART_DEF_SENSED_CAUTION_SUMMARY = "analyzeChartDefSensedCautionSummary.xml";

}
