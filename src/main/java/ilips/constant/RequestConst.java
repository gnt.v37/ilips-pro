package ilips.constant;

/**
 * リクエスト定数定義クラス
 */
public class RequestConst {
    /** ログイン表示情報 */
    public static final String LOGIN_DISP_INFO = "loginDispInfo";

    /** 遷移元 */
    public static final String FORWARD_FROM = "from";

    /**
     * ユーティリティクラスのため、コンストラクタは使用禁止。
     */
    private RequestConst() {
    }
}
