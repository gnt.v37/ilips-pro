package ilips.constant;

/**
 * セッション定数定義クラス
 */
public class SessionConst {
    /** 言語 */
    public static final String LANGUAGE = "language";

    /** ログイン情報 */
    public static final String LOGIN_INFO = "loginInfo";

    /** 製品選択情報 */
    public static final String PRODUCT_SELECT_INFO = "productSelectInfo";

    /** FTP管理情報 */
    public static final String FTP_MANAGER = "ftpManager";

    /** CSIGSのFTP管理情報 */
    public static final String CSIGS_FTP_MANAGER = "csigsFtpManager";
    /** CSIGSのトレンドグラフデータ */
    public static final String CSIGS_TREND_CHART_DATA = "csigsTrendChartData";

    /** アラーム履歴一覧情報 */
    public static final String ALARM_LOG_LIST = "alarmLogList";
    /** アラーム一覧ページャー情報 */
    public static final String ALARM_LOG_LIST_PAGER_INFO = "alarmLogListPagerInfo";
    /** アラーム一覧情報 */
    public static final String ALARM_LOG_OCCUR_CONFIRM_CHKLIST = "alarmLogOccurConfirmChklist";
    /** アラーム一覧情報 */
    public static final String ALARM_LOG_RECOVER_CONFIRM_CHKLIST = "alarmLogRecoverConfirmChklist";

    /** アラーム一覧情報 */
    public static final String ALARM_LIST = "alarmList";

    /** アラーム一覧ページャー情報 */
    public static final String ALARM_LIST_PAGER_INFO = "alarmListPagerInfo";

    /** アラーム一覧情報 */
    public static final String ALARM_OCCUR_CONFIRM_CHKLIST = "alarmOccurConfirmChklist";

    /** アラーム一覧情報 */
    public static final String ALARM_RECOVER_CONFIRM_CHKLIST = "alarmRecoverConfirmChklist";

    /** クッキー名(最新ログイン) */
    public static final String COOKIE_LATEST_LOGIN_NAME = "rmpfLatestLogin";

    /** クッキー名(現在ログイン) */
    public static final String COOKIE_CURRENT_LOGIN_NAME = "rmpfCurrentLogin";

    /** クッキー名(強制ログアウト) */
    public static final String COOKIE_IS_LOGOUT = "rmpfIsLogout";

    /** クッキー名(解像度) */
    public static final String COOKIE_RESOLUTION = "rmpfResolution";

    /** クッキー名(言語) */
    public static final String COOKIE_LANGUAGE = "language";

    /** クッキー名(認証方法) */
    public static final String COOKIE_CERTIFY_METHOD = "certifyMethod";

    /** 部品交換日時リスト */
    public static final String EXCHANGE_PARTS_DATE_LIST = "exchangePartsDateList";

    /** グラフ機能呼び出し元 */
    public static final String CHART_FUNCTION_TYPE = "chartFunctionType";

    /** セッションタイムアウト更新周期 */
    public static final String SESSION_TIMEOUT_UPDATE_INTERVAL = "sessionTimeoutUpdateInterval";

    /** バッチデータ検索　ワークパターン */
    public static final String BATCH_DATA_SEARCH_WORK_PATTERN = "batchDataSearchWorkPattern";

    /** バッチデータ検索　期間Start */
    public static final String BATCH_DATA_SEARCH_START_DATE = "batchDataSearchStartDate";

    /** バッチデータ検索　期間End */
    public static final String BATCH_DATA_SEARCH_END_DATE = "batchDataSearchEndDate";

    /** バッチ処理帳票　ファイル名 */
    public static final String BATCH_DATA_FILE_NAME = "batchDataFileName";

    /** バッチ処理帳票　ワークブック */
    public static final String BATCH_DATA_WORK_BOOK = "batchDataWorkBook";

    /** 前画面スタック */
    public static final String REFERER_STACK = "referStack";

    /** メンテナンスレポート入力内容 */
    public static final String MAINTENANCE_REPORT_DATA = "maintenanceReportData";

    /** 共有グラフグループ */
    public static final String SHARED_CHART_GROUP = "sharedChartGroup";

    /** ブロック・リスト表示情報 */
    public static final String DISP_LIST_PATTERN = "dispListPattern";

    /** お客様情報 */
    public static final String USER_SELECT_INFO = "userInfo";

    /** 設置場所情報 */
    public static final String LOCATION_SELECT_INFO = "locationInfo";

    /** グラフ情報 */
    public static final String AMCHART_INFO = "amChartInfo";

    /** Excel帳票ダウンロードの進捗率(1-100) */
    public static final String ORIGINAL_REPORT_PROGRESS = "excelInfo.progress";

    /** Excel帳票ダウンロードのキャンセル要求 */
    public static final String ORIGINAL_REPORT_CANCEL = "excelInfo.cancel";

    /** Excel帳票の一時ファイルパス */
    public static final String TEMP_REPORT_EXCEL_FILE = "excelInfo.tempReportFile";

    /** Excel帳票の一時保存フォーム */
    public static final String TEMP_REPORT_EXCEL_FORM = "excelInfo.tempReportForm";

    /** 認証方法(SAML)  */
    public static final String CERTIFY_METHOD_SAML = "saml";

    /** 認証方法(パスワード)  */
    public static final String CERTIFY_METHOD_PASSWORD = "password";

    // 20171120 FPT - Add const for Preventive maintenance disp chart - ADD - Start
    public static final String ANALYZE_PARAMETER = "analyzeParameter";

    public static final String ANALYZE_RESULT_DETAIL = "analyzeResultDetail";

    /** グラフ表示期間From */
    public static final String CHART_DISP_DATE_FROM = "chartDispDateFrom";

    /** グラフ表示期間To */
    public static final String CHART_DISP_DATE_TO = "chartDispDateTo";

    /** グラフ詳細検索範囲 */
    public static final String CHART_DETAIL_RANGE_HASH = "chartDetailRangeHash";
    /**

    /** グラフゲージ設定 */
    public static final String GAUGE_CONFIG = "gaugeConfig";

    /** グラフ表示データ設定(ゲージ) */
    public static final String DISP_DATA_GAUGE = "dispDataGauge";

    /** グラフデータ種別 */
    public static final String CHART_KIND = "chartKind";

    /** グラフデータ設定 */
    public static final String CHART_DATA = "chartData";

    /** グラフ表示データ設定 */
    public static final String DISP_DATA = "dispData";

    /** グラフ表示データ設定(イベント) */
    public static final String DISP_DATA_EVENT = "dispDataEvent";

    /** グラフ軸設定 */
    public static final String AXIS_CONFIG = "axisConfig";

    /** グラフ目盛線設定 */
    public static final String SCALE_CONFIG = "scaleConfig";

    /** グラフ凡例設定 */
    public static final String LEGEND_CONFIG = "legendConfig";

    /** グラフその他設定 */
    public static final String OTHER_CONFIG = "otherConfig";

    /** 線一覧情報 */
    public static final String LINE_LIST_DATA = "lineListData";

    /** 画像一覧情報 */
    public static final String IMG_LIST_DATA = "imgListData";

    /** グラフデータ(一般) */
    public static final String CHART_DATA_GENERAL = "chartDataGeneral";

    /** グラフデータ(詳細) */
    public static final String CHART_DATA_DETAIL = "chartDataDetail";

    /** グラフ表示データ設定(スキャッタープロット) */
    public static final String DISP_DATA_SCATTER_PLOT = "dispDataScatterPlot";

    // 20171120 FPT - Add const for Preventive maintenance disp chart - ADD - End

    // 20180419 FPT - thread safe session - MOD - Start
    /**
     * Is csv output
     */
    public static final String IS_CSV_OUTPUT = "isCsvOutput";

	/**
     * Is file output
     */
    public static final String IS_FILE_OUTPUT = "isFileOutput";

    /**
     * Is json output
     */
    public static final String IS_JSON_OUTPUT = "isJsonOutput";
    // 20180419 FPT - thread safe session - MOD - End

    /**
     * コンストラクタは使用禁止。
     */
    private SessionConst() {
    }
}
