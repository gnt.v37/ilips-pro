package ilips.constant;

/**
 * アプリケーション共通定数定義クラス
 */
public class AppConst {
    /** ページャーのデフォルトの表示件数 */
    public static final int PAGER_DEF_DISP_CNT = 20;

    /** DBのtrue値 */
    public static final int DB_TRUE_VALUE = 1;

    /** DBのfalse値 */
    public static final int DB_FALSE_VALUE = 0;

    /** 日付フォーマット */
    public static final String DATE_FORMAT_DATE = "yyyy/MM/dd";

    /** 日付フォーマット */
    public static final String DATE_FORMAT_YEAR_MONTH = "yyyy/MM";

    /** 日付フォーマット */
    public static final String DATE_FORMAT_MONTH_DATE_TIME = "MM/dd HH:mm";

    /** 日時フォーマット */
    public static final String DATE_FORMAT_DATE_TIME = "yyyy/MM/dd HH:mm:ss";

    /** 日時フォーマット */
    public static final String DATE_FORMAT_DATE_TIME_EN = "MM/dd/yyyy HH:mm:ss";

    /** 日時フォーマット */
    public static final String DATE_FORMAT_DATE_TIME_NO_SEC = "yyyy/MM/dd HH:mm";

    /** 時間フォーマット */
    public static final String DATE_FORMAT_TIME = "HH:mm:ss";

    /** 日付フォーマット区切りなし */
    public static final String DATE_FORMAT_DATE_NOT_SEPARATE = "yyyyMMdd";

    /** 日時フォーマット区切りなし */
    public static final String DATE_FORMAT_DATE_TIME_NOT_SEPARATE = "yyyyMMddHHmmss";

    /** 年月フォーマット区切りなし */
    public static final String DATE_FORMAT_YYYY_MM_NOT_SEPARATE = "yyyyMM";

    /** 年月フォーマット区切りなし */
    public static final String DATE_FORMAT_YYYY_MM = "yyyy/MM";

    /** 年月フォーマット区切りなし */
    public static final String DATE_FORMAT_YYYY = "yyyy";

    /** 年月フォーマット区切りなし */
    public static final String DATE_FORMAT_MM = "MM";

    /** 年月フォーマット区切りなし */
    public static final String DATE_FORMAT_D = "d";

    /** JDBC日時フォーマット */
    public static final String DATE_FORMAT_DATE_TIME_JDBC = "yyyy-MM-dd HH:mm:ss";

    /** 製品状態正常値名称 */
    public static final String PRODUCT_STATUS_NORMAL = "word.normal";

    /** 製品状態異常値名称 */
    public static final String PRODUCT_STATUS_ABNORMAL = "word.abnormal";

    /** 確認済文言 */
    public static final String WORD_CONFIRMED = "word.confirmed";

    /** 未確認文言 */
    public static final String WORD_UNCONFIRMED = "word.unconfirmed";

    /** 製品情報正常スタイル */
    public static final String PRODUCT_STATUS_NORMAL_STYLE = "statusNormal";

    /** 製品情報異常スタイル */
    public static final String PRODUCT_STATUS_ERROR_STYLE = "statusError";

    /** 実施期限オーバースタイル */
    public static final String EXECUTE_LIMIT_OVER_STYLE = "limitOver";

    /** 推奨時期オーバースタイル */
    public static final String EXECUTE_RECOMEND_OVER_STYLE = "recomendOver";

    /** チェックボックスのtrue値 */
    public static final String CHKBOX_TRUE_VALUE = "1";

    /** 納品形態の値「1」 */
    public static final int PRODUCT_DELIVERY_LEASE = 1;

    /** 納品形態の値「2」 */
    public static final int PRODUCT_DELIVERY_PURCHASE = 2;

    /** 納品形態「リース」 */
    public static final String PRODUCT_DELIVERY_LEASE_STR = "word.lease";

    /** 納品形態「買取」 */
    public static final String PRODUCT_DELIVERY_PURCHASE_STR = "word.purchase";

    /** ボタンの無効設定 */
    public static final String STRING_DISABLED = "disabled";

    /** チェック設定 */
    public static final String STRING_CHECKED = "checked";

    /** 月 */
    public static final String STRING_MONTH = "word.month1";

    /** 自動更新間隔デフォルト値 */
    public static final int AUTO_UPDATE_DEF_INTERVAL = 60;

    /** アクセスレベル「0:アクセス不可」 */
    public static final int ACCESS_LEVEL_NOT_ACCESS = 0;

    /** アクセスレベル「1:参照可」 */
    public static final int ACCESS_LEVEL_REFERABLE = 1;

    /** アラーム表示「1:表示」 */
    public static final int DISP_ALARM = 1;

    /** 対象組織「使用組織」 */
    public static final String TARGET_ORG_CUSTOMER_COLUMN = "customer_org_id";

    /** 対象組織「メンテナンス組織」 */
    public static final String TARGET_ORG_MAINTE_COLUMN = "mainte_org_id";

    /** 対象組織「所有組織」 */
    public static final String TARGET_ORG_OWNER_COLUMN = "owner_org_id";

    /** 部品寿命種別 ヶ月 */
    public static final int PARTS_LIMIT_TYPE_MONTH = 3;

    // 20180106 FPT - Add limit for image - ADD – Start
    /** 製品画像ファイルサイズ制限 */
    public static final int BASEINFO_IMG_FILE_SIZE_LIMIT = 10;

    /** 画像ファイルフォーマット配列 */
    public static final String[] IMG_FILE_FORMAT_ARY = { "gif", "jpg", "jpeg", "png" };
    // 20180106 FPT - Add limit for image - ADD – End

    /** 日付単位：時間 */
    public static final String UNIT_HOUR = "HH";

    /** 日付単位：日 */
    public static final String UNIT_DAY = "dd";

    /** 日付単位：週 */
    public static final String UNIT_WEEKLY = "ww";

    /** 日付単位：月 */
    public static final String UNIT_MONTH = "MM";

    /** データ種別(選択された値) */
    public static final String DATA_KIND_SELECT = "select";

    /** データ種別(平均値) */
    public static final String DATA_KIND_AVE = "ave";

    /** データ種別(標準偏差) */
    public static final String DATA_KIND_SD = "sd";

    /** データ種別(最小値) */
    public static final String DATA_KIND_MIN = "min";

    /** データ種別(最大値) */
    public static final String DATA_KIND_MAX = "max";

    /** データ種別(区間開始日時の値) */
    public static final String DATA_KIND_INTERVAL_START = "intervalStart";

    /** データ種別(区間終了日時の値) */
    public static final String DATA_KIND_INTERVAL_END = "intervalEnd";

    /** データ種別(区間中間日時の値) */
    public static final String DATA_KIND_INTERVAL_HALF = "intervalHalf";

    /** 間引き種別「0:間引きしない」 */
    public static final int PRESCALE_KIND_NOT = 0;

    /** 間引き種別「1:xxx毎に一点とする」 */
    public static final int PRESCALE_KIND_INTERVAL = 1;

    /** 間引き種別「2:指定区間をxxx点に間引きする」 */
    public static final int PRESCALE_KIND_POINT = 2;

    /** 間引き種別「3:指定区間の点数を1/xxxに間引きする」 */
    public static final int PRESCALE_KIND_DEVIDE = 3;

    // 言語
    /**  日本語。 */
    public static final String LOCALE_JP = "ja";
    /**  英語。 */
    public static final String LOCALE_EN = "en";

    /** 回線使用中判断間隔(分) */
    public static final int LINE_IN_USE_JUDGE_INTERVAL = 3;

    /** 回線使用中更新間隔(分) */
    public static final int LINE_IN_USE_UPDATE_INTERVAL = 1;

    /** 遷移元（グローバル監視センター） */
    public static final String FORWARD_FROM_VALUE_GLOBAL = "global";

    /** メンテナンスタイプの固定数(9) */
    public static final int MAINTE_TYPE_NUM = 9;

    /** ブロック表示 */
    public static final String DISP_LIST_BLOCK = "block";

    /** リスト表示 */
    public static final String DISP_LIST_LIST = "list";

    /** 稼働URL */
    public static final String OPERATION_URL =
            "http://133.162.234.120/ope_data_analysis/kadou_sep_req.php";

    /** 選択中タブ（製品一覧） */
    public static final String TAB_SELECT_PRODUCT = "productList";
    /** 選択中タブ（アラーム一覧） */
    public static final String TAB_SELECT_ALARM = "alarmList";
    /** 選択中タブ（グラフ全体） */
    public static final String TAB_SELECT_CHART_ALL = "chartAll";
    /** 選択中タブ（タグ値一覧） */
    public static final String TAB_SELECT_TAGVALUE = "tagValue";
    /** 選択中タブ（管理者メニュー） */
    public static final String TAB_SELECT_ADMINISTRATOR_MENU = "administrator";

    /** 製品情報選択中タブ（メンテナンス履歴） */
    public static final String TAB_PRODUCT_SELECT_MAINTE_LOG = "mainteLog";
    /** 製品情報選択中タブ（アラーム履歴） */
    public static final String TAB_PRODUCT_SELECT_ALARM_LOG = "alarmLog";
    /** 製品情報選択中タブ（製品詳細） */
    public static final String TAB_PRODUCT_SELECT_PRODUCT_BASE_INFO = "productBaseInfo";
    /** 製品情報選択中タブ（データダウンロード） */
    public static final String TAB_PRODUCT_SELECT_DOWNLOAD_DATA = "downloadData";
    /** 製品情報選択中タブ（帳票向けデータダウンロード） */
    public static final String TAB_PRODUCT_SELECT_FORM_DOWNLOAD_DATA = "formDownloadData";
    /** 製品情報選択中タブ（グラフ表示(単体)） */
    public static final String TAB_PRODUCT_SELECT_CHART_PRODUCT = "chartProduct";

    /** タグ値一覧のブロックあたりの最大行数 */
    public static final int TAGINFO_ROWS_PER_BLOCK = 10;
    /** タグ値一覧のページあたりのブロック数 */
    public static final int TAGINFO_BLOCK_COUNT = 3;

    /** メンテナンスタイプ番号 その他（メンテナンスタイプデータ未登録） */
    public static final int MAINTE_TYPE_OTHERS = -1;
    /** メンテナンスタイプ名称 その他（メンテナンスタイプデータ未登録） */
    public static final String MAINTE_TYPE_NAME_OTHERS = "その他";

    // 20171127 FPT - Add new constant - ADD - Start
    /** Selected tab (shared) */
    public static final String TAB_SELECT_CHART_GROUP = "chartGroup";

    /** Permission developer */
    public static final int PERMISSION_DEVELOPER = 800;
    // 20171127 FPT - Add new constant - ADD - End

    // 20171114 FPT - Menu tab 【解析結果一覧表示画面】 list  - Add - Start
    public static final String TAB_PRODUCT_SELECT_MAINTENANCE_LST_PRODUCT = "preventiveMaintenanceList";
    // 20171114 FPT - Menu tab 【解析結果一覧表示画面】 list  - Add - End

    // 20171114 FPT - Menu tab 【解析結果一覧表示画面】 list  - Add - Start
    public static final int APP_TARGET_LOGISTICS = 1;
    // 20171114 FPT - Menu tab 【解析結果一覧表示画面】 list  - Add - End

    // 20171114 FPT - Const Analyze - Add - Start
    public static final int ANALYZE_RESULT_ERR = 1;
    public static final String DATE_TIME_CSS_ERR = "dateTimeCssErr";
    public static final String ANALYZE_TARGET_TAG_VALUE_CSS_ERR = "analyzeTargetTagValueCssErr";
    public static final String LIMIT_VALUE_CSS_ERR = "limitValueCssErr";
    public static final int ANALYZE_RESULT_ALARM = 2;
    public static final String DATE_TIME_CSS_ALARM = "dateTimeCssAlarm";
    public static final String ANALYZE_TARGET_TAG_VALUE_CSS_ALARM = "analyzeTargetTagValueCssAlarm";
    public static final String LIMIT_VALUE_CSS_ALARM = "limitValueCssAlarm";
    public static final int ANALYZE_RESULT_CAUTION = 3;
    public static final String DATE_TIME_CSS_CAUTION = "dateTimeCssCaution";
    public static final String ANALYZE_TARGET_TAG_VALUE_CSS_CAUTION = "analyzeTargetTagValueCssCaution";
    public static final String LIMIT_VALUE_CSS_CAUTION = "limitValueCssCaution";
    public static final int ANALYZE_RESULT_INVALID_VALUE = 4;
    public static final String DATE_TIME_CSS_INVALID = "dateTimeCssInvalid";
    public static final String ANALYZE_TARGET_TAG_VALUE_CSS_INVALID = "analyzeTargetTagValueCssInvalid";
    public static final String LIMIT_VALUE_CSS_INVALID = "limitValueCssInvalid";
    public static final String DATE_TIME_CSS_NORMAL = "dateTime";
    public static final String ANALYZE_TARGET_TAG_VALUE_CSS_NORMAL = "analyzeTargetTagValue";
    public static final String LIMIT_VALUE_CSS_NORMAL = "limitValue";
    public static final int ANALYZE_TYPE_NOT_ANALYZE = 1;
    /** 現在値と移動平均の比較 */
    public static final int ANALYZE_TYPE_AVERAGE_COMPARE = 1;
    /**
     * 2:移動平均
     */
    public static final int ANALYZE_TYPE_AVERAGE = 2;
    /**
     * 3:集約値
     */
    public static final int ANALYZE_TYPE_SUMMARY_VALUE = 3;

    /**
     * 4:TakeOff種別付 現在値と閾値の比較
     */
    public static final int ANALYZE_TYPE_TAKEOFF_LIMIT_VALUE_COMPARE = 4;

    /**
     * 5:TakeOff種別付 移動平均
     */
    public static final int ANALYZE_TYPE_TAKEOFF_AVERAGE = 5;
    /**
     * 6:TakeOff種別付 移動分散
     */
    public static final int ANALYZE_TYPE_TAKEOFF_DISPERSION = 6;
    /**
     * 7:TakeOff種別付 ベースカーブ比較
     */
    public static final int ANALYZE_TYPE_TAKEOFF_BASE_CURVE = 7;
    /**
     * 8:MT法異常診断
     */
    public static final int ANALYZE_TYPE_MT = 8;

    public static final String ILLEGAL_REAL_DATA = "1.79769E308";

    /**
     * 定周期
     */
    public static final String EXECUTE_TIMING_CONSTANT_CYCLE = "3";

    /**
     * 定時（毎月）
     */
    public static final String EXECUTE_TIMING_EVERY_MONTH = "4";

    /**
     * 定時（毎週）
     */
    public static final String EXECUTE_TIMING_EVERY_WEEK = "5";

    /**
     * 定時（毎日）
     */
    public static final String EXECUTE_TIMING_EVERY_DAY = "6";

    /**
     * 定時（毎時）
     */
    public static final String EXECUTE_TIMING_EVERY_HOUR = "7";

    /** 現在値と設定値の比較 */
    public static final int ANALYZE_TYPE_CONFIG_VALUE_COMPARE = 2;

    public static final String DEFAULT_NODE_GAS = "Gas";
    public static final String DEFAULT_NODE_OIL = "Oil";

    public static final int FUEL_TYPE_OIL = 1;
    public static final int FUEL_TYPE_GAS = 2;

    public static final int CONNECTION_TYPE_A = 1;
    public static final int CONNECTION_TYPE_B = 2;
    public static final int CONNECTION_TYPE_C = 3;

    public static final String DEFAULT_NODE_GAS_A = "GasA";
    public static final String DEFAULT_NODE_GAS_B = "GasB";
    public static final String DEFAULT_NODE_GAS_C = "GasC";

    public static final String DEFAULT_NODE_OIL_A = "OilA";
    public static final String DEFAULT_NODE_OIL_B = "OilB";
    public static final String DEFAULT_NODE_OIL_C = "OilC";

    public static final int TAG_OFFSET = 1000;

    /**
     * サンプル数指定
     */
    public static final String ANALYZE_DATA_RANGE_SAMPLE = "1";

    // 20180106 FPT - Add event type - ADD - Start
    /** アラーム */
    public static final int EVENT_TYPE_ALARM = 1;
    // 20180106 FPT - Add event type - ADD - End

    public static final String VALID_STATUS = "1";

    /**
     * 定時
     */
    public static final String EXECUTE_TIMING_EVERY = "4";

    // 20171120 FPT - Add type analyse - ADD - Start
    // 予防保全のグラフ種類
    /**
     * 異常検知グラフ
     */
    public static int ANALYZE_CHART_SENSED_TROUBLE = 1;

    /**
     * 警報検知グラフ
     */
    public static int ANALYZE_CHART_SENSED_ALARM = 2;

    /**
     * 注意検知グラフ
     */
    public static int ANALYZE_CHART_SENSED_CAUTION = 3;

    /**
     * MD値グラフ
     */
    public static int ANALYZE_CHART_MD_VALUE = 4;
    /**
     * MD値増加率グラフ
     */
    public static int ANALYZE_CHART_MD_VALUE_INCREASE = 5;
    /**
     * ベースカーブグラフ
     */
    public static int ANALYZE_CHART_BASE_CURVE = 6;

    /** 画像ファイルサイズ制限 */
    public static final int IMG_FILE_SIZE_LIMIT = 3;

    /** 画像ファイル登録件数制限 */
    public static final int IMG_FILE_REGIST_CNT_LIMIT = 40;

    public static final String[][] analyzeParamItemNotAnalyze = {
            { "1", "word.configName" },
            { "2", "word.yAxisName" },
            { "3", "word.validInvalidItem" },
            { "4", "word.timing" },
            { "5", "word.analyzeKind" },
            { "6", "word.analyzeTagNo" },
            { "7", "word.invalidUpperLimit" },
            { "8", "word.invalidUpperCompCondition" },
            { "9", "word.invalidLowerLimit" },
            { "10", "word.invalidLowerCompCondition" },
            { "11", "word.filteringNum" },
            { "12", "word.filtering1ValidInvalid" },
            { "13", "word.filtering1TagId" },
            { "14", "word.filtering1UpperLimit" },
            { "15", "word.filtering1UpperCompCondition" },
            { "16", "word.filtering1LowerLimit" },
            { "17", "word.filtering1LowerCompCondition" },
            { "18", "word.filtering2ValidInvalid" },
            { "19", "word.filtering2TagId" },
            { "20", "word.filtering2UpperLimit" },
            { "21", "word.filtering2UpperCompCondition" },
            { "22", "word.filtering2LowerLimit" },
            { "23", "word.filtering2LowerCompCondition" },
            { "24", "word.filtering3ValidInvalid" },
            { "25", "word.filtering3TagId" },
            { "26", "word.filtering3UpperLimit" },
            { "27", "word.filtering3UpperCompCondition" },
            { "28", "word.filtering3LowerLimit" },
            { "29", "word.filtering3LowerCompCondition" },
            { "30", "word.filtering4ValidInvalid" },
            { "31", "word.filtering4TagId" },
            { "32", "word.filtering4UpperLimit" },
            { "33", "word.filtering4UpperCompCondition" },
            { "34", "word.filtering4LowerLimit" },
            { "35", "word.filtering4LowerCompCondition" },
            { "36", "word.filtering5ValidInvalid" },
            { "37", "word.filtering5TagId" },
            { "38", "word.filtering5UpperLimit" },
            { "39", "word.filtering5UpperCompCondition" },
            { "40", "word.filtering5LowerLimit" },
            { "41", "word.filtering5LowerCompCondition" },
            { "42", "word.sensedTroubleUpperLimit" },
            { "43", "word.sensedTroubleUpperCompCondition" },
            { "44", "word.sensedTroubleLowerLimit" },
            { "45", "word.sensedTroubleLowerCompCondition" },
            { "46", "word.sensedTroubleCompForm" },
            { "47", "word.sensedTroubleCompTagId" },
            { "48", "word.sensedTroubleCompBase" },
            { "49", "word.sensedAlarmUpperLimit" },
            { "50", "word.sensedAlarmUpperCompCondition" },
            { "51", "word.sensedAlarmLowerLimit" },
            { "52", "word.sensedAlarmLowerCompCondition" },
            { "53", "word.sensedAlarmCompForm" },
            { "54", "word.sensedAlarmCompTagId" },
            { "55", "word.sensedAlarmCompBase" },
            { "56", "word.sensedCautionUpperLimit" },
            { "57", "word.sensedCautionUpperCompCondition" },
            { "58", "word.sensedCautionLowerLimit" },
            { "59", "word.sensedCautionLowerCompCondition" },
            { "60", "word.sensedCautionCompForm" },
            { "61", "word.sensedCautionCompTagId" },
            { "62", "word.sensedCautionCompBase" },
            { "63", "word.analyzeStartDate" }
    };
    public static final String[][] analyzeParamRemarkNotAnalyze = {
            { "1", "word.configNameRemark" },
            { "2", "word.yAxisNameRemark" },
            { "3", "word.validInvalidItemRemark" },
            { "4", "word.timingRemark" },
            { "5", "word.analyzeKindRemark" },
            { "6", "word.analyzeTagNoRemark" },
            { "7", "word.invalidUpperLimitRemark" },
            { "8", "word.invalidUpperCompConditionRemark" },
            { "9", "word.invalidLowerLimitRemark" },
            { "10", "word.invalidLowerCompConditionRemark" },
            { "11", "word.filteringNumRemark" },
            { "12", "word.filtering1ValidInvalidRemark" },
            { "13", "word.filtering1TagIdRemark" },
            { "14", "word.filtering1UpperLimitRemark" },
            { "15", "word.filtering1UpperCompConditionRemark" },
            { "16", "word.filtering1LowerLimitRemark" },
            { "17", "word.filtering1LowerCompConditionRemark" },
            { "18", "word.filtering2ValidInvalidRemark" },
            { "19", "word.filtering2TagIdRemark" },
            { "20", "word.filtering2UpperLimitRemark" },
            { "21", "word.filtering2UpperCompConditionRemark" },
            { "22", "word.filtering2LowerLimitRemark" },
            { "23", "word.filtering2LowerCompConditionRemark" },
            { "24", "word.filtering3ValidInvalidRemark" },
            { "25", "word.filtering3TagIdRemark" },
            { "26", "word.filtering3UpperLimitRemark" },
            { "27", "word.filtering3UpperCompConditionRemark" },
            { "28", "word.filtering3LowerLimitRemark" },
            { "29", "word.filtering3LowerCompConditionRemark" },
            { "30", "word.filtering4ValidInvalidRemark" },
            { "31", "word.filtering4TagIdRemark" },
            { "32", "word.filtering4UpperLimitRemark" },
            { "33", "word.filtering4UpperCompConditionRemark" },
            { "34", "word.filtering4LowerLimitRemark" },
            { "35", "word.filtering4LowerCompConditionRemark" },
            { "36", "word.filtering5ValidInvalidRemark" },
            { "37", "word.filtering5TagIdRemark" },
            { "38", "word.filtering5UpperLimitRemark" },
            { "39", "word.filtering5UpperCompConditionRemark" },
            { "40", "word.filtering5LowerLimitRemark" },
            { "41", "word.filtering5LowerCompConditionRemark" },
            { "42", "word.sensedTroubleUpperLimitRemark" },
            { "43", "word.sensedTroubleUpperCompConditionRemark" },
            { "44", "word.sensedTroubleLowerLimitRemark" },
            { "45", "word.sensedTroubleLowerCompConditionRemark" },
            { "46", "word.sensedTroubleCompFormRemark" },
            { "47", "word.sensedTroubleCompTagIdRemark" },
            { "48", "word.sensedTroubleCompBaseRemark" },
            { "49", "word.sensedAlarmUpperLimitRemark" },
            { "50", "word.sensedAlarmUpperCompConditionRemark" },
            { "51", "word.sensedAlarmLowerLimitRemark" },
            { "52", "word.sensedAlarmLowerCompConditionRemark" },
            { "53", "word.sensedAlarmCompFormRemark" },
            { "54", "word.sensedAlarmCompTagIdRemark" },
            { "55", "word.sensedAlarmCompBaseRemark" },
            { "56", "word.sensedCautionUpperLimitRemark" },
            { "57", "word.sensedCautionUpperCompConditionRemark" },
            { "58", "word.sensedCautionLowerLimitRemark" },
            { "59", "word.sensedCautionLowerCompConditionRemark" },
            { "60", "word.sensedCautionCompFormRemark" },
            { "61", "word.sensedCautionCompTagIdRemark" },
            { "62", "word.sensedCautionCompBaseRemark" },
            { "63", "word.analyzeStartDateRemark" }

    };
    public static final String[][] analyzeParamItemMoveAverage = {
            { "1", "word.configName" },
            { "2", "word.yAxisName" },
            { "3", "word.validInvalidItem" },
            { "4", "word.timing" },
            { "5", "word.analyzeKind" },
            { "6", "word.analyzeTagNo" },
            { "7", "word.invalidUpperLimit" },
            { "8", "word.invalidUpperCompCondition" },
            { "9", "word.invalidLowerLimit" },
            { "10", "word.invalidLowerCompCondition" },
            { "11", "word.moveAverageScopeForm" },
            { "12", "word.moveAverageDataNum" },
            { "13", "word.moveAverageDataSpan" },
            { "14", "word.moveAverageDataSpanUnit" },
            { "15", "word.filteringNum" },
            { "16", "word.filtering1ValidInvalid" },
            { "17", "word.filtering1TagId" },
            { "18", "word.filtering1UpperLimit" },
            { "19", "word.filtering1UpperCompCondition" },
            { "20", "word.filtering1LowerLimit" },
            { "21", "word.filtering1LowerCompCondition" },
            { "22", "word.filtering2ValidInvalid" },
            { "23", "word.filtering2TagId" },
            { "24", "word.filtering2UpperLimit" },
            { "25", "word.filtering2UpperCompCondition" },
            { "26", "word.filtering2LowerLimit" },
            { "27", "word.filtering2LowerCompCondition" },
            { "28", "word.filtering3ValidInvalid" },
            { "29", "word.filtering3TagId" },
            { "30", "word.filtering3UpperLimit" },
            { "31", "word.filtering3UpperCompCondition" },
            { "32", "word.filtering3LowerLimit" },
            { "33", "word.filtering3LowerCompCondition" },
            { "34", "word.filtering4ValidInvalid" },
            { "35", "word.filtering4TagId" },
            { "36", "word.filtering4UpperLimit" },
            { "37", "word.filtering4UpperCompCondition" },
            { "38", "word.filtering4LowerLimit" },
            { "39", "word.filtering4LowerCompCondition" },
            { "40", "word.filtering5ValidInvalid" },
            { "41", "word.filtering5TagId" },
            { "42", "word.filtering5UpperLimit" },
            { "43", "word.filtering5UpperCompCondition" },
            { "44", "word.filtering5LowerLimit" },
            { "45", "word.filtering5LowerCompCondition" },
            { "46", "word.sensedTroubleUpperLimit" },
            { "47", "word.sensedTroubleUpperCompCondition" },
            { "48", "word.sensedTroubleLowerLimit" },
            { "49", "word.sensedTroubleLowerCompCondition" },
            { "50", "word.sensedAlarmUpperLimit" },
            { "51", "word.sensedAlarmUpperCompCondition" },
            { "52", "word.sensedAlarmLowerLimit" },
            { "53", "word.sensedAlarmLowerCompCondition" },
            { "54", "word.sensedCautionUpperLimit" },
            { "55", "word.sensedCautionUpperCompCondition" },
            { "56", "word.sensedCautionLowerLimit" },
            { "57", "word.sensedCautionLowerCompCondition" },
            { "58", "word.analyzeStartDate" },
            { "59", "word.maintenanceDate" }
    };
    public static final String[][] analyzeParamRemarkMoveAverage = {
            { "1", "word.configNameRemark" },
            { "2", "word.yAxisNameRemark" },
            { "3", "word.validInvalidItemRemark" },
            { "4", "word.timingRemark" },
            { "5", "word.analyzeKindRemark" },
            { "6", "word.analyzeTagNoRemark" },
            { "7", "word.invalidUpperLimitRemark" },
            { "8", "word.invalidUpperCompConditionRemark" },
            { "9", "word.invalidLowerLimitRemark" },
            { "10", "word.invalidLowerCompConditionRemark" },
            { "11", "word.moveAverageScopeFormRemark" },
            { "12", "word.moveAverageDataNumRemark" },
            { "13", "word.moveAverageDataSpanRemark" },
            { "14", "word.moveAverageDataSpanUnitRemark" },
            { "15", "word.filteringNumRemark" },
            { "16", "word.filtering1ValidInvalidRemark" },
            { "17", "word.filtering1TagIdRemark" },
            { "18", "word.filtering1UpperLimitRemark" },
            { "19", "word.filtering1UpperCompConditionRemark" },
            { "20", "word.filtering1LowerLimitRemark" },
            { "21", "word.filtering1LowerCompConditionRemark" },
            { "22", "word.filtering2ValidInvalidRemark" },
            { "23", "word.filtering2TagIdRemark" },
            { "24", "word.filtering2UpperLimitRemark" },
            { "25", "word.filtering2UpperCompConditionRemark" },
            { "26", "word.filtering2LowerLimitRemark" },
            { "27", "word.filtering2LowerCompConditionRemark" },
            { "28", "word.filtering3ValidInvalidRemark" },
            { "29", "word.filtering3TagIdRemark" },
            { "30", "word.filtering3UpperLimitRemark" },
            { "31", "word.filtering3UpperCompConditionRemark" },
            { "32", "word.filtering3LowerLimitRemark" },
            { "33", "word.filtering3LowerCompConditionRemark" },
            { "34", "word.filtering4ValidInvalidRemark" },
            { "35", "word.filtering4TagIdRemark" },
            { "36", "word.filtering4UpperLimitRemark" },
            { "37", "word.filtering4UpperCompConditionRemark" },
            { "38", "word.filtering4LowerLimitRemark" },
            { "39", "word.filtering4LowerCompConditionRemark" },
            { "40", "word.filtering5ValidInvalidRemark" },
            { "41", "word.filtering5TagIdRemark" },
            { "42", "word.filtering5UpperLimitRemark" },
            { "43", "word.filtering5UpperCompConditionRemark" },
            { "44", "word.filtering5LowerLimitRemark" },
            { "45", "word.filtering5LowerCompConditionRemark" },
            { "46", "word.sensedTroubleUpperLimitRemark" },
            { "47", "word.sensedTroubleUpperCompConditionRemark" },
            { "48", "word.sensedTroubleLowerLimitRemark" },
            { "49", "word.sensedTroubleLowerCompConditionRemark" },
            { "50", "word.sensedAlarmUpperLimitRemark" },
            { "51", "word.sensedAlarmUpperCompConditionRemark" },
            { "52", "word.sensedAlarmLowerLimitRemark" },
            { "53", "word.sensedAlarmLowerCompConditionRemark" },
            { "54", "word.sensedCautionUpperLimitRemark" },
            { "55", "word.sensedCautionUpperCompConditionRemark" },
            { "56", "word.sensedCautionLowerLimitRemark" },
            { "57", "word.sensedCautionLowerCompConditionRemark" },
            { "58", "word.analyzeStartDateRemark" },
            { "59", "word.maintenanceDateRemark" }
    };

    public static final String[][] analyzeParamItemSummaryValue = {
            { "1", "word.configName" },
            { "2", "word.yAxisName" },
            { "3", "word.analyzeKind" },
            { "4", "word.timing" },
            { "5", "word.analyzeTagNo" },
            { "6", "word.invalidUpperLimit" },
            { "7", "word.invalidUpperCompCondition" },
            { "8", "word.invalidLowerLimit" },
            { "9", "word.invalidLowerCompCondition" },
            { "10", "word.moveAverageCalcConf" },
            { "11", "word.moveAverageScopeForm" },
            { "12", "word.moveAverageDataNum" },
            { "13", "word.moveAverageDataSpan" },
            { "14", "word.moveAverageDataSpanUnit" },
            { "15", "word.filteringNum" },
            { "16", "word.filtering1ValidInvalid" },
            { "17", "word.filtering1TagId" },
            { "18", "word.filtering1UpperLimit" },
            { "19", "word.filtering1UpperCompCondition" },
            { "20", "word.filtering1LowerLimit" },
            { "21", "word.filtering1LowerCompCondition" },
            { "22", "word.filtering2ValidInvalid" },
            { "23", "word.filtering2TagId" },
            { "24", "word.filtering2UpperLimit" },
            { "25", "word.filtering2UpperCompCondition" },
            { "26", "word.filtering2LowerLimit" },
            { "27", "word.filtering2LowerCompCondition" },
            { "28", "word.filtering3ValidInvalid" },
            { "29", "word.filtering3TagId" },
            { "30", "word.filtering3UpperLimit" },
            { "31", "word.filtering3UpperCompCondition" },
            { "32", "word.filtering3LowerLimit" },
            { "33", "word.filtering3LowerCompCondition" },
            { "34", "word.filtering4ValidInvalid" },
            { "35", "word.filtering4TagId" },
            { "36", "word.filtering4UpperLimit" },
            { "37", "word.filtering4UpperCompCondition" },
            { "38", "word.filtering4LowerLimit" },
            { "39", "word.filtering4LowerCompCondition" },
            { "40", "word.filtering5ValidInvalid" },
            { "41", "word.filtering5TagId" },
            { "42", "word.filtering5UpperLimit" },
            { "43", "word.filtering5UpperCompCondition" },
            { "44", "word.filtering5LowerLimit" },
            { "45", "word.filtering5LowerCompCondition" },
            { "46", "word.sensedTroubleType" },
            { "47", "word.sensedTroubleUpperLimit" },
            { "48", "word.sensedTroubleUpperCompCondition" },
            { "49", "word.sensedTroubleLowerLimit" },
            { "50", "word.sensedTroubleLowerCompCondition" },
            { "51", "word.sensedAlarmType" },
            { "52", "word.sensedAlarmUpperLimit" },
            { "53", "word.sensedAlarmUpperCompCondition" },
            { "54", "word.sensedAlarmLowerLimit" },
            { "55", "word.sensedAlarmLowerCompCondition" },
            { "56", "word.sensedCautionType" },
            { "57", "word.sensedCautionUpperLimit" },
            { "58", "word.sensedCautionUpperCompCondition" },
            { "59", "word.sensedCautionLowerLimit" },
            { "60", "word.sensedCautionLowerCompCondition" },
            { "61", "word.analyzeStartDate" },
            { "62", "word.comment" }
    };
    public static final String[][] analyzeParamRemarkSummaryValue = {
            { "1", "word.configNameRemark" },
            { "2", "word.yAxisNameRemark" },
            { "3", "word.analyzeKindRemark" },
            { "4", "word.timingRemark" },
            { "5", "word.analyzeTagNoRemark" },
            { "6", "word.invalidUpperLimitRemark" },
            { "7", "word.invalidUpperCompConditionRemark" },
            { "8", "word.invalidLowerLimitRemark" },
            { "9", "word.invalidLowerCompConditionRemark" },
            { "10", "word.moveAverageCalcConfRemark" },
            { "11", "word.moveAverageScopeFormRemark" },
            { "12", "word.moveAverageDataNumRemark" },
            { "13", "word.moveAverageDataSpanRemark" },
            { "14", "word.moveAverageDataSpanUnitRemark" },
            { "15", "word.filteringNumRemark" },
            { "16", "word.filtering1ValidInvalidRemark" },
            { "17", "word.filtering1TagIdRemark" },
            { "18", "word.filtering1UpperLimitRemark" },
            { "19", "word.filtering1UpperCompConditionRemark" },
            { "20", "word.filtering1LowerLimitRemark" },
            { "21", "word.filtering1LowerCompConditionRemark" },
            { "22", "word.filtering2ValidInvalidRemark" },
            { "23", "word.filtering2TagIdRemark" },
            { "24", "word.filtering2UpperLimitRemark" },
            { "25", "word.filtering2UpperCompConditionRemark" },
            { "26", "word.filtering2LowerLimitRemark" },
            { "27", "word.filtering2LowerCompConditionRemark" },
            { "28", "word.filtering3ValidInvalidRemark" },
            { "29", "word.filtering3TagIdRemark" },
            { "30", "word.filtering3UpperLimitRemark" },
            { "31", "word.filtering3UpperCompConditionRemark" },
            { "32", "word.filtering3LowerLimitRemark" },
            { "33", "word.filtering3LowerCompConditionRemark" },
            { "34", "word.filtering4ValidInvalidRemark" },
            { "35", "word.filtering4TagIdRemark" },
            { "36", "word.filtering4UpperLimitRemark" },
            { "37", "word.filtering4UpperCompConditionRemark" },
            { "38", "word.filtering4LowerLimitRemark" },
            { "39", "word.filtering4LowerCompConditionRemark" },
            { "40", "word.filtering5ValidInvalidRemark" },
            { "41", "word.filtering5TagIdRemark" },
            { "42", "word.filtering5UpperLimitRemark" },
            { "43", "word.filtering5UpperCompConditionRemark" },
            { "44", "word.filtering5LowerLimitRemark" },
            { "45", "word.filtering5LowerCompConditionRemark" },
            { "46", "word.sensedTroubleTypeRemark" },
            { "47", "word.sensedTroubleUpperLimitRemark" },
            { "48", "word.sensedTroubleUpperCompConditionRemark" },
            { "49", "word.sensedTroubleLowerLimitRemark" },
            { "50", "word.sensedTroubleLowerCompConditionRemark" },
            { "51", "word.sensedAlarmTypeRemark" },
            { "52", "word.sensedAlarmUpperLimitRemark" },
            { "53", "word.sensedAlarmUpperCompConditionRemark" },
            { "54", "word.sensedAlarmLowerLimitRemark" },
            { "55", "word.sensedAlarmLowerCompConditionRemark" },
            { "56", "word.sensedCautionTypeRemark" },
            { "57", "word.sensedCautionUpperLimitRemark" },
            { "58", "word.sensedCautionUpperCompConditionRemark" },
            { "59", "word.sensedCautionLowerLimitRemark" },
            { "60", "word.sensedCautionLowerCompConditionRemark" },
            { "61", "word.analyzeStartDateRemark" },
            { "62", "word.commentRemark" }
    };

    // 4:閾値比較
    public static final String[][] analyzeParamItemLimitValue = {
            { "1", "word.configName" },
            { "2", "word.yAxisName" },
            { "3", "word.analyzeKind" },
            { "4", "word.timing" },
            { "5", "word.analyzeTagNo" },
            { "6", "word.invalidUpperLimit" },
            { "7", "word.invalidUpperCompCondition" },
            { "8", "word.invalidLowerLimit" },
            { "9", "word.invalidLowerCompCondition" },
            { "10", "word.toTypeFilteringValidInvalid" },
            { "11", "word.T2TagId2" },
            { "12", "word.N1C2TagId2" },
            { "13", "word.ToType" },
            { "14", "word.filteringNum" },
            { "15", "word.filtering1ValidInvalid" },
            { "16", "word.filtering1TagId" },
            { "17", "word.filtering1UpperLimit" },
            { "18", "word.filtering1UpperCompCondition" },
            { "19", "word.filtering1LowerLimit" },
            { "20", "word.filtering1LowerCompCondition" },
            { "21", "word.filtering2ValidInvalid" },
            { "22", "word.filtering2TagId" },
            { "23", "word.filtering2UpperLimit" },
            { "24", "word.filtering2UpperCompCondition" },
            { "25", "word.filtering2LowerLimit" },
            { "26", "word.filtering2LowerCompCondition" },
            { "27", "word.filtering3ValidInvalid" },
            { "28", "word.filtering3TagId" },
            { "29", "word.filtering3UpperLimit" },
            { "30", "word.filtering3UpperCompCondition" },
            { "31", "word.filtering3LowerLimit" },
            { "32", "word.filtering3LowerCompCondition" },
            { "33", "word.filtering4ValidInvalid" },
            { "34", "word.filtering4TagId" },
            { "35", "word.filtering4UpperLimit" },
            { "36", "word.filtering4UpperCompCondition" },
            { "37", "word.filtering4LowerLimit" },
            { "38", "word.filtering4LowerCompCondition" },
            { "39", "word.filtering5ValidInvalid" },
            { "40", "word.filtering5TagId" },
            { "41", "word.filtering5UpperLimit" },
            { "42", "word.filtering5UpperCompCondition" },
            { "43", "word.filtering5LowerLimit" },
            { "44", "word.filtering5LowerCompCondition" },
            { "45", "word.sensedTroubleUpperLimit" },
            { "46", "word.sensedTroubleUpperCompCondition" },
            { "47", "word.sensedTroubleLowerLimit" },
            { "48", "word.sensedTroubleLowerCompCondition" },
            { "49", "word.sensedAlarmUpperLimit" },
            { "50", "word.sensedAlarmUpperCompCondition" },
            { "51", "word.sensedAlarmLowerLimit" },
            { "52", "word.sensedAlarmLowerCompCondition" },
            { "53", "word.sensedCautionUpperLimit" },
            { "54", "word.sensedCautionUpperCompCondition" },
            { "55", "word.sensedCautionLowerLimit" },
            { "56", "word.sensedCautionLowerCompCondition" },
            { "57", "word.analyzeStartDate" },
            { "58", "word.lastUpdateDate" },
            { "59", "word.comment" }
    };
    public static final String[][] analyzeParamRemarkLimitValue = {
            { "1", "word.configNameRemark" },
            { "2", "word.yAxisNameRemark" },
            { "3", "word.analyzeKindRemark" },
            { "4", "word.timingRemark" },
            { "5", "word.analyzeTagNoRemark" },
            { "6", "word.invalidUpperLimitRemark" },
            { "7", "word.invalidUpperCompConditionRemark" },
            { "8", "word.invalidLowerLimitRemark" },
            { "9", "word.invalidLowerCompConditionRemark" },
            { "10", "word.toTypeFilteringValidInvalidRemark" },
            { "11", "word.T2TagId2Remark" },
            { "12", "word.N1C2TagId2Remark" },
            { "13", "word.ToTypeRemark" },
            { "14", "word.filteringNumRemark" },
            { "15", "word.filtering1ValidInvalidRemark" },
            { "16", "word.filtering1TagIdRemark" },
            { "17", "word.filtering1UpperLimitRemark" },
            { "18", "word.filtering1UpperCompConditionRemark" },
            { "19", "word.filtering1LowerLimitRemark" },
            { "20", "word.filtering1LowerCompConditionRemark" },
            { "21", "word.filtering2ValidInvalidRemark" },
            { "22", "word.filtering2TagIdRemark" },
            { "23", "word.filtering2UpperLimitRemark" },
            { "24", "word.filtering2UpperCompConditionRemark" },
            { "25", "word.filtering2LowerLimitRemark" },
            { "26", "word.filtering2LowerCompConditionRemark" },
            { "27", "word.filtering3ValidInvalidRemark" },
            { "28", "word.filtering3TagIdRemark" },
            { "29", "word.filtering3UpperLimitRemark" },
            { "30", "word.filtering3UpperCompConditionRemark" },
            { "31", "word.filtering3LowerLimitRemark" },
            { "32", "word.filtering3LowerCompConditionRemark" },
            { "33", "word.filtering4ValidInvalidRemark" },
            { "34", "word.filtering4TagIdRemark" },
            { "35", "word.filtering4UpperLimitRemark" },
            { "36", "word.filtering4UpperCompConditionRemark" },
            { "37", "word.filtering4LowerLimitRemark" },
            { "38", "word.filtering4LowerCompConditionRemark" },
            { "39", "word.filtering5ValidInvalidRemark" },
            { "40", "word.filtering5TagIdRemark" },
            { "41", "word.filtering5UpperLimitRemark" },
            { "42", "word.filtering5UpperCompConditionRemark" },
            { "43", "word.filtering5LowerLimitRemark" },
            { "44", "word.filtering5LowerCompConditionRemark" },
            { "45", "word.sensedTroubleUpperLimitRemark" },
            { "46", "word.sensedTroubleUpperCompConditionRemark" },
            { "47", "word.sensedTroubleLowerLimitRemark" },
            { "48", "word.sensedTroubleLowerCompConditionRemark" },
            { "49", "word.sensedAlarmUpperLimitRemark" },
            { "50", "word.sensedAlarmUpperCompConditionRemark" },
            { "51", "word.sensedAlarmLowerLimitRemark" },
            { "52", "word.sensedAlarmLowerCompConditionRemark" },
            { "53", "word.sensedCautionUpperLimitRemark" },
            { "54", "word.sensedCautionUpperCompConditionRemark" },
            { "55", "word.sensedCautionLowerLimitRemark" },
            { "56", "word.sensedCautionLowerCompConditionRemark" },
            { "57", "word.analyzeStartDateRemark" },
            { "58", "word.lastUpdateDateRemark" },
            { "59", "word.commentRemark" }
    };
    //	5:Takeoff移動平均
    public static final String[][] analyzeParamItemTakeoffMoveAverage = {
            { "1", "word.configName" },
            { "2", "word.yAxisName" },
            { "3", "word.analyzeKind" },
            { "4", "word.timing" },
            { "5", "word.analyzeTagNo" },
            { "6", "word.invalidUpperLimit" },
            { "7", "word.invalidUpperCompCondition" },
            { "8", "word.invalidLowerLimit" },
            { "9", "word.invalidLowerCompCondition" },
            { "10", "word.moveAverageScopeForm" },
            { "11", "word.moveAverageDataNum" },
            { "12", "word.moveAverageDataSpan" },
            { "13", "word.moveAverageDataSpanUnit" },
            { "14", "word.toTypeFilteringValidInvalid" },
            { "15", "word.T2TagId2" },
            { "16", "word.N1C2TagId2" },
            { "17", "word.ToType" },
            { "18", "word.filteringNum" },
            { "19", "word.filtering1ValidInvalid" },
            { "20", "word.filtering1TagId" },
            { "21", "word.filtering1UpperLimit" },
            { "22", "word.filtering1UpperCompCondition" },
            { "23", "word.filtering1LowerLimit" },
            { "24", "word.filtering1LowerCompCondition" },
            { "25", "word.filtering2ValidInvalid" },
            { "26", "word.filtering2TagId" },
            { "27", "word.filtering2UpperLimit" },
            { "28", "word.filtering2UpperCompCondition" },
            { "29", "word.filtering2LowerLimit" },
            { "30", "word.filtering2LowerCompCondition" },
            { "31", "word.filtering3ValidInvalid" },
            { "32", "word.filtering3TagId" },
            { "33", "word.filtering3UpperLimit" },
            { "34", "word.filtering3UpperCompCondition" },
            { "35", "word.filtering3LowerLimit" },
            { "36", "word.filtering3LowerCompCondition" },
            { "37", "word.filtering4ValidInvalid" },
            { "38", "word.filtering4TagId" },
            { "39", "word.filtering4UpperLimit" },
            { "40", "word.filtering4UpperCompCondition" },
            { "41", "word.filtering4LowerLimit" },
            { "42", "word.filtering4LowerCompCondition" },
            { "43", "word.filtering5ValidInvalid" },
            { "44", "word.filtering5TagId" },
            { "45", "word.filtering5UpperLimit" },
            { "46", "word.filtering5UpperCompCondition" },
            { "47", "word.filtering5LowerLimit" },
            { "48", "word.filtering5LowerCompCondition" },
            { "49", "word.sensedTroubleType" },
            { "50", "word.sensedTroubleUpperLimit" },
            { "51", "word.sensedTroubleUpperCompCondition" },
            { "52", "word.sensedTroubleLowerLimit" },
            { "53", "word.sensedTroubleLowerCompCondition" },
            { "54", "word.sensedAlarmType" },
            { "55", "word.sensedAlarmUpperLimit" },
            { "56", "word.sensedAlarmUpperCompCondition" },
            { "57", "word.sensedAlarmLowerLimit" },
            { "58", "word.sensedAlarmLowerCompCondition" },
            { "59", "word.sensedCautionType" },
            { "60", "word.sensedCautionUpperLimit" },
            { "61", "word.sensedCautionUpperCompCondition" },
            { "62", "word.sensedCautionLowerLimit" },
            { "63", "word.sensedCautionLowerCompCondition" },
            { "64", "word.analyzeStartDate" },
            { "65", "word.lastUpdateDateOfAnalyzing" },
            { "66", "word.comment" },
            { "67", "word.updateTimesOfAnalyzing" }
    };
    public static final String[][] analyzeParamRemarkTakeoffMoveAverage = {
            { "1", "word.configNameRemark" },
            { "2", "word.yAxisNameRemark" },
            { "3", "word.analyzeKindRemark" },
            { "4", "word.timingRemark" },
            { "5", "word.analyzeTagNoRemark" },
            { "6", "word.invalidUpperLimitRemark" },
            { "7", "word.invalidUpperCompConditionRemark" },
            { "8", "word.invalidLowerLimitRemark" },
            { "9", "word.invalidLowerCompConditionRemark" },
            { "10", "word.moveAverageScopeFormRemark" },
            { "11", "word.moveAverageDataNumRemark" },
            { "12", "word.moveAverageDataSpanRemark" },
            { "13", "word.moveAverageDataSpanUnitRemark" },
            { "14", "word.toTypeFilteringValidInvalidRemark" },
            { "15", "word.T2TagId2Remark" },
            { "16", "word.N1C2TagId2Remark" },
            { "17", "word.ToTypeRemark" },
            { "18", "word.filteringNumRemark" },
            { "19", "word.filtering1ValidInvalidRemark" },
            { "20", "word.filtering1TagIdRemark" },
            { "21", "word.filtering1UpperLimitRemark" },
            { "22", "word.filtering1UpperCompConditionRemark" },
            { "23", "word.filtering1LowerLimitRemark" },
            { "24", "word.filtering1LowerCompConditionRemark" },
            { "25", "word.filtering2ValidInvalidRemark" },
            { "26", "word.filtering2TagIdRemark" },
            { "27", "word.filtering2UpperLimitRemark" },
            { "28", "word.filtering2UpperCompConditionRemark" },
            { "29", "word.filtering2LowerLimitRemark" },
            { "30", "word.filtering2LowerCompConditionRemark" },
            { "31", "word.filtering3ValidInvalidRemark" },
            { "32", "word.filtering3TagIdRemark" },
            { "33", "word.filtering3UpperLimitRemark" },
            { "34", "word.filtering3UpperCompConditionRemark" },
            { "35", "word.filtering3LowerLimitRemark" },
            { "36", "word.filtering3LowerCompConditionRemark" },
            { "37", "word.filtering4ValidInvalidRemark" },
            { "38", "word.filtering4TagIdRemark" },
            { "39", "word.filtering4UpperLimitRemark" },
            { "40", "word.filtering4UpperCompConditionRemark" },
            { "41", "word.filtering4LowerLimitRemark" },
            { "42", "word.filtering4LowerCompConditionRemark" },
            { "43", "word.filtering5ValidInvalidRemark" },
            { "44", "word.filtering5TagIdRemark" },
            { "45", "word.filtering5UpperLimitRemark" },
            { "46", "word.filtering5UpperCompConditionRemark" },
            { "47", "word.filtering5LowerLimitRemark" },
            { "48", "word.filtering5LowerCompConditionRemark" },
            { "49", "word.sensedTroubleTypeRemark2" },
            { "50", "word.sensedTroubleUpperLimitRemark" },
            { "51", "word.sensedTroubleUpperCompConditionRemark" },
            { "52", "word.sensedTroubleLowerLimitRemark" },
            { "53", "word.sensedTroubleLowerCompConditionRemark" },
            { "54", "word.sensedAlarmTypeRemark2" },
            { "55", "word.sensedAlarmUpperLimitRemark" },
            { "56", "word.sensedAlarmUpperCompConditionRemark" },
            { "57", "word.sensedAlarmLowerLimitRemark" },
            { "58", "word.sensedAlarmLowerCompConditionRemark" },
            { "59", "word.sensedCautionTypeRemark2" },
            { "60", "word.sensedCautionUpperLimitRemark" },
            { "61", "word.sensedCautionUpperCompConditionRemark" },
            { "62", "word.sensedCautionLowerLimitRemark" },
            { "63", "word.sensedCautionLowerCompConditionRemark" },
            { "64", "word.analyzeStartDateRemark" },
            { "65", "word.lastUpdateDateOfAnalyzingRemark" },
            { "66", "word.commentRemark" },
            { "67", "word.updateTimesOfAnalyzingRemark" }
    };
    //	6:Takeoff移動分散
    public static final String[][] analyzeParamItemDispersion = {
            { "1", "word.configName" },
            { "2", "word.yAxisName" },
            { "3", "word.analyzeKind" },
            { "4", "word.timing" },
            { "5", "word.analyzeTagNo" },
            { "6", "word.invalidUpperLimit" },
            { "7", "word.invalidUpperCompCondition" },
            { "8", "word.invalidLowerLimit" },
            { "9", "word.invalidLowerCompCondition" },
            { "10", "word.moveVarianceScopeForm" },
            { "11", "word.moveVarianceDataNum" },
            { "12", "word.moveVarianceDataSpan" },
            { "13", "word.moveVarianceDataSpanUnit" },
            { "14", "word.toTypeFilteringValidInvalid" },
            { "15", "word.T2TagId2" },
            { "16", "word.N1C2TagId2" },
            { "17", "word.ToType" },
            { "18", "word.filteringNum" },
            { "19", "word.filtering1ValidInvalid" },
            { "20", "word.filtering1TagId" },
            { "21", "word.filtering1UpperLimit" },
            { "22", "word.filtering1UpperCompCondition" },
            { "23", "word.filtering1LowerLimit" },
            { "24", "word.filtering1LowerCompCondition" },
            { "25", "word.filtering2ValidInvalid" },
            { "26", "word.filtering2TagId" },
            { "27", "word.filtering2UpperLimit" },
            { "28", "word.filtering2UpperCompCondition" },
            { "29", "word.filtering2LowerLimit" },
            { "30", "word.filtering2LowerCompCondition" },
            { "31", "word.filtering3ValidInvalid" },
            { "32", "word.filtering3TagId" },
            { "33", "word.filtering3UpperLimit" },
            { "34", "word.filtering3UpperCompCondition" },
            { "35", "word.filtering3LowerLimit" },
            { "36", "word.filtering3LowerCompCondition" },
            { "37", "word.filtering4ValidInvalid" },
            { "38", "word.filtering4TagId" },
            { "39", "word.filtering4UpperLimit" },
            { "40", "word.filtering4UpperCompCondition" },
            { "41", "word.filtering4LowerLimit" },
            { "42", "word.filtering4LowerCompCondition" },
            { "43", "word.filtering5ValidInvalid" },
            { "44", "word.filtering5TagId" },
            { "45", "word.filtering5UpperLimit" },
            { "46", "word.filtering5UpperCompCondition" },
            { "47", "word.filtering5LowerLimit" },
            { "48", "word.filtering5LowerCompCondition" },
            { "49", "word.sensedTroubleUpperLimit" },
            { "50", "word.sensedTroubleUpperCompCondition" },
            { "51", "word.sensedTroubleLowerLimit" },
            { "52", "word.sensedTroubleLowerCompCondition" },
            { "53", "word.sensedAlarmUpperLimit" },
            { "54", "word.sensedAlarmUpperCompCondition" },
            { "55", "word.sensedAlarmLowerLimit" },
            { "56", "word.sensedAlarmLowerCompCondition" },
            { "57", "word.sensedCautionUpperLimit" },
            { "58", "word.sensedCautionUpperCompCondition" },
            { "59", "word.sensedCautionLowerLimit" },
            { "60", "word.sensedCautionLowerCompCondition" },
            { "61", "word.analyzeStartDate" },
            { "62", "word.lastUpdateDateOfAnalyzing" },
            { "63", "word.comment" },
            { "64", "word.updateTimesOfAnalyzing" }
    };
    public static final String[][] analyzeParamRemarkDispersion = {
            { "1", "word.configNameRemark" },
            { "2", "word.yAxisNameRemark" },
            { "3", "word.analyzeKindRemark" },
            { "4", "word.timingRemark" },
            { "5", "word.analyzeTagNoRemark" },
            { "6", "word.invalidUpperLimitRemark" },
            { "7", "word.invalidUpperCompConditionRemark" },
            { "8", "word.invalidLowerLimitRemark" },
            { "9", "word.invalidLowerCompConditionRemark" },
            { "10", "word.moveVarianceScopeFormRemark" },
            { "11", "word.moveVarianceDataNumRemark" },
            { "12", "word.moveVarianceDataSpanRemark" },
            { "13", "word.moveVarianceDataSpanUnitRemark" },
            { "14", "word.toTypeFilteringValidInvalidRemark" },
            { "15", "word.T2TagId2Remark" },
            { "16", "word.N1C2TagId2Remark" },
            { "17", "word.ToTypeRemark" },
            { "18", "word.filteringNumRemark" },
            { "19", "word.filtering1ValidInvalidRemark" },
            { "20", "word.filtering1TagIdRemark" },
            { "21", "word.filtering1UpperLimitRemark" },
            { "22", "word.filtering1UpperCompConditionRemark" },
            { "23", "word.filtering1LowerLimitRemark" },
            { "24", "word.filtering1LowerCompConditionRemark" },
            { "25", "word.filtering2ValidInvalidRemark" },
            { "26", "word.filtering2TagIdRemark" },
            { "27", "word.filtering2UpperLimitRemark" },
            { "28", "word.filtering2UpperCompConditionRemark" },
            { "29", "word.filtering2LowerLimitRemark" },
            { "30", "word.filtering2LowerCompConditionRemark" },
            { "31", "word.filtering3ValidInvalidRemark" },
            { "32", "word.filtering3TagIdRemark" },
            { "33", "word.filtering3UpperLimitRemark" },
            { "34", "word.filtering3UpperCompConditionRemark" },
            { "35", "word.filtering3LowerLimitRemark" },
            { "36", "word.filtering3LowerCompConditionRemark" },
            { "37", "word.filtering4ValidInvalidRemark" },
            { "38", "word.filtering4TagIdRemark" },
            { "39", "word.filtering4UpperLimitRemark" },
            { "40", "word.filtering4UpperCompConditionRemark" },
            { "41", "word.filtering4LowerLimitRemark" },
            { "42", "word.filtering4LowerCompConditionRemark" },
            { "43", "word.filtering5ValidInvalidRemark" },
            { "44", "word.filtering5TagIdRemark" },
            { "45", "word.filtering5UpperLimitRemark" },
            { "46", "word.filtering5UpperCompConditionRemark" },
            { "47", "word.filtering5LowerLimitRemark" },
            { "48", "word.filtering5LowerCompConditionRemark" },
            { "49", "word.sensedTroubleUpperLimitRemark" },
            { "50", "word.sensedTroubleUpperCompConditionRemark" },
            { "51", "word.sensedTroubleLowerLimitRemark" },
            { "52", "word.sensedTroubleLowerCompConditionRemark" },
            { "53", "word.sensedAlarmUpperLimitRemark" },
            { "54", "word.sensedAlarmUpperCompConditionRemark" },
            { "55", "word.sensedAlarmLowerLimitRemark" },
            { "56", "word.sensedAlarmLowerCompConditionRemark" },
            { "57", "word.sensedCautionUpperLimitRemark" },
            { "58", "word.sensedCautionUpperCompConditionRemark" },
            { "59", "word.sensedCautionLowerLimitRemark" },
            { "60", "word.sensedCautionLowerCompConditionRemark" },
            { "61", "word.analyzeStartDateRemark" },
            { "62", "word.lastUpdateDateOfAnalyzingRemark" },
            { "63", "word.commentRemark" },
            { "64", "word.updateTimesOfAnalyzingRemark" }
    };
    //	7:ベースカーブ
    public static final String[][] analyzeParamItemBaseCurve = {
            { "1", "word.configName" },
            { "2", "word.xAxisName" },
            { "3", "word.yAxisName" },
            { "4", "word.analyzeKind" },
            { "5", "word.timing" },
            { "6", "word.analyzeTagIdXAxis" },
            { "7", "word.analyzeTagIdYAxis" },
            { "8", "word.baseCurveConfForm" },
            { "9", "word.polynomialCoefficientA0" },
            { "10", "word.polynomialCoefficientA1" },
            { "11", "word.polynomialCoefficientA2" },
            { "12", "word.polynomialCoefficientA3" },
            { "13", "word.pointGroupCoordDataFileName" },
            { "14", "word.toTypeFilteringValidInvalid" },
            { "15", "word.T2TagId2" },
            { "16", "word.N1C2TagId2" },
            { "17", "word.ToType" },
            { "18", "word.filteringNum" },
            { "19", "word.filtering1ValidInvalid" },
            { "20", "word.filtering1TagId" },
            { "21", "word.filtering1UpperLimit" },
            { "22", "word.filtering1UpperCompCondition" },
            { "23", "word.filtering1LowerLimit" },
            { "24", "word.filtering1LowerCompCondition" },
            { "25", "word.filtering2ValidInvalid" },
            { "26", "word.filtering2TagId" },
            { "27", "word.filtering2UpperLimit" },
            { "28", "word.filtering2UpperCompCondition" },
            { "29", "word.filtering2LowerLimit" },
            { "30", "word.filtering2LowerCompCondition" },
            { "31", "word.filtering3ValidInvalid" },
            { "32", "word.filtering3TagId" },
            { "33", "word.filtering3UpperLimit" },
            { "34", "word.filtering3UpperCompCondition" },
            { "35", "word.filtering3LowerLimit" },
            { "36", "word.filtering3LowerCompCondition" },
            { "37", "word.filtering4ValidInvalid" },
            { "38", "word.filtering4TagId" },
            { "39", "word.filtering4UpperLimit" },
            { "40", "word.filtering4UpperCompCondition" },
            { "41", "word.filtering4LowerLimit" },
            { "42", "word.filtering4LowerCompCondition" },
            { "43", "word.filtering5ValidInvalid" },
            { "44", "word.filtering5TagId" },
            { "45", "word.filtering5UpperLimit" },
            { "46", "word.filtering5UpperCompCondition" },
            { "47", "word.filtering5LowerLimit" },
            { "48", "word.filtering5LowerCompCondition" },
            { "49", "word.sensedTroubleUpperLimit" },
            { "50", "word.sensedTroubleUpperCompCondition" },
            { "51", "word.sensedTroubleLowerLimit" },
            { "52", "word.sensedTroubleLowerCompCondition" },
            { "53", "word.sensedAlarmUpperLimit" },
            { "54", "word.sensedAlarmUpperCompCondition" },
            { "55", "word.sensedAlarmLowerLimit" },
            { "56", "word.sensedAlarmLowerCompCondition" },
            { "57", "word.sensedCautionUpperLimit" },
            { "58", "word.sensedCautionUpperCompCondition" },
            { "59", "word.sensedCautionLowerLimit" },
            { "60", "word.sensedCautionLowerCompCondition" },
            { "61", "word.analyzeStartDate" },
            { "62", "word.lastConfigDate" },
            { "63", "word.comment" }
    };
    public static final String[][] analyzeParamRemarkBaseCurve = {
            { "1", "word.configNameRemark" },
            { "2", "word.xAxisNameRemark" },
            { "3", "word.yAxisNameRemark" },
            { "4", "word.analyzeKindRemark" },
            { "5", "word.timingRemark" },
            { "6", "word.analyzeTagIdXAxisRemark" },
            { "7", "word.analyzeTagIdYAxisRemark" },
            { "8", "word.baseCurveConfFormRemark" },
            { "9", "word.polynomialCoefficientRemark" },
            { "10", "word.dojo" },
            { "11", "word.dojo" },
            { "12", "word.dojo" },
            { "13", "word.pointGroupCoordDataFileNameRemark" },
            { "14", "word.toTypeFilteringValidInvalidRemark" },
            { "15", "word.T2TagId2Remark" },
            { "16", "word.N1C2TagId2Remark" },
            { "17", "word.ToTypeRemark" },
            { "18", "word.filteringNumRemark" },
            { "19", "word.filtering1ValidInvalidRemark" },
            { "20", "word.filtering1TagIdRemark" },
            { "21", "word.filtering1UpperLimitRemark" },
            { "22", "word.filtering1UpperCompConditionRemark" },
            { "23", "word.filtering1LowerLimitRemark" },
            { "24", "word.filtering1LowerCompConditionRemark" },
            { "25", "word.filtering2ValidInvalidRemark" },
            { "26", "word.filtering2TagIdRemark" },
            { "27", "word.filtering2UpperLimitRemark" },
            { "28", "word.filtering2UpperCompConditionRemark" },
            { "29", "word.filtering2LowerLimitRemark" },
            { "30", "word.filtering2LowerCompConditionRemark" },
            { "31", "word.filtering3ValidInvalidRemark" },
            { "32", "word.filtering3TagIdRemark" },
            { "33", "word.filtering3UpperLimitRemark" },
            { "34", "word.filtering3UpperCompConditionRemark" },
            { "35", "word.filtering3LowerLimitRemark" },
            { "36", "word.filtering3LowerCompConditionRemark" },
            { "37", "word.filtering4ValidInvalidRemark" },
            { "38", "word.filtering4TagIdRemark" },
            { "39", "word.filtering4UpperLimitRemark" },
            { "40", "word.filtering4UpperCompConditionRemark" },
            { "41", "word.filtering4LowerLimitRemark" },
            { "42", "word.filtering4LowerCompConditionRemark" },
            { "43", "word.filtering5ValidInvalidRemark" },
            { "44", "word.filtering5TagIdRemark" },
            { "45", "word.filtering5UpperLimitRemark" },
            { "46", "word.filtering5UpperCompConditionRemark" },
            { "47", "word.filtering5LowerLimitRemark" },
            { "48", "word.filtering5LowerCompConditionRemark" },
            { "49", "word.sensedTroubleUpperLimitRemark" },
            { "50", "word.sensedTroubleUpperCompConditionRemark" },
            { "51", "word.sensedTroubleLowerLimitRemark" },
            { "52", "word.sensedTroubleLowerCompConditionRemark" },
            { "53", "word.sensedAlarmUpperLimitRemark" },
            { "54", "word.sensedAlarmUpperCompConditionRemark" },
            { "55", "word.sensedAlarmLowerLimitRemark" },
            { "56", "word.sensedAlarmLowerCompConditionRemark" },
            { "57", "word.sensedCautionUpperLimitRemark" },
            { "58", "word.sensedCautionUpperCompConditionRemark" },
            { "59", "word.sensedCautionLowerLimitRemark" },
            { "60", "word.sensedCautionLowerCompConditionRemark" },
            { "61", "word.analyzeStartDateRemark" },
            { "62", "word.lastConfigDateRemark" },
            { "63", "word.commentRemark" }
    };
    public static final String[][] analyzeParamItemMt = {
            { "1", "word.configName" },
            { "2", "word.analyzeKind" },
            { "3", "word.analyzeTiming" },
            { "4", "word.analyzeCycle" },
            { "5", "word.analyzeCycleUnit" },
            { "6", "word.analyzeDay" },
            { "7", "word.analyzeDayOfWeek" },
            { "8", "word.analyzeHour" },
            { "9", "word.analyzeMinute" },
            { "10", "word.unitSpaceDataFile" },
            { "11", "word.preConditionDataFile" },
            { "12", "word.mdValueIncreaseMoveAverageCalcWay" },
            { "13", "word.moveAverageCalcSampleNum" },
            { "14", "word.moveAverageCalcDataSpan" },
            { "15", "word.moveAverageCalcSpanUnit" },
            { "16", "word.mdValueAlarmValidInvalid" },
            { "17", "word.mdValueAlarmCode" },
            { "18", "word.mdValueThresholdValue" },
            { "19", "word.mdValueOverSampleNum" },
            { "20", "word.mdValueIncreaseAlarmValidInvalid" },
            { "21", "word.mdValueIncreaseAlarmCode" },
            { "22", "word.mdValueIncreaseRateThresholdValue" },
            { "23", "word.mdValueIncreaseOverSampleNum" }
    };

    public static final String[][] analyzeParamRemarkMt = {
            { "1", "word.configNameRemark" },
            { "2", "word.analyzeKindRemark" },
            { "3", "word.analyzeTimingRemark" },
            { "4", "word.analyzeCycleRemark" },
            { "5", "word.analyzeCycleUnitRemark" },
            { "6", "word.analyzeDayRemark" },
            { "7", "word.analyzeDayOfWeekRemark" },
            { "8", "word.analyzeHourRemark" },
            { "9", "word.analyzeMinuteRemark" },
            { "10", "word.unitSpaceDataFileRemark" },
            { "11", "word.preConditionDataFileRemark" },
            { "12", "word.mdValueIncreaseMoveAverageCalcWayRemark" },
            { "13", "word.moveAverageCalcSampleNumRemark" },
            { "14", "word.moveAverageCalcDataSpanRemark" },
            { "15", "word.moveAverageCalcSpanUnitRemark" },
            { "16", "word.mdValueAlarmValidInvalidRemark" },
            { "17", "word.mdValueAlarmCodeRemark" },
            { "18", "word.mdValueThresholdValueRemark" },
            { "19", "word.mdValueOverSampleNumRemark" },
            { "20", "word.mdValueIncreaseAlarmValidInvalidRemark" },
            { "21", "word.mdValueIncreaseAlarmCodeRemark" },
            { "22", "word.mdValueIncreaseRateThresholdValueRemark" },
            { "23", "word.mdValueIncreaseOverSampleNumRemark" }
    };
    /** グラフ幅 */
    public static final double CHART_WIDTH = 0.8;
    public static final String CHART_WIDTH_CSS = "80%";

    /**
     * 解析待ち
     */
    public static final int ANALYZE_WAIT = 1;

    /**
     * 解析処理中
     */
    public static final int UNDER_ANALYZING = 2;

    /**
     * 解析完了
     */
    public static final int ANALYZED = 3;

    public static final int ANALYZE_RESULT_ERR_END = -1;
    public static final int ANALYZE_RESULT_NORMAL = 0;
    public static final int ANALYZE_RESULT_INIT_VALUE = 5;
    public static final int ANALYZE_RESULT_OUT = 6;

    // 20171120 FPT - Add type analyse - ADD - End
    /** 復旧メール送信完了 */
    public static final int RECOVER_MAIL_SEND_COMPLETE = 1;

    /** メール送信方法 TO */
    public static final String SEND_TYPE_TO = "0";
    /** メール送信方法 CC */
    public static final String SEND_TYPE_CC = "1";
    /** メール送信方法 BCC */
    public static final String SEND_TYPE_BCC = "2";

    /** 選択中タブ（アラームメール送信先一覧） */
    public static final String TAB_SELECT_ALARM_SENDTO_LIST = "alarmSendTo";

    /** 選択中タブ（テストメール送信） */
    public static final String TAB_SELECT_ALARM_TESTMAILSEND = "alarmTestMailSend";

    /** 選択中タブ（CSIGS機器一覧） */
    public static final String TAB_CSIGS_CONFIG_LIST = "csigsConfig";

    /** マスタ区分（テストメール送信用アラームコード）*/
    public static final int MASTER_KBN_TESTMAIL_ALARM_CODE = 9001;

    /** パッシブモードを使用する 使用しない*/
    public static final String NO_USE_PASSIVE_MODE = "0";

    /** パッシブモードを使用する 使用する*/
    public static final String USE_PASSIVE_MODE = "1";

    /** CSIGS設定編集画面 画面モード：登録*/
    public static final String CSIGS_EDIT_ADD = "1";

    /** CSIGS設定編集画面 画面モード：編集*/
    public static final String CSIGS_EDIT_EDIT = "2";

    /** CSIGS設定編集画面 画面モード：削除*/
    public static final String CSIGS_EDIT_DELETE = "3";

    public static final String TAB_PERSON_DATA_LIST = "persondata";

    /**
     * ユーティリティクラスのため、コンストラクタは使用禁止。
     */
    private AppConst() {
    }
}
