package ilips.constant;

/**
 * アプリケーション共通定数定義クラス
 */
public class ViewConst {
    /** アラーム名称の最大表示文字数 */
    public static final int ALARM_NAME_MAX_LENGTH       = 20;

    /** アラームレベルの最大表示文字数 */
    public static final int ALARM_LEVEL_MAX_LENGTH      = 20;

    /** 名前の最大表示文字数 */
    public static final int PERSON_NAME_MAX_LENGTH      = 20;

    /** メーカ名称の最大表示文字数 */
    public static final int MAKER_NAME_MAX_LENGTH       = 30;

    /** 型式名称の最大表示文字数 */
    public static final int MODEL_NAME_MAX_LENGTH       = 30;

    /** 製品名称の最大表示文字数 */
    public static final int PRODUCT_NAME_MAX_LENGTH     = 30;

    /** 工事番号の最大表示文字数 */
    public static final int WORK_NO_MAX_LENGTH          = 10;

    /** 工事名称の最大表示文字数 */
    public static final int WORK_NAME_MAX_LENGTH        = 30;

    /** 組織名称の最大表示文字数 */
    public static final int ORG_NAME_MAX_LENGTH         = 40;

    /** 部品名称の最大表示文字数 */
    public static final int PARTS_NAME_MAX_LENGTH       = 40;

    /** 部品種別の最大表示文字数 */
    public static final int PARTS_TYPE_MAX_LENGTH       = 30;

    /** 親部品名の最大表示文字数 */
    public static final int PARENT_PARTS_MAX_LENGTH     = 30;

    /** 担当者フリー項目の最大表示文字数 */
    public static final int PERSON_FREE_MAX_LENGTH      = 40;

    /** メンテナンス内容の最大表示文字数 */
    public static final int DESCRIPTION_MAX_LENGTH      = 60;

    /** ファイル名称の最大表示文字数 */
    public static final int FILE_NAME_MAX_LENGTH        = 30;

    /** Emailの最大表示文字数 */
    public static final int EMAIL_MAX_LENGTH            = 40;

    /**
     * ユーティリティクラスのため、コンストラクタは使用禁止。
     */
    private ViewConst() {
    }
}
