package ilips.constant;

/**
 * リソース制限定義クラス
 */
public class ResourceLimitConst {
    /** 製品一覧ダウンロード */
    public static final int PRODUCT_LIST_DOWNLOAD = 10000;

    /** メンテナンス履歴ダウンロード */
    public static final int MAINTE_LOG_LIST_DOWNLOAD = 10000;

    /** サマリデータダウンロード */
    public static final int SUMMARY_DOWNLOAD = 100000;

    /** 生データダウンロード(期間) */
    public static final int RAW_DOWNLOAD_TERM = 1000 * 60 * 60 * 24;

    /** グラフ表示 */
    public static final int CHART_NORMAL = 1000;

    /**
     * ユーティリティクラスのため、コンストラクタは使用禁止。
     */
    private ResourceLimitConst() {
    }
}
