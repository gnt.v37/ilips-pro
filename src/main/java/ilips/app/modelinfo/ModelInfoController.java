package ilips.app.modelinfo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/account")
public class ModelInfoController {

    @GetMapping("/modelInfo")
    public String index() {
        return "modelInfo/modelInfo";
    }
}
