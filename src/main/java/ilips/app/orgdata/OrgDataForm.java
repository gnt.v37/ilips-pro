package ilips.app.orgdata;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * Class name:   Form select parent id
 * 
 * @author ThangNT2
 * Created Date: Jul 20, 2018
 */
@Data
@ToString
public class OrgDataForm {

	/** select parent id */
	private Integer selectParentId;
}
