package ilips.app.orgdata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ilips.constant.MsgConst;
import ilips.domain.model.orgdata.OrgData;
import ilips.domain.model.orgdata.OrgDataDisp;
import ilips.domain.model.orgdata.OrgDataGridRowCollection;
import ilips.domain.model.user.UserDetail;
import ilips.domain.service.message.MessageService;
import ilips.domain.service.orgdata.OrgDataService;
import ilips.domain.validator.orgdata.OrgDataGridRowValidator;

@Controller
@RequestMapping("/account")
public class OrgDataController {

	/** organization service */
	private final OrgDataService orgDataService;

	/** validator */
	private MessageService messageService;

	@Autowired
	public OrgDataController(OrgDataService orgDataService,
			MessageService messageService) {
		this.orgDataService = orgDataService;
		this.messageService = messageService;
	}

	@GetMapping("/orgData")
	public String index() {
		return "orgData/orgData";
	}

	/**
	 * Load data
	 * 
	 * @param form form select parent id
	 * @param bindingResult binding result
	 * @throws BindException
	 * @return display data
	 */
	@GetMapping("/orgData/load/")
	@ResponseBody
	public OrgDataDisp loadData(@Valid OrgDataForm form, BindingResult bindingResult) throws BindException {

		// init object
		OrgDataDisp orgDataDisp = new OrgDataDisp();

		if (bindingResult.hasErrors()) {

			throw new BindException(bindingResult);
		}

		// Authenticated
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDetail userDetail = (UserDetail) auth.getPrincipal();

		// Get permission
		Integer permission = userDetail.getSystemPermission();

		// Get user in organization
		Integer userOrgId = userDetail.getPerson().getOrgId();
		List<OrgData> orgDataList = orgDataService.getListOrgData(userOrgId, form.getSelectParentId(), permission);

		orgDataDisp.setOrgDataList(orgDataList);

		return orgDataDisp;
	}

	/**
	 * Update, Delete, Insert Action
	 * 
	 * @param collection row collection including: insert, update,delete
	 * @param form select parent id
	 * @param bindingResult 
	 * @return message error or load data
	 * @throws BindException
	 */
	@RequestMapping(value = "/orgData/save/", method = RequestMethod.POST)
	@ResponseBody
	public Map<Integer, String> saveData(@RequestBody OrgDataGridRowCollection collection,
			BindingResult bindingResult) throws BindException {

		// Init error code list
		Map<Integer, String> errorCodeList = new HashMap<>();

		// Binding Result
		if (bindingResult.hasErrors()) {
			throw new BindException(bindingResult);
		}
		
		try {
		OrgDataGridRowValidator validator = new OrgDataGridRowValidator(orgDataService, messageService, collection);
		
		// validate
		errorCodeList = validator.validationRow();

		// if it doesn't have any error
		if (errorCodeList.isEmpty()) {

			// Authenticated
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			UserDetail userDetail = (UserDetail) auth.getPrincipal();
			Integer userOrgId = userDetail.getPerson().getOrgId();

			// update and data
			orgDataService.updateData(collection, collection.getSelectParentId(), userOrgId);
		}
		} catch (Exception e) {
			
			errorCodeList.put(-1, messageService.text(MsgConst.ERR_EM155, e.getCause().getMessage()));
			
			// if has error, stop programming
			return errorCodeList;
		}
		return errorCodeList;
	}
}