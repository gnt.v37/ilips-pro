package ilips.app.selectwork;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ilips.domain.model.selectwork.SelectWorkData;
import ilips.domain.service.selectwork.SelectWorkService;

@Controller
@RequestMapping("/account")
public class SelectWorkController {

	private SelectWorkService selectWorkService;
	
	@Autowired
	public SelectWorkController(SelectWorkService selectWorkService) {
		this.selectWorkService = selectWorkService;
	}
	
	/**
	 * Open pop-pup, show work list
	 *
	 * @return list work
	 */
	@GetMapping("/selectwork/")
	@ResponseBody
	public List<SelectWorkData> index() {
		
		return this.selectWorkService.getAll();
	}
}
