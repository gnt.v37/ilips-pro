package ilips.app.workinfo;

import ilips.constant.MsgConst;
import ilips.domain.model.workinfo.WorkInfoDisp;
import ilips.domain.model.workinfo.WorkInfo;
import ilips.domain.model.workinfo.WorkInfoGridRowCollection;
import ilips.domain.service.common.CommonService;
import ilips.domain.service.message.MessageService;
import ilips.domain.service.workinfo.WorkInfoService;
import ilips.domain.validator.workinfo.WorkInfoGridRowValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller class handle requests from Work data screen.
 *
 * @author QuynhPP
 * created on 7/19/2018
 */
@Controller
@RequestMapping("/account")
public class WorkInfoController {

    /**
     * Work data service
     */
    private final WorkInfoService workInfoService;

    /**
     * Message service
     */
    private final MessageService messageService;

    /**
     * Common service
     */
    private final CommonService commonService;

    @Autowired
    public WorkInfoController(WorkInfoService workInfoService,
                              MessageService messageService,
                              CommonService commonService) {
        this.workInfoService = workInfoService;
        this.messageService = messageService;
        this.commonService = commonService;
    }

    /**
     * Init screen.
     *
     * @return
     */
    @GetMapping("/workInfo")
    public String index() {
        return "workInfo/workInfo";
    }

    /**
     * Load database to display on screen.
     *
     * @return Work data to display.
     * @throws BindException
     */
    @GetMapping("/workInfo/loadData")
    @ResponseBody
    public WorkInfoDisp loadData() {

        // Init obj contains display data
        WorkInfoDisp workInfoDisp = new WorkInfoDisp();

        // Get work data for grid
        List<WorkInfo> workInfoList = workInfoService.getWorkDataList();

        workInfoDisp.setWorkInfoList(workInfoList);

        return workInfoDisp;
    }

    /**
     * Save grid data to database.
     *
     * @param workInfoGridRowCollection collection of work data grid.
     * @param bindingResult binding result.
     * @return error list.
     * @throws BindException
     */
    @PostMapping("/workInfo/saveData")
    @ResponseBody
    public Map<Integer, String> saveData(@RequestBody WorkInfoGridRowCollection workInfoGridRowCollection,
                                         BindingResult bindingResult)
            throws BindException {

        // Init error messages list
        Map<Integer, String> errList;

        // If binding param has errors
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        try {
            // Check validate for grid rows
            WorkInfoGridRowValidator workInfoGridRowValidator =
                    new WorkInfoGridRowValidator(workInfoGridRowCollection, workInfoService, messageService);
            errList = workInfoGridRowValidator.validateGridRowData();

            // None errors, save data.
            if (errList.isEmpty()) {
                workInfoService.updateWorkDataList(workInfoGridRowCollection);
            }
        } catch (Exception ex) {
            errList = new HashMap<>();
            errList.put(-1, messageService.text(MsgConst.ERR_EM155, ex.getCause().getMessage()));
            return errList;
        }

        return errList;
    }
}
