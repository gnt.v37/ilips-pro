package ilips.app.home;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ilips.configure.CustomResourceMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

@Controller
@RequestMapping("/")
public class HomeController {
    private final CustomResourceMessage customResourceMessage;
    private final ObjectMapper jacksonObjectMapper;

    @Autowired
    public HomeController(MessageSource messageSource, ObjectMapper jacksonObjectMapper) {
        this.customResourceMessage = (CustomResourceMessage) messageSource;
        this.jacksonObjectMapper = jacksonObjectMapper;
    }

    @GetMapping()
    public String index() {
        return "home/home";
    }

    @GetMapping("/messages.js")
    public String getMessages(HttpServletResponse response, Model model, Locale locale) throws JsonProcessingException {
        model.addAttribute("properties", jacksonObjectMapper.writeValueAsString(customResourceMessage.getAllProperties(locale)));
        return "message";
    }
}
