package ilips.app.login;

import ilips.domain.model.user.User;
import ilips.domain.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String getRegister(@RequestParam(required = false, defaultValue = "") String name, Model model) {
        LoginForm form = new LoginForm();
        form.setUsername(name);
        model.addAttribute("loginForm", form);
        return "user/register";
    }

    @PostMapping("/register")
    public String postRegister(@Valid @ModelAttribute LoginForm loginForm, BindingResult bindingResult, Model model) {
        if (!bindingResult.hasErrors()) {
            try {
                User user = new User("", "");
                loginForm.setUsername(user.getUsername());
                loginForm.setPassword("Ahihi");
                model.addAttribute("loginForm", loginForm);
                model.addAttribute("success", true);
            } catch (Exception e) {
                model.addAttribute("errorMsg", e.getMessage());
            }
        }

        return "user/register";
    }

    @GetMapping("/info")
    public String info() {
        return "user/info";
    }

    @GetMapping("/groupInfo400")
    @PreAuthorize("hasRole('400')")
    public String groupInfo() {
        return "user/groupInfo";
    }

    @GetMapping("/groupInfo500")
    @PreAuthorize("hasRole('500')")
    public String groupInfo500() {
        return "user/groupInfo";
    }
}
