package ilips.app.referableorg;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Form class for contains input parameters
 */
@Data
@ToString
public class ReferableOrgDataForm {

    @NotNull(message = "Org Id cannot be null")
    @Min(value = 0L, message = "Org Id must be positive integer")
    public Integer selectReferableOrgId;

}
