package ilips.app.referableorg;

import ilips.constant.MsgConst;
import ilips.domain.model.referableorgdata.ReferableOrgData;
import ilips.domain.model.referableorgdata.ReferableOrgDataDisp;
import ilips.domain.model.referableorgdata.ReferableOrgDataGridRowCollection;
import ilips.domain.service.common.CommonService;
import ilips.domain.service.message.MessageService;
import ilips.domain.service.referableorgdata.ReferableOrgDataService;
import ilips.domain.validator.referableorgdata.ReferableOrgGridRowValidator;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Controller class handle requests for Person data screen.
 *
 * @author ThanhNP4
 * create on 7/19/2018
 */
@Controller
@RequestMapping("/account")
public class ReferableOrgController {

    /**
     * ReferableOrg data service
     */
    private final ReferableOrgDataService referableOrgDataService;

    /**
     * Message service
     */
    private final MessageService messageService;

    /**
     * Validator for grid row
     */
    private final ReferableOrgGridRowValidator referableOrgGridRowValidator;

    /**
     * Common service
     */
    private final CommonService commonService;

    public ReferableOrgController(ReferableOrgDataService referableOrgDataService, MessageService messageService,
                                  ReferableOrgGridRowValidator referableOrgGridRowValidator,
                                  CommonService commonService) {
        this.referableOrgDataService = referableOrgDataService;
        this.messageService = messageService;
        this.referableOrgGridRowValidator = referableOrgGridRowValidator;
        this.commonService = commonService;
    }

    /**
     * Init screen
     *
     * @return
     */
    @GetMapping("/referableOrg")
    public String index() {
        return "referableOrg/referableOrg";
    }

    /**
     * Get data for display grid
     *
     * @param form
     * @param bindingResult
     * @return Display data
     * @throws BindException
     */
    @PostMapping("/referableOrg/loadData")
    @ResponseBody
    public ReferableOrgDataDisp loadData(@Valid ReferableOrgDataForm form, BindingResult bindingResult)
            throws BindException {

        // Init obj contain display data
        ReferableOrgDataDisp referableOrgDataDisp = new ReferableOrgDataDisp();

        // If binding param has errors
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        // Get parameters
        int selectReferableOrgId = form.selectReferableOrgId;


        // Get person data for grid
        List<ReferableOrgData> referableOrgDataList = referableOrgDataService.getReferableOrgDataList(selectReferableOrgId);

        // Get system permission data

        // Set display data
        referableOrgDataDisp.setReferableOrgDataList(referableOrgDataList);


        return referableOrgDataDisp;
    }

    /**
     * Saving grid data to database
     *
     * @param referableOrgDataGridRowCollection
     * @param bindingResult
     * @return List error messages
     * @throws BindException
     */
    @PostMapping("/referableOrg/saveData")
    @ResponseBody
    public Map<Integer, String> saveData(@RequestBody ReferableOrgDataGridRowCollection referableOrgDataGridRowCollection,
                                         BindingResult bindingResult) throws BindException {
        // Int error messager list
        Map<Integer, List<String>> errCodeList = new HashMap<>();

        // If binding param has errors
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        // Check validate for grid row
        errCodeList = referableOrgGridRowValidator.validateGridRowData(referableOrgDataGridRowCollection);

        // None Error for grid row
        if (errCodeList.isEmpty()) {
            try {
                referableOrgDataService.updateWorkDataList(referableOrgDataGridRowCollection);
            } catch (Exception ex) {
                Map<Integer, String> errList = new HashMap<>();
                errList.put(-1, messageService.text(MsgConst.ERR_EM155, ex.getCause().getMessage()));
                return errList;
            }
        }
        return convertErrCodeList(errCodeList);
    }

    /**
     * Convert Err codes to Err messages
     *
     * @param errList
     * @return Error messages
     */
    private Map<Integer, String> convertErrCodeList(Map<Integer, List<String>> errList) {

        Map<Integer, String> convertErrList = new HashMap<>();

        // reverse err list
        for (Map.Entry<Integer, List<String>> errItem : errList.entrySet()) {
            StringBuilder errMsg = new StringBuilder();

            // reverse err code list
            for (Iterator<String> idx = errItem.getValue().iterator(); idx.hasNext(); ) {
                String errCode = idx.next();

                // convert to err message
                errMsg.append(messageService.text(errCode));
                if (idx.hasNext()) {
                    errMsg.append(", ");
                }
            }

            // add to err message list
            convertErrList.put(errItem.getKey(), errMsg.toString());
        }

        return convertErrList;
    }

}

