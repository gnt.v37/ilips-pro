package ilips.app.selectorgwindow;

import ilips.domain.model.selectorgwindow.SelectOrgWindow;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/account")
public class SelectOrgWindowController {

    @GetMapping("/selectOrgWindow")
    @ResponseBody
    public Object index() {
        /*sample data*/
        List<SelectOrgWindow> listJs = new ArrayList<>();
        SelectOrgWindow selectOrgWindow1 = new SelectOrgWindow("1","#", " Parent");
        SelectOrgWindow selectOrgWindow2 = new SelectOrgWindow("2","#", " Parent2");
        SelectOrgWindow selectOrgWindow3 = new SelectOrgWindow("3","2", " child 1 of paren2");
        SelectOrgWindow selectOrgWindow4 = new SelectOrgWindow("4","2", " child 2 of paren3");
        listJs.add(selectOrgWindow1);
        listJs.add(selectOrgWindow2);
        listJs.add(selectOrgWindow3);
        listJs.add(selectOrgWindow4);
        return listJs;
    }
}
