package ilips.app.alarmlevel;

import ilips.domain.model.AlarmLevel.AlarmLevelDataGridRowCollection;
import ilips.domain.model.AlarmLevel.AlarmLevelDisp;
import ilips.domain.service.alarm.AlarmLevelService;
import ilips.domain.service.common.CommonService;
import ilips.domain.service.message.MessageService;
import ilips.domain.validator.alarm.AlarmLevelGridRowValidator;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/account")
public class AlarmLevelController {

    private final AlarmLevelService alarmLevelService;

    private final MessageService messageService;

    private final CommonService commonService;

    public AlarmLevelController(AlarmLevelService alarmLevelService,
                                MessageService messageService,
                                CommonService commonService){
        this.alarmLevelService = alarmLevelService;
        this.messageService = messageService;
        this.commonService = commonService;
    }
    @GetMapping("/alarmLevel")
    public String index() {
        return "alarmLevel/alarmLevel";
    }


    @PostMapping("/alarmLevel/loadData")
    @ResponseBody
    public AlarmLevelDisp loadData() {

        // Init obj contain display data
        AlarmLevelDisp alarmLevelDisp = new AlarmLevelDisp();

        // If binding param has errors
        /*if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }*/
        alarmLevelDisp.setAlarmLevelDataList(alarmLevelService.getAlarmLevelDataList());

        return alarmLevelDisp;
    }


    @PostMapping("/alarmLevel/saveData")
    @ResponseBody
    public Map<Integer, String> saveData(@RequestBody AlarmLevelDataGridRowCollection alarmLevelDataGridRowCollection, BindingResult bindingResult) throws BindException {

        // Init error messages list
        Map<Integer, String> errList;

        // If binding param has errors
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        try {
            // Check validate for grid rows
            AlarmLevelGridRowValidator alarmLevelGridRowValidator =
                    new AlarmLevelGridRowValidator(alarmLevelDataGridRowCollection, alarmLevelService, messageService);
            errList = alarmLevelGridRowValidator.validateGridRowData();

            // Non errors, saving data
            if (errList.isEmpty()) {
                alarmLevelService.updateAlarmLevelData(alarmLevelDataGridRowCollection);
            }
        } catch (Exception ex) {
            errList = new HashMap<>();
            errList.put(-1, messageService.text("", ex.getCause().getMessage()));
            return errList;
        }

        return errList;
    }
}
