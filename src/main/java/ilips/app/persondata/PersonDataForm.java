package ilips.app.persondata;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Form class for contains input parameters
 */
@Data
@ToString
public class PersonDataForm {

    @NotNull(message = "Org Id cannot be null")
    @Min(value = 0L, message = "Org Id must be positive integer")
    public Integer selectOrgId;
}
