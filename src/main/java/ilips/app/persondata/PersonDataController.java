package ilips.app.persondata;

import ilips.constant.MsgConst;
import ilips.domain.model.persondata.PersonData;
import ilips.domain.model.persondata.PersonDataDisp;
import ilips.domain.model.persondata.PersonDataGridRowCollection;
import ilips.domain.model.systempermission.SystemPermission;
import ilips.domain.model.user.UserDetail;
import ilips.domain.service.common.CommonService;
import ilips.domain.service.message.MessageService;
import ilips.domain.service.persondata.PersonDataService;
import ilips.domain.validator.persondata.PersonDataGridRowValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller class handle requests for Person data screen.
 */
@Controller
@RequestMapping("/account")
public class PersonDataController {

    /**
     * Person data service
     */
    private final PersonDataService personDataService;

    /**
     * Message service
     */
    private final MessageService messageService;

    /**
     * Common service
     */
    private final CommonService commonService;

    @Autowired
    public PersonDataController(PersonDataService personDataService,
                                MessageService messageService,
                                CommonService commonService) {
        this.personDataService = personDataService;
        this.messageService = messageService;
        this.commonService = commonService;
    }

    /**
     * Init screen
     *
     * @return
     */
    @GetMapping("/personData")
    public String index() {
        return "personData/personData";
    }

    /**
     * Get data for display grid
     *
     * @param form
     * @param bindingResult
     * @return Display data
     * @throws BindException
     */
    @PostMapping("/personData/loadData")
    @ResponseBody
    public PersonDataDisp loadData(@Valid PersonDataForm form, BindingResult bindingResult) throws BindException {

        // Init obj contain display data
        PersonDataDisp personDataDisp = new PersonDataDisp();

        // If binding param has errors
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        // Get parameters
        int selectOrgId = form.selectOrgId;

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetail userDetail = (UserDetail) auth.getPrincipal();
        int userPermission = userDetail.getSystemPermission();

        // Get person data for grid
        List<PersonData> personDataList = personDataService.getPersonDataList(selectOrgId, userPermission);

        // Get system permission data
        List<SystemPermission> systemPermissionList = commonService.getSystemPermissionList();

        // Set display data
        personDataDisp.setPersonDataList(personDataList);
        personDataDisp.setSystemPermissionList(systemPermissionList);

        return personDataDisp;
    }

    /**
     * Saving grid data to database
     *
     * @param personDataGridRowCollection personDataGridRowCollection
     * @param bindingResult               bindingResult
     * @return List error messages
     * @throws BindException
     */
    @PostMapping("/personData/saveData")
    @ResponseBody
    public Map<Integer, String> saveData(@RequestBody PersonDataGridRowCollection personDataGridRowCollection, BindingResult bindingResult) throws BindException {

        // Init error messages list
        Map<Integer, String> errList = new HashMap<>();

        // If binding param has errors
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        try {
            // Check validate for grid rows
            PersonDataGridRowValidator personDataGridRowValidator = new PersonDataGridRowValidator(personDataGridRowCollection, personDataService, messageService);
            errList = personDataGridRowValidator.validateGridRowData();

            // Non errors, saving data
            if (errList.isEmpty()) {
                personDataService.updatePersonData(personDataGridRowCollection);
            }
        } catch (Exception ex) {
            errList.put(-1, messageService.text(MsgConst.ERR_EM155, ex.getCause().getMessage()));
        }

        return errList;
    }
}


