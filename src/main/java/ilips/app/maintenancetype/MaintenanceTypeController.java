package ilips.app.maintenancetype;

import ilips.constant.MsgConst;
import ilips.domain.model.maintenancetype.MainteTypeData;
import ilips.domain.model.maintenancetype.MainteTypeDataDisp;
import ilips.domain.model.maintenancetype.MainteTypeDataGridRowCollection;
import ilips.domain.service.maintenancetype.MainteTypeService;
import ilips.domain.service.message.MessageService;
import ilips.domain.validator.maintenancetype.MainteTypeDataGridRowValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * class Mainte Type Data Repository.
 * @author PhuongVV
 * created on 7/20/2018
 */
@Controller
@RequestMapping("/account/maintenanceType")
public class MaintenanceTypeController {

    //Maintenance Type Service.
    private final MainteTypeService mainteTypeService;

    //Message Service.
    private final MessageService messageService;

    @Autowired
    public MaintenanceTypeController(MainteTypeService mainteTypeService, MessageService messageService) {
        this.mainteTypeService = mainteTypeService;
        this.messageService = messageService;
    }

    /**
     * init page.
     *
     * @return page jsp
     */
    @GetMapping
    public String index() {
        return "maintenanceType/maintenanceType";
    }

    /**
     *Get data from DB.
     *
     * @param form
     * @param bindingResult
     * @return json object from DB.
     *
     * @throws BindException
     */
    @PostMapping("/loadData")
    @ResponseBody
    public MainteTypeDataDisp loadData(@Valid MainteTypeDataForm form, BindingResult bindingResult) throws BindException {

        MainteTypeDataDisp mainteTypeDataDisp = new MainteTypeDataDisp();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        int selectMakerId = form.selectMakerId;
        int selectModelId = form.selectModelId;

        List<MainteTypeData> mainteTypeDataList = mainteTypeService.getMainteTypeList(selectMakerId, selectModelId);

        mainteTypeDataDisp.setMainteTypeDataList(mainteTypeDataList);

        return mainteTypeDataDisp;
    }

    /**
     * Save data to DB
     *
     * @param mainteTypeDataGridRowCollection
     * @param bindingResult
     * @return json object errror if exists.
     *
     * @throws BindException
     */
    @PostMapping("/saveData")
    @ResponseBody
    public Map<Integer, String> saveData(@RequestBody MainteTypeDataGridRowCollection mainteTypeDataGridRowCollection,
                                         BindingResult bindingResult) throws BindException {

        Map<Integer, String> errCodeList = new HashMap<>();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }


        MainteTypeDataGridRowValidator mainteTypeDataGridRowValidator = new MainteTypeDataGridRowValidator(mainteTypeService, mainteTypeDataGridRowCollection, messageService);

        errCodeList = mainteTypeDataGridRowValidator.validateGridRowData(mainteTypeDataGridRowCollection);

        if (errCodeList.isEmpty()) {
            try{
                mainteTypeService.updateMainteTypeData(mainteTypeDataGridRowCollection);
            } catch (Exception ex) {
                errCodeList.put(-1, messageService.text(MsgConst.ERR_EM155, ex.getCause().getMessage()));
            }
        }

        return errCodeList;
    }
}
