package ilips.app.maintenancetype;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@ToString
public class MainteTypeDataForm {
    @NotNull(message = "Maker Org Id cannot be null")
    @Min(value = 0L, message = "Maker Org Id must be positive integer")
    public Integer selectMakerId;

    @NotNull(message = "Model Id cannot be null")
    @Min(value = 0L, message = "Model Id must be positive integer")
    public Integer selectModelId;
}
