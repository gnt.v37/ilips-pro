package ilips.app.sample;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ToString
public class SampleForm {
    @NotNull
    private Integer id;
    @NotBlank
    private String name;
}
