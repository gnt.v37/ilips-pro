package ilips.app.sample;

import ilips.domain.service.common.CommonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/sample")
@Slf4j
public class SampleController {

    private final CommonService sampleService;

    @Autowired
    public SampleController(CommonService sampleService) {
        this.sampleService = sampleService;
    }

    @GetMapping
    public String index() {
        return "sample/sample";
    }

    @GetMapping("/{id}/{name}")
    @ResponseBody
    public Map<String, String> getItem(@PathVariable String id,
                                       @PathVariable String name) {
        log.info("Id: " + id + " - Name: " + name);
        Map<String, String> rs = new HashMap<>();
        rs.put("test1", "test1");
        rs.put("test2", "test2");
        rs.put("test3", "test3");
        rs.put("test4", sampleService.getModelName(15001, 100));
        return rs;
    }

    @PostMapping
    public ResponseEntity postItem(@Valid SampleForm form, BindingResult bindingResult) {
        log.info(form.toString());
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.accepted().build();
    }
}
