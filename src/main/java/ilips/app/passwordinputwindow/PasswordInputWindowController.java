package ilips.app.passwordinputwindow;

import ilips.domain.model.passwordinput.PasswordInputWindow;
import ilips.domain.validator.passwordinput.PasswordInputWindowValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

@Controller
@RequestMapping("/account/personData")
public class PasswordInputWindowController {

    private final PasswordInputWindowValidator passwordInputWindowValidator;

    @Autowired
    public PasswordInputWindowController(PasswordInputWindowValidator passwordInputWindowValidator) {
        this.passwordInputWindowValidator = passwordInputWindowValidator;
    }

    @GetMapping("/passwordInputWindow")
    @ResponseBody
    public String getPasswordInputWindow(@RequestParam(value = "password") String password) {
        this.passwordInputWindowValidator.setPasswordInputWindow(new PasswordInputWindow(password));
        return this.passwordInputWindowValidator.getSHA256Password();
    }


}

