<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>

<script>
    var errorsDataErr = "<m:message code='error.dataErr'/>";
    var infoConfirmEditing = "<m:message code='info.confirmEditing'/>";
    var infoConfirmCancel = "<m:message code='info.confirmCancel'/>";
    var infoConfirmSave = "<m:message code='info.confirmSave'/>";
    var required = "<m:message code='word.required'/>";
    var workInfoID = "<m:message code='word.workInfoID'/>";
    var workInfoConstructionNum = "<m:message code='word.workInfoConstructionNum'/>";
    var workInfoConstructionName = "<m:message code='word.workInfoConstructionName'/>";
    var workInfoDescription = "<m:message code='word.workInfoDescription'/>";

    var workInfoIDRequired = workInfoID + required;
    var workInfoConstructionNumRequired = workInfoConstructionNum + required;
    var workInfoConstructionNameRequired = workInfoConstructionName + required;
</script>