<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>

<script>
    var errorsDataErr = "<m:message code='error.dataErr'/>";
    var infoConfirmEditing = "<m:message code='info.confirmEditing'/>";
    var infoConfirmCancel = "<m:message code='info.confirmCancel'/>";
    var infoConfirmSave = "<m:message code='info.confirmSave'/>";
    var required = "<m:message code='word.required'/>";
    var orgDataID = "<m:message code='word.orgDataID'/>";
    var orgDataName = "<m:message code='word.orgDataName'/>";
    var orgDataManufacturerAttr = "<m:message code='word.orgDataManufacturerAttr'/>";
    var orgDataCustomerAttr = "<m:message code='word.orgDataCustomerAttr'/>";
    var orgDataOwningOrgAttr = "<m:message code='word.orgDataOwningOrgAttr'/>";
    var orgDataMaintenanceOrgAttr = "<m:message code='word.orgDataMaintenanceOrgAttr'/>";
    var orgDataOrgCode = "<m:message code='word.orgDataOrgCode'/>";
    var orgDataOrgClassification = "<m:message code='word.orgDataOrgClassification'/>";
    var orgDataOrgAbbreviation = "<m:message code='word.orgDataOrgAbbreviation'/>";
    var orgDataOrgAddress = "<m:message code='word.orgDataOrgAddress'/>";
    var orgDataOrgDetails = "<m:message code='word.orgDataOrgDetails'/>";

    var orgDataIDRequired = orgDataID + required;
    var orgDataNameRequired = orgDataName + required;
    var orgDataManufacturerAttrRequired = orgDataManufacturerAttr + required;
    var orgDataCustomerAttrRequired = orgDataCustomerAttr + required;
    var orgDataOwningOrgAttrRequired = orgDataOwningOrgAttr + required;
    var orgDataMaintenanceOrgAttrRequired = orgDataMaintenanceOrgAttr + required;

    var yes= "<m:message code='word.yes'/>";
    var no =  "<m:message code='word.no'/>";
    var staticListSelect = [{ Name:no, Id: 0 },{ Name:yes, Id: 1 },];

</script>