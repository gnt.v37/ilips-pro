<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>

<script>
    var errorsDataErr = "<m:message code='error.dataErr'/>";
    var infoConfirmEditing = "<m:message code='info.confirmEditing'/>";
    var infoConfirmCancel = "<m:message code='info.confirmCancel'/>";
    var infoConfirmSave = "<m:message code='info.confirmSave'/>";
    var required = "<m:message code='word.required'/>";
    var personDataID = "<m:message code='word.personDataID'/>";
    var personDataName = "<m:message code='word.personDataName'/>";
    var personDataUsrType = "<m:message code='word.personDataUsrType'/>";
    var personDataPwd = "<m:message code='word.personDataPwd'/>";
    var personDataLoginID = "<m:message code='word.personDataLoginID'/>";
    var personDataAdr = "<m:message code='word.personDataAdr'/>";
    var personDataTel = "<m:message code='word.personDataTel'/>";
    var personDataFax = "<m:message code='word.personDataFax'/>";
    var personDataMail = "<m:message code='word.personDataMail'/>";
    var personDataPwdExpireDate = "<m:message code='word.personDataPwdExpireDate'/>";
    var personDataAccExpireDate = "<m:message code='word.personDataAccExpireDate'/>";
    var personDataIdRequired = personDataID + required;
    var personDataNameRequired = personDataName + required;
    var personDataUsrTypeRequired = personDataUsrType + required;
</script>