<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>

<script>
    var errorsDataErr = "<m:message code='error.dataErr'/>";
    var infoConfirmEditing = "<m:message code='info.confirmEditing'/>";
    var infoConfirmCancel = "<m:message code='info.confirmCancel'/>";
    var infoConfirmSave = "<m:message code='info.confirmSave'/>";
    var required = "<m:message code='word.required'/>";
    var maintenanceTypeID = "<m:message code='word.maintenanceTypeID'/>";
    var maintenanceTypeName = "<m:message code='word.maintenanceTypeName'/>";
    var maintenanceTypeIDRequired = maintenanceTypeID + required;
    var maintenanceTypeNameRequired = maintenanceTypeName + required;

</script>