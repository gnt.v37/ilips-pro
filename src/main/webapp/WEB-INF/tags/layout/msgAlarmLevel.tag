<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>

<script>
    var errorsDataErr = "<m:message code='error.dataErr'/>";
    var infoConfirmEditing = "<m:message code='info.confirmEditing'/>";
    var infoConfirmCancel = "<m:message code='info.confirmCancel'/>";
    var infoConfirmSave = "<m:message code='info.confirmSave'/>";
    var required = "<m:message code='word.required'/>";
    var alarmLevelID = "<m:message code='word.alarmLevelID'/>";
    var alarmLevelName = "<m:message code='word.alarmLevelName'/>";
    var alarmLevelDescription = "<m:message code='word.alarmLevelDescription'/>";
    var alarmLevelEventType = "<m:message code='word.alarmLevelEventType'/>";
    var alarmLevelAlarmIndication = "<m:message code='word.alarmLevelAlarmIndication'/>";

    var alarmLevelIDRequired = alarmLevelID + required;
    var alarmLevelNameRequired = alarmLevelName + required;

    var alarmLevelAlarm= "<m:message code='word.alarmLevelAlarm'/>";
    var alarmLevelDailyReport = "<m:message code='word.alarmLevelDailyReport'/>";
    var alarmLevelMonthlyReport= "<m:message code='word.alarmLevelMonthlyReport'/>";
    var alarmLevelMonthlyReportPeriodicDistribution = "<m:message code='word.alarmLevelMonthlyReportPeriodicDistribution'/>";
    var alarmLevelMaintenanceTimeDelivery= "<m:message code='word.alarmLevelMaintenanceTimeDelivery'/>";
    var alarmLevelBulkUpload =  "<m:message code='word.alarmLevelBulkUpload'/>";
    var alarmLevelPreMainReportPerDelivery= "<m:message code='word.alarmLevelPreMainReportPerDelivery'/>";
    //list select event type
    var listSelectEventType = [
    { Name:alarmLevelAlarm, Id: 1 },{ Name:alarmLevelDailyReport, Id: 2 },{ Name:alarmLevelMonthlyReport, Id: 3 },
    { Name:alarmLevelMonthlyReportPeriodicDistribution, Id: 4 },{ Name:alarmLevelMaintenanceTimeDelivery, Id: 5 },
    { Name:alarmLevelBulkUpload, Id: 6 },{ Name:alarmLevelPreMainReportPerDelivery, Id: 7},
    ];

    var alarmLevelNotDo= "<m:message code='word.alarmLevelNotDo'/>";
    var alarmLevelDo= "<m:message code='word.alarmLevelDo'/>";
    //list select alarm indication
    var listSelectAlarmIndication = [{ Name:alarmLevelDo, Id: 1 },{ Name:alarmLevelNotDo, Id: 0 },];
</script>