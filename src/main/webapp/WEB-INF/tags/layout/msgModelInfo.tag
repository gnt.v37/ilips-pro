<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>

<script>
    var errorsDataErr = "<m:message code='error.dataErr'/>";
    var infoConfirmEditing = "<m:message code='info.confirmEditing'/>";
    var infoConfirmCancel = "<m:message code='info.confirmCancel'/>";
    var infoConfirmSave = "<m:message code='info.confirmSave'/>";
    var required = "<m:message code='word.required'/>";
    var modelInfoID = "<m:message code='word.modelInfoID'/>";
    var modelInfoName = "<m:message code='word.modelInfoName'/>";
    var modelInfoInstructions = "<m:message code='word.modelInfoInstructions'/>";

    var modelInfoIDRequired = modelInfoID + required;
    var modelInfoNameRequired = modelInfoName + required;
    var modelInfoInstructionsRequired = modelInfoInstructions + required;
</script>