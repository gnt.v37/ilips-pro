<%@ tag description="Overall Page template" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title><m:message code="layout.base.title" /></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/bootstrap4/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/main.css"/>
    <jsp:invoke fragment="header"/>
</head>
<body>
    <%@include file="parts/header.jsp"%>
    <div class="container-fluid bg-light fullHeight">
        <div class="row">
            <%--<sec:authorize access="isAuthenticated()">
                <%@include file="leftMenu.jsp"%>
            </sec:authorize>--%>
            <main role="main" class="col ilip-content">
                <jsp:doBody/>
            </main>
        </div>
    </div>
    <jsp:invoke fragment="footer"/>
    <script src="${pageContext.request.contextPath}/assets/js/common/jquery/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/common/bootstrap4/bootstrap.bundle.js"></script>
    <script>
    var _BASE = "${pageContext.request.contextPath}";
    var _LOCALE = "${pageContext.response.locale}";
    </script>
    <script src="${pageContext.request.contextPath}/assets/js/common/core.js"></script>
    <%@include file="parts/footer.jsp"%>
</body>
</html>