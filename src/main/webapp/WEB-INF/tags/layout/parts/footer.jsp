<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


    <div class="footer bg-light">
        <div class="container">
            <p class="copyright">All rights reversed. Copyright @IHI Corporation</p>
            <p class="version">ILIPSPRO V2.16</p>
        </div>
    </div>

