<%@tag description="Modal Template" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@attribute name="footer" fragment="true" %>
<%@attribute name="id" %>
<%@attribute name="title" %>
<%@attribute name="_class" required="false" %>
<div class="modal fade ${_class}" id="${id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">${title}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <jsp:doBody/>
            </div>
            <div class="modal-footer">
                <jsp:invoke fragment="footer"/>
            </div>
        </div>
    </div>
</div>