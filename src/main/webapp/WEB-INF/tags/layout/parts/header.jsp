<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags" %>

    <nav class="navbar navbar-expand-lg navbar-light bg-light ilip-navbar">
        <div class="container">
        <a class="navbar-brand" href="<c:url value='/'/>">
            <m:message code="info.navBrand"/>
        </a>

        <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
            <ul class="navbar-nav px-3">
                <sec:authorize access="isAuthenticated()">
                    <li class="nav-item dropdown show">
                        <a class="nav-link dropdown-toggle" href="#" id="userInformationDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <sec:authentication property="principal.username"/>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userInformationDropdown">
                            <%--<a class="dropdown-item" href="javascript:void()">Information</a>--%>
                            <a class="dropdown-item" href="javascript:signOut()" id="userSignOut">Sign out</a>
                        </div>
                    </li>
                </sec:authorize>
                <sec:authorize access="!isAuthenticated()">
                <%--<li class="nav-item">
                    <a class="nav-link" href="<c:url value='/login'/>">Sign In</a>
                </li>--%>
                </sec:authorize>
            </ul>
        </div>
        </div>
    </nav>
