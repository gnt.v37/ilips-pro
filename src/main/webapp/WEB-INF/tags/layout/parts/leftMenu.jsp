    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


        <nav class="col-md-2 d-none d-md-block sidebar ilip-menu">
        <ul class="nav flex-column nav-pills">
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.csigsSetting"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.productAdditionWizard"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.dataCollectionModule"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/account/maintenanceType">
        <m:message code="word.maintenanceType"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/account/modelInfo">
        <m:message code="word.modelInformation"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.productInfo_1"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.productInfo_2"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/account/referableOrg">
        <m:message code="word.referableOrganization"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.createFormTemplate"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.sharedGraphSetting"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.tagDefinition"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/account/workInfo">
        <m:message code="word.constructionInformation"/>
        </a>
        </li>

        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.destinationList"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.sendTestMail"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.alarmCode"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/account/alarmLevel">
        <m:message code="word.alarmLevel"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.mailTranmissionSettings"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/account/orgData">
        <m:message code="word.organizationData"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.permissionSetting"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/account/personData">
        <m:message code="word.personData"/>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">
        <m:message code="word.systemPrivilege"/>
        </a>
        </li>

        </ul>
        </nav>