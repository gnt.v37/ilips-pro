<%@ tag description="Overall Page template" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title><m:message code="layout.base.title"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/bootstrap4/bootstrap.min.css"/>
    <jsp:invoke fragment="header"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/main.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jsgrid/jsgrid-theme.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jsgrid/jsgrid.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jstree_default/style.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jquery-ui/jquery-ui.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jquery-ui/jquery-ui-timepicker-addon.min.css"/>
</head>
    <body>
        <%@include file="parts/header.jsp" %>
        <div class="container-fluid bg-light fullHeight">
            <div class="container wrpContent">
                <div class="row">
                    <sec:authorize access="isAuthenticated()">
                        <%@include file="parts/leftMenu.jsp" %>
                    </sec:authorize>
                    <main role="main" class="col-md-10 col ilip-content">
                        <jsp:doBody/>
                    </main>
                </div>
            </div>
        </div>
        <%@include file="parts/footer.jsp" %>
        <script>
            var _BASE = "${pageContext.request.contextPath}";
            var _LOCALE = "${pageContext.response.locale}";
        </script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jquery/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/bootstrap4/bootstrap.bundle.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/cleave/cleave.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jquery-validation-1.17/jquery.validate.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jquery-validation-1.17/additional-methods.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/lodash/lodash.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsTree/jquery.jstree.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jquery-ui/jquery-ui.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jquery-ui/datepicker-${pageContext.response.locale}.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jquery-ui/jquery-ui-timepicker-addon.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jquery-ui/jquery-ui-timepicker-addon-i18n.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jquery-ui/jquery-ui-timepicker-${pageContext.response.locale}.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/jsGrid.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/i18n/jsGrid-ja.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/jsGrid-custom.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/core.js"></script>
        <jsp:invoke fragment="footer"/>
    </body>
</html>