<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>

<script>
    var errorsDataErr = "<m:message code='error.dataErr'/>";
    var infoConfirmEditing = "<m:message code='info.confirmEditing'/>";
    var infoConfirmCancel = "<m:message code='info.confirmCancel'/>";
    var infoConfirmSave = "<m:message code='info.confirmSave'/>";
    var required = "<m:message code='word.required'/>";
    var referableOrganization = "<m:message code='word.referableOrganization'/>";
    var referableOrganizationRequired = referableOrganization + required;
</script>