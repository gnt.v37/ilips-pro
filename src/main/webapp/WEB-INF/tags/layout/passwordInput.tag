<%@tag description="Select Password" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@attribute name="footer" fragment="true" %>
<%@attribute name="id" %>
<%@attribute name="title" %>
<%@attribute name="_class" required="false" %>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>
<div class="modal fade ${_class}" id="${id}" tabindex="-1" role="dialog" aria-labelledby="Select Password" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><m:message code='word.passwordInputTitle'/></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
             <form class="form-horizontal">
                <div class="form-group row">
                 <label class="col-sm-4 control-label" for="password">
                    <m:message code='word.passwordInputPassword'/>
                 </label>
                 <div class="col-sm-8">
                      <input class="form-control" type="password" id="password">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 control-label" for="reenterPassword">
                    <m:message code='word.passwordInputReinputPassword'/>
                  </label>
                  <div class="col-sm-8">
                    <input class="form-control" type="password" id="reenterPassword">
                  </div>
                </div>
             </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="btnPasswordInputOk"><m:message code='word.ok'/></button>
                <button type="button" class="btn btn-primary" id="btnPasswordInputCancel"><m:message code='word.cancel'/></button>
                <jsp:invoke fragment="footer"/>
            </div>
        </div>
    </div>
    <script>
    var _BASE = "${pageContext.request.contextPath}";
    var _LOCALE = "${pageContext.response.locale}";
    </script>
    <script src="${pageContext.request.contextPath}/assets/js/passwordInput/passwordInput.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/passwordInput/passwordInput.service.js"></script>

    <script>
        var passwordTooSmall = "<m:message code='error.EM143'/>";
        var passwordNotMatchRegex = "<m:message code='error.EM144'/>";
        var passwordNotMatchReinput = "<m:message code='error.EM180'/>";
    </script>
</div>
