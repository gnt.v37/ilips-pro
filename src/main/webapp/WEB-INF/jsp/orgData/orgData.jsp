<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" contentType="text/html" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="parts" tagdir="/WEB-INF/tags/layout/parts" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<ui:msgOrgData></ui:msgOrgData>
<ui:default>
    <jsp:attribute name="header">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jsgrid/jsgrid-theme.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jsgrid/jsgrid.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jstree_default/style.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/orgData/orgData.${pageContext.response.locale}.css"/>
    </jsp:attribute>
    <jsp:attribute name="footer">
        <script src="${pageContext.request.contextPath}/assets/js/common/jsTree/jquery.jstree.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jquery-ui/jquery-ui.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/jsGrid.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/i18n/jsGrid-ja.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/jsGrid-custom.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/orgData/orgData.service.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/orgData/orgData.js"></script>
    </jsp:attribute>
    <jsp:body>
        <div class="right_content">
            <div class="form-group">
                <h2 class="content_title">
                    <m:message code='word.organizationData'/>
                </h2>
                <h4 class="selectedOrg">
                    <m:message code='word.orgDataParentOrg'/>
                </h4>
                <div class="form-inline wrpSelectOrg">
                    <textarea id="selectOrgName" rows="3" cols="30" readonly="readonly" class="form-control selectOrgName"></textarea>
                    <button type="button" class="btn btn-secondary" id="selectOrgBtn"><m:message code='word.orgDataOrgSelection'/></button>
                    <button type="button" class="btn btn-secondary" id="clearOrgBtn"><m:message code='word.orgDataSelectionClear'/></button>
                </div>
            </div>

            <textarea id="errMsg" rows="4" cols="100" class="form-control wrpErrMsg" style="display: none"></textarea>

            <div class="form-group">
                <div id="jsGrid"></div>
            </div>
            <div class="form-group wrpActionBtn">
                <button type="button" class="btn btn-primary" id="btnConfirm"><m:message code='word.confirm'/></button>
                <button type="button" class="btn btn-secondary" id="btnCancel"><m:message code='word.cancel'/></button>
            </div>
        </div>
         <m:message code='word.orgDataOrgSelection' var="titleModel"/>
         <parts:modal id="selectOrgModal" title="${titleModel}">
            <jsp:attribute name="footer">
                <button type="button" class="btn btn-secondary"><m:message code='word.ok'/></button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><m:message code='word.cancel'/></button>
            </jsp:attribute>
            <jsp:body>
                <form>
                    this is modal
                </form>
            </jsp:body>
         </parts:modal>
    </jsp:body>
</ui:default>