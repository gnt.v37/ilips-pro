<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" contentType="text/html" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<ui:default>
<jsp:attribute name="header"></jsp:attribute>
<jsp:attribute name="footer"/>
<jsp:body>
    <div class="container">
        <h1>User <sec:authentication property="principal.username" /> Information!</h1>
        <span class="label label-primary">
            <m:message code="user.info.hello"/>
        </span>

        <sec:authorize access="hasAuthority('1')">
        <h3>
            <span class="label label-success">This content will appear if user have GrantedAuthority(1)</span>
        </h3>
        </sec:authorize>
        <sec:authorize access="!hasAuthority('10')">
            <h3>
                <span class="label label-danger">This content will appear if user don't have GrantedAuthority(10)</span>
            </h3>
        </sec:authorize>
        <sec:authorize access="hasRole('400')">
            <h3>
                <span class="label label-danger">This content will appear if user in GROUP 400</span>
            </h3>
        </sec:authorize>
        <sec:authorize access="!hasRole('400')">
            <h3>
                <span class="label label-danger">This content will appear if user not in GROUP 400</span>
            </h3>
        </sec:authorize>
    </div>
</jsp:body>
</ui:default>