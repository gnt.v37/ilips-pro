<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" contentType="text/html" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>
<ui:default>
  <jsp:attribute name="header"></jsp:attribute>
  <jsp:attribute name="footer"></jsp:attribute>
  <jsp:body>
    <div class="container">
      <h1>User Register Form!</h1>

        <c:choose>
            <c:when test="${not empty errorMsg}">
                <div class="alert alert-danger">${errorMsg}</div>
            </c:when>
            <c:when test="${success}">
                <div class="alert alert-success">Success</div>
            </c:when>
            <c:otherwise>

            </c:otherwise>
        </c:choose>

      <form:form method="POST" action="/user/register" modelAttribute="loginForm">
        <div class="form-group">
          <label for="username"><m:message code="login.username"/></label>
          <form:input path="username" id="username" class="form-control" />
          <form:errors path="username" cssClass="help-block" />
        </div>
        <div class="form-group">
          <label for="password"><m:message code="login.password"/></label>
          <form:input path="password" id="password" class="form-control" />
          <form:errors path="password" cssClass="help-block" />
        </div>
        <button type="submit" class="btn btn-default">Submit</button>

      </form:form>
    </div>
  </jsp:body>
</ui:default>