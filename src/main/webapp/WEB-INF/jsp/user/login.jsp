<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" contentType="text/html" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/fmt" %>
<ui:default>
  <jsp:attribute name="header">
  </jsp:attribute>
  <jsp:attribute name="footer">
  </jsp:attribute>
  <jsp:body>
    <div class="container">
      <div id="loginbox" class="mainbox col-md-7">
        <div class="card card-info">
          <div class="card-header">
            <div class="card-title">Sign In</div>
            <%--<div class="forgetPass"><a href="#">Forgot password?</a></div>--%>
          </div>
          <div class="card-body">
            <c:if test="${success}">
              <div class="alert alert-success">Success</div>
            </c:if>
            <c:if test="${param.error != null}">
              <div class="alert alert-danger">
                <m:message code="login.error.badCredential"/>
              </div>
            </c:if>
            <form:form method="POST" action="/login" cssClass="form-horizontal">
              <div class="form-group">
                <label for="username"><m:message code="login.username"/> </label>
                <input type="text" class="form-control" name="username" id="username"/>
              </div>
              <div class="form-group">
                <label for="password"><m:message code="login.password"/></label>
                <input type="password" class="form-control" name="password" id="password"/>
              </div>
              <div class="input-group">
                <div class="form-check form-check-inline">
                  <label class="form-check-label">
                    <input class="form-check-input langCode" type="radio" name="langCode" value="en" id="langCodeEn"><m:message code="info.langEn" />
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <label class="form-check-label">
                    <input class="form-check-input langCode" type="radio" name="langCode" value="ja" id="langCodeJa"><m:message code="info.langJa" />
                  </label>
                </div>
              </div>
              <div class="wrpLoginBtn form-group">
                <button type="submit" class="btn btn-primary"><m:message code="login.submit"/></button>
              </div>
            </form:form>
          </div>
        </div>
      </div>
    </div>
  </jsp:body>
</ui:default>