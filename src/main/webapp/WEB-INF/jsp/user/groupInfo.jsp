<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" contentType="text/html" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<ui:default>
<jsp:attribute name="header"></jsp:attribute>
<jsp:attribute name="footer"/>
<jsp:body>
    <div class="container">
        <h1>User GROUP <sec:authentication property="principal.group" /> Information!</h1>
        <div>
            <span class="label label-success">User can view this page if have group 400</span>
        </div>
        <span class="label label-primary">
            <m:message code="user.info.hello"/>
        </span>
    </div>
</jsp:body>
</ui:default>