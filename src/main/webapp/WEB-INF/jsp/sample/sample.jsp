<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" contentType="text/html" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<ui:default>
    <jsp:attribute name="header">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/sample.css"/>
    </jsp:attribute>
    <jsp:attribute name="footer">
        <script type="text/javascript"
                src="${pageContext.request.contextPath}/assets/js/sample/sample.service.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/sample/sample.js"></script>
    </jsp:attribute>
    <jsp:body>
        <div class="container">
            <h1>Login Form!</h1>

            <form:form method="POST" action="/sample" novalidate="true" id="form">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" name="exampleInputEmail1"
                           aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="exampleInputNumber">Number required with Min = 5, Max = 10000</label>
                    <input type="number" class="form-control" id="exampleInputNumber" name="exampleInputNumber"
                           min="5" max="10000" required
                           aria-describedby="emailHelp" placeholder="Enter Number">
                </div>
                <div class="form-group">
                    <label for="exampleInputText">Text with minLength = 10, maxLength = 20</label>
                    <input type="text" class="form-control" id="exampleInputText" name="exampleInputText"
                           minlength="10" maxlength="20" required
                           aria-describedby="exampleInputTextHelp" placeholder="Enter Text">
                </div>

                <button type="submit" class="btn btn-default">
                    <m:message code="login.submit"/>
                </button>
                <button type="button" class="btn btn-default" id="btnShowModal">
                    Show Modal
                </button>
                <button type="button" class="btn btn-secondary" id="btnGetInfo">Get Information</button>
                <button type="button" class="btn btn-secondary" id="btnTestMessage">Test message "user.info.hello"</button>
            </form:form>
        </div>
        <ui:modal id="sampleModal" title="This is sample dialog">
            <jsp:attribute name="footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </jsp:attribute>
            <jsp:body>
                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Recipient:</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Message:</label>
                        <textarea class="form-control" id="message-text"></textarea>
                    </div>
                </form>
            </jsp:body>
        </ui:modal>
    </jsp:body>
</ui:default>