<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" contentType="text/html" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="parts" tagdir="/WEB-INF/tags/layout/parts" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<ui:msgReferableOrg></ui:msgReferableOrg>
<ui:default>
    <jsp:attribute name="header">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jsgrid/jsgrid-theme.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jsgrid/jsgrid.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/referableOrg/referableOrg.${pageContext.response.locale}.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jstree_default/style.css" />
    </jsp:attribute>
    <jsp:attribute name="footer">
        <script src="${pageContext.request.contextPath}/assets/js/common/jquery-ui/jquery-ui.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/jsGrid.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/i18n/jsGrid-ja.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/jsGrid-custom.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/referableOrg/referableOrg.service.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/referableOrg/referableOrg.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsTree/jquery.jstree.js"></script>
    </jsp:attribute>
    <jsp:body>

        <div class="right_content">
            <div class="form-group">
                <h2 class="content_title">
                    <m:message code='word.referableOrganization'/>
                </h2>
                <h4 class="selectedOrg">
                    <m:message code='word.referableOrgSelectedOrg'/>
                </h4>
                <div class="form-inline wrpSelectOrg">
                    <textarea id="selectOrgName" rows="3" cols="30" readonly="readonly" class="form-control selectOrgName"></textarea>
                    <button type="button" class="btn btn-secondary" id="selectOrgBtn"><m:message code='word.referableOrgSelection'/></button>
                </div>

            </div>

            <textarea id="errMsg" rows="4" cols="100" class="form-control wrpErrMsg" style="display: none"></textarea>

            <div class="form-group">
                <div id="jsGrid"></div>
            </div>
            <div class="form-group wrpActionBtn">
                <button type="button" class="btn btn-primary" id="btnConfirm"><m:message code='word.confirm'/></button>
                <button type="button" class="btn btn-secondary" id="btnCancel"><m:message code='word.cancel'/></button>
            </div>
        </div>
         <m:message code='word.referableOrgSelection' var="titleModel"/>
         <ui:modalSelectOrgWindow id="selectOrgModal" title="${titleModel}">
            <jsp:attribute name="footer">
                 <button id="btnOk" type="button" class="btn btn-secondary"><m:message code='word.ok'/></button>
                 <button id="btnCancel" type="button" class="btn btn-primary" data-dismiss="modal"><m:message code='word.cancel'/></button>
             </jsp:attribute>
            <jsp:body>
                <div id="jsTree"></div>
            </jsp:body>
         </ui:modalSelectOrgWindow>
    </jsp:body>
</ui:default>