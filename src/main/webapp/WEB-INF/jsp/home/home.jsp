<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" contentType="text/html" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<ui:defaultHome>
    <jsp:attribute name="header"></jsp:attribute>
    <jsp:attribute name="footer"/>
    <jsp:body>
        <div class="ilip-menu-home">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="wrpMenuItem">
                            <div class="headerMenu headerAccount"><m:message code="word.headerAccount" /></div>
                            <div class="contentMenu">
                                <nav class="d-none d-md-block sidebar">
                                    <ul class="nav flex-column nav-pills">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">
                                                <m:message code="word.mailTranmissionSettings" />
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/account/orgData">
                                                <m:message code="word.organizationData" />
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">
                                                <m:message code="word.permissionSetting" />
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/account/personData">
                                                <m:message code="word.personData" />
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">
                                                <m:message code="word.systemPrivilege" />
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="wrpMenuItem">
                        <div class="headerMenu headerAlarm"><m:message code="word.headerAlarm" /></div>
                        <div class="contentMenu">
                            <nav class="d-none d-md-block sidebar">
                                <ul class="nav flex-column nav-pills">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <m:message code="word.destinationList" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <m:message code="word.sendTestMail" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <m:message code="word.alarmCode" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="/account/alarmLevel">
                                            <m:message code="word.alarmLevel" />
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="wrpMenuItem">
                        <div class="headerMenu headerProduct"><m:message code="word.headerProduct" /></div>
                        <div class="contentMenu">
                            <nav class="d-none d-md-block sidebar">
                                <ul class="nav flex-column nav-pills">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <m:message code="word.csigsSetting" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <m:message code="word.productAdditionWizard" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <m:message code="word.dataCollectionModule" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="/account/maintenanceType">
                                            <m:message code="word.maintenanceType" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="/account/modelInfo">
                                            <m:message code="word.modelInformation" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <m:message code="word.productInfo_1" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <m:message code="word.productInfo_2" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="/account/referableOrg">
                                            <m:message code="word.referableOrganization" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <m:message code="word.createFormTemplate" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <m:message code="word.sharedGraphSetting" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <m:message code="word.tagDefinition" />
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="/account/workInfo">
                                            <m:message code="word.constructionInformation" />
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</ui:defaultHome>