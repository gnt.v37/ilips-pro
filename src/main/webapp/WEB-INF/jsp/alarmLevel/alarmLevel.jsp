<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" contentType="text/html" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="parts" tagdir="/WEB-INF/tags/layout/parts" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<ui:msgAlarmLevel></ui:msgAlarmLevel>
<ui:default>
    <jsp:attribute name="header">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jsgrid/jsgrid-theme.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jsgrid/jsgrid.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/alarmLevel/alarmLevel.${pageContext.response.locale}.css"/>
    </jsp:attribute>
    <jsp:attribute name="footer">
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/jsGrid.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/i18n/jsGrid-ja.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/jsGrid-custom.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/alarmLevel/alarmLevel.service.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/alarmLevel/alarmLevel.js"></script>
    </jsp:attribute>
    <jsp:body>
        <div class="right_content">
            <div class="form-group">
                <h2 class="content_title">
                    <m:message code='word.alarmLevel'/>
                </h2>
            </div>

            <textarea id="errMsg" rows="4" cols="100" class="form-control wrpErrMsg" style="display: none"></textarea>

            <div class="form-group">
                <div id="jsGrid"></div>
            </div>
            <div class="form-group wrpActionBtn">
                <button type="button" class="btn btn-primary" id="btnConfirm"><m:message code='word.confirm'/></button>
                <button type="button" class="btn btn-secondary" id="btnCancel"><m:message code='word.cancel'/></button>
            </div>
        </div>
    </jsp:body>
</ui:default>