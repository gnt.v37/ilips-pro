<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" contentType="text/html" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<ui:msgPersonData></ui:msgPersonData>
<ui:default>
    <jsp:attribute name="header">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/personData/personData.${pageContext.response.locale}.css"/>
    </jsp:attribute>
    <jsp:attribute name="footer">
        <script src="${pageContext.request.contextPath}/assets/js/personData/personData.service.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/personData/personData.js"></script>
    </jsp:attribute>
    <jsp:body>
        <div class="right_content">
            <div class="form-group">
                <h2 class="content_title">
                    <m:message code='word.personData'/>
                </h2>
                <h4 class="selectedOrg">
                    <m:message code='word.personDataAffiliationOrg'/>
                </h4>
                <div class="form-inline wrpSelectOrg">
                    <textarea id="selectOrgName" rows="3" cols="30" readonly="readonly" class="form-control selectOrgName"></textarea>
                    <button type="button" class="btn btn-secondary" id="selectOrgBtn"><m:message code='word.personDataOrgSelection'/></button>
                </div>
            </div>

            <textarea id="errMsg" rows="4" cols="100" class="form-control wrpErrMsg" style="display: none"></textarea>

            <div class="form-group">
                <div id="jsGrid"></div>
            </div>
            <div class="form-group wrpActionBtn">
                <button type="button" class="btn btn-primary" id="btnConfirm" disabled><m:message code='word.confirm'/></button>
                <button type="button" class="btn btn-secondary" id="btnCancel" disabled><m:message code='word.cancel'/></button>
            </div>
        </div>
        <ui:passwordInput id="passwordInput"></ui:passwordInput>
    </jsp:body>
</ui:default>