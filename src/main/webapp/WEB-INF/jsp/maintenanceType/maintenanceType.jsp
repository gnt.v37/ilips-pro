<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" contentType="text/html" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="parts" tagdir="/WEB-INF/tags/layout/parts" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="m" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<ui:msgMaintenanceType></ui:msgMaintenanceType>
<ui:default>
    <jsp:attribute name="header">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jsgrid/jsgrid-theme.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jsgrid/jsgrid.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common/jstree_default/style.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/maintenanceType/maintenanceType.${pageContext.response.locale}.css"/>
    </jsp:attribute>
    <jsp:attribute name="footer">
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/jsGrid.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/i18n/jsGrid-ja.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/common/jsGrid/jsGrid-custom.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/maintenanceType/maintenanceType.service.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/maintenanceType/maintenanceType.js"></script>
    </jsp:attribute>
    <jsp:body>
        <div class="right_content">
            <div class="form-group">
                <h2 class="content_title">
                    <m:message code='word.maintenanceType'/>
                </h2>
                <div class="form-group">
                   <div class="form-inline mb-2 selectModel">
                     <label for="txtManufacturer" class="d-flex justify-content-start"><m:message code='word.maintenanceTypeManufacturer'/></label>
                     <input id="txtManufacturer" type="text" class="form-control" readonly>
                   </div>
                   <div class="form-inline selectModel">
                     <label for="txtModel" class="d-flex justify-content-start"><m:message code='word.maintenanceTypeModel'/></label>
                     <input id="txtModel" type="text" class="form-control" readonly>
                     <button id="selectModelBtn" type="button" class="btn btn-secondary ml-3" ><m:message code='word.maintenanceTypeModelSelection'/></button>
                     <button id="selectModelOtherBtn" type="button" class="btn btn-secondary position-absolute"><m:message code='word.maintenanceTypeUseOtherTypes'/></button>
                   </div>
                </div>
            </div>

            <textarea id="errMsg" rows="4" cols="100" class="form-control wrpErrMsg" style="display: none"></textarea>

            <div class="form-group">
                <div id="jsGrid"></div>
            </div>
            <div class="form-group wrpActionBtn">
                <button type="button" class="btn btn-primary" id="btnConfirm"><m:message code='word.confirm'/></button>
                <button type="button" class="btn btn-secondary" id="btnCancel"><m:message code='word.cancel'/></button>
            </div>
        </div>
         <m:message code='word.maintenanceTypeModelSelection' var="titleModal"/>
         <parts:modal id="selectModelModal" title="${titleModal}">
            <jsp:attribute name="footer">
                <button type="button" class="btn btn-secondary"><m:message code='word.ok'/></button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><m:message code='word.cancel'/></button>
            </jsp:attribute>
            <jsp:body>
                <form>
                    this is modal
                </form>
            </jsp:body>
         </parts:modal>
    </jsp:body>
</ui:default>