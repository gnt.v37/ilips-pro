/**
 * Define all service for calling to backend
 */
(function(w) {
    'use strict';
    var URL_LOAD_DATA = _BASE + "/account/personData/loadData";
    var URL_SAVE_DATA = _BASE + "/account/personData/saveData";
    var URL_OPEN_ORG_DATA_TREE = _BASE + "/account/orgData";

    w.PersonData = {
        getPersonDataByOrgId: getPersonDataByOrgId,
        savePersonData: savePersonData,
        openOrgDataTreeDialog: openOrgDataTreeDialog
    };

    function getPersonDataByOrgId(selectOrgId) {
        if (!selectOrgId) return;

        var postData = {
            selectOrgId: selectOrgId
        };
        return $.post(URL_LOAD_DATA, postData);
    }

    function savePersonData(arrInsertedData, arrUpdatedData, arrDeletedData) {
        var postData = {
            arrInsertedData: arrInsertedData,
            arrUpdatedData: arrUpdatedData,
            arrDeletedData: arrDeletedData
        };

        return $.ajax({
            type: "POST",
            contentType : 'application/json; charset=utf-8',
            dataType : 'json',
            url: URL_SAVE_DATA,
            data: JSON.stringify(postData)
        });
    }
    
    function openOrgDataTreeDialog() {
        return $.get(URL_OPEN_ORG_DATA_TREE);
    }
})(window);