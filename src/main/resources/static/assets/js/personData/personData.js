/**
 * ======================================
 * GLOBAL VARIABLES
 * ======================================
 */
// init arrays
// NOT CHANGED
var ARR_COLUMNS_FIXED = [1, 2];
var COLUMN_ROW_NO = 2;
var IS_GRID_CHANGED = false;

// CAN CHANGED
// var COLUMN_ROW_ORG_ID = 3; (not using)
var COLUMN_FOCUS_WHEN_INSERTING = 4;
var COLUMN_ROW_PERSON_DATA_ID = 4;

// For grid
var GRID_TABLE;
var ARR_UPDATED_ROW = [];
var ARR_DELETED_ROW = [];
var ARR_INSERTED_ROW = [];

// For screen
var personData = [];
var sysPermissionData = [];

// Dummy var
//var selectOrgId = 10;
var selectOrgId = 6600;

/**
 * ======================================
 * READY FUNCTION
 * ======================================
 */
$(function() {
    'use strict';


    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");

    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader(header, token);
        }
    });

    // call dummy function
    loadGridData();

    // Event click button Confirm
    $("#btnConfirm").click(function() {
        if ($("tbody tr").hasClass("jsgrid-edit-row") || ($("tfoot tr").hasClass("jsgrid-insert-row")
                && !($('tfoot tr.jsgrid-insert-row').css('display') == 'none'))) {
            var r = confirm(infoConfirmEditing);
            if (r == true) {
                $("#jsGrid").jsGrid("cancelEdit");
                confirmSave();
                return true;
            } else {
                return false;
            }
        }
        confirmSave();
    });

    // Event click button Cancel
    $("#btnCancel").click(function() {
        var r = confirm(infoConfirmCancel);
        if (r == true) {
            setStateDisableBtn(true);
            resetGridSavingArray();
            clearErrors();
            loadGridData();
        } else {
            return false;
        }
    });

    // Event click button Organization Selection
           $("#selectOrgBtn").click(function() {
//            var a = InputPassword('#passwordInput');
//                 a.then(function (b) {
//                   console.log(b);
//                   document.getElementById("selectOrgName").value = b;
//                 })

           });
});

/**
 * ======================================
 * FUNCTIONS
 * ======================================
 */

/**
 * Get list person data and handle response.
 */
function loadGridData() {
    PersonData.getPersonDataByOrgId(selectOrgId).then(function(resp) {
        if (typeof resp == "undefined") {
            alert(errorsDataErr);
        } else {
            personData = resp.personDataList;
            addColumnNoGridData(personData);

            sysPermissionData = resp.systemPermissionList;
            createGridTable();

            setStateDisableBtn(true);
        }
    });
}

/**
 * Saving grid data
 */
function saveGridData() {

    // Prepare data
    var copyArrInsertedRow = jQuery.extend(true, [], ARR_INSERTED_ROW);
    var copyArrUpdatedRow = jQuery.extend(true, [], ARR_UPDATED_ROW);
    var copyArrDeletedRow = jQuery.extend(true, [], ARR_DELETED_ROW);
    var intersecInsertAndDeleteArr = _.intersection(copyArrInsertedRow, copyArrDeletedRow);
    var copyArrInsertedRow = _.difference(copyArrInsertedRow, intersecInsertAndDeleteArr);
    var copyArrDeletedRow = _.difference(copyArrDeletedRow, intersecInsertAndDeleteArr);
    var intersecUpdateAndDeleteArr = _.intersection(copyArrUpdatedRow, copyArrDeletedRow);
    var copyArrUpdatedRow = _.difference(copyArrUpdatedRow, intersecUpdateAndDeleteArr);

    // clear server errors
    clearErrors();

    // get item data
    var arrInsertedData = [];
    var arrUpdatedData = [];
    var arrDeletedData = [];

    $.each(copyArrInsertedRow, function( index, value ) {
        var arrInsertedItem = getItemByColumnNo(value);
        arrInsertedData.push(arrInsertedItem);
    });

    $.each(copyArrUpdatedRow, function( index, value ) {
        var arrUpdatedItem = getItemByColumnNo(value);
        arrUpdatedData.push(arrUpdatedItem);
    });

    $.each(copyArrDeletedRow, function( index, value ) {
        var arrDeletedItem = getItemByColumnNo(value);
        arrInsertedData.push(arrDeletedItem);
    });

    // call service saving grid data
    PersonData.savePersonData(arrInsertedData, arrUpdatedData, arrDeletedData).then(function(resp) {
        if (typeof resp == "undefined") {
            alert(errorsDataErr);
        } else {
            var errList = resp;
            if (!jQuery.isEmptyObject(errList)) {
                $("#errMsg").show();
                $.each(errList, function(index, value) {
                    writeErrorMessages(index, value);
                });
            } else {
                loadGridData();
            }
        }
    });
}

/**
 * Confirm alert before saving grid data.
 */
function confirmSave() {
    var r2 = confirm(infoConfirmSave);
    if (r2 == true) {
        saveGridData();
    } else {
        return false;
    }
}

/**
 * Get row by row No
 */
function getRowByColumnNo(rowNum) {
    return $("#jsGrid tbody tr td:nth-child(" + COLUMN_ROW_NO + ")").filter(function() {
        return $(this).text() === rowNum;
    }).closest("tr");
}

/**
 * Create Grid
 */
function createGridTable() {

    // reset saving array
    resetGridSavingArray();

    // set locate for grid
    if (_LOCALE == "ja") {
        jsGrid.locale("ja");
    }

    // init Grid table
    GRID_TABLE = new jsGrid.Grid($("#jsGrid"), {
        width: "100%",
        height: "550px",

        inserting: true,
        editing: true,
        sorting: true,
        paging: false,
        add: true,
        data: personData,
        fields: [{
            type: "control",
            editButton: true,
            width: 60,
            headerTemplate: function() {
                console.log(this);
            }
        },
            {
                name: "rowNumber",
                title: "No",
                type: "number",
                width: 60,
                align: "left",
                editing: false,
                readOnly: false,
                insertTemplate: function(value, item) {
                    var newNo = parseInt(getArraySize(personData)) + 1;
                    return newNo;
                },
                insertValue: function() {
                    var newNo = parseInt(getArraySize(personData)) + 1;
                    return newNo;
                }
            },
            {
                name: "orgId",
                type: "number",
                css: "hide",
                width: 0,
                insertValue: function() {
                    return selectOrgId;
                },
                insertTemplate: function() {
                    return selectOrgId;
                }
            },
            {
                title: personDataIdRequired,
                name: "personId",
                type: "number",
                width: 120,
                validate: [{
                    validator: "required",
                    title: personDataID
                },{
                    message: "Field must be between -2147483648 and 2147483647",
                    title: personDataID,
                    validator: function(value, item) {
                        var INT_MIN_JAVA = -2147483648;
                        var INT_MAX_JAVA = 2147483647;
                        return value >= INT_MIN_JAVA && value <= INT_MAX_JAVA;
                    }
                }, {
                    message: "Field has duplicated",
                    title: personDataID,
                    validator: function(value, item) {

                        value = parseInt(value);
                        var isDuplicate = false;
                        var $editingRow = $("#jsGrid").jsGrid("option", "_editingRow");

                        for (var i = 0; i < personData.length; i++) {
                            var text = parseInt(personData[i]["personId"]);
                            if ($editingRow) {
                                var personIdRowEditing = parseInt($editingRow.data("JSGridItem").personId);

                                //console.log(personIdRowEditing);
                                if (text == value && value != personIdRowEditing) {
                                    isDuplicate = true;
                                }
                            } else {
                                if (text == value) {
                                    isDuplicate = true;
                                }
                            }
                        }

                        return isDuplicate ? false : true;
                    }
                }],
                align: "left",
                insertTemplate: function(value) {

                    if(!this.inserting) return "";

                    var dataSize = parseInt(getArraySize(personData));
                    if(!dataSize) {
                        return this.insertControl = this._createTextBox();
                    } else {
                        var elementHasMaxPersonId = getElementHasMaxPropInArray(personData, "personId");
                        if (!elementHasMaxPersonId) return;

                        var newPersonId = parseInt(elementHasMaxPersonId.personId) + 1;
                        var $textBox = $("<input>").attr("type", "number").prop("readonly", !!this.readOnly).val(newPersonId);
                        var $result = this.insertControl = $textBox;
                        return $result;
                    }
                }
            },
            {
                title: personDataNameRequired,
                name: "personName",
                type: "text",
                width: 150,
                validate: {
                    validator: "required",
                    title: personDataName
                },
                align: "left"
            },
            {
                title: personDataUsrTypeRequired,
                name: "permission",
                type: "select",
                width: 200,
                items: sysPermissionData,
                valueField: "permission",
                textField: "permissionName",
                validate: [{
                    validator: "required",
                    title: personDataUsrType
                }],
                align: "left"
            },
            {
                title: personDataLoginID,
                name: "loginId",
                type: "text",
                width: 150,
                align: "left"
            },
            {
                title: personDataPwd,
                name: "password",
                type: "text",
                width: 500,
                readOnly: true,
                align: "left",
                editTemplate: function(value, item) {
                    if (!this.editing) {
                        return this.itemTemplate.apply(this, arguments);
                    }

                    var $result = this.editControl = this._createTextBox();

                    //dblclick item passwword
                    $result.dblclick(function() {
                        var _this = this;
                        InputPassword('#passwordInput').then(function (passwordEncrypt) {
                            _this.value = passwordEncrypt;
                        })
                    });

                    $result.val(value);
                    return $result;
                },
                _createTextBox: function() {
                    var textbox = $("<input>").attr("type", "text").prop("readonly", !!this.readOnly);

                    textbox.click(function() {
                        console.log(this);
                    });

                    return textbox;
                }
            },
            {
                title: personDataAdr,
                name: "addr",
                type: "text",
                width: 200,
                align: "left"
            },
            {
                title: personDataTel,
                name: "tel",
                type: "text",
                width: 150,
                align: "left"
            },
            {
                title: personDataFax,
                name: "fax",
                type: "text",
                width: 150,
                align: "left"
            },
            {
                title: personDataMail,
                name: "email",
                type: "text",
                width: 200,
                align: "left"
            },
            {
                title: personDataPwdExpireDate,
                name: "passwordExpirationUtc",
                type: "dateTime",
                readOnly: true,
                width: 200,
                align: "left"
            },
            {
                title: personDataAccExpireDate,
                name: "accountExpirationUtc",
                type: "dateTime",
                readOnly: true,
                width: 200,
                align: "left"
            }
        ],
        rowClick: function(args) {

            var $target = $(args.event.target);

            if ($target.closest("#jsGrid .jsgrid-control-field").length) {
                // handle cell click
                return;
            }

            // otherwise handle row click
            var $row = $(args.event.target).closest("tr");
            var enableEditItem = true;

            if (this._editingRow) {
                var updateItem = this.updateItem();
                if(updateItem == 0) {
                    enableEditItem = false;
                } else if (updateItem == 1) {
                    enableEditItem = true;
                } else if (updateItem !== 0) {
                    updateItem.done($.proxy(function() {
                        enableEditItem = true;
                    }, this));
                    return;
                }
            }

            if(enableEditItem) {
                this.editing && this.editItem($row);
            }

            if (this.inserting) {
                var insertItem = this.insertItem();
                insertItem.done(function() {
                    GRID_TABLE.clearInsert();
                });
            }
        },

        _getItemFieldValue: function(item, field) {

            if (item == undefined) return;

            var props = field.name.split('.');
            var result = item[props.shift()];

            while (result && props.length) {
                result = result[props.shift()];
            }

            return result;
        },

        updateItem: function(item, editedItem) {

            if (arguments.length === 1) {
                editedItem = item;
            }

            var $row = item ? this.rowByItem(item) : this._editingRow;
            editedItem = editedItem || this._getValidatedEditedItem();

            if (!editedItem) return 0;

            // customize
            var prevEditData = $row.data("JSGridItem");
            var copyPrevEditData = jQuery.extend(true, {}, prevEditData);
            delete copyPrevEditData["rowNumber"];
            if (_.isMatchWith(copyPrevEditData, editedItem, customizer)) {
                return 1;
            }
            // end customize

            return this._updateRow($row, editedItem);
        },

        onItemEditing: function(args) {
            setTimeout(function() {
                UpdateColPos(ARR_COLUMNS_FIXED);
            }, 1);
        },

        deleteItem: function(item) {

            // Detect grid changes
            detectGridChanged();

            var $row = this.rowByItem(item);
            if (!$row.length) return;

            var $tdRow = $row.children('td');
            var rowNum = $tdRow.eq(COLUMN_ROW_NO - 1).text();

            if (existInArray(ARR_INSERTED_ROW, rowNum)) {

                if (existInArray(ARR_DELETED_ROW, rowNum)) {
                    ARR_DELETED_ROW.remove(rowNum);
                    $tdRow.removeClass("markDeleted");
                    $tdRow.addClass("markInserted");
                } else {
                    ARR_DELETED_ROW.push(rowNum);
                    $tdRow.removeClass("markInserted");
                    $tdRow.addClass("markDeleted");
                }
                return;
            }

            if (existInArray(ARR_UPDATED_ROW, rowNum)) {

                if (existInArray(ARR_DELETED_ROW, rowNum)) {
                    ARR_DELETED_ROW.remove(rowNum);
                    $tdRow.removeClass("markDeleted");
                    $tdRow.addClass("markUpdated");
                } else {
                    ARR_DELETED_ROW.push(rowNum);
                    $tdRow.removeClass("markUpdated");
                    $tdRow.addClass("markDeleted");
                }
                return;
            }

            if (existInArray(ARR_DELETED_ROW, rowNum)) {
                ARR_DELETED_ROW.remove(rowNum);
                $tdRow.removeClass("markDeleted");
            } else {
                ARR_DELETED_ROW.push(rowNum);
                $tdRow.addClass("markDeleted");
            }
        },
        onItemDeleted: function(args) {},

        onItemInserted: function(args) {

            // Detect grid changes
            detectGridChanged();

            var item = args.item;
            var $row = this.rowByItem(item);
            if (!$row.length) return;

            var $tdRow = $row.children('td');
            var rowNum = $tdRow.eq(COLUMN_ROW_NO - 1).text();

            ARR_INSERTED_ROW.push(rowNum);
            $("#jsGrid .jsgrid-insert-mode-button").click();
        },

        onItemUpdated: function(args) {

            // Detect grid changes
            detectGridChanged();

            UpdateColPos(ARR_COLUMNS_FIXED);

            var $row = args.row;
            if (!$row.length) return;
            var $tdRow = $row.children('td');
            var rowNum = $tdRow.eq(COLUMN_ROW_NO - 1).text();

            if (existInArray(ARR_INSERTED_ROW, rowNum)) {
                $tdRow.addClass("markInserted");
                return;
            }

            if (existInArray(ARR_DELETED_ROW, rowNum)) {
                ARR_DELETED_ROW.remove(rowNum);
                ARR_UPDATED_ROW.push(rowNum);
                $tdRow.removeClass("markDeleted");
                $tdRow.addClass("markUpdated");
                return;
            }

            if (existInArray(ARR_UPDATED_ROW, rowNum)) {
                $tdRow.addClass("markUpdated");
            } else {
                ARR_UPDATED_ROW.push(rowNum);
                $tdRow.addClass("markUpdated");
            }
        },
        onRefreshed: function(args) {

            // Update position fixed columns when scroll
            UpdateColPos(ARR_COLUMNS_FIXED);

            var dataSize = parseInt(getArraySize(personData));
            if (dataSize > 0) {
                // Insert row to bottom grid
                $("#jsGrid .jsgrid-grid-header").css("overflow-x", "hidden");
                var insertRow = args.grid._insertRow;
                var gridBody = args.grid._bodyGrid;
                $("<tfoot></tfoot>").appendTo(gridBody).append(insertRow);
            } else {
                $("#jsGrid .jsgrid-grid-header").css("overflow-x", "auto");
            }

            // Set focus to first input of row inserting
            setFocusWhenInserting();

            // Marked color column again after refresh
            $.each(ARR_INSERTED_ROW, function( index, value ) {
                markColColorAfterRefresh(value, "markInserted");
            });

            $.each(ARR_UPDATED_ROW, function( index, value ) {
                markColColorAfterRefresh(value, "markUpdated");
            });

            $.each(ARR_DELETED_ROW, function( index, value ) {
                markColColorAfterRefresh(value, "markDeleted");
            });
        }
    });

    // Update position fixed columns when scroll
    $('#jsGrid .jsgrid-grid-body').scroll(function() {
        UpdateColPos(ARR_COLUMNS_FIXED);
    });
}

/**
 * Detect Grid changes
 */
function detectGridChanged(){
    setStateDisableBtn(false);
}

/**
 * Set active/inactive button Confirm and Cancel
 */
function setStateDisableBtn(state) {
    $('#btnConfirm').disable(state);
    $('#btnCancel').disable(state);
}

/**
 * Set focus on first input of row inserting
 */
function setFocusWhenInserting() {
    $("#jsGrid tfoot tr:last-child td:nth-child(" + COLUMN_FOCUS_WHEN_INSERTING + ") input").focus();
    $("#jsGrid tfoot tr:last-child td:nth-child(" + COLUMN_FOCUS_WHEN_INSERTING + ") select").focus();
}

/**
 * Mark column color after insert, update, delete on Grid
 */
function markColColorAfterRefresh(rowNum, typeMarkColor) {
    $("#jsGrid tbody tr td:nth-child(" + COLUMN_ROW_NO + ")").filter(function() {
        return $(this).text() === rowNum;
    }).closest("tr").children("td").removeClass(function (index, className) {
        return (className.match (/(^|\s)mark\S+/g) || []).join(' ');
    }).addClass(typeMarkColor);
}

/**
 * Reset saving arrays
 */
function resetGridSavingArray() {
    ARR_UPDATED_ROW = [];
    ARR_DELETED_ROW = [];
    ARR_INSERTED_ROW = [];
}
