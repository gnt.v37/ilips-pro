/**
 * Define all service for calling to backend
 */
(function(w) {
    'use strict';
    var URL_OPEN_ORG_DATA_TREE = _BASE + "/account/selectOrgWindow";

    w.SelectOrgWindow = {
        openOrgDataTreeDialog: openOrgDataTreeDialog
    };

    function openOrgDataTreeDialog() {
      return $.get(URL_OPEN_ORG_DATA_TREE);
    }
})(window);