/* init code, call once when all page ready */
$(function () {
    'use strict';

    $("#btnShowModal").on('click', function () {
        $('#sampleModal').modal('show')
    });

    $("#btnGetInfo").on('click', function () {
        Sample.getItem(1, 2).then(function (resp) {
            console.log(resp);
        });
    });
    $("#btnTestMessage").on('click', function () {
        alert(_MESSAGE['user.info.hello']);
    });

    $("#form").validate({
        errorElement: "div",
        errorClass: 'is-invalid',
        validClass: 'is-valid',
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            Sample.postItem(1, 2).then(function (resp) {
                alert('success');
            }, function (reason) {
                alert('fail');
                console.log(reason);
            });
        }
    });
    /*
    * Define all other function here for processing business flow.
    * */
});
