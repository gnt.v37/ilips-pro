/**
 * Define all service for calling to backend
 */
(function (w) {
    'use strict';
    var URL = _BASE + "/sample";

    w.Sample = {
        getItem: getItem,
        postItem: postItem
    };

    function getItem(id, name) {
        return $.get(URL + "/" + id + "/" + name);
    }

    function postItem(id, name) {
        var form = {
            id: id,
            name: name
        }
        return $.post(URL, form);
    }

})(window);