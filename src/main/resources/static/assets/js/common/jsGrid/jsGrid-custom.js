
/**
 * ======================================
 * GLOBAL VARIABLES
 * ======================================
 */


/**
 * ======================================
 * READY FUNCTION
 * ======================================
 */
$(function () {

});


/**
 * ======================================
 * FUNCTIONS
 * ======================================
 */

/**
 * Clear Grid errors
 */
function clearErrors() {
    $("#errMsg").hide();
    $("#errMsg").html('');
}

/**
 * Write Grid error messages
 */
function writeErrorMessages(index, value) {
    var rowIndex = parseInt(index);
    $("#errMsg").append("Row " + rowIndex);
    $("#errMsg").append(" - ");
    $("#errMsg").append(value);
    $("#errMsg").append("\n");
}

/**
 * Update position column on Grid when scrolling.
 */
function UpdateColPos(cols) {
    var left = $('.jsgrid-grid-body').scrollLeft() < $('.jsgrid-grid-body .jsgrid-table').width() - $('.jsgrid-grid-body').width() + 16
        ? $('.jsgrid-grid-body').scrollLeft() : $('.jsgrid-grid-body .jsgrid-table').width() - $('.jsgrid-grid-body').width() + 16;

    $.each( cols, function( key, value ) {
        $('.jsgrid-header-row th:nth-child(-n+' + value + '), .jsgrid-filter-row td:nth-child(-n+' + value + '), .jsgrid-insert-row td:nth-child(-n+' + value + '), .jsgrid-grid-body tr td:nth-child(-n+' + value + ')')
            .css({
                "position": "relative",
                "left": left
            });
    });
}

/**
 * Add new column No to grid data
 */
function addColumnNoGridData(data) {
    var d = $.Deferred();
    d.resolve($.map(data, function(item, itemIndex) {
        return $.extend(item, {
            "rowNumber": itemIndex + 1
        });
    }));
    return data;
}

function getItemByColumnNo(rowNum) {
    var rowNum = rowNum;
    var $row = getRowByColumnNo(rowNum);
    var rowData = $row.data("JSGridItem");

    var arrItem = {};
    arrItem["rowNum"] = rowNum;
    arrItem["rowData"] = rowData;
    return arrItem;
}
