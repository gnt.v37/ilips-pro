/**
 * ======================================
 * READY FUNCTION
 * ======================================
 */
$(function(){
    'use strict';

    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");

    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        }
    });

    // set selected Checkbox language
    if(_LOCALE == "en") {
        $("#langCodeEn").prop('checked', true);
    } else if(_LOCALE == "ja") {
        $("#langCodeJa").prop('checked', true);
    }

    // sign out
    $('#userSignOut').on('click', function(){
        $.post(_BASE + '/logout').then(function(){
            location.href = _BASE + '/';
        });
    });

    // event change Checkbox language
    $(".langCode").on("change", function() {
        var langCode = $('input[name="langCode"]:checked').val();
        if (langCode != ''){
            window.location.replace('?lang=' + langCode);
        }
    });
});

/**
 * ======================================
 * COMMON JAVASCRIPT FUNCTIONS
 * ======================================
 */

function strPad(input, length, string) {
    string = string || '0'; input = input + '';
    return input.length >= length ? input : new Array(length - input.length + 1).join(string) + input;
}

/**
 * Check element exist in array
 */
function existInArray(arr, value) {
    if (jQuery.inArray(value, arr) !== -1) {
        return true;
    }
    return false;
}

/**
 * Remove element in array
 */
Array.prototype.remove = function() {
    var what, a = arguments,
        L = a.length,
        ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

/**
 * Get array size
 */
function getArraySize(arr) {
    return arr.length;
}

/**
 * Get element which has max prop in array
 */
function getElementHasMaxPropInArray(arr, prop) {
    var max;
    for (var i = 0; i < arr.length; i++) {
        if (!max || parseInt(arr[i][prop]) > parseInt(max[prop]))
            max = arr[i];
    }
    return max;
}

/**
 * Empty: "" and null is equal
 */
function isAllEmpty(value) {
    if (value == null || value == "") return true;
}

/**
 * Customize for isMatchWith
 */
function customizer(fVal, sVal) {
    if (isAllEmpty(fVal) && isAllEmpty(sVal)) {
        return true;
    }
}

/**
 * Disable function
 */
jQuery.fn.extend({
    disable: function(state) {
        return this.each(function() {
            this.disabled = state;
        });
    }
});
