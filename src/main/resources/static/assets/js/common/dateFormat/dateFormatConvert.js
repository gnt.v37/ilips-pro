/*
jp.co.ihi.is3.framework.model.util.common.DateFormatConvertクラスをJavascriptに移植したもの
--------------------------------------------------------
*/

/**
 * クラス宣言
 */
var DateFormatConvert = function() {
  this._init();
};

/**
 * クラス定義
 */
DateFormatConvert.prototype = {

	/**
	 * コンストラクタ
	 */
    _init: function() {
        this.formatConfigHash = {};

        this.formatConfigHash['yyyy/MM/dd'] = { 'ja': 'yyyy/MM/dd', 'en': 'MM/dd/yyyy' };
        this.formatConfigHash['yyyy/MM'] = { 'ja': 'yyyy/MM', 'en': 'MM/yyyy' };
        this.formatConfigHash['MM/dd HH:mm'] = { 'ja': 'MM/dd HH:mm', 'en': 'MM/dd HH:mm' };
        this.formatConfigHash['yyyy/MM/dd HH:mm:ss'] = { 'ja': 'yyyy/MM/dd HH:mm:ss', 'en': 'MM/dd/yyyy HH:mm:ss' };
        this.formatConfigHash['HH:mm:ss'] = { 'ja': 'HH:mm:ss', 'en': 'HH:mm:ss' };
        this.formatConfigHash['yyyyMMdd'] = { 'ja': 'yyyyMMdd', 'en': 'MMddyyyy' };
        this.formatConfigHash['yyyyMMddHHmmss'] = { 'ja': 'yyyyMMddHHmmss', 'en': 'MMddyyyyHHmmss' };
        this.formatConfigHash['yyyyMM'] = { 'ja': 'yyyyMM', 'en': 'MMyyyy' };


    },

    /**
     * 日本語フォーマットを指定したロケールに該当するフォーマットに変換する
     *
	 * @param japaneseFormat 日本語フォーマット
     * @return
     */
    getFormat: function(japaneseFormat, locale) {

    	if(!this.formatConfigHash[japaneseFormat]){
    		return japaneseFormat;
    	}else{
        	if(!this.formatConfigHash[japaneseFormat][locale]){
        		return japaneseFormat;
        	}else{
        		return this.formatConfigHash[japaneseFormat][locale];
        	}
    	}
    }

}
