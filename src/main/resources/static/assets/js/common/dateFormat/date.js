/**
 * 年プルダウンを更新する
 *
 * @param string yearTagId   年プルダウンタグID
 * @param string yearTagVal  年プルダウン値
 * @return void
 */
function updateYear(yearTagId, yearTagVal) {
    var selectYear  = $('#' + yearTagId);
    var year = yearTagVal;

    selectYear.clearOptions();

    var YYYY_RANGE = 10;

    var start = parseInt(yearTagVal) - YYYY_RANGE;
    var end = parseInt(yearTagVal) + YYYY_RANGE;

	for(var i= start;i <=end; ++i){
    	var strYear = String(i);

		var selected = false;
    	if(i == year){
    		selected = true;
    	}

    	selectYear.addOption(strYear, strYear, selected)
	}

}
/**
 * 日付プルダウンを更新する
 *
 * @param string yearTagId   年プルダウンタグID
 * @param string monthTagId  月プルダウンタグID
 * @param string dayTagId    日プルダウンタグID
 * @param string yearTagVal  年プルダウン値
 * @param string monthTagVal 月プルダウン値
 * @param string dayTagVal   日プルダウン値
 * @return void
 */
function updateDay(yearTagId, monthTagId, dayTagId, yearTagVal, monthTagVal, dayTagVal) {
    var selectYear  = $('#' + yearTagId);
    var year = yearTagVal;

    var selectMonth  = $('#' + monthTagId);
    var month = monthTagVal;

	var selectDay  = $('#' + dayTagId);
    var current = dayTagVal;

    var date = new Date(year, month, 0);

    selectDay.clearOptions();

    var isSet = false;
    for(var i=1; i<=date.getDate(); ++i) {
    	var strDate = String(i);

    	if(strDate.length < 2){
    		strDate = "0" + String(strDate);
    	}

    	var selected = false;
    	if(i == current){
    		selected = true;
    		isSet = true
    	}

    	if(i == date.getDate() && (isSet == false)){
    		selected = true;
    	}

    	selectDay.addOption(strDate, strDate, selected)
    }
}

