/* Japanese translation for the jQuery Timepicker Addon */
/* Written by Jun Omae */
(function($) {
	$.timepicker.regional['ja'] = {
		timeOnlyTitle: 'Select a time',
		timeText: 'time',
		hourText: 'h',
		minuteText: 'm',
		secondText: 's',
		millisecText: 'ms',
		microsecText: 'µs',
		timezoneText: 'time zone',
		currentText: 'current time',
		closeText: 'close',
		buttonImage: "../../../pages/images/calendar.png",
		changeYear: true,
		changeMonth: true,
		constrainInput:true,
// CHG S
// bef
//		timeFormat: 'HH:mm',
// aft
		timeFormat: 'HH:mm:ss',
// CHG E
		amNames: ['AM', 'AM', 'A'],
		pmNames: ['PM', 'PM', 'P'],
		isRTL: false
	};
	$.timepicker.setDefaults($.timepicker.regional['ja']);
})(jQuery);
