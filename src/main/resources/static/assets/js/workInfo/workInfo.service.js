/**
 * Define all service for calling to backend
 */
(function(w) {
    'use strict';
    var URL_LOAD_DATA = _BASE + "/account/workInfo/loadData";
    var URL_SAVE_DATA = _BASE + "/account/workInfo/saveData";

    w.WorkData = {
        loadWorkInfo: loadWorkInfo,
        saveWorkInfo: saveWorkInfo
    };

    function loadWorkInfo() {

        return $.get(URL_LOAD_DATA);
    }

    function saveWorkInfo(arrInsertedData, arrUpdatedData, arrDeletedData) {
        var postData = {
            arrInsertedData: arrInsertedData,
            arrUpdatedData: arrUpdatedData,
            arrDeletedData: arrDeletedData
        };

        return $.ajax({
            type: "POST",
            contentType : 'application/json; charset=utf-8',
            dataType : 'json',
            url: URL_SAVE_DATA,
            data: JSON.stringify(postData)
        });
    }

})(window);