/**
 * Define all service for calling to backend
 */
(function(w) {
    'use strict';
    var URL_LOAD_DATA = _BASE + "/account/referableOrg/loadData";
    var URL_SAVE_DATA = _BASE + "/account/referableOrg/saveData";
    var URL_OPEN_REFERABLE_ORG_DATA_TREE = _BASE + "/account/referableOrg";

    w.ReferableOrgData = {
        getReferableOrgDataByOrgId: getReferableOrgDataByOrgId,
        saveReferenceOrgData: saveReferenceOrgData,
        openReferableOrgDataTreeDialog: openReferableOrgDataTreeDialog
    };

    function getReferableOrgDataByOrgId(selectReferableOrgId) {
        if (!selectReferableOrgId) return;

        var postData = {
            selectReferableOrgId: selectReferableOrgId
        };
        return $.post(URL_LOAD_DATA, postData);
    }

    function saveReferenceOrgData(arrInsertedData, arrUpdatedData, arrDeletedData) {
        var postData = {
            arrInsertedData: arrInsertedData,
            arrUpdatedData: arrUpdatedData,
            arrDeletedData: arrDeletedData
        };

        return $.ajax({
            type: "POST",
            contentType : 'application/json; charset=utf-8',
            dataType : 'json',
            url: URL_SAVE_DATA,
            data: JSON.stringify(postData)
        });
    }

    function openReferableOrgDataTreeDialog() {
        return $.get(URL_OPEN_REFERABLE_ORG_DATA_TREE);
    }
})(window);