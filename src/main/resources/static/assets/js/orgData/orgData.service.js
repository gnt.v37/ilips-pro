/**
 * Define all service for calling to backend
 */
(function(w) {
	'use strict';
	var URL_LOAD_DATA = _BASE + "/account/orgData/load/";
	var URL_SAVE_DATA = _BASE + "/account/orgData/save/";
	var URL_OPEN_ORG_DATA_TREE = _BASE + "/account/orgData";

	w.OrgData = {
		getOrgData : getOrgData,
		saveOrgData : saveOrgData,
		openOrgDataTreeDialog : openOrgDataTreeDialog
	};

	function getOrgData(selectParentId) {

		var postData = {
			selectParentId : selectParentId
		};
		return $.get(URL_LOAD_DATA, postData);
	}

	function saveOrgData(arrInsertedOrgData, arrUpdatedOrgData,
			arrDeletedOrgData, selectParentId) {
		var postData = {
			selectParentId : selectParentId,
			arrInsertedOrgData : arrInsertedOrgData,
			arrUpdatedOrgData : arrUpdatedOrgData,
			arrDeletedOrgData : arrDeletedOrgData
		};

		return $.ajax({
			type : "POST",
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			url : URL_SAVE_DATA,
			data : JSON.stringify(postData)
		});
	}

	function openOrgDataTreeDialog() {
		return $.get(URL_OPEN_ORG_DATA_TREE);
	}
})(window);