/**
 * ======================================
 * GLOBAL VARIABLES
 * ======================================
 */

// init arrays
var fixedGridColumn = 1;
var arrUpdatedRow = [];
var arrDeletedRow = [];
var arrInsertedRow = [];

var orgData = [];
// Dummy var
var selectParentId = 10000;

var JSGRID = "JSGrid",
    JSGRID_DATA_KEY = JSGRID,
    JSGRID_ROW_DATA_KEY = "JSGridItem",
    JSGRID_EDIT_ROW_DATA_KEY = "JSGridEditRow",
    SORT_ORDER_ASC = "asc",
    SORT_ORDER_DESC = "desc",
    FIRST_PAGE_PLACEHOLDER = "{first}",
    PAGES_PLACEHOLDER = "{pages}",
    PREV_PAGE_PLACEHOLDER = "{prev}",
    NEXT_PAGE_PLACEHOLDER = "{next}",
    LAST_PAGE_PLACEHOLDER = "{last}",
    PAGE_INDEX_PLACEHOLDER = "{pageIndex}",
    PAGE_COUNT_PLACEHOLDER = "{pageCount}",
    ITEM_COUNT_PLACEHOLDER = "{itemCount}",
    EMPTY_HREF = "javascript:void(0);";

/**
 * ======================================
 * READY FUNCTION
 * ======================================
 */
$(function () {
    'use strict';

    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");

    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        }
    });

    // call dummy function
    loadGridData();

    // Event click button Confirm
    $("#btnConfirm").click(function() {
        if ($("tbody tr").hasClass("jsgrid-edit-row") || ($("tfoot tr").hasClass("jsgrid-insert-row") && !($('tfoot tr.jsgrid-insert-row').css('display') == 'none'))) {
            var r = confirm(infoConfirmEditing);
            if (r == true) {
                confirmSave();
                return true;
            } else {
                return false;
            }
        }
        confirmSave();
    });

    // Event click button Cancel
    $("#btnCancel").click(function() {
        var r = confirm(infoConfirmCancel);
        if (r == true) {
            resetGridSavingArray();
            clearErrors();
            loadGridData();
        } else {
            return false;
        }
    });

    // Event click button Organization Selection
    $("#selectOrgBtn").click(function () {
           $('#selectOrgModal').modal('show');
     });
});


/**
 * ======================================
 * FUNCTIONS
 * ======================================
 */

/**
 * Get list referenceOrg data and handle response.
 */
function loadGridData() {;
     OrgData.getOrgData(selectParentId).then(function(resp){
    	 if (typeof resp == "undefined") {
             alert(errorsDataErr);
         } else {
             orgData = resp.orgDataList;
             createGridTable();
         }
     });
}

/**
 * Saving grid data
 */
function saveGridData() {

    // Prepare data
    prepareSaveGridData();

    // clear server errors
    clearErrors();

    var arrInsertedData = [];
    var arrUpdatedData = [];
    var arrDeletedData = [];

    for (idx = 0; idx < arrInsertedRow.length; idx++) {
        arrInsertedItem = {};
        arrInsertedItem["rowIndex"] = arrInsertedRow[idx];
        arrInsertedItem["rowData"] = $("#jsGrid").jsGrid("option", "data")[arrInsertedRow[idx]];
        arrInsertedData.push(arrInsertedItem);
    }

    for (idx = 0; idx < arrUpdatedRow.length; idx++) {
        arrUpdatedItem = {};
        arrUpdatedItem["rowIndex"] = arrUpdatedRow[idx];
        arrUpdatedItem["rowData"] = $("#jsGrid").jsGrid("option", "data")[arrUpdatedRow[idx]];
        arrUpdatedData.push(arrUpdatedItem);
    }

    for (idx = 0; idx < arrDeletedRow.length; idx++) {
        arrDeletedItem = {};
        arrDeletedItem["rowIndex"] = arrDeletedRow[idx];
        arrDeletedItem["rowData"] = $("#jsGrid").jsGrid("option", "data")[arrDeletedRow[idx]];
        arrDeletedData.push(arrDeletedItem);
    }


    OrgData.saveOrgData(arrInsertedData, arrUpdatedData, arrDeletedData, selectParentId).then(function(resp){
    	if (typeof resp == "undefined") {
            alert(errorsDataErr);
        } else {
            var errList = resp;
            if (!jQuery.isEmptyObject(errList)) {
                $("#errMsg").show();
                $.each(errList, function( index, value ) {
                    writeErrorMessages(index, value);
                });
            } else {
                loadGridData();
            }
        }
    });
}

/**
 * Create Grid
 */
function createGridTable() {

    // reset saving array
    resetGridSavingArray();

    // set locate for grid
    if (_LOCALE == "ja") {
        jsGrid.locale("ja");
    }

    if (orgData.length === 0) {
        $(".wrpActionBtn").hide();
    } else {
        $(".wrpActionBtn").show();
    }

    // init Grid
    $("#jsGrid").jsGrid({
        width: "100%",
        height: "550px",
        autoload: true,
        inserting: true,
        editing: true,
        sorting: true,
        paging: false,
        add: true,
        data: orgData,
        fields: [
            {
                type: "control",
                editButton: true,
                width: 60
            },
            {
            	name:"parentId",
            	type:"number",
            	css:"hide",
            	width:0,
            	itemTemplate: function(value, item) {
                    return selectParentId;
                },
                insertValue: function() {
                    return selectParentId;
                },
                insertTemplate: function() {
                    return selectParentId;
                }
            },
            {
                title: orgDataIDRequired,
                name: "orgId",
                type: "number",
                width: 100,
                validate: [
                    {validator: "required",title: orgDataID },
                    {validator: "range", param: [1, 2147483646],title: orgDataID }
                ],
                align: "left"
            },
            {
                title: orgDataNameRequired,
                name: "orgName",
                type: "text",
                width: 250,
                validate: [
                     {validator: "required", title: orgDataName },
                     {validator: "maxLength", title: orgDataName ,param: 400}
                ],
                align: "left"
            },
            {
                title: orgDataManufacturerAttrRequired,
                name: "attrMaker",
                type: "select",
                items: staticListSelect,
                valueField: "Id",
                textField: "Name",
                width: 250,
                validate: {
                    validator: "required",
                    title: orgDataManufacturerAttr
                },
                align: "left"
            },
            {
                title: orgDataCustomerAttrRequired,
                name: "attrCustomer",
                type: "select",
                items: staticListSelect,
                valueField: "Id",
                textField: "Name",
                width: 250,
                validate: {
                    validator: "required",
                    title: orgDataCustomerAttr
                },
                align: "left"
            },
            {
                title: orgDataOwningOrgAttrRequired,
                name: "attrOwner",
                type: "select",
                items: staticListSelect,
                valueField: "Id",
                textField: "Name",
                width: 300,
                validate: {
                    validator: "required",
                    title: orgDataOwningOrgAttr
                },
                align: "left"
            },
            {
                title: orgDataMaintenanceOrgAttrRequired,
                name: "attrMainte",
                type: "select",
                items: staticListSelect,
                valueField: "Id",
                textField: "Name",
                width: 350,
                validate: {
                    validator: "required",
                    title: orgDataMaintenanceOrgAttr
                },
                align: "left"
            },
            {
                title: orgDataOrgCode,
                name: "orgCode",
                type: "text",
                width: 200,
                validate: [
                     {validator: "maxLength", title: orgDataOrgCode ,param: 400}
                ],
                align: "left"
            },
            {
                title: orgDataOrgClassification,
                name: "orgType",
                type: "text",
                width: 200,
                validate: [
                     {validator: "maxLength", title: orgDataOrgClassification ,param: 400}
                ],
                align: "left"
            },
            {
                title: orgDataOrgAbbreviation,
                name: "shortName",
                type: "text",
                width: 200,
                validate: [
                     {validator: "maxLength", title: orgDataOrgAbbreviation ,param: 400}
                ],
                align: "left"
            },
            {
                title: orgDataOrgAddress,
                name: "address",
                type: "text",
                width: 200,
                validate: [
                     {validator: "maxLength", title: orgDataOrgAddress ,param: 400}
                ],
                align: "left"
            },
            {
                title: orgDataOrgDetails,
                name: "addition",
                type: "text",
                width: 200,
                validate: [
                     {validator: "maxLength", title: orgDataOrgDetails ,param: 400}
                ],
                align: "left"
            }
        ],
        rowClick: function(args) {
            var $target = $(args.event.target);

            if ($target.closest(".jsgrid-control-field").length) {
                // handle cell click
                return;
            }

            // otherwise handle row click
            if (this.editing) {
                this.editItem($target.closest("tr"));
            }
        },

        onItemEditing: function(args) {
            setTimeout(function() {
                UpdateColPos(fixedGridColumn);
            }, 1);
        },

        deleteItem: function(item) {

            var $row = this.rowByItem(item);
            if (!$row.length) return;
            var rowIndex = this._rowIndex($row);
            var $tdRow = $row.children('td');

            if (existInArray(arrInsertedRow, rowIndex)) {

                if (existInArray(arrDeletedRow, rowIndex)) {
                    arrDeletedRow.remove(rowIndex);
                    $tdRow.removeClass("markDeleted");
                    $tdRow.addClass("markInserted");
                } else {
                    arrDeletedRow.push(rowIndex);
                    $tdRow.removeClass("markInserted");
                    $tdRow.addClass("markDeleted");
                }
                return;
            }

            if (existInArray(arrUpdatedRow, rowIndex)) {

                if (existInArray(arrDeletedRow, rowIndex)) {
                    arrDeletedRow.remove(rowIndex);
                    $tdRow.removeClass("markDeleted");
                    $tdRow.addClass("markUpdated");
                } else {
                    arrDeletedRow.push(rowIndex);
                    $tdRow.removeClass("markUpdated");
                    $tdRow.addClass("markDeleted");
                }
                return;
            }

            if (existInArray(arrDeletedRow, rowIndex)) {
                arrDeletedRow.remove(rowIndex);
                $tdRow.removeClass("markDeleted");
            } else {
                arrDeletedRow.push(rowIndex);
                $tdRow.addClass("markDeleted");
            }
        },
        onItemDeleted: function(args) {},
        onItemInserted: function(args) {

            var item = args.item;
            var $row = this.rowByItem(item);
            if (!$row.length) return;
            var rowIndex = this._rowIndex($row);
            arrInsertedRow.push(rowIndex);
            $(".jsgrid-insert-mode-button").click();
        },

        onItemUpdated: function(args) {
            UpdateColPos(fixedGridColumn);

            var item = args.item;
            var $row = this.rowByItem(item);
            if (!$row.length) return;
            var rowIndex = this._rowIndex($row);
            var $tdRow = $row.children('td');

            if (existInArray(arrInsertedRow, rowIndex)) {
                arrDeletedRow.remove(rowIndex);
                $tdRow.removeClass("markDeleted");
                $tdRow.addClass("markInserted");
                return;
            }

            if (existInArray(arrDeletedRow, rowIndex)) {
                arrDeletedRow.remove(rowIndex);
                $tdRow.removeClass("markDeleted");
                $tdRow.addClass("markUpdated");
                return;
            }

            if (existInArray(arrUpdatedRow, rowIndex)) {
                $tdRow.addClass("markUpdated");
            } else {
                arrUpdatedRow.push(rowIndex);
                $tdRow.addClass("markUpdated");
            }
        },

        onRefreshed: function(args) {

            UpdateColPos(fixedGridColumn);
            var insertRow = args.grid._insertRow;
            var gridBody = args.grid._bodyGrid;
            $("<tfoot></tfoot>").appendTo(gridBody).append(insertRow);

            $("tfoot tr:last-child td:nth-child(2) input").focus();
            $("tfoot tr:last-child td:nth-child(2) select").focus();

            for (rIndex = 0; rIndex < arrInsertedRow.length; rIndex++) {
                $("tbody tr:nth-child(" + (arrInsertedRow[rIndex] + 1) + ")").children('td').addClass("markInserted");
            }

            for (rIndex = 0; rIndex < arrUpdatedRow.length; rIndex++) {
                $("tbody tr:nth-child(" + (arrUpdatedRow[rIndex] + 1) + ")").children('td').addClass("markUpdated");
            }

            for (rIndex = 0; rIndex < arrDeletedRow.length; rIndex++) {
                $("tbody tr:nth-child(" + (arrDeletedRow[rIndex] + 1) + ")").children('td').addClass("markDeleted");
            }
        }
    });

    // Update position column when scroll
    $('.jsgrid-grid-body').scroll(function() {
        UpdateColPos(fixedGridColumn);
    });
}

/**
 * Prepare saving data
 */
function prepareSaveGridData() {
    for (rIndex = 0; rIndex < arrDeletedRow.length; rIndex++) {
        var removeItem = arrDeletedRow[rIndex];
        arrInsertedRow = jQuery.grep(arrInsertedRow, function(value) {
            return value != removeItem;
        });
        arrUpdatedRow = jQuery.grep(arrUpdatedRow, function(value) {
            return value != removeItem;
        });
    }
}

/**
 * Reset saving arrays
 */
function resetGridSavingArray() {
    arrUpdatedRow = [];
    arrDeletedRow = [];
    arrInsertedRow = [];
}
