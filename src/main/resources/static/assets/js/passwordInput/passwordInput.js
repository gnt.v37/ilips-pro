/**
 * ======================================
 * FUNCTIONS
 * ======================================
 */

/**
 * Input Password
 */
function InputPassword(id) {
    'use strict';
    var dfd = jQuery.Deferred();
    var $confirm = $(id);
    //reset value
    document.getElementById("password").value = "";
    document.getElementById("reenterPassword").value = "";
    document.getElementById("password").focus();
    $confirm.modal('show')

    $('#btnPasswordInputOk').off('click').click(function () {
        var password = document.getElementById("password").value;
        var reEnterPassword = document.getElementById("reenterPassword").value;
        var isContainCaps = /[A-Z]/.test(password);
        var isContainLower = /[a-z]/.test(password);
        var isContainDigit = /[0-9]/.test(password);
        var isContainCode = /[\x21-\x2F\x3A-\x40\x5B-\x60\x7B-\x7E]/.test(password);

        if (!password && !reEnterPassword) {
            $confirm.modal('hide');
            dfd.resolve(null);
            return 1;
        }
        //error check
        if (password.length < 8) {
            alert(passwordTooSmall);
            document.getElementById("password").value = "";
            document.getElementById("reenterPassword").value = "";
            return;
        }

        if (!isContainCaps || !isContainLower || !isContainDigit || !isContainCode)
        {
            alert(passwordNotMatchRegex);
            document.getElementById("password").value = "";
            document.getElementById("reenterPassword").value = "";
            return;
        }

        if (password != reEnterPassword) {
            alert(passwordNotMatchReinput);
            document.getElementById("password").value = "";
            document.getElementById("reenterPassword").value = "";
            return;
        }

        //TODO encrypt
        var response = PasswordInputWindow.convertSHA256(password);

        $confirm.modal('hide');

        response.done(function (convertSHA256Password) {
            dfd.resolve(convertSHA256Password);
        });

        return 1;
    });
    $('#btnPasswordInputCancel').off('click').click(function () {
        $confirm.modal('hide');
        return 0;
    });

    return dfd.promise();
}

