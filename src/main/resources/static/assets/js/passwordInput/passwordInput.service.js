(function (window, document) {

    let PasswordInputWindow = (function () {

        const urls = {
            URL_GET_SHA256_PASSWORD: _BASE + 'personData/passwordInputWindow'
        };

        let passwordEncrypt = null;

        class PasswordInputWindow {

            constructor() {
                this.resolve = null;
            }

            convertSHA256(password) {
                return $.ajax({
                    url: urls.URL_GET_SHA256_PASSWORD + `?password=${password}`,
                    method: "GET"
                });
            }

            set passwordEncrypt(password) {
                passwordEncrypt = this.resolve(password);
            }

            get passwordEncrypt() {
                return new Promise(resolve => this.resolve = resolve);
            }

        }

        return PasswordInputWindow;
    })();


    window.PasswordInputWindow = new PasswordInputWindow();

})(window, document);