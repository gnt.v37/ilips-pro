/**
 * Define all service for calling to backend
 */
(function(w) {
    'use strict';
    var URL_LOAD_DATA = _BASE + "/account/maintenanceType/loadData";
    var URL_SAVE_DATA = _BASE + "/account/maintenanceType/saveData";

      w.MainteType = {
            getMainteTypeByMakerIdAndOrgId: getMainteTypeByMakerIdAndOrgId,
            saveMainteTypeData: saveMainteTypeData
        };

        function getMainteTypeByMakerIdAndOrgId(selectMakerId, selectModelId){
            if (!selectMakerId || !selectModelId) return;

            var postData = {
                 selectMakerId: selectMakerId,
                 selectModelId: selectModelId
             };

             return $.post(URL_LOAD_DATA, postData);
        }

        function saveMainteTypeData(arrInsertedData, arrUpdatedData, arrDeletedData) {
                var postData = {
                    arrInsertedData: arrInsertedData,
                    arrUpdatedData: arrUpdatedData,
                    arrDeletedData: arrDeletedData
                };

                return $.ajax({
                    type: "POST",
                    contentType : 'application/json; charset=utf-8',
                    dataType : 'json',
                    url: URL_SAVE_DATA,
                    data: JSON.stringify(postData)
                });
         }
})(window);
