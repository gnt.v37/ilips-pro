# Reference
 - https://o7planning.org/vi/11681/huong-dan-su-dung-spring-boot-va-jsp
 
# Tag libs
 - https://www.tutorialspoint.com/jsp/jsp_standard_tag_library.htm
 - http://tiles.apache.org/
 
## Spring tag form: 

 - http://www.baeldung.com/spring-mvc-form-tags
 - https://docs.spring.io/spring/docs/4.2.x/spring-framework-reference/html/spring-tld.html
 
## Spring Validation Form
 - https://www.journaldev.com/2668/spring-validation-example-mvc-validator

## Best Practice

 - https://stackoverflow.com/questions/1296235/jsp-tricks-to-make-templating-easier 
 
## Spring JDBC Template
 
 - https://docs.spring.io/spring/docs/4.0.x/spring-framework-reference/html/jdbc.html
 - https://o7planning.org/vi/11663/huong-dan-su-dung-spring-boot-spring-jdbc-va-spring-transaction
 - https://gist.github.com/LeeMoby/595b61d72a1a0363dc80b7eb785faef9

## Spring Security
 
 - http://websystique.com/spring-security/spring-security-4-method-security-using-preauthorize-postauthorize-secured-el/
 - 
 
## Spring Exception ERROR Handler

 - https://www.mkyong.com/spring-mvc/spring-mvc-exceptionhandler-example/
 
# FRONTEND

 - https://jqueryvalidation.org/ 1.17.0
 - http://jquery.com/ 3.3.1
 - https://getbootstrap.com 4.1.1
 - http://js-grid.com/ 1.5.3
 